package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import interfaces.DotInterface;
import utils.Constants;
import utils.DotView;
import utils.GeneralFunctions;
import utils.Global;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Clinic;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;

public class ClosestClinic extends BaseFragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.gridParent)
    GridLayout gridParent;
    @Bind(R.id.tvIFound)
    DotView tvIFound;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    private PojoAppointmentRequest pojoAppointmentRequest;
    private List<Clinic> clinicList;
    private String redirectType = "";
    // TODO: Rename and change types of parameters


    public ClosestClinic() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ClosestClinic newInstance(String pojoAppointmentRequest, String redirectType) {
        ClosestClinic fragment = new ClosestClinic();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, pojoAppointmentRequest);
        args.putString(ARG_PARAM2, redirectType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    PojoAppointmentRequest.class);
            redirectType = getArguments().getString(ARG_PARAM2);
        }
    }

    private int getClinic() {
        for (int i = 0; i < gridParent.getChildCount(); i++) {
            CheckedTextView ct = (CheckedTextView) gridParent.getChildAt(i);
            if (ct.isChecked())
                return i;
        }
        return -1;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (pojoAppointmentRequest.getTime_slot() == null) {
            tvIFound.setText(getString(R.string.found_openings) + getString(R.string.at_these_clinics_closest_to_you));
        } else
            tvIFound.setText(getString(R.string.i_found_openings_on) +
                    " (" + pojoAppointmentRequest.getTime_slot() + ") " + getString(R.string.at_these_clinics_closest_to_you));
        tvIFound.post(tvIFound.run);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                clinicList = result.getNear_clinic();
                if (clinicList.size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    for (int i = 0; i < clinicList.size(); i++) {
                        TextView tv = (TextView) LayoutInflater.from(getActivity()).
                                inflate(R.layout.radio_button_row, null);
                        tv.setText(clinicList.get(i).getClinic_name());
                        gridParent.addView(tv);
                    }
                } else tvNoData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {
                tvNoData.setVisibility(View.VISIBLE);
            }
        });
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_NEAR_BY_CLINIC;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.user_id = loggedInUser.user_id;
        requestData.parameters.latitude = Global.latitude;
        requestData.parameters.longitude = Global.longitude;

        makeRequest(requestData);
    }


    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                tvIFound.stopHandler();
                int pos = getClinic();
                if (pos == -1) {
                    showToast(getString(R.string.please_select_clinic));
                } else {
                    pojoAppointmentRequest.setClinic(clinicList.get(pos));
                    if (redirectType.equals(Constants.SCHEDULE)) {
                        GeneralFunctions.addFragmentFromRight(getFragmentManager()
                                , ScheduleAppointment.newInstance(
                                        new Gson().toJson(pojoAppointmentRequest), ""),
                                R.id.flAppointmentParent);
                    }
                }
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_closest_clinic, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        tvIFound.resume();
    }

    @Override
    public void stopDot() {
        tvIFound.stopHandler();
    }
}
