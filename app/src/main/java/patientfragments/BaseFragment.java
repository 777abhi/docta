package patientfragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import doctorfragments.WaitForConsultRequest;
import preferences.UserPrefsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Constants;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RestClient;
import webservices.pojos.Appointment;
import webservices.pojos.Clinic;
import webservices.pojos.CommonPojo;
import webservices.pojos.Pharmacy;
import webservices.pojos.User;


/**
 * Created by AMIT on 11/27/2015.
 */
public class BaseFragment extends Fragment {
    protected ProgressWheel pBaseBar;
    protected int year = 1980, month = 0, day = 1;
    protected User loggedInUser;
    protected UserPrefsManager userPrefsManager;
    protected Clinic favClinic;
    protected Pharmacy favPharmacy;
    protected DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    protected DateFormat timeFormat = new SimpleDateFormat("hh:mm a");
    protected DateFormat monthDateFormat = new SimpleDateFormat("MMM d, yyyy");
    protected DateFormat confirmationDateFormat = new SimpleDateFormat("MMM\n d \nEEE");
    protected DateFormat dayMonthFormat = new SimpleDateFormat("EEEE\nMMMM d");
    //    protected DateFormat confirmationDateFormatString = new SimpleDateFormat("EEEE, MMMM d yyyy");
    AlertDialog locationAlertDialog;
    private ServiceCallback serviceCallback;
    private MyReceiver myReceiver = new MyReceiver();
    private NotificationListener notificationListener;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().registerReceiver(myReceiver, new IntentFilter(Constants.PUSH_ACTION));
        userPrefsManager = new UserPrefsManager(getActivity());
        loggedInUser = userPrefsManager.getLoggedInUser();
        favClinic = userPrefsManager.getFavClinic();
        favPharmacy = userPrefsManager.getFavPharmacy();
    }

    protected String getDate() {
        Date date = new Date();
        return dateFormat.format(date);
    }

    protected Appointment getAppointment() {
        return userPrefsManager.getAppointment();
    }

    protected String getDateFromString(String date) {
        Date startDate = null;
        try {
            startDate = dateFormat.parse(date);
            return monthDateFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    protected String getConfirmationDateFromString(String date) {
        Date startDate = null;
        try {
            startDate = dateFormat.parse(date);
            return confirmationDateFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    protected String getConfirmationDate(String d) {
        Calendar cal = Calendar.getInstance();
        try {
            Date date = dateFormat.parse(d);
            cal.setTime(date);
            int day = cal.get(Calendar.DATE);

            if (!((day > 10) && (day < 19)))
                switch (day % 10) {
                    case 1:
                        return new SimpleDateFormat("EEEE, MMMM d'st' yyyy").format(date);
                    case 2:
                        return new SimpleDateFormat("EEEE, MMMM d'nd' yyyy").format(date);
                    case 3:
                        return new SimpleDateFormat("EEEE, MMMM d'rd' yyyy").format(date);
                    default:
                        return new SimpleDateFormat("EEEE, MMMM d'th' yyyy").format(date);
                }
            return new SimpleDateFormat("EEEE, MMMM d'th' yyyy").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    protected String getDateInDayMonth(String date) {
        Date startDate = null;
        try {
            startDate = dateFormat.parse(date);
            return dayMonthFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    protected String getNextTimeSlot(String currentTimeSlot) {
        DateFormat monthDateFormat = new SimpleDateFormat("hh:mm a");
        Calendar calendar = Calendar.getInstance();
        try {
            Date d = monthDateFormat.parse(currentTimeSlot);
            d.setTime(d.getTime() + 900000);
            return monthDateFormat.format(d);
        } catch (Exception e) {

        }
        return "";
    }

    protected String getTime() {
        Date date = new Date();
        return timeFormat.format(date);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    protected void showProgress() {
        pBaseBar.setVisibility(View.VISIBLE);
    }

    protected void setServiceCallbackListener(ServiceCallback serviceCallback) {
        this.serviceCallback = serviceCallback;
    }

    protected void setNotificationListener(NotificationListener notificationListener) {
        this.notificationListener = notificationListener;
    }

    protected void hideProgress() {
        pBaseBar.setVisibility(View.INVISIBLE);
    }

    protected void showToast(int messageId) {
        try {
            Toast.makeText(getActivity(), getString(messageId), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    public void makeRequest(final RequestData requestData) {
        Log.v("rrrrrrrrrrrrrrr", new Gson().toJson(requestData));
        if (pBaseBar != null)
            showProgress();
        Call<CommonPojo> call = RestClient.get().makeRequest(requestData);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                if (pBaseBar != null)
                    hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    CommonPojo data = response.body();
                    data.requestName = requestData.request;
                    if (data.getMsg() != null)
                        showToast(data.getMsg());
                    if (data.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                        try {
                            serviceCallback.onResult(data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else try {
                        serviceCallback.onFailure();
                        ((WaitForConsultRequest) BaseFragment.this).setRating(response.body().getDoctorRating());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    showToast(R.string.there_might_be_some_problem);
                }
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    if (pBaseBar != null)
                        hideProgress();
                    handleException(t);
                } catch (Exception e) {

                }
            }
        });
    }

    public void showLocationSettingsAlert(final Context mContext) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                .title("Location Settings")
                .content("Location is not enabled. Do you want to enable it from settings menu?")
                .positiveText(R.string.yes)
                .theme(Theme.LIGHT)
                .autoDismiss(false)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        dialog.dismiss();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.show();
    }

    protected void showToast(String message) {
        try {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    protected void showKeyBoard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected Drawable getDrawable(int drawableId) {
        return ContextCompat.getDrawable(getActivity(), drawableId);
    }

    protected int getColor(int colorId) {
        return ContextCompat.getColor(getActivity(), colorId);
    }

    protected void hideKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void handleException(Throwable e) {
        if (e instanceof IOException) {
            showToast(R.string.no_internet);
        } else {
            showToast(R.string.there_might_be_some_problem);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(myReceiver);
    }

    public String getMd5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public interface ServiceCallback {
        void onResult(CommonPojo result) throws Exception;

        void onFailure() throws Exception;
    }

    public interface NotificationListener {
        void onNotification(Intent intent);
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v("intent", intent.getStringExtra(Constants.MESSAGE_KEY) + "");
            if (myReceiver != null && notificationListener != null) {
                notificationListener.onNotification(intent);
            }
        }
    }
}
