package patientfragments;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import interfaces.DotInterface;
import utils.DotView;
import webservices.pojos.PojoAppointmentRequest;


public class PatientQuestionStep1Fragment extends BaseFragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.etSympotomDesc)
    EditText etSympotomDesc;
    @Bind(R.id.tvHeading)
    DotView tvHeading;
    @Bind(R.id.tvNext)
    TextView tvNext;

    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;
    private String title = "";

    public PatientQuestionStep1Fragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PatientQuestionStep1Fragment newInstance(String param1, String param2) {
        PatientQuestionStep1Fragment fragment = new PatientQuestionStep1Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        etSympotomDesc.clearFocus();
        hideKeyBoard(etSympotomDesc);
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                tvHeading.stopHandler();
                pojoAppointmentRequest.setPrimaryHealthConcern(etSympotomDesc.getText().toString());
                if (TextUtils.isEmpty(pojoAppointmentRequest.primaryHealthConcern))
                    return;
                tvHeading.stopHandler();
                EventBus.getDefault().post(pojoAppointmentRequest);
                getActivity().onBackPressed();

//                if (!TextUtils.isEmpty(etSympotomDesc.getText().toString().trim())) {
//                    if (pojoAppointmentRequest.isSeriousHealthProblem()) {
//                        pojoAppointmentRequest.primaryHealthConcern = etSympotomDesc.getText().toString();
//                        GeneralFunctions.addFragmentFromRight(getFragmentManager()
//                                , PatientQuestionStep2Fragment.newInstance(
//                                        new Gson().toJson(pojoAppointmentRequest)), R.id.flAppointmentParent);
//                    } else
//                        GeneralFunctions.addFragmentFromRight(getFragmentManager()
//                                , PatientQuestionStep3Fragment.newInstance(
//                                        new Gson().toJson(pojoAppointmentRequest)), R.id.flAppointmentParent);
//
//                }
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_question_step1, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(
                    getArguments().getString(ARG_PARAM1), PojoAppointmentRequest.class);
            title = getArguments().getString(ARG_PARAM2);
            if (!title.isEmpty())
                tvHeading.setText(title);
        }
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.done));
        tvHeading.post(tvHeading.run);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        tvHeading.resume();
    }

    @Override
    public void stopDot() {
        tvHeading.stopHandler();
    }
}
