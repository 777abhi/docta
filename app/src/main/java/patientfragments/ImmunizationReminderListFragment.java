package patientfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.ImmunizationAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import webservices.pojos.Common;


public class ImmunizationReminderListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam2;

    private Common data;

    public ImmunizationReminderListFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ImmunizationReminderListFragment newInstance(String param1) {
        ImmunizationReminderListFragment fragment = new ImmunizationReminderListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            data = new Gson().fromJson(getArguments().getString(ARG_PARAM1), Common.class);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        tvNext.setVisibility(View.GONE);
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvNoData.setText(getString(R.string.no_data));
        rvData.setAdapter(new ImmunizationAdapter(getActivity(), data.getVaccineList()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


}
