package patientfragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapters.DoctorsSelectionAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import utils.DataHolder;
import utils.Global;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;


public class DoctorSelectionFragmentCopy extends BaseFragment {//implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_LOCATION = 116;
    @Bind(R.id.rvData)
    RecyclerView rvDoctors;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.ivPrevMonth)
    ImageView ivPrevMonth;
    @Bind(R.id.tvSelectedMonthName)
    TextView tvSelectedMonthName;
    @Bind(R.id.ivNextMonth)
    ImageView ivNextMonth;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    private DoctorsSelectionAdapter adapter;
    private int limit = 0;
    private Calendar today;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;
    private List<User> doctorList = new ArrayList<>();

    public DoctorSelectionFragmentCopy() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DoctorSelectionFragmentCopy newInstance(String pojoAppointmentRequest, String availableTime) {
        DoctorSelectionFragmentCopy fragment = new DoctorSelectionFragmentCopy();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, pojoAppointmentRequest);
        args.putString(ARG_PARAM2, availableTime);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_selection, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        if (today.get(Calendar.DAY_OF_WEEK) ==
                Calendar.SUNDAY) {
            today.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH) + 1);
        }
        if (today.getTime().compareTo(DataHolder.selectedDate.getTime()) == 0) {
            ivPrevMonth.setVisibility(View.INVISIBLE);
        }
        tvNext.setVisibility(View.GONE);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().
                    getString(ARG_PARAM1), PojoAppointmentRequest.class);
        }
        rvDoctors.setLayoutManager(new LinearLayoutManager(getActivity()));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                hideProgress();
                if (result.getUsers().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    if (limit == 0) {
                        doctorList.clear();
                        doctorList.addAll(result.getUsers());
                        adapter = new DoctorsSelectionAdapter(DoctorSelectionFragmentCopy.this,
                                doctorList, pojoAppointmentRequest);
                        rvDoctors.setAdapter(adapter);
                    } else {
                        doctorList.addAll(result.getUsers());
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    if (limit == 0) {
                        doctorList.clear();
                        if (adapter != null) adapter.notifyDataSetChanged();
                        tvNoData.setVisibility(View.VISIBLE);
                    } else {
                        adapter.setShowMore(false);
                        tvNoData.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure() throws Exception {
                hideProgress();
                tvNoData.setVisibility(View.VISIBLE);
            }
        });
        tvSelectedMonthName.setText(monthDateFormat.format(DataHolder.selectedDate.getTime()));
        boolean permission = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (Build.VERSION.SDK_INT >= 23 && !permission) {
            FragmentCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            getDoctors(0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {

                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    getActivity().onBackPressed();
                    // Log.i(TAG, "Permission has been denied by user");

                } else {

                    //Log.i(TAG, "Permission has been granted by user");
                    getDoctors(0);
                }
                break;
            }
        }
    }

    @OnClick({R.id.tvBack, R.id.ivPrevMonth, R.id.ivNextMonth})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.ivPrevMonth:
                DataHolder.selectedDate.set(Calendar.DAY_OF_MONTH,
                        DataHolder.selectedDate.get(Calendar.DAY_OF_MONTH) - 1);

                if (DataHolder.selectedDate.get(Calendar.DAY_OF_WEEK) ==
                        Calendar.SUNDAY) {
                    DataHolder.selectedDate.set(Calendar.DAY_OF_MONTH,
                            DataHolder.selectedDate.get(Calendar.DAY_OF_MONTH) - 1);
                }

                tvSelectedMonthName.setText(monthDateFormat.format(DataHolder.selectedDate.getTime()));
                if (today.getTime().compareTo(DataHolder.selectedDate.getTime()) == 0) {
                    ivPrevMonth.setVisibility(View.INVISIBLE);
                }
                getDoctors(0);
                break;
            case R.id.ivNextMonth:
                ivPrevMonth.setVisibility(View.VISIBLE);
                DataHolder.selectedDate.set(Calendar.DAY_OF_MONTH,
                        DataHolder.selectedDate.get(Calendar.DAY_OF_MONTH) + 1);
                if (DataHolder.selectedDate.get(Calendar.DAY_OF_WEEK) ==
                        Calendar.SUNDAY) {
                    DataHolder.selectedDate.set(Calendar.DAY_OF_MONTH,
                            DataHolder.selectedDate.get(Calendar.DAY_OF_MONTH) + 1);
                }
                tvSelectedMonthName.setText(monthDateFormat.format(DataHolder.selectedDate.getTime()));
                getDoctors(0);
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void getDoctors(int limit) {
        showProgress();
        this.limit = limit;
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_TIME_SLOTS;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.user_id = loggedInUser.user_id;
        requestData.parameters.service_id = pojoAppointmentRequest.getServiceId();
        requestData.parameters.ttime = getTime();
        requestData.parameters.ddate = dateFormat.format(DataHolder.selectedDate.getTime());
        requestData.parameters.limit = limit + "";
        requestData.parameters.clinic_id = loggedInUser.clinic_id;
        requestData.parameters.latitude = Global.latitude;
        requestData.parameters.longitude = Global.longitude;

        makeRequest(requestData);
    }
}
