package patientfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import emrfragments.CurrentMedicalRecord;
import emrfragments.FamilyMedicalHistory;
import utils.Constants;
import utils.GeneralFunctions;

public class MedicalHistory extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public MedicalHistory() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MedicalHistory.
     */
    // TODO: Rename and change types and number of parameters
    public static MedicalHistory newInstance(String param1, String param2) {
        MedicalHistory fragment = new MedicalHistory();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medical_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.medical));
    }

    @OnClick({R.id.tvBack, R.id.tvMedical, R.id.tvFamily})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvMedical:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        CurrentMedicalRecord.newInstance(Constants.TYPE_PATIENT, ""), R.id.flAppointmentParent);
                break;
            case R.id.tvFamily:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        FamilyMedicalHistory.newInstance(Constants.TYPE_PATIENT, ""), R.id.flAppointmentParent);
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
