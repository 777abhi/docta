package patientfragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapters.NotificationAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;


public class NotificationFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvNotifications)
    RecyclerView rvNotifications;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.tvClearAll)
    TextView tvClearAll;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String yesterDayDate;
    private List<Common> notificationList = new ArrayList<Common>();


    public NotificationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pBaseBar = pBar;
        tvNoData.setText(getString(R.string.no_notifications));
        yesterDayDate = getYesterdayDateString();
        rvNotifications.setLayoutManager(new LinearLayoutManager(getActivity()));
        final NotificationAdapter notificationAdapter =
                new NotificationAdapter(getActivity(), notificationList);
        rvNotifications.setAdapter(notificationAdapter);
        notificationAdapter.setNotificationProcessor(new NotificationAdapter.INotification() {
            @Override
            public void deleteANotification(int pos) {
                RequestData requestData = new RequestData();
                if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
                    requestData.type = Constants.TYPE_PATIENT;
                    requestData.request = RequestName.DELETE_NOTIFICATION;
                    requestData.parameters.pat_id = loggedInUser.user_id;
                } else {
                    requestData.type = Constants.TYPE_DOCTOR;
                    requestData.request = RequestName.DELETE_DOC_NOTIFICATION;
                    requestData.parameters.doc_id = loggedInUser.user_id;
                }
                requestData.parameters.notification_id = notificationList.get(pos).getId();
                requestData.parameters.clearall = "";
                requestData.parameters.ddate = notificationList.get(pos).getDdate();
                makeRequest(requestData);
            }

            @Override
            public void deleteDateNotifications(int pos) {
                RequestData requestData = new RequestData();
                if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
                    requestData.type = Constants.TYPE_PATIENT;
                    requestData.request = RequestName.DELETE_NOTIFICATION;
                    requestData.parameters.pat_id = loggedInUser.user_id;
                } else {
                    requestData.type = Constants.TYPE_DOCTOR;
                    requestData.request = RequestName.DELETE_DOC_NOTIFICATION;
                    requestData.parameters.doc_id = loggedInUser.user_id;
                }
                requestData.parameters.notification_id = notificationList.get(pos).getId();
                requestData.parameters.clearall = "today";
                requestData.parameters.ddate = notificationList.get(pos).getDdate();
                makeRequest(requestData);
            }
        });
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.requestName.equals(RequestName.DELETE_NOTIFICATION) ||
                        result.requestName.equals(RequestName.DELETE_DOC_NOTIFICATION)) {
                    getNotifications();
                    return;
                }
                notificationList.clear();
                String date = "";
                for (int i = 0; i < result.getNotification_list().size(); i++) {
                    if (!date.equals(result.getNotification_list().get(i).getDdate())) {
                        Common header = new Common();
                        header.setIsHeader(true);
                        header.setDdate(result.getNotification_list().get(i).getDdate());
                        notificationList.add(header);
                    }
                    date = result.getNotification_list().get(i).getDdate();
                    notificationList.add(result.getNotification_list().get(i));
                    if (i == 0 && getDate().equals(notificationList.get(i).getDdate()))
                        notificationList.get(i).setIsToday(true);
                    if (i != 0 && notificationList.get(i).isHeader() && notificationList.get(i).
                            getDdate().equals(yesterDayDate)) {
                        notificationList.get(i).setIsYesterDay(true);
                    }
                }
                if (notificationList.size() == 0) {
                    tvNoData.setVisibility(View.VISIBLE);
                    tvClearAll.setVisibility(View.GONE);
                } else {
                    tvNoData.setVisibility(View.GONE);
                    tvClearAll.setVisibility(View.VISIBLE);
                    notificationAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });

        setNotificationListener(new NotificationListener() {
            @Override
            public void onNotification(Intent intent) {
                getNotifications();
            }
        });
        getNotifications();
    }

    private String getYesterdayDateString() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());
    }

    private void getNotifications() {
        RequestData requestData = new RequestData();
        if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
            requestData.request = RequestName.GET_PATIENT_NOTIFICATIONS;
            requestData.type = Constants.TYPE_PATIENT;
            requestData.parameters.pat_id = loggedInUser.user_id;
        } else {
            requestData.request = RequestName.GET_DOCTOR_NOTIFICATIONS;
            requestData.type = Constants.TYPE_DOCTOR;
            requestData.parameters.doc_id = loggedInUser.user_id;
        }
        makeRequest(requestData);
    }

    @OnClick({R.id.tvClearAll})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvClearAll:
                RequestData requestData = new RequestData();
                if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
                    requestData.parameters.pat_id = loggedInUser.user_id;
                    requestData.type = Constants.TYPE_PATIENT;
                    requestData.request = RequestName.DELETE_NOTIFICATION;
                } else {
                    requestData.parameters.doc_id = loggedInUser.user_id;
                    requestData.type = Constants.TYPE_DOCTOR;
                    requestData.request = RequestName.DELETE_DOC_NOTIFICATION;
                }
                requestData.parameters.clearall = "all";
                makeRequest(requestData);
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
