package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.TabsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;


public class HealthReminders extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_LOCATION = 116;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvOrder)
    TextView tvOrder;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public HealthReminders() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConsultHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HealthReminders newInstance(String param1, String param2) {
        HealthReminders fragment = new HealthReminders();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final String titles[] = {getString(R.string.medication),
                getString(R.string.appointment), getString(R.string.vacine)};
        frTopBar.setVisibility(View.GONE);
        hideProgress();
        tvOrder.setText(getString(R.string.dashboard));
        tvOrder.setTextColor(getColor(R.color.black));
        tvOrder.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        tvOrder.setVisibility(View.VISIBLE);
        TabsAdapter tabsAdapter = new TabsAdapter(getChildFragmentManager());
        tabsAdapter.addFragment(MedicationReminder.newInstance("", ""),
                titles[0]);
        tabsAdapter.addFragment(AppointmentReminder.newInstance("", ""),
                titles[1]);
        tabsAdapter.addFragment(VaccineReminder.newInstance("", ""),
                titles[2]);
        pager.setAdapter(tabsAdapter);
        tabLayout.setupWithViewPager(pager);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.commonpager, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
