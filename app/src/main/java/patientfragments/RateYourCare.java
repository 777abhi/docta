package patientfragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;

public class RateYourCare extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.receptionistRating)
    RatingBar receptionistRating;
    @Bind(R.id.nurseRating)
    RatingBar nurseRating;
    @Bind(R.id.doctorRating)
    RatingBar doctorRating;
    @Bind(R.id.overallRating)
    RatingBar overallRating;
    @Bind(R.id.etAdditionalComment)
    EditText etAdditionalComment;
    @Bind(R.id.tvSubmit)
    TextView tvSubmit;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvOverallRateText)
    TextView tvOverallRateText;

    // TODO: Rename and change types of parameters
    private String appointmentId;
    private String mParam2;


    public RateYourCare() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RateYourCare.
     */
    // TODO: Rename and change types and number of parameters
    public static RateYourCare newInstance(String param1, String param2) {
        RateYourCare fragment = new RateYourCare();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            appointmentId = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.requestName.equals(RequestName.GET_APPOINTMENT_CLINIC)) {
                    tvOverallRateText.setText(getString(R.string.overall_experience) + " " +
                            result.getClinic().getClinic_name() + " to your family and friends");
                } else {
                    userPrefsManager.saveAppointmentId(null);
                    getActivity().onBackPressed();
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });

        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_APPOINTMENT_CLINIC;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.pat_id = loggedInUser.user_id;
        requestData.parameters.apoint_id = appointmentId;
        makeRequest(requestData);
    }

    @OnClick({R.id.tvSubmit, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvSubmit:
                RequestData requestData = new RequestData();
                requestData.request = RequestName.RATE_APP;
                requestData.type = Constants.TYPE_PATIENT;
                requestData.parameters.pat_id = loggedInUser.user_id;
                requestData.parameters.apoint_id = appointmentId;
                requestData.parameters.rec_rating = (int) receptionistRating.getRating();
                requestData.parameters.nurse_rating = (int) nurseRating.getRating();
                requestData.parameters.doc_rating = (int) doctorRating.getRating();
                requestData.parameters.ttime = getTime();
                requestData.parameters.overall_rating = (int) overallRating.getRating();
                requestData.parameters.notes = etAdditionalComment.getText().toString();
                makeRequest(requestData);
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rate_your_care, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
