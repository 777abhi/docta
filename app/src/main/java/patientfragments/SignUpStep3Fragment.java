package patientfragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.DoctaHomeActivity;
import com.sanguinebits.docta.MainActivity;
import com.sanguinebits.docta.R;

import java.io.File;
import java.util.Stack;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Constants;
import utils.WebConstants;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.CommonPojo;
import webservices.pojos.User;


public class SignUpStep3Fragment extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.iv1)
    ImageView iv1;
    @Bind(R.id.iv2)
    ImageView iv2;
    @Bind(R.id.iv3)
    ImageView iv3;
    @Bind(R.id.iv4)
    ImageView iv4;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    private Stack<String> passcode = new Stack<>();
    private User user;
    private String passCode = "";
    // TODO: Rename parameter arguments, choose names that match

    // TODO: Rename and change types and number of parameters

    public SignUpStep3Fragment() {
        // Required empty public constructor
    }

    public static SignUpStep3Fragment newInstance(User user) {
        SignUpStep3Fragment fragment = new SignUpStep3Fragment();
        Bundle extras = new Bundle();
        extras.putString(ARG_PARAM1, new Gson().toJson(user));
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick({R.id.tvBack, R.id.tv0, R.id.tv1, R.id.tv2, R.id.tv3, R.id.tv4, R.id.tv5, R.id.tv6
            , R.id.tv7, R.id.tv8, R.id.tv9, R.id.tvDelete, R.id.tvReset, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.tvNext:
                if (passcode.size() < 4) {
                    showToast(R.string.passcode_should_be_of_length_4);
                    return;
                } else {
                    while (!passcode.empty()) {
                        passCode = passcode.pop() + passCode;
                    }
                    resetIndicator();
                }
                createUser();
                break;
            case R.id.tv0:
                if (passcode.size() < 4) {
                    passcode.push("0");
                    setIndicator();
                }
                break;
            case R.id.tv1:
                if (passcode.size() < 4) {
                    passcode.push("1");
                    setIndicator();
                }
                break;
            case R.id.tv2:
                if (passcode.size() < 4) {
                    passcode.push("2");
                    setIndicator();
                }
                break;
            case R.id.tv3:
                if (passcode.size() < 4) {
                    passcode.push("3");
                    setIndicator();
                }
                break;
            case R.id.tv4:
                if (passcode.size() < 4) {
                    passcode.push("4");
                    setIndicator();
                }
                break;
            case R.id.tv5:
                if (passcode.size() < 4) {
                    passcode.push("5");
                    setIndicator();
                }
                break;
            case R.id.tv6:
                if (passcode.size() < 4) {
                    passcode.push("6");
                    setIndicator();
                }
                break;
            case R.id.tv7:
                if (passcode.size() < 4) {
                    passcode.push("7");
                    setIndicator();
                }
                break;
            case R.id.tv8:
                if (passcode.size() < 4) {
                    passcode.push("8");
                    setIndicator();
                }
                break;
            case R.id.tv9:
                if (passcode.size() < 4) {
                    passcode.push("9");
                    setIndicator();
                }
                break;
            case R.id.tvDelete:
                if (passcode.size() > 0) {
                    passcode.pop();
                    setIndicatorAfterDelete();
                }
                break;
            case R.id.tvReset:
                if (passcode.size() > 0) {
                    passcode.clear();
                    resetIndicator();
                }
                break;
            default:
                break;
        }
    }

    private void createUser() {
        showProgress();
        RequestBody requestBody = null;
        if (!TextUtils.isEmpty(user.photo))
            requestBody = RequestBody.create(
                    MediaType.parse(Constants.MIME_TYPE_IMAGE), new File(user.photo));
        RequestBody typeBody = RequestBody.create(MediaType.parse("text/plain"), WebConstants.USER_TYPE_PATIENT);
        Call<CommonPojo> call = RestClient.get().register(requestBody, typeBody,
                RequestName.PATIENT_REGISTER, user.password, WebConstants.ANDROID, user.fname,
                user.lname, user.email, user.phone, user.age, user.gender,
                userPrefsManager.getRegId(), user.dob, passCode, null, "");
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    CommonPojo pojo = response.body();
                    showToast(pojo.getMsg());
                    if (pojo.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                        user.user_id = pojo.getUser_id();
                        user.qbid = pojo.getQbid();
                        user.qb_password = pojo.getQb_password();
                        user.photo = pojo.getImage();
                        user.passcode = passCode;
                        userPrefsManager.saveLoginUser(user);
                        if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT))
                            startActivity(new Intent(getActivity(), MainActivity.class));
                        else
                            startActivity(new Intent(getActivity(), DoctaHomeActivity.class));
                        getActivity().finish();
                    }
                } else {
                    showToast(R.string.there_might_be_some_problem);
                }
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    hideProgress();
                    handleException(t);
                } catch (Exception e) {

                }
            }
        });
    }

    private void setIndicatorAfterDelete() {
        int size = passcode.size();
        switch (size) {
            case 0:
                iv1.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                break;
            case 1:
                iv2.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                break;
            case 2:
                iv3.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                break;
            case 3:
                iv4.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                break;
        }
    }

    private void setIndicator() {
        int size = passcode.size();
        switch (size) {
            case 1:
                iv1.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                break;
            case 2:
                iv2.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                break;
            case 3:
                iv3.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                break;
            case 4:
                iv4.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                break;
        }
    }

    private void resetIndicator() {
        iv1.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
        iv2.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
        iv3.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
        iv4.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        user = new Gson().fromJson(getArguments().getString(ARG_PARAM1), User.class);
        pBaseBar = pBar;
        hideProgress();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up_step3, container, false);
        return view;
    }
    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
