package patientfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;


public class ChooseVisitTypeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvNext)
    TextView tvNext;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ChooseVisitTypeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChooseVisitTypeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChooseVisitTypeFragment newInstance(String param1, String param2) {
        ChooseVisitTypeFragment fragment = new ChooseVisitTypeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_choose_visit_type, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.INVISIBLE);
    }

    @OnClick({R.id.tvVideoVisit, R.id.tvClinicVisit, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvVideoVisit:
                EventBus.getDefault().post(getString(R.string.video_visit));
                break;
            case R.id.tvClinicVisit:
                EventBus.getDefault().post(getString(R.string.in_person_visit));
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
        getActivity().onBackPressed();
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
