package patientfragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import adapters.CalenderRecyclerAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import interfaces.DotInterface;
import utils.Constants;
import utils.DotView;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.AvailableTime;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;

public class ScheduleAppointment extends BaseFragment implements
        CalenderRecyclerAdapter.FragmentAdapterCommunicator, DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public GregorianCalendar cal_month, cal_month_copy;
    @Bind(R.id.tvSelectedMonthName)
    TextView tvSelectedMonthName;
    @Bind(R.id.rvCalender)
    RecyclerView rvCalender;
    @Bind(R.id.gridParent)
    GridLayout gridParent;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.IFoundTheseOpeningsAt)
    DotView tvIFoundTheseOpeningsAt;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.ivPrevMonth)
    ImageView ivPrevMonth;
    private int currentMonth, currentYear, currentDay, monthTracker;
    private boolean isFirstTime = true;
    private List<List<AvailableTime>> availableTimeList = new ArrayList<>();
    private List<List<List<AvailableTime>>> timeTrackerList = new ArrayList<>();

    private CalenderRecyclerAdapter cal_adapter;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;
    private String sourceFragment;
    private boolean showNextMonth = false;
    private List<AvailableTime> selectedTimeList = new ArrayList<>();
    private AvailableTime availableTime;
    private String ddate;


    public ScheduleAppointment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ScheduleAppointment newInstance(String param1, String sourceFragment) {
        ScheduleAppointment fragment = new ScheduleAppointment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, sourceFragment);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().
                    getString(ARG_PARAM1), PojoAppointmentRequest.class);
            sourceFragment = getArguments().getString(ARG_PARAM2);
            if (sourceFragment.equals(Constants.TAG_CONSULT_REQUEST))
                tvIFoundTheseOpeningsAt.setVisibility(View.GONE);
        }
        ivPrevMonth.setVisibility(View.INVISIBLE);
        if (pojoAppointmentRequest.getClinic() != null) {
            tvIFoundTheseOpeningsAt.setText(getString(R.string.i_found_these_openings_at) + " " +
                    pojoAppointmentRequest.getClinic().getClinic_name());
        }
        tvIFoundTheseOpeningsAt.post(tvIFoundTheseOpeningsAt.run);
        rvCalender.setLayoutManager(new GridLayoutManager(getActivity(), 7));
        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        monthTracker = currentMonth = cal_month.get(GregorianCalendar.MONTH);
        currentYear = cal_month.get(GregorianCalendar.YEAR);
        currentDay = cal_month.get(GregorianCalendar.DAY_OF_MONTH);
        ddate = getDate();
        tvSelectedMonthName.setText(DateFormat.format("MMMM yyyy", cal_month));
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        cal_adapter = new CalenderRecyclerAdapter(ScheduleAppointment.this, cal_month, availableTimeList);
        cal_adapter.setCurrentDay(currentDay);
        rvCalender.setAdapter(cal_adapter);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                //  availableTimeList.clear();
                // availableTimeList.addAll(result.getTime());
                availableTimeList = result.getTime();
                cal_adapter.setAvailableTimeList(availableTimeList);
                timeTrackerList.add(result.getTime());
                cal_adapter.notifyDataSetChanged();
                if (showNextMonth)
                    setNextMonth();
                refreshCalendar();
                if (isFirstTime) {
                    isFirstTime = false;
                    setTimeGrid(availableTimeList.get(currentDay - 1)); // by default show first day time slot
                }
            }

            @Override
            public void onFailure() {
                //tvNoData.setVisibility(View.VISIBLE);
            }
        });
        getMonthAvailableTimeSlots();
    }

    public void setTimeGrid(List<AvailableTime> timeSlotList) {
        selectedTimeList = timeSlotList;
        gridParent.removeAllViews();
        for (int i = 0; i < timeSlotList.size(); i++) {
            AvailableTime time = timeSlotList.get(i);
            TextView tv = (TextView) LayoutInflater.from(getActivity()).
                    inflate(R.layout.radio_button_row, null);
            tv.setText(time.getTime_slot());
            gridParent.addView(tv);
        }
    }

    private void getMonthAvailableTimeSlots() {
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_TIME_SLOTS;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.user_id = loggedInUser.user_id;
        requestData.parameters.service_id = pojoAppointmentRequest.getServiceId();
        requestData.parameters.clinic_id = pojoAppointmentRequest.getClinic().getClinic_id();
        requestData.parameters.ddate = ddate;
        if (ddate.equals(getDate())) {
            requestData.parameters.ttime = getTime();
        } else requestData.parameters.ttime = "12:00 AM";

        makeRequest(requestData);
    }

    public void setDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        Date date = calendar.getTime();
        ddate = dateFormat.format(date);

    }

    private String getTimeSlot() {
        for (int i = 0; i < gridParent.getChildCount(); i++) {
            CheckedTextView ct = (CheckedTextView) gridParent.getChildAt(i);
            if (ct.isChecked()) {
                availableTime = selectedTimeList.get(i);
                return ct.getText().toString();
            }
        }
        return "";
    }

    @Override
    public void setTimeSlot(List<AvailableTime> availableTimeSlotList) {

        setTimeGrid(availableTimeSlotList);
    }

    @OnClick({R.id.tvBack, R.id.tvNext, R.id.ivPrevMonth, R.id.ivNextMonth})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                if (TextUtils.isEmpty(cal_adapter.getDateSelected())) {
                    showToast(R.string.please_select_date);
                    return;
                } else if (TextUtils.isEmpty(getTimeSlot())) {
                    showToast(R.string.please_select_available_time);
                    return;
                } else {
                    pojoAppointmentRequest.setAppointment_date(cal_adapter.getDateSelected());
                    pojoAppointmentRequest.setTime_slot(getTimeSlot());
                    pojoAppointmentRequest.setUser_id(loggedInUser.user_id);
                }
                tvIFoundTheseOpeningsAt.resume();
                if (sourceFragment.equals(Constants.TAG_CONSULT_REQUEST)) {
                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
                            , ConfirmAppointment.newInstance(
                                    new Gson().toJson(pojoAppointmentRequest), true),
                            R.id.flRequestFragmentContainer);
                } else
                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
                            , DoctorSelectionFragment.newInstance(
                                    new Gson().toJson(pojoAppointmentRequest), new Gson().toJson(availableTime)),
                            R.id.flAppointmentParent);
                break;
            case R.id.ivPrevMonth:
                monthTracker--;
                timeTrackerList.remove(timeTrackerList.size() - 1);
                availableTimeList = timeTrackerList.get(timeTrackerList.size() - 1);
                cal_adapter.setAvailableTimeList(availableTimeList);
                setDate(1, monthTracker, currentYear);
                gridParent.removeAllViews();
                cal_adapter.previousView = null;
                cal_adapter.defaultSelection = -1;
                setPreviousMonth();
                refreshCalendar();
                break;
            case R.id.ivNextMonth:
                monthTracker++;
                setDate(1, monthTracker, currentYear);
                cal_adapter.previousView = null;
                gridParent.removeAllViews();
                cal_adapter.setCurrentDay(0);
                cal_adapter.defaultSelection = -1;
                showNextMonth = true;
                getMonthAvailableTimeSlots();
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_schedule_appointment, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }
        if (ivPrevMonth.getVisibility() == View.INVISIBLE)
            ivPrevMonth.setVisibility(View.VISIBLE);
    }

    @Override
    public void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }
        if (currentMonth == cal_month.get(GregorianCalendar.MONTH) &&
                currentYear == cal_month.get(GregorianCalendar.YEAR)) {
            ivPrevMonth.setVisibility(View.INVISIBLE);
            cal_adapter.setCurrentDay(currentDay);
            setTimeGrid(availableTimeList.get(currentDay - 1));
        }
    }

    @Override
    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tvSelectedMonthName.setText(DateFormat.format("MMMM yyyy", cal_month));
    }
    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        tvIFoundTheseOpeningsAt.resume();
    }

    @Override
    public void stopDot() {
        tvIFoundTheseOpeningsAt.stopHandler();
    }
}
