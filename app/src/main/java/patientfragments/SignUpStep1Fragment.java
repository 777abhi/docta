package patientfragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.pojos.User;

public class SignUpStep1Fragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.tvTermsAndConditions)
    TextView tvTermsAndConditions;

    // TODO: Rename and change types of parameters


    public SignUpStep1Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignUpStep1Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignUpStep1Fragment newInstance(String param1, String param2) {
        SignUpStep1Fragment fragment = new SignUpStep1Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.tvNext, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvNext:
                if (TextUtils.isEmpty(etEmail.getText()))
                    showToast(R.string.empty_email);
                else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches())
                    showToast(R.string.invalid_email);
                else if (TextUtils.isEmpty(etPassword.getText()))
                    showToast(R.string.empty_password);
                else if (etPassword.getText().length() < Constants.PASSWORD_LENGTH)
                    showToast(R.string.password_should_be_of_length_8);
                else {
                    etEmail.clearFocus();
                    etPassword.clearFocus();
                    hideKeyBoard(etPassword);
                    User user = new User();
                    user.email = etEmail.getText().toString();
                    user.password = etPassword.getText().toString();
                    GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                            SignUpStep2Fragment.newInstance(user), R.id.flFragmentContainer);
                }
                break;
            case R.id.tvBack:
                getFragmentManager().popBackStackImmediate();
                break;
            default:
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up_step1, container, false);
        ButterKnife.bind(this, view);
        SpannableStringBuilder sBuilder = new SpannableStringBuilder(getString(
                R.string.by_creating_account_you_agree_to_terms_n_contions));

        String termsOfService = getString(R.string.terms_of_service);
        int termsOfServiceStartIndex = sBuilder.toString().indexOf(termsOfService);
        int termsOfServiceEndIndex = termsOfServiceStartIndex + termsOfService.length();

        String privacy_policy = getString(R.string.privacy_policy);
        int privacyPolicyStartIndex = sBuilder.toString().indexOf(privacy_policy);
        int privacyPolicyEndIndex = privacyPolicyStartIndex + privacy_policy.length();

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(getActivity(), R.color.black));

            }
        };
        ClickableSpan privacyPolicyClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(getActivity(), R.color.black));
            }
        };
        sBuilder.setSpan(clickableSpan, termsOfServiceStartIndex, termsOfServiceEndIndex,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sBuilder.setSpan(privacyPolicyClickableSpan, privacyPolicyStartIndex, privacyPolicyEndIndex,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTermsAndConditions.setText(sBuilder, TextView.BufferType.SPANNABLE);
        tvTermsAndConditions.setMovementMethod(LinkMovementMethod.getInstance());
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
