package patientfragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import adapters.SymptomOptionsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import interfaces.DotInterface;
import utils.DotView;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;


public class PatientQuestionStep2Fragment extends Fragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.rvAvailableTimes)
    RecyclerView rvAvailableTimes;
    @Bind(R.id.dotView)
    DotView dotView;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;
    private SymptomOptionsAdapter adapter;
    private String[] dataSource;

    public PatientQuestionStep2Fragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PatientQuestionStep2Fragment newInstance(String param1) {
        PatientQuestionStep2Fragment fragment = new PatientQuestionStep2Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().
                    getString(ARG_PARAM1), PojoAppointmentRequest.class);
        }
    }


    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                if (adapter.getSelectedPosition() != -1) {
                    dotView.stopHandler();
                    pojoAppointmentRequest.setWhenDidYouStartFeelingThisWay(
                            dataSource[adapter.getSelectedPosition()]);
                    GeneralFunctions.addFragmentFromRight(getFragmentManager(), PatientQuestionStep3Fragment.
                            newInstance(new Gson().toJson(pojoAppointmentRequest)), R.id.flAppointmentParent);
                }
                break;
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dataSource = getResources().getStringArray(R.array.symptoms_feeling_options);
        rvAvailableTimes.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SymptomOptionsAdapter(getActivity(), dataSource);
        rvAvailableTimes.setAdapter(adapter);
        dotView.post(dotView.run);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_question_step2, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        dotView.resume();
    }

    @Override
    public void stopDot() {
        dotView.stopHandler();
    }
}
