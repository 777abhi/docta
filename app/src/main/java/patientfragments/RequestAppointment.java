package patientfragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import interfaces.DotInterface;
import utils.DotView;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;


public class RequestAppointment extends Fragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvScheduleAppointment)
    DotView tvScheduleAppointment;
    @Bind(R.id.rgSelection)
    RadioGroup rgSelection;

    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;

    public RequestAppointment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static RequestAppointment newInstance(String param1) {
        RequestAppointment fragment = new RequestAppointment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().
                    getString(ARG_PARAM1), PojoAppointmentRequest.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request_appointment, container, false);
        ButterKnife.bind(this, view);
        tvScheduleAppointment.setText(getString(R.string.do_you_want_to_schedule_an_appointment_now));
        tvScheduleAppointment.post(tvScheduleAppointment.run);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                tvScheduleAppointment.stopHandler();
                if (rgSelection.getCheckedRadioButtonId() == R.id.rbYes) {
//                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
//                            , PatientQuestionStep1Fragment.newInstance(
//                                    new Gson().toJson(pojoAppointmentRequest), ""), R.id.flAppointmentParent);
                    GeneralFunctions.addFragmentFromRight(getFragmentManager(), AppointmentRequestFragment.
                                    newInstance(new Gson().toJson(pojoAppointmentRequest)),
                            R.id.flAppointmentParent);
                } else if (rgSelection.getCheckedRadioButtonId() == R.id.rbNo) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        tvScheduleAppointment.resume();
    }

    @Override
    public void stopDot() {
        tvScheduleAppointment.stopHandler();
    }
}
