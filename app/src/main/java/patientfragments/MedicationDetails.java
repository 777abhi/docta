package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import webservices.pojos.PojoMedication;


public class MedicationDetails extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvMedicationName)
    TextView tvMedicationName;
    @Bind(R.id.tvQuantity)
    TextView tvQuantity;
    @Bind(R.id.tvDose)
    TextView tvDose;
    @Bind(R.id.tvFrequency)
    TextView tvFrequency;
    @Bind(R.id.tvStart)
    TextView tvStart;
    @Bind(R.id.tvEnd)
    TextView tvEnd;

    private PojoMedication pojoMedication;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public MedicationDetails() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MedicationDetails.
     */
    // TODO: Rename and change types and number of parameters
    public static MedicationDetails newInstance(String param1, String param2) {
        MedicationDetails fragment = new MedicationDetails();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoMedication = new Gson().fromJson(getArguments().getString(ARG_PARAM1), PojoMedication.class);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medication_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.medication_details));
        tvMedicationName.setText(pojoMedication.name);
        tvStart.setText(getDateFromString(pojoMedication.startDate));
        tvQuantity.setText(pojoMedication.medication_quantity);
        tvEnd.setText(getDateFromString(pojoMedication.endDate));
        tvFrequency.setText(pojoMedication.quantity + getString(R.string.space) + pojoMedication.dosetype.toLowerCase() +
                getString(R.string.in_the_morning) + pojoMedication.time1.toLowerCase() + getString(R.string.and)
                + pojoMedication.time2.toLowerCase());
        tvDose.setText(pojoMedication.dose);
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
