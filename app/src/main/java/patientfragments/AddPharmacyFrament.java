package patientfragments;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.PharmacyAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import utils.CommonEvent;
import utils.Constants;
import utils.Global;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.Pharmacy;

public class AddPharmacyFrament extends BaseFragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.etName)
    EditText etName;
    @Bind(R.id.rClinics)
    RecyclerView rvData;
    @Bind(R.id.rgSelection)
    RadioGroup rgSelection;
    @Bind(R.id.listParent)
    LinearLayout listParent;
    @Bind(R.id.frameMapParent)
    FrameLayout mapParent;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    private GoogleMap mGoogleMap;
    private List<Pharmacy> filterList = new ArrayList<>();
    private List<Pharmacy> pharmacyList = new ArrayList<>();
    private Pharmacy selectedPharmacy;
    private PharmacyAdapter pharmacyAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private MapFragment supportMapFragment;

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            applyFilter(s.toString());
        }
    };

    public AddPharmacyFrament() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AddPharmacyFrament newInstance(String param1, String param2) {
        AddPharmacyFrament fragment = new AddPharmacyFrament();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void applyFilter(String searchText) {
        filterList.clear();
        if (searchText.isEmpty()) {
            filterList.addAll(pharmacyList);
            pharmacyAdapter.notifyDataSetChanged();
            return;
        }
        for (int i = 0; i < pharmacyList.size(); i++) {
            if ((pharmacyList.get(i).getName()).startsWith(searchText)) {
                filterList.add(pharmacyList.get(i));
            }
            pharmacyAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_clinic, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        tvNext.setVisibility(View.GONE);
        tvNoData.setVisibility(View.GONE);
        tvNoData.setText(getString(R.string.no_pharmacy_found));
        tvTitle.setText(getString(R.string.my_pharmacies));
        etName.addTextChangedListener(textWatcher);
        FragmentManager fm = getChildFragmentManager();
        supportMapFragment = MapFragment.newInstance();
        fm.beginTransaction().replace(R.id.frameMapParent, supportMapFragment).commit();

        rgSelection.setEnabled(false);
        supportMapFragment.getMapAsync(this);
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                if (result.requestName.equals(RequestName.GET_ALL_PHARMACY)) { // response for all clinics
                    showLocationsOnMap(result.getNear_pharmacy());
                    pharmacyList = result.getPharmacy_list();
                    filterList.addAll(pharmacyList);
                    pharmacyAdapter = new PharmacyAdapter(AddPharmacyFrament.this, filterList);
                    rvData.setAdapter(pharmacyAdapter);
                } else if (result.requestName.equals(RequestName.ADD_FAV_PHARMACY)) { // response for fav clinic api
                    if (selectedPharmacy != null) // when add fav clinic from map
                        userPrefsManager.saveFavPharmacy(selectedPharmacy);
                    else// when add fav clinic from list
                        userPrefsManager.saveFavPharmacy(pharmacyAdapter.selectedPharmacy);
                    for (int i = 0; i < filterList.size(); i++) {
                        if (i == pharmacyAdapter.selectedClinicFavPosition)
                            filterList.get(i).setIsFav("1");
                        else filterList.get(i).setIsFav("0");
                    }
                    pharmacyAdapter.notifyDataSetChanged();
                    EventBus.getDefault().post(new CommonEvent(CommonEvent.FAV_PHARMACY_ADDED));
                }
            }

            @Override
            public void onFailure() {
            }
        });
        rgSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbList) {
                    mapParent.setVisibility(View.GONE);
                    if (pharmacyList.size() == 0) {
                        tvNoData.setVisibility(View.VISIBLE);
                        listParent.setVisibility(View.GONE);
                    } else {
                        tvNoData.setVisibility(View.GONE);
                        listParent.setVisibility(View.VISIBLE);
                    }
                } else {
                    tvNoData.setVisibility(View.GONE);
                    listParent.setVisibility(View.GONE);
                    mapParent.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event

    private void showLocationsOnMap(final List<Pharmacy> pharmacyList) {
        try {
            LatLngBounds.Builder mLatLngBoundsBuilder = new LatLngBounds.Builder();
            for (int i = 0; i < pharmacyList.size(); i++) {
                Pharmacy obj = pharmacyList.get(i);
                mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(
                        R.drawable.ic_marker_clinic)).position(new LatLng(Double.parseDouble(obj.getLati()),
                        Double.parseDouble(obj.getLongi()))).title(
                        obj.getName() + "," + obj.getLocation()).snippet(String.valueOf(i)));

                mLatLngBoundsBuilder.include(new LatLng(Double.parseDouble(obj.getLati()),
                        Double.parseDouble(obj.getLongi())));
                mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                    @Override
                    public void onInfoWindowClick(final Marker marker) {
                        String content = getString(R.string.do_you_want_to_add_this_pharmacy_to_favorite);
                        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                                .content(content)
                                .positiveText(R.string.yes)
                                .theme(Theme.LIGHT)
                                .title(getString(R.string.add_to_fav_pharmacy))
                                .cancelable(false)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        // TODO
                                        int pos = Integer.parseInt(marker.getSnippet());
                                        selectedPharmacy = pharmacyList.get(pos);
                                        RequestData requestData = new RequestData();
                                        requestData.request = RequestName.ADD_FAV_PHARMACY;
                                        requestData.type = Constants.TYPE_PATIENT;
                                        requestData.parameters.user_id = loggedInUser.user_id;
                                        requestData.parameters.pharm_id = pharmacyList.get(pos).getId();
                                        makeRequest(requestData);
                                    }
                                })
                                .negativeText(R.string.no)
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        // TODO
                                        dialog.dismiss();
                                    }
                                });
                        MaterialDialog dialog = builder.build();
                        dialog.show();
                    }

                });
            }
            LatLngBounds bounds = mLatLngBoundsBuilder.build();
            final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds,
                    500, 500, 0);
            mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    mGoogleMap.animateCamera(cameraUpdate);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
       /* MapFragment f;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            f = ((MapFragment) getFragmentManager()
                    .findFragmentById(R.id.myMapFragment));
        } else {
            f = ((MapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.myMapFragment));
        }
        if (f != null) {
            getFragmentManager().beginTransaction().remove(f).commit();
        }*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_ALL_PHARMACY;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.user_id = loggedInUser.user_id;
        requestData.parameters.latitude = Global.latitude;
        requestData.parameters.longitude = Global.longitude;
        makeRequest(requestData);
    }
}
