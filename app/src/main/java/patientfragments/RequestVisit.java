package patientfragments;

import android.app.FragmentManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import interfaces.DotInterface;
import utils.Constants;
import utils.DotView;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;

public class RequestVisit extends BaseFragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final int REQUEST_WRITE_STORAGE = 112;
    @Bind(R.id.tvHeading)
    DotView tvHeading;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;


    public RequestVisit() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static RequestVisit newInstance(String param1) {
        RequestVisit fragment = new RequestVisit();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    PojoAppointmentRequest.class);
        } else {
            pojoAppointmentRequest = new PojoAppointmentRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {

                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    // Log.i(TAG, "Permission has been denied by user");

                } else {
                    openLoaderFaragment();
                    //Log.i(TAG, "Permission has been granted by user");

                }
                return;
            }
        }
    }

    @OnClick({R.id.tvRequestVisit, R.id.tvScheduleAppointment})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvRequestVisit:
                tvHeading.stopHandler();
                pojoAppointmentRequest.visitType = getString(R.string.video_visit);
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , DisclaimerFragment.newInstance(new Gson().toJson(
                                pojoAppointmentRequest)), R.id.flAppointmentParent);

                break;
            case R.id.tvScheduleAppointment:
                tvHeading.stopHandler();
                GeneralFunctions.addFragmentFromRight(getFragmentManager(), AppointmentRequestFragment.
                                newInstance(new Gson().toJson(pojoAppointmentRequest)),
                        R.id.flAppointmentParent);
                break;
        }
    }

    private void openLoaderFaragment() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(),
                LoaderFragment.newInstance(new Gson().toJson(
                        pojoAppointmentRequest)), R.id.flAppointmentParent,
                Constants.LOADER_FRAGMENT);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvHeading.setText(getString(R.string.how_will_you_like_to_get_care_today));
        tvHeading.post(tvHeading.run);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request_visit, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        tvHeading.resume();
    }

    @Override
    public void stopDot() {
        tvHeading.stopHandler();
    }
}
