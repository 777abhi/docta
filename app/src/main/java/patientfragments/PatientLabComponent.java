package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.PatientLabAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class PatientLabComponent extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvPageTitle)
    TextView tvPageTitle;
    @Bind(R.id.divider)
    View divider;
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.linearParent)
    LinearLayout linearParent;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;


    // TODO: Rename and change types of parameters
    // private Vitals vitals;
    private String mParam2;
    //List<Common> data = new ArrayList<>();


    public PatientLabComponent() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PatientLabComponent newInstance(String param1, String type) {
        PatientLabComponent fragment = new PatientLabComponent();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pBaseBar = pBar;
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.test_result));
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));


        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                if (result.getCurrent_vital_list().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    rvData.setAdapter(new PatientLabAdapter(Constants.TYPE_PATIENT,
                            PatientLabComponent.this, Constants.COMMON, result.getCurrent_vital_list()));
                } else tvNoData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        });
        RequestData requestData = new RequestData();
        requestData.type = Constants.EMR;
        requestData.request = RequestName.GET_LAB;
        requestData.parameters.pat_id = loggedInUser.user_id;
        makeRequest(requestData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


}
