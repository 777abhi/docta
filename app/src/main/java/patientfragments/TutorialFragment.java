package patientfragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TutorialFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "position";
    @Bind(R.id.ivImg)
    ImageView ivImg;
    @Bind(R.id.tvTutorialText)
    TextView tvTutorialText;
    @Bind(R.id.tvTutorialHeading)
    TextView tvTutorialHeading;

    // TODO: Rename and change types of parameters
    private int posotion;

    public TutorialFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static TutorialFragment newInstance(int position) {
        TutorialFragment fragment = new TutorialFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            posotion = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tutorial, container, false);
        ButterKnife.bind(this, view);
        switch (posotion) {
            case 1:
                ivImg.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.easy));
                tvTutorialText.setText(getString(R.string.connect_with_your_doctor));
                tvTutorialHeading.setVisibility(View.GONE);
                break;
            case 3:
                ivImg.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.affordable));
                tvTutorialText.setText(getString(R.string.simple_prices_no_surprising));
                tvTutorialHeading.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
