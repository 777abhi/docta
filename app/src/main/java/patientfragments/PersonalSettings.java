package patientfragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.DoctaHomeActivity;
import com.sanguinebits.docta.MainActivity;
import com.sanguinebits.docta.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import utils.CommonEvent;
import utils.Constants;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.User;

public class PersonalSettings extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.etFirstName)
    EditText etFirstName;
    @Bind(R.id.etLastName)
    EditText etLastName;
    @Bind(R.id.tvDob)
    TextView tvDob;
    @Bind(R.id.rbMale)
    RadioButton rbMale;
    @Bind(R.id.rbFemale)
    RadioButton rbFemale;
    @Bind(R.id.rgGender)
    RadioGroup rgGender;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.etPhone)
    EditText etPhone;
    @Bind(R.id.etMedicalSchool)
    EditText etMedicalSchool;
    @Bind(R.id.spSpeciality)
    Spinner spSpeciality;
    @Bind(R.id.etYearOfPractice)
    EditText etYearOfPractice;
    @Bind(R.id.etAreaOfInterest)
    EditText etAreaOfInterest;
    @Bind(R.id.etHobby)
    EditText etHobby;
    @Bind(R.id.tvSex)
    TextView tvSex;
    @Bind(R.id.tvSubmit)
    TextView tvSubmit;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    private String picturePath;
    private ProgressDialog mProgressDialog;
    private String date = "2016-01-01";
// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

// TODO: Rename and change types of parameters


    public PersonalSettings() {
        // Required empty public constructor
    }

    public static PersonalSettings newInstance(User user) {
        PersonalSettings fragment = new PersonalSettings();
        Bundle extras = new Bundle();
        extras.putString(ARG_PARAM1, new Gson().toJson(user));
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick({R.id.tvSubmit, R.id.tvBack, R.id.tvDob})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvDob:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMaxDate(now);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.tvSubmit:
                etFirstName.clearFocus();
                etLastName.clearFocus();
                hideKeyBoard(etHobby);
                String sex = R.id.rbMale == rgGender.getCheckedRadioButtonId() ?
                        Constants.MALE : Constants.FEMALE;
                loggedInUser.fname = etFirstName.getText().toString();
                loggedInUser.lname = etLastName.getText().toString();
                loggedInUser.phone = etPhone.getText().toString().replace("+233 ", "");
                loggedInUser.dob = tvDob.getText().toString();
                loggedInUser.medical_school = etMedicalSchool.getText().toString();
                loggedInUser.speciality = spSpeciality.getSelectedItem().toString();
                loggedInUser.yop = etYearOfPractice.getText().toString();
                loggedInUser.aoi = etAreaOfInterest.getText().toString();
                loggedInUser.hobby = etHobby.getText().toString();
                loggedInUser.sex = sex;
                updateUser();
                break;
            case R.id.tvBack:
                getFragmentManager().popBackStackImmediate();
                break;
        }
    }


    private void updateUser() {
        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.request = RequestName.DOC_UPDATE_PERSONAL_INFO;
        requestData.parameters.doc_id = loggedInUser.user_id;
        requestData.parameters.fname = loggedInUser.fname;
        requestData.parameters.lname = loggedInUser.lname;
        requestData.parameters.sex = loggedInUser.sex;
        requestData.parameters.dob = loggedInUser.dob;
        //requestData.parameters.speciality = loggedInUser.speciality;
        requestData.parameters.speciality = (spSpeciality.getSelectedItemPosition() + 1) + "";
        requestData.parameters.medical_school = loggedInUser.medical_school;
        requestData.parameters.yop = loggedInUser.yop;
        requestData.parameters.aoi = loggedInUser.aoi;
        requestData.parameters.hobby = loggedInUser.hobby;
        makeRequest(requestData);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;

        ArrayList<String> specialityList = new ArrayList<>();
        specialityList.add(getString(R.string.general_practitioner));
//        specialityList.add(getString(R.string.dermatologist));
//        specialityList.add(getString(R.string.pediatrician));
//        specialityList.add(getString(R.string.ob_gyn));
        ArrayAdapter<String> specialityAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.row_single_textview, R.id.tvOptionName, specialityList);
        spSpeciality.setAdapter(specialityAdapter);
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.personal_settings));
        etFirstName.setText(loggedInUser.fname);
        etLastName.setText(loggedInUser.lname);
        etPhone.setText(loggedInUser.phone);
        tvDob.setText(loggedInUser.dob);
        etMedicalSchool.setText(loggedInUser.medical_school);
        if (TextUtils.isEmpty(loggedInUser.speciality)) ;
//        spSpeciality.setSelection(Integer.parseInt(loggedInUser.speciality));
        etYearOfPractice.setText(loggedInUser.yop);
        etAreaOfInterest.setText(loggedInUser.aoi);
        etHobby.setText(loggedInUser.hobby);

        if (loggedInUser.sex.equals(Constants.MALE))
            rbMale.setChecked(true);
        else rbFemale.setChecked(true);


        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                    userPrefsManager.saveLoginUser(loggedInUser);
                    EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                    if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT))
                        startActivity(new Intent(getActivity(), MainActivity.class));
                    else
                        startActivity(new Intent(getActivity(), DoctaHomeActivity.class));
                    getActivity().finish();
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal_settings, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void displayDate() {
        date = year + "-" + (month + 1) + "-" + day;
        tvDob.setText(date);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
        }
        mProgressDialog.setMessage(getString(R.string.processing_image));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }
// TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int y, int m, int d) {
        year = y;
        month = m;
        day = d;
        displayDate();
    }
}
