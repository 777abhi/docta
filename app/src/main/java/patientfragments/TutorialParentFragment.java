package patientfragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;


public class TutorialParentFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final int PAGE_COUNT = 1;
    //private static final int PAGE_COUNT = 5;

    @Bind(R.id.tutorialPager)
    ViewPager tutorialPager;
    @Bind(R.id.iv1)
    ImageView iv1;
    @Bind(R.id.iv2)
    ImageView iv2;
    @Bind(R.id.iv3)
    ImageView iv3;
    @Bind(R.id.iv4)
    ImageView iv4;
    @Bind(R.id.iv5)
    ImageView iv5;
    private ImageView ivSelected;

    public TutorialParentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tutorial_parent, container, false);
        ButterKnife.bind(this, view);
        ivSelected = iv1;
        tutorialPager.setAdapter(new TutorialPagerAdapter(getFragmentManager()));
        tutorialPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (ivSelected != null)
                    ivSelected.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                switch (position) {
                    case 0:
                        ivSelected = iv1;
                        iv1.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                        break;
                    case 1:
                        ivSelected = iv2;
                        iv2.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                        break;
                    case 2:
                        ivSelected = iv3;
                        iv3.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                        break;
                    case 3:
                        ivSelected = iv4;
                        iv4.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                        break;
                    case 4:
                        ivSelected = iv5;
                        iv5.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private class TutorialPagerAdapter extends FragmentPagerAdapter {
        public TutorialPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    // return WelcomeFragment.newInstance();
                    return LoginFragment.newInstance();
                case 1:
                    return TutorialFragment.newInstance(position);
                case 2:
                    return ConvientFragment.newInstance();
                case 3:
                    return TutorialFragment.newInstance(position);
                case 4:
                    return LoginFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

}
