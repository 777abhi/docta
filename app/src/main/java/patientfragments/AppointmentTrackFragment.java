package patientfragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import adapters.AppointmentTrackAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;

public class AppointmentTrackFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public GregorianCalendar cal_month, cal_month_copy;
    @Bind(R.id.tvSelectedMonthName)
    TextView tvSelectedMonthName;
    @Bind(R.id.rvCalender)
    RecyclerView rvCalender;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    private int currentMonth, currentYear, monthTracker, currentDay;

    private AppointmentTrackAdapter appointmentTrackAdapter;
    // TODO: Rename and change types of parameters
    private String ddate;


    public AppointmentTrackFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AppointmentTrackFragment newInstance(String param1, String sourceFragment) {
        AppointmentTrackFragment fragment = new AppointmentTrackFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, sourceFragment);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        rvCalender.setLayoutManager(new GridLayoutManager(getActivity(), 7));
        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        currentYear = cal_month.get(GregorianCalendar.YEAR);
        monthTracker = currentMonth = cal_month.get(GregorianCalendar.MONTH);
        currentDay = cal_month.get(GregorianCalendar.DAY_OF_MONTH);
        ddate = getDate();
        tvSelectedMonthName.setText(DateFormat.format("MMMM yyyy", cal_month));
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        appointmentTrackAdapter = new AppointmentTrackAdapter(AppointmentTrackFragment.this, cal_month);
        appointmentTrackAdapter.setCurrentDay(currentDay);
        rvCalender.setAdapter(appointmentTrackAdapter);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                List<Common> commonList = result.getCurrent_list();
                appointmentTrackAdapter.setTrackList(commonList);
                appointmentTrackAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
        getAppointmentCalander();
    }

    private void getAppointmentCalander() {
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_APPOINTMENT_CALANDER;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.pat_id = loggedInUser.user_id;
        if (monthTracker == currentMonth)
            requestData.parameters.ddate = getDate();
        else
            requestData.parameters.ddate = ddate;
        requestData.parameters.ttime = getTime();
        makeRequest(requestData);
    }


    public void setDate(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        Date date = calendar.getTime();
        ddate = dateFormat.format(date);

    }

    @OnClick({R.id.tvBack, R.id.ivPrevMonth, R.id.ivNextMonth})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.ivPrevMonth:
                monthTracker--;
                setDate(1, monthTracker, currentYear);
                setPreviousMonth();
                refreshCalendar();
                getAppointmentCalander();
                break;
            case R.id.ivNextMonth:
                monthTracker++;
                setDate(1, monthTracker, currentYear);
                setNextMonth();
                refreshCalendar();
                getAppointmentCalander();
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_track, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }


    public void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }
//        if (ivPrevMonth.getVisibility() == View.INVISIBLE)
//            ivPrevMonth.setVisibility(View.VISIBLE);
    }

    public void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }
        /*if (currentMonth == cal_month.get(GregorianCalendar.MONTH) &&
                currentYear == cal_month.get(GregorianCalendar.YEAR)) {
            ivPrevMonth.setVisibility(View.INVISIBLE);
        }*/
    }

    public void refreshCalendar() {
        appointmentTrackAdapter.refreshDays();
        appointmentTrackAdapter.notifyDataSetChanged();
        tvSelectedMonthName.setText(DateFormat.format("MMMM yyyy", cal_month));
    }
    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
