package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import utils.GeneralFunctions;
import views.ConcentricView;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoMedication;


public class MedicationReminder extends BaseFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTime)
    TextView tvTime;
    @Bind(R.id.tvTapToTake)
    TextView tvTapToTake;
    @Bind(R.id.tvTargetTime1)
    TextView tvTargetTime1;
    @Bind(R.id.tvTargetTime2)
    TextView tvTargetTime2;
    @Bind(R.id.linearParent)
    LinearLayout linearParent;
    @Bind(R.id.concentricView)
    ConcentricView concentricView;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private boolean isAllTaken = false;
    private List<PojoMedication> medicationList;

    private int cycleTime = 60000;// 1 minutes cycle
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                concentricView.setTime(tvTargetTime1, tvTargetTime2);
                handler.postDelayed(runnable, cycleTime);
            } catch (Exception e) {

            }
        }
    };

    public MedicationReminder() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MedicationReminder.
     */
    // TODO: Rename and change types and number of parameters
    public static MedicationReminder newInstance(String param1, String param2) {
        MedicationReminder fragment = new MedicationReminder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.ivCalander})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.ivCalander:
                GeneralFunctions.addFragmentFromRight(getActivity().getFragmentManager(),
                        MedicationTrackFragment.newInstance("", ""), R.id.flAppointmentParent);
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medication_reminder, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTapToTake.setOnClickListener(this);
        String date = getDateInDayMonth(getDate());
        String d[] = date.split("\n");
        SpannableString styledString
                = new SpannableString(date);
        styledString.setSpan(new RelativeSizeSpan(0.65f), d[0].length(), styledString.length(), 0);
        styledString.setSpan(new ForegroundColorSpan(getColor(R.color.disclaimer_text_color)),
                d[0].length(), styledString.length(), 0);
        tvTime.setText(styledString);
        handler.post(runnable);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                tvTapToTake.setCompoundDrawables(null, null, null, null);
                if (result.requestName.equals(RequestName.TAKE_MEDICATION)) {
                    int countAllTaken = 0;
                    for (int i = 0; i < medicationList.size(); i++) {
                        if (medicationList.get(i).taken.equals(Constants.MEDICATION_TAKEN))
                            countAllTaken++;
                    }
                    if (countAllTaken == medicationList.size()) {
                        isAllTaken = true;
                        concentricView.setAllTaken(true);
                        concentricView.setTime(tvTargetTime1, tvTargetTime2);
                        tvTapToTake.setCompoundDrawablesWithIntrinsicBounds(null,
                                getDrawable(R.drawable.check), null, null);
                        tvTapToTake.clearAnimation();
                        tvTapToTake.setText(getString(R.string.all_taken));
                        linearParent.removeAllViews();
                        setMedicationData();
                    } else {
                        Animation myFadeInAnimation =
                                AnimationUtils.loadAnimation(getActivity(), R.anim.text_flash);
                        tvTapToTake.startAnimation(myFadeInAnimation);
                    }
                    return;
                }
                medicationList = result.getCurrent_medication();
                if (result.getTaken().equals(Constants.MEDICATION_TAKEN)) {
                    tvTapToTake.setCompoundDrawablesWithIntrinsicBounds(null,
                            getDrawable(R.drawable.check), null, null);
                    tvTapToTake.setText(getString(R.string.all_taken));
                    isAllTaken = true;
                    tvTapToTake.clearAnimation();
                    concentricView.setAllTaken(isAllTaken);
                    concentricView.setTime(tvTargetTime1, tvTargetTime2);
                } else {
                    Animation myFadeInAnimation =
                            AnimationUtils.loadAnimation(getActivity(), R.anim.text_flash);
                    tvTapToTake.startAnimation(myFadeInAnimation);
                }
                if (medicationList.size() == 0) {
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(getString(R.string.no_medicaation_due));
                    concentricView.setVisibility(View.GONE);
                    tvTargetTime1.setVisibility(View.GONE);
                    tvTargetTime2.setVisibility(View.GONE);
                    tvTapToTake.setVisibility(View.GONE);
                    // tvNoData.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
                } else
                    setMedicationData();
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
        getMedicationsForToday();
    }

    private void setMedicationData() {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 30);

        TextView hView = (TextView) LayoutInflater.from(getActivity())
                .inflate(R.layout.row_medication_reminder_header, null, false);
        hView.setText(Calendar.getInstance().before(calendar) ? "8:30 AM" : "8:00 PM");
        linearParent.addView(hView);
        for (int i = 0; i < medicationList.size(); i++) {
            PojoMedication pojoMedication = medicationList.get(i);
            View view = LayoutInflater.from(getActivity())
                    .inflate(R.layout.row_today_medication, null, false);
            ViewHolder viewHolder = new ViewHolder(view);
            viewHolder.tvMedicationame.setText(pojoMedication.name);
            viewHolder.tvQuantity.setText("Take " + pojoMedication.quantity + " pill");
            viewHolder.cbTakeMedication.setOnClickListener(MedicationReminder.this);
            if (pojoMedication.taken.equals(Constants.MEDICATION_TAKEN)) {
                viewHolder.cbTakeMedication.setImageResource(R.drawable.check_on);
            }
            viewHolder.cbTakeMedication.setTag(i);
            linearParent.addView(view);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void getMedicationsForToday() {
        RequestData request = new RequestData();
        request.request = RequestName.GET_TODAY_MEDICATION;
        request.type = Constants.EMR;
        request.parameters.pat_id = loggedInUser.user_id;
        request.parameters.ttime = getTime();
        makeRequest(request);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cbTakeMedication:
                int pos = (int) v.getTag();
                if (medicationList.get(pos).taken.equals(Constants.MEDICATION_NOT_TAKEN)) {
                    medicationList.get(pos).taken = Constants.MEDICATION_TAKEN;
                    ((ImageView) v).setImageResource(R.drawable.check_on);
                    takeMedication(pos);
                }
                break;
            case R.id.tvTapToTake:
                if (!isAllTaken) {
                    for (int i = 0; i < medicationList.size(); i++)
                        medicationList.get(i).taken = Constants.MEDICATION_TAKEN;
                    takeMedication(-1);// check all
                }
                break;
        }
    }

    private void takeMedication(int pos) {
        Calendar rightNow = Calendar.getInstance();
        float totalMinutes = rightNow.get(Calendar.HOUR_OF_DAY) * 60 + rightNow.get(Calendar.MINUTE);
        RequestData requestData = new RequestData();
        requestData.type = Constants.EMR;
        requestData.request = RequestName.TAKE_MEDICATION;
        requestData.parameters.pat_id = loggedInUser.user_id;
        requestData.parameters.daytime = totalMinutes > 510 ? "e" : "m";
        String id = "";
        if (pos == -1) {
            for (int i = 0; i < medicationList.size(); i++) {
                id += medicationList.get(i).id + (i == medicationList.size() - 1 ? "" : ",");
            }
        } else id = medicationList.get(pos).id;
        requestData.parameters.medication_id = id;
        makeRequest(requestData);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvMedicationame)
        TextView tvMedicationame;
        @Bind(R.id.tvQuantity)
        TextView tvQuantity;
        @Bind(R.id.cbTakeMedication)
        ImageView cbTakeMedication;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
