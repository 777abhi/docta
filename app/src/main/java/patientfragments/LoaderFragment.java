package patientfragments;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.gson.Gson;
import com.sanguinebits.docta.CallActivity;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import services.VideoCallService;
import utils.CommonEvent;
import utils.Constants;
import utils.DataHolder;
import views.MyArcView;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.NotificationHandler;
import webservices.pojos.PojoAppointmentRequest;

public class LoaderFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public String appointId = "";
    @Bind(R.id.myArcView)
    MyArcView myArcView;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;

    public LoaderFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static LoaderFragment newInstance(String param1) {
        LoaderFragment fragment = new LoaderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_loader, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    PojoAppointmentRequest.class);
        }
        RotateAnimation anim = new RotateAnimation(0.0f, 360f, Animation.RELATIVE_TO_SELF, .5f,
                Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(Constants.ROTATION_PERIOD);
        myArcView.setAnimation(anim);

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {

                if (result.requestName.equals(RequestName.REQUEST_VIDEO_VISIT)) {
                    appointId = result.getAppoint_id();
                    DataHolder.appointmentId = result.getAppoint_id();
                } else if (result.requestName.equals(RequestName.GET_AVAIL_DOC)) {
                    userPrefsManager.saveAssignedDoctorProfile(result.getUser());
                    getActivity().stopService(new Intent(getActivity(), VideoCallService.class));
                    startCallActivity();
                } else if (result.requestName.equals(RequestName.CANCEl_VISIT)) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    EventBus.getDefault().post(new CommonEvent(CommonEvent.SET_TAB_INDICATOR));
                    //addMainFragment();
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });

        setNotificationListener(new NotificationListener() {
            @Override
            public void onNotification(Intent intent) {
                NotificationHandler ce = new Gson().fromJson(
                        intent.getStringExtra(Constants.MESSAGE_KEY), NotificationHandler.class);
                if (ce.getMsg().equals(Constants.NOTIFICATION_TYPE_GET_AVAILABLE_DOCTOR))
                    getAvailableDoctor();
            }
        });
        requestVideoVisit();

    }

    private void requestVideoVisit() {
        RequestData requestData = new RequestData();
        requestData.request = RequestName.REQUEST_VIDEO_VISIT;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.user_id = loggedInUser.user_id;
        requestData.parameters.health_concern = pojoAppointmentRequest.getPrimaryHealthConcern();
        requestData.parameters.symptom_json = new Gson().toJson(pojoAppointmentRequest.sympotoms);
        requestData.parameters.ttime = getTime();
        requestData.parameters.full_symptom_json = new Gson().toJson(pojoAppointmentRequest);
        requestData.parameters.service_id = pojoAppointmentRequest.getServiceId();
        makeRequest(requestData);
    }

    @OnClick({R.id.tvCancel})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvCancel:
                showCancelConfirmationDialog();
                break;
        }
    }

    private void showCancelConfirmationDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                .content(R.string.cancel_visit)
                .positiveText(R.string.yes)
                .theme(Theme.LIGHT)
                .title(getString(R.string.do_you_want_to_cancel_visit))
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        if (TextUtils.isEmpty(appointId)) {
                            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            EventBus.getDefault().post(new CommonEvent(CommonEvent.SET_TAB_INDICATOR));
                            //addMainFragment();
                        } else {
                            cancelVisit();
                        }
                    }
                })
                .negativeText(R.string.no)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        dialog.dismiss();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.show();
    }

    private void startCallActivity() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        EventBus.getDefault().post(new CommonEvent(CommonEvent.SET_TAB_INDICATOR));
        Intent intent = new Intent(getActivity(), CallActivity.class);
        intent.putExtra("type", Constants.TYPE_PATIENT);
        startActivityForResult(intent, Constants.CALL_ACTIVITY_CLOSE);
    }

    private void addMainFragment() {
//        GeneralFunctions.addFragmentFromRight(getFragmentManager(),
//                new MainFragment(), R.id.flFragmentContainer);
    }

    public void onBackPress() {
        if (TextUtils.isEmpty(appointId)) {
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            EventBus.getDefault().post(new CommonEvent(CommonEvent.SET_TAB_INDICATOR));
            //addMainFragment();
        } else
            showCancelConfirmationDialog();
    }

    private void cancelVisit() {
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.CANCEl_VISIT;
        apiRequest.type = Constants.TYPE_PATIENT;
        apiRequest.parameters.pat_id = loggedInUser.user_id;
        apiRequest.parameters.apoint_id = appointId;
        makeRequest(apiRequest);
    }

    private void getAvailableDoctor() {
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_AVAIL_DOC;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.apoint_id = appointId;
        requestData.parameters.pat_id = loggedInUser.user_id;
        makeRequest(requestData);//
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
