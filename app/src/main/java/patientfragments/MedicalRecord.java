package patientfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import emrfragments.CurrentImmunizationCopy;
import emrfragments.EMRMedication;
import emrfragments.LabFrament;
import emrfragments.VitalFragment;
import utils.Constants;
import utils.GeneralFunctions;


public class MedicalRecord extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public MedicalRecord() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MedicalRecord.
     */
    // TODO: Rename and change types and number of parameters
    public static MedicalRecord newInstance(String param1, String param2) {
        MedicalRecord fragment = new MedicalRecord();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medical_record, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.my_medical_record));

    }

    @OnClick({R.id.tvBack, R.id.tvMedications, R.id.tvNext, R.id.tvTestResult,
            R.id.tvMedicalHistory, R.id.tvAllergies, R.id.tvVaccination, R.id.tvVitals})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvVitals:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new VitalFragment(), R.id.flAppointmentParent);
                break;
            case R.id.tvTestResult:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new LabFrament(), R.id.flAppointmentParent);
//                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
//                        new PatientLabComponent(), R.id.flAppointmentParent);
                break;
            case R.id.tvMedications:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        EMRMedication.newInstance(Constants.TYPE_PATIENT, ""), R.id.flAppointmentParent);
                break;
            case R.id.tvVaccination:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new CurrentImmunizationCopy(), R.id.flAppointmentParent);
//                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
//                        ImmunizationFragment.newInstance(Constants.TYPE_PATIENT, ""), R.id.flAppointmentParent);
                break;
            case R.id.tvAllergies:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        Allergies.newInstance(Constants.TYPE_PATIENT, ""), R.id.flAppointmentParent);
                break;
            case R.id.tvMedicalHistory:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new MedicalHistory(), R.id.flAppointmentParent);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
