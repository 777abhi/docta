package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import views.RoundCornerMultiColorProgressBar;
import webservices.pojos.Common;
import webservices.pojos.User;
import webservices.pojos.Vital;


public class TestDetailFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvResultValue)
    TextView tvResultValue;
    @Bind(R.id.tvPatientName)
    TextView tvPatientName;
    @Bind(R.id.tvDateTime)
    TextView tvDateTime;
    @Bind(R.id.testProgress)
    RoundCornerMultiColorProgressBar testProgress;

    private Vital labComponent;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private int indicator = -1;

    private ArrayList<String> rangeList = new ArrayList<>();

    public TestDetailFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TestDetailFragment newInstance(String param1) {
        TestDetailFragment fragment = new TestDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            labComponent = new Gson().fromJson(getArguments().getString(ARG_PARAM1), Vital.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test_result, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.done));
        tvTitle.setText(getString(R.string.test_details));
        tvDateTime.setText(labComponent.getDdate());

        Common component = labComponent.getComp_detail();
        User patient = labComponent.getProfile();
        tvPatientName.setText(patient.getFullName());
        rangeList.add(component.getGreen());
        rangeList.add(component.getYellow());
        rangeList.add(component.getRed());
        if (labComponent.getUnit().equals(getString(R.string.no_unit))) {
            tvResultValue.setText(labComponent.getComp_value()); //
        } else {
            simplyfyRange(component.getRed(), Constants.RED);
            if (indicator == -1) {
                simplyfyRange(component.getGreen(), Constants.GREEN);
                if (indicator == -1)
                    simplyfyRange(component.getYellow(), Constants.YELLOW);
            }
        }
        if (indicator != -1) {
            testProgress.setData(indicator, labComponent.getComp_value()
                    + " " + labComponent.getUnit(), rangeList);
        } else testProgress.setVisibility(View.GONE);
    }

    private void simplyfyRange(String rangeColor, int colorCode) {
        if (TextUtils.isEmpty(rangeColor)) {
            tvResultValue.setText(labComponent.getComp_value()); //
        } else if (rangeColor.toLowerCase().contains(Constants.OR)) {
            String arr[] = rangeColor.toLowerCase().split(Constants.OR);
            findRange(arr[0], colorCode);
            if (indicator == -1)
                findRange(arr[1], colorCode);

        } else if (rangeColor.contains(Constants.DASH)) {
            findRange(rangeColor, colorCode);
        }
    }

    private int findRange(String cv, int colorCode) {
        float low = Float.parseFloat(cv.split(Constants.DASH)[0]);
        float high = Float.parseFloat(cv.split(Constants.DASH)[1]);
        float temp = Float.parseFloat(labComponent.getComp_value());
        if (temp >= low && temp < high) {
            indicator = colorCode;
        }
        return indicator;
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                getActivity().onBackPressed();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
