package patientfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import adapters.VisitAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;


public class VirtualVisit extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvVisits;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    // TODO: Rename and change types of parameters
    private List<Appointment> visitHistory;


    public VirtualVisit() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static VirtualVisit newInstance(String param1) {
        VirtualVisit fragment = new VirtualVisit();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvVisits.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (getArguments() != null) {
            visitHistory = new Gson().fromJson(getArguments().
                    getString(ARG_PARAM1), CommonPojo.class).getVideo_visit();
        }
        if (visitHistory.size() == 0)
            tvNoData.setVisibility(View.VISIBLE);
        else {
            rvVisits.setAdapter(new VisitAdapter(getActivity(), visitHistory));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recycler_only_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
