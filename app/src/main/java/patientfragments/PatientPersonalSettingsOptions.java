package patientfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import doctorfragments.EmailPasswordSettings;
import utils.GeneralFunctions;


public class PatientPersonalSettingsOptions extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvNext)
    TextView tvNext;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public PatientPersonalSettingsOptions() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PersonalSettingsOptions.
     */
    // TODO: Rename and change types and number of parameters
    public static PatientPersonalSettingsOptions newInstance(String param1, String param2) {
        PatientPersonalSettingsOptions fragment = new PatientPersonalSettingsOptions();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_personal_settings_options, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
    }

    @OnClick({R.id.tvBack, R.id.tvPersonalInformation, R.id.tvEmailPassword, R.id.tvChangePasscode})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvChangePasscode:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , ChangePasscode.newInstance(getString(R.string.enter_your_old_passcode)),
                        R.id.flAppointmentParent);
                break;
            case R.id.tvPersonalInformation:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , new PatientPersonalSettings(), R.id.flAppointmentParent);
                break;
            case R.id.tvEmailPassword:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , new EmailPasswordSettings(), R.id.flAppointmentParent);
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
