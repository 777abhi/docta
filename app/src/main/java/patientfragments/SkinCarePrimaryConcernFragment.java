package patientfragments;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import interfaces.DotInterface;
import utils.DotView;
import webservices.pojos.PojoAppointmentRequest;

/**
 * Created by OM on 3/28/2016.
 */
public class SkinCarePrimaryConcernFragment extends BaseFragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvHeading)
    DotView tvHeading;
    @Bind(R.id.rgParent)
    RadioGroup rgParent;
    @Bind(R.id.etPrimaryHealthConcern)
    EditText etPrimaryHealthConcern;
    @Bind(R.id.rbOther)
    RadioButton rbOther;
    @Bind(R.id.otherParent)
    LinearLayout otherParent;


    private int[] arr;
    private int max;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;

    public SkinCarePrimaryConcernFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SkinCarePrimaryConcernFragment newInstance(String param1) {
        SkinCarePrimaryConcernFragment fragment = new SkinCarePrimaryConcernFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    PojoAppointmentRequest.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.skin_care_concern_fragment, container, false);
        ButterKnife.bind(this, view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                if (rgParent.getCheckedRadioButtonId() == R.id.rbOther) {
                    pojoAppointmentRequest.primaryHealthConcern =
                            etPrimaryHealthConcern.getText().toString();
                }
                if (TextUtils.isEmpty(pojoAppointmentRequest.primaryHealthConcern))
                    return;
                tvHeading.stopHandler();
                EventBus.getDefault().post(pojoAppointmentRequest);
                getActivity().onBackPressed();
//                if (rgParent.getCheckedRadioButtonId() == R.id.rbOther) {
//                    pojoAppointmentRequest.primaryHealthConcern =
//                            etPrimaryHealthConcern.getText().toString();
//                }
//                if (pojoAppointmentRequest.primaryHealthConcern.isEmpty())
//                    return;
//                tvHeading.stopHandler();
//                if (pojoAppointmentRequest.isSeriousHealthProblem())
//                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
//                            , PatientQuestionStep2Fragment.newInstance(
//                                    new Gson().toJson(pojoAppointmentRequest)), R.id.flAppointmentParent);
//                else
//                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
//                            , PatientQuestionStep3Fragment.newInstance(
//                                    new Gson().toJson(pojoAppointmentRequest)), R.id.flAppointmentParent);

                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvHeading.setText(getString(R.string.please_select_your_primary_healrh_concern));
        tvHeading.post(tvHeading.run);
        rbOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherParent.setVisibility(View.VISIBLE);
            }
        });
        rgParent.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int pos = rgParent.indexOfChild(rgParent.findViewById(checkedId));
                pojoAppointmentRequest.primaryHealthConcern =
                        ((RadioButton) rgParent.getChildAt(pos)).getText().toString();
                if (checkedId == R.id.rbOther) {
                    otherParent.setVisibility(View.VISIBLE);
                } else otherParent.setVisibility(View.GONE);

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        tvHeading.resume();
    }

    @Override
    public void stopDot() {
        tvHeading.stopHandler();
    }
}