package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.PatientChildAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.User;

public class PatientChildFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.rvData)
    RecyclerView rvUserLists;
    @Bind(R.id.tvNoData)
    TextView tvNoUsers;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public PatientChildFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PatientChildFragment newInstance(String param1, String param2) {
        PatientChildFragment fragment = new PatientChildFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , new AddUser(), R.id.flAppointmentParent);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.add));
        tvTitle.setText(getString(R.string.all_users));
        tvNoUsers.setText(getString(R.string.no_new_users));
        tvNoUsers.setVisibility(View.GONE);
        rvUserLists.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        pBaseBar = pBar;
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                tvNoUsers.setVisibility(View.GONE);
                rvUserLists.setVisibility(View.VISIBLE);
                rvUserLists.setAdapter(
                        new PatientChildAdapter(getActivity(), result.getChild_list()));
            }

            @Override
            public void onFailure() {
                tvNoUsers.setVisibility(View.VISIBLE);
                rvUserLists.setVisibility(View.GONE);
            }
        });
        getChilds();
    }

    public void onEvent(CommonEvent commonEvent) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getChilds();
                break;
            case CommonEvent.PROFILE_CHANGE:
                getChilds();
                break;
        }
    }

    private void getChilds() {
        if (userPrefsManager.getParentUser() == null || userPrefsManager.getLoggedInUser().user_id.
                equals(userPrefsManager.getParentUser().user_id)) {
            tvNext.setVisibility(View.VISIBLE);
            RequestData requestData = new RequestData();
            requestData.request = RequestName.GET_PAT_CHILD;
            requestData.type = Constants.TYPE_PATIENT;
            requestData.parameters.user_id = userPrefsManager.getParentUser() == null ?
                    loggedInUser.user_id : userPrefsManager.getParentUser().user_id;
            makeRequest(requestData);
        } else {
            tvNoUsers.setVisibility(View.GONE);
            tvNext.setVisibility(View.GONE);
            rvUserLists.setVisibility(View.VISIBLE);
            List<User> userList = new ArrayList<>();
            userList.add(userPrefsManager.getParentUser());
            rvUserLists.setAdapter(
                    new PatientChildAdapter(getActivity(), userList));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
