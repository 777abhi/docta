package patientfragments;

import android.app.Activity;
import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import interfaces.DotInterface;
import utils.Constants;
import utils.DotView;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;

public class PatientQuestionStep3Fragment extends BaseFragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.etOtherSymptoms)
    EditText etOtherSymptoms;
    @Bind(R.id.tvHeading)
    DotView tvHeading;
    @Bind(R.id.gridGeneralSymptomms)
    GridLayout gridGeneralSymptomms;
    @Bind(R.id.gridHeadNeck)
    GridLayout gridHeadNeck;
    @Bind(R.id.gridChest)
    GridLayout gridChest;
    @Bind(R.id.gridGastrointestinal)
    GridLayout gridGastrointestinal;
    @Bind(R.id.gridUrinary)
    GridLayout gridUrinary;
    @Bind(R.id.gridMusculoskeletal)
    GridLayout gridMusculoskeletal;
    @Bind(R.id.gridSkin)
    GridLayout gridSkin;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    private int[] arr;
    private int max;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;

    public PatientQuestionStep3Fragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PatientQuestionStep3Fragment newInstance(String param1) {
        PatientQuestionStep3Fragment fragment = new PatientQuestionStep3Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public static int getMax(int[] inputArray) {
        int maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_question_step3, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        arr = new int[]{gridGeneralSymptomms.getChildCount(),
                gridHeadNeck.getChildCount(),
                gridChest.getChildCount(),
                gridGastrointestinal.getChildCount(),
                gridUrinary.getChildCount(),
                gridMusculoskeletal.getChildCount(),
                gridSkin.getChildCount()};
        max = getMax(arr);
        return view;
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                tvHeading.stopHandler();
                pojoAppointmentRequest.sympotoms.skinSympotoms.clear();
                pojoAppointmentRequest.sympotoms.musculoskeletalSympotoms.clear();
                pojoAppointmentRequest.sympotoms.urinarySympotoms.clear();
                pojoAppointmentRequest.sympotoms.gastrointestinalSympotoms.clear();
                pojoAppointmentRequest.sympotoms.chestSympotoms.clear();
                pojoAppointmentRequest.sympotoms.generalSympotoms.clear();
                pojoAppointmentRequest.sympotoms.headNeckSympotoms.clear();
                pojoAppointmentRequest.sympotoms.otherSymptoms = etOtherSymptoms.getText().toString();
                for (int i = 0; i < max; i++) {
                    if (i < arr[0] && ((CheckedTextView) gridGeneralSymptomms.getChildAt(i)).isChecked()) {
                        pojoAppointmentRequest.sympotoms.generalSympotoms.add(
                                ((CheckedTextView) gridGeneralSymptomms.getChildAt(i)).getText().toString());
                        pojoAppointmentRequest.sympotoms.generalSympotomsPositions.add(i);
                    }
                    if (i < arr[1] && ((CheckedTextView) gridHeadNeck.getChildAt(i)).isChecked()) {
                        pojoAppointmentRequest.sympotoms.headNeckSympotoms.add(
                                ((CheckedTextView) gridHeadNeck.getChildAt(i)).getText().toString());
                        pojoAppointmentRequest.sympotoms.headNeckSympotomsPositions.add(i);
                    }
                    if (i < arr[2] && ((CheckedTextView) gridChest.getChildAt(i)).isChecked()) {
                        pojoAppointmentRequest.sympotoms.chestSympotoms.add(
                                ((CheckedTextView) gridChest.getChildAt(i)).getText().toString());
                        pojoAppointmentRequest.sympotoms.chestSympotomsPositions.add(i);
                    }
                    if (i < arr[3] && ((CheckedTextView) gridGastrointestinal.getChildAt(i)).isChecked()) {
                        pojoAppointmentRequest.sympotoms.gastrointestinalSympotoms.add(
                                ((CheckedTextView) gridGastrointestinal.getChildAt(i)).getText().toString());
                        pojoAppointmentRequest.sympotoms.gastrointestinalSympotomsPositions.add(i);
                    }

                    if (i < arr[4] && ((CheckedTextView) gridUrinary.getChildAt(i)).isChecked()) {
                        pojoAppointmentRequest.sympotoms.urinarySympotoms.add(
                                ((CheckedTextView) gridUrinary.getChildAt(i)).getText().toString());
                        pojoAppointmentRequest.sympotoms.urinarySympotomsPositions.add(i);
                    }

                    if (i < arr[5] && ((CheckedTextView) gridMusculoskeletal.getChildAt(i)).isChecked()) {
                        pojoAppointmentRequest.sympotoms.musculoskeletalSympotoms.add(
                                ((CheckedTextView) gridMusculoskeletal.getChildAt(i)).getText().toString());
                        pojoAppointmentRequest.sympotoms.musculoskeletalSympotomsPositions.add(i);
                    }
                    if (i < arr[6] && ((CheckedTextView) gridSkin.getChildAt(i)).isChecked()) {
                        pojoAppointmentRequest.sympotoms.skinSympotoms.add(
                                ((CheckedTextView) gridSkin.getChildAt(i)).getText().toString());
                        pojoAppointmentRequest.sympotoms.skinSympotomsPositions.add(i);
                    }
                }
                if (!pojoAppointmentRequest.isSeriousHealthProblem()) {
                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
                            , RequestVisit.newInstance(new Gson().toJson(
                                    pojoAppointmentRequest)), R.id.flAppointmentParent);
                } else if (favClinic == null) {
                    if (!((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE)).
                            isProviderEnabled(LocationManager.NETWORK_PROVIDER) &&
                            !((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE)).
                                    isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        showLocationSettingsAlert(getActivity());
                    } else
                        GeneralFunctions.addFragmentFromRight(getFragmentManager()
                                , ClosestClinic.newInstance(
                                        new Gson().toJson(pojoAppointmentRequest), Constants.SCHEDULE),
                                R.id.flAppointmentParent);
                } else {
                    pojoAppointmentRequest.setClinic(favClinic);
                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
                            , ScheduleAppointment.newInstance(new Gson().toJson(
                                    pojoAppointmentRequest), ""), R.id.flAppointmentParent);
                }
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    PojoAppointmentRequest.class);
        }
        tvHeading.setText(getString(R.string.what_other_symptoms_you_experiencing));
        tvHeading.post(tvHeading.run);
        //getFavClinic();
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        tvHeading.resume();
    }

    @Override
    public void stopDot() {
        tvHeading.stopHandler();
    }
}
