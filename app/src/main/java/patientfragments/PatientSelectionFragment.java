package patientfragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.UserNameAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;


public class PatientSelectionFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.patientList)
    RecyclerView patientList;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvNoOtherUser)
    TextView tvNoOtherUser;

    private UserNameAdapter adapter;
    private PojoAppointmentRequest pojoAppointmentRequest;
    // TODO: Rename and change types of parameters
    private List<User> userList = new ArrayList<>();

    public PatientSelectionFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PatientSelectionFragment newInstance(String param1) {
        PatientSelectionFragment fragment = new PatientSelectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    PojoAppointmentRequest.class);
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pBaseBar = pBar;
        tvNext.setVisibility(View.GONE);
        userList.add(0, loggedInUser);
        adapter = new UserNameAdapter(getActivity(), userList);
        patientList.setAdapter(adapter);
        patientList.setLayoutManager(new LinearLayoutManager(getActivity()));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                userList.addAll(result.getChild_list());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure() {
                tvNoOtherUser.setVisibility(View.VISIBLE);
            }
        });
        getChilds();
    }

    private void getChilds() {
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_PAT_CHILD;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.user_id = loggedInUser.user_id;
        makeRequest(requestData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_selection, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                if (adapter != null && adapter.getSelectedPosition() != -1) {
                    pojoAppointmentRequest.setPatient(userList.get(adapter.getSelectedPosition()));
                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
                            , DisclaimerFragment.newInstance(new Gson().toJson(
                                    pojoAppointmentRequest)), R.id.flAppointmentParent);
                }

                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
