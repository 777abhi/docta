package patientfragments;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import interfaces.LocationGetListener;
import utils.Constants;
import utils.DataHolder;
import utils.GeneralFunctions;
import utils.Global;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;


public class AppointmentRequestFragment extends BaseFragment implements
        DatePickerDialog.OnDateSetListener,
        DatePickerDialog.OnDateChangedListener {


    private static final String ARG_PARAM1 = "param1";
    private static final int REQUEST_LOCATION = 116;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvWhoIsPatient)
    TextView tvWhoIsPatient;
    @Bind(R.id.tvVisitType)
    TextView tvVisitType;
    @Bind(R.id.tvDate)
    TextView tvDate;
    @Bind(R.id.tvIAmSearchingFor)
    TextView tvIAmSearchingFor;
    @Bind(R.id.tvPrimaryConcern)
    TextView tvPrimaryConcern;
    @Bind(R.id.pBar) ProgressWheel pBar;
    private PojoAppointmentRequest pojoAppointmentRequest;
    private DatePickerDialog dpd;
    private Calendar now;
    private int color;

    public AppointmentRequestFragment() {
        // Required empty public constructor
    }

    public static AppointmentRequestFragment newInstance(String param1) {
        AppointmentRequestFragment fragment = new AppointmentRequestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_request, container, false);
        ButterKnife.bind(this, view);
        pojoAppointmentRequest = new PojoAppointmentRequest();
        now = Calendar.getInstance();
        now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        return view;
    }


    @OnClick({R.id.tvWhoIsPatient, R.id.tvDate, R.id.tvIAmSearchingFor,
            R.id.tvPrimaryConcern, R.id.tvRequestNow, R.id.tvVisitType, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvWhoIsPatient:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , PatientSelectionFragment.newInstance(new Gson().toJson(
                                pojoAppointmentRequest)), R.id.flAppointmentParent);
                break;
            case R.id.tvIAmSearchingFor:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , MainFragment.newInstance(new Gson().toJson(pojoAppointmentRequest)),
                        R.id.flAppointmentParent);
                break;
            case R.id.tvPrimaryConcern:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , ServiceTypeFragment.newInstance(new Gson().toJson(pojoAppointmentRequest)),
                        R.id.flAppointmentParent);
//                if (pojoAppointmentRequest.getServiceId().equals(Constants.GENERAL) ||
//                        pojoAppointmentRequest.getServiceId().equals(Constants.CHILD_CARE)) {
//                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), PrimaryHealthConcernFragment.
//                                    newInstance(new Gson().toJson(pojoAppointmentRequest)),
//                            R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
//                } else if (pojoAppointmentRequest.getServiceId().equals(Constants.SKIN_CARE)) {
//                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), SkinCarePrimaryConcernFragment.
//                                    newInstance(new Gson().toJson(pojoAppointmentRequest)),
//                            R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
//                } else {
//                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), PatientQuestionStep1Fragment.
//                                    newInstance(new Gson().toJson(pojoAppointmentRequest),
//                                            getString(R.string.please_describe_your_primary_health_concern)),
//                            R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
//                }
                break;
            case R.id.tvRequestNow:
                boolean permission = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
                if (Build.VERSION.SDK_INT >= 23 && !permission) {
                    FragmentCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            REQUEST_LOCATION);
                } else {
                    openDoctorSelectionFragment();
                }
                break;
            case R.id.tvVisitType:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , new ChooseVisitTypeFragment(),
                        R.id.flAppointmentParent);
                break;
            case R.id.tvDate:
                if (now.get(Calendar.DAY_OF_WEEK) ==
                        Calendar.SUNDAY) {
                    now.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH) + 1);
                }
                dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMinDate(now);
                dpd.registerOnDateChangedListener(this);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;

        }
    }

    public void onEvent(User selectedUser) {
        pojoAppointmentRequest.setPatient(selectedUser);

        SpannableStringBuilder spannableStringBuilder = null;
        if (selectedUser.user_id.equals(loggedInUser.user_id)) {
            spannableStringBuilder = new SpannableStringBuilder(
                    getString(R.string.who_is_the_patient) + "\n" + getString(R.string.me));
        } else {
            spannableStringBuilder = new SpannableStringBuilder(
                    getString(R.string.who_is_the_patient) + "\n" + selectedUser.getFullName());
        }
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.who_is_the_patient).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.who_is_the_patient).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvWhoIsPatient.setText(spannableStringBuilder);
    }

    public void onEvent(String visitType) {
        pojoAppointmentRequest.visitType = visitType;
        SpannableStringBuilder spannableStringBuilder5 = new SpannableStringBuilder(
                getString(R.string.visit_type) + "\n" + visitType);
        spannableStringBuilder5.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.visit_type).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder5.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.visit_type).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvVisitType.setText(spannableStringBuilder5);
    }

    public void onEvent(PojoAppointmentRequest pojoAppointmentRequest) {
        this.pojoAppointmentRequest = pojoAppointmentRequest;

        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(
                getString(R.string.i_am_searching_for) + "\n" +
                        pojoAppointmentRequest.getServiceName() + " " + getString(R.string.service));
        spannableStringBuilder2.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.i_am_searching_for).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder2.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.i_am_searching_for).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvIAmSearchingFor.setText(spannableStringBuilder2);

        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(
                getString(R.string.my_primary_concern_is) + "\n" +
                        pojoAppointmentRequest.getPrimaryHealthConcern());
        spannableStringBuilder3.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.my_primary_concern_is).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder3.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.my_primary_concern_is).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvPrimaryConcern.setText(spannableStringBuilder3);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        EventBus.getDefault().register(this);
        pBaseBar = pBar;
//        if (getArguments() != null) {
//            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(
//                    ARG_PARAM1), PojoAppointmentRequest.class);
//        }
        pojoAppointmentRequest.setPatient(loggedInUser);
        pojoAppointmentRequest.setServiceId(Constants.GENERAL);
        pojoAppointmentRequest.setServiceName(getString(R.string.general));
        pojoAppointmentRequest.primaryHealthConcern =
                getString(R.string.malaria);
        pojoAppointmentRequest.visitType = getString(R.string.video_visit);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(
                getString(R.string.who_is_the_patient) + "\n" + getString(R.string.me));
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.who_is_the_patient).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.who_is_the_patient).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvWhoIsPatient.setText(spannableStringBuilder);

        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(
                getString(R.string.i_am_searching_for) + "\n" + getString(R.string.general)
                        + " " + getString(R.string.service));
        spannableStringBuilder2.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.i_am_searching_for).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder2.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.i_am_searching_for).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvIAmSearchingFor.setText(spannableStringBuilder2);

        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(
                getString(R.string.my_primary_concern_is) + "\n" + getString(R.string.malaria));
        spannableStringBuilder3.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.my_primary_concern_is).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder3.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.my_primary_concern_is).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvPrimaryConcern.setText(spannableStringBuilder3);


        DataHolder.selectedDate = now;
        if (now.get(Calendar.DAY_OF_WEEK) ==
                Calendar.SUNDAY) {
            now.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH) + 1);
        }
        SpannableStringBuilder spannableStringBuilder4 = new SpannableStringBuilder(
                getString(R.string.on) + "\n" + monthDateFormat.format(now.getTime()));
        spannableStringBuilder4.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.on).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder4.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.on).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvDate.setText(spannableStringBuilder4);

        SpannableStringBuilder spannableStringBuilder5 = new SpannableStringBuilder(
                getString(R.string.visit_type) + "\n" + getString(R.string.video_visit));
        spannableStringBuilder5.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.visit_type).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder5.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.visit_type).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvVisitType.setText(spannableStringBuilder5);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        now.set(Calendar.YEAR, year);
        now.set(Calendar.MONTH, monthOfYear);
        now.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        SpannableStringBuilder spannableStringBuilder4 = new SpannableStringBuilder(
                getString(R.string.on) + "\n" + monthDateFormat.format(now.getTime()));
        spannableStringBuilder4.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.on).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder4.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.on).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvDate.setText(spannableStringBuilder4);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {

                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    // Log.i(TAG, "Permission has been denied by user");

                } else {

                    //Log.i(TAG, "Permission has been granted by user");
                    if (!((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE)).
                            isProviderEnabled(LocationManager.NETWORK_PROVIDER) &&
                            !((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE)).
                                    isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        showLocationSettingsAlert(getActivity());
                    } else {
                        openDoctorSelectionFragment();
                    }
                }
                break;
            }
        }
    }

    private void openDoctorSelectionFragment() {
        Global.getLocation(pBaseBar, new LocationGetListener() {
            @Override
            public void onLocationRetrieved() {
                hideProgress();
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , DoctorSelectionFragmentCopy.newInstance(
                                new Gson().toJson(pojoAppointmentRequest), ""),
                        R.id.flAppointmentParent);
            }
        });
    }

    @Override
    public void onDateChanged() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, dpd.getSelectedDay().getDay());
        calendar.set(Calendar.MONTH, dpd.getSelectedDay().getMonth());
        calendar.set(Calendar.YEAR, dpd.getSelectedDay().getYear());
        if (color == 0)
            color = ((AppCompatButton) dpd.getView().findViewById(R.id.ok)).getCurrentTextColor();
        if (calendar.get(Calendar.DAY_OF_WEEK) ==
                Calendar.SUNDAY) {
            ((AppCompatButton) dpd.getView().findViewById(R.id.ok)).setTextColor(Color.LTGRAY);
            dpd.getView().findViewById(R.id.ok).setEnabled(false);
        } else {
            ((AppCompatButton) dpd.getView().findViewById(R.id.ok)).setTextColor(color);
            dpd.getView().findViewById(R.id.ok).setEnabled(true);
        }
    }
}

