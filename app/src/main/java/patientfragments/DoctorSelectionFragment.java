package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.DoctorsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import interfaces.DotInterface;
import utils.Constants;
import utils.DotView;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;


public class DoctorSelectionFragment extends BaseFragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvDoctors;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvHeading)
    DotView dotView;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.etSearch)
    EditText etSearch;
    private DoctorsAdapter adapter;
    private List<User> doctorsList = new ArrayList<>();
    private List<User> tempList;

    private String date;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            applyFilter(s.toString());
        }
    };

    public DoctorSelectionFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DoctorSelectionFragment newInstance(String pojoAppointmentRequest, String date) {
        DoctorSelectionFragment fragment = new DoctorSelectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, pojoAppointmentRequest);
        args.putString(ARG_PARAM2, date);

        fragment.setArguments(args);
        return fragment;
    }

    private void applyFilter(String s) {
        doctorsList.clear();
        if (s.isEmpty()) {
            doctorsList.addAll(tempList);
            adapter.notifyDataSetChanged();
            return;
        }
        for (int i = 0; i < tempList.size(); i++) {
            if (tempList.get(i).getFullName().toLowerCase().startsWith(s.toLowerCase()))
                doctorsList.add(tempList.get(i));
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.addTextChangedListener(textWatcher);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().
                    getString(ARG_PARAM1), PojoAppointmentRequest.class);
            date = getArguments().
                    getString(ARG_PARAM1);
        }
        dotView.setVisibility(View.VISIBLE);
        dotView.setText(getString(R.string.please_select_a_doctor));
        rvDoctors.setLayoutManager(new LinearLayoutManager(getActivity()));
        //dotView.post(dotView.run);
        tvNext.setVisibility(View.INVISIBLE);

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                tempList = result.getUsers();
                doctorsList.addAll(tempList);
                adapter = new DoctorsAdapter(getActivity(),doctorsList, pojoAppointmentRequest);
                rvDoctors.setAdapter(adapter);
            }

            @Override
            public void onFailure() throws Exception {

            }
        });

        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_FOLLOW_UP_APPOINTMENT_DOCTORS;
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.parameters.clinic_id = pojoAppointmentRequest.getClinic().getClinic_id();
        requestData.parameters.appointment_date = date;
        requestData.parameters.ttime = getTime();
        makeRequest(requestData);

    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        dotView.resume();
    }

    @Override
    public void stopDot() {
        dotView.stopHandler();
    }

}
