package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;


public class VaccineReminder extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.ivCalander)
    ImageView ivCalander;
    @Bind(R.id.linearParent)
    LinearLayout linearParent;
    @Bind(R.id.tvTime)
    TextView tvTime;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public VaccineReminder() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MedicationReminder.
     */
    // TODO: Rename and change types and number of parameters
    public static VaccineReminder newInstance(String param1, String param2) {
        VaccineReminder fragment = new VaccineReminder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.ivCalander})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.ivCalander:
                GeneralFunctions.addFragmentFromRight(getActivity().getFragmentManager(),
                        VaccineTrackFragment.newInstance("", ""), R.id.flAppointmentParent);
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_reminder, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String date = getDateInDayMonth(getDate());
        String d[] = date.split("\n");
        SpannableString styledString
                = new SpannableString(date);
        styledString.setSpan(new RelativeSizeSpan(0.65f), d[0].length(), styledString.length(), 0);
        styledString.setSpan(new ForegroundColorSpan(getColor(R.color.disclaimer_text_color)),
                d[0].length(), styledString.length(), 0);
        tvTime.setText(styledString);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.getCurrent_list().size() == 0) {
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(getString(R.string.no_vaccine_due));
                   // tvNoData.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
                } else {
                    for (int i = 0; i < result.getCurrent_list().size(); i++) {
                        Common vaccine = result.getCurrent_list().get(i);
                        View view = LayoutInflater.from(getActivity())
                                .inflate(R.layout.row_single_textview, null, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        viewHolder.tvVaccineName.setText(vaccine.getName());
                        linearParent.addView(view);
                    }
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
        getVaccineForToday();
    }

    private void getVaccineForToday() {
        RequestData request = new RequestData();
        request.request = RequestName.GET_TODAY_VACCINE;
        request.type = Constants.EMR;
        request.parameters.pat_id = loggedInUser.user_id;
        makeRequest(request);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    class ViewHolder {
        @Bind(R.id.tvOptionName)
        TextView tvVaccineName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
