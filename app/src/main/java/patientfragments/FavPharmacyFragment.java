package patientfragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import interfaces.LocationGetListener;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import utils.Global;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;

public class FavPharmacyFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_LOCATION = 116;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvAddYourClinicsHere)
    TextView tvAddYourPharmaciyHere;
    @Bind(R.id.lvClinicList)
    SwipeMenuListView lvClinicList;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvAddClinic)
    TextView tvAddClinic;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public FavPharmacyFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FavPharmacyFragment newInstance(String param1, String param2) {
        FavPharmacyFragment fragment = new FavPharmacyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fav_clinic, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        pBaseBar = pBar;
        tvNext.setVisibility(View.GONE);
        tvAddClinic.setText(getString(R.string.add_new_pharmacy));
        tvAddYourPharmaciyHere.setText(getString(R.string.add_your_pharmacies_here));
        tvTitle.setText(getString(R.string.my_pharmacies));
        if (favPharmacy != null) {
            setData();
        } else tvAddYourPharmaciyHere.setVisibility(View.VISIBLE);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                userPrefsManager.saveFavPharmacy(null);
                lvClinicList.setVisibility(View.GONE);
                tvAddClinic.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {
                lvClinicList.setVisibility(View.GONE);
                tvAddYourPharmaciyHere.setVisibility(View.VISIBLE);
            }
        });

    }

    private void setData() {
        tvAddYourPharmaciyHere.setVisibility(View.GONE);
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getActivity().getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.RED));
                // set item width
                openItem.setWidth(dpToPx(120));
                // set item title
                openItem.setTitle("Delete");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
            }
        };

// set creator
        lvClinicList.setMenuCreator(creator);
        lvClinicList.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        RequestData requestData = new RequestData();
                        requestData.request = RequestName.REMOVE_FAV_PHARMACY;
                        requestData.type = Constants.TYPE_PATIENT;
                        requestData.parameters.user_id = loggedInUser.user_id;
                        requestData.parameters.pharm_id = favPharmacy.getId();
                        makeRequest(requestData);
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
        lvClinicList.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        ArrayList<String> itemList = new ArrayList<>();
        itemList.add(favPharmacy.getName());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.row_single_textview, R.id.tvOptionName, itemList);
        lvClinicList.setAdapter(adapter);
    }

    public void onEvent(CommonEvent e) {
        switch (e.EVENT_TYPE) {
            case CommonEvent.FAV_PHARMACY_ADDED:
                favPharmacy = userPrefsManager.getFavPharmacy();
                tvAddYourPharmaciyHere.setVisibility(View.GONE);
                lvClinicList.setVisibility(View.VISIBLE);
                setData();
                break;
        }
    }

    @OnClick({R.id.tvBack, R.id.tvAddClinic})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvAddClinic:
                boolean permission = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
                if (Build.VERSION.SDK_INT >= 23 && !permission) {
                    FragmentCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            REQUEST_LOCATION);
                } else {
                    addPharmacy();
                }
                break;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {

                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    // Log.i(TAG, "Permission has been denied by user");

                } else {

                    //Log.i(TAG, "Permission has been granted by user");
                    addPharmacy();
                }
                break;
            }
        }
    }

    private void addPharmacy() {
        if (!((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE)).
                isProviderEnabled(LocationManager.NETWORK_PROVIDER) &&
                !((LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE)).
                        isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showLocationSettingsAlert(getActivity());
        } else {
            Global.getLocation(pBar, new LocationGetListener() {
                @Override
                public void onLocationRetrieved() {
                    GeneralFunctions.addFragmentFromRight(getFragmentManager()
                            , new AddPharmacyFrament(), R.id.flAppointmentParent);
                }
            });
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
