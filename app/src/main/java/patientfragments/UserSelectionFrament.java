package patientfragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import preferences.UserPrefsManager;
import utils.Constants;
import utils.GeneralFunctions;

public class UserSelectionFrament extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvPatient)
    TextView tvPatient;
    @Bind(R.id.tvDoctor)
    TextView tvDoctor;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public UserSelectionFrament() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserSelectionFrament.
     */
    // TODO: Rename and change types and number of parameters
    public static UserSelectionFrament newInstance(String param1, String param2) {
        UserSelectionFrament fragment = new UserSelectionFrament();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_selection_frament, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.tvPatient, R.id.tvDoctor})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvPatient:
                new UserPrefsManager(getActivity()).saveUserType(Constants.TYPE_PATIENT);
                GeneralFunctions.replaceFragFromRight(getFragmentManager()
                        , new TutorialParentFragment(), R.id.flFragmentContainer);
                break;
            case R.id.tvDoctor:
                new UserPrefsManager(getActivity()).saveUserType(Constants.TYPE_DOCTOR);
                GeneralFunctions.replaceFragFromRight(getFragmentManager()
                        , new TutorialParentFragment(), R.id.flFragmentContainer);
                break;
            default:
                break;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
