package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;


public class AppointmentReminder extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTime)
    TextView tvTime;
    @Bind(R.id.linearParent)
    LinearLayout linearParent;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public AppointmentReminder() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MedicationReminder.
     */
    // TODO: Rename and change types and number of parameters
    public static AppointmentReminder newInstance(String param1, String param2) {
        AppointmentReminder fragment = new AppointmentReminder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.ivCalander})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.ivCalander:
                GeneralFunctions.addFragmentFromRight(getActivity().getFragmentManager(),
                        AppointmentTrackFragment.newInstance("", ""), R.id.flAppointmentParent);
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_reminder, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        String date = getDateInDayMonth(getDate());
        String d[] = date.split("\n");
        SpannableString styledString
                = new SpannableString(date);
        styledString.setSpan(new RelativeSizeSpan(0.65f), d[0].length(), styledString.length(), 0);
        styledString.setSpan(new ForegroundColorSpan(getColor(R.color.disclaimer_text_color)),
                d[0].length(), styledString.length(), 0);
        tvTime.setText(styledString);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.getToday().size() == 0) {
                    tvNoData.setVisibility(View.VISIBLE);
                    tvNoData.setText(getString(R.string.no_appointment_today));
                    //tvNoData.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
                } else {
                    tvNoData.setVisibility(View.GONE);
                    for (int i = 0; i < result.getToday().size(); i++) {
                        Appointment appointment = result.getToday().get(i);
                        PojoAppointmentRequest pojoAppointmentRequest = new Gson().fromJson(
                                appointment.getApointment_data(), PojoAppointmentRequest.class);
                        View view = LayoutInflater.from(getActivity())
                                .inflate(R.layout.row_today_appointment, null, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        viewHolder.tvTime.setText(appointment.getTtime());
                        viewHolder.tvClinic.setText(appointment.getClinic().getClinic_name());
                        viewHolder.tvDoctor.setText(pojoAppointmentRequest.getDoctor().getFullName());
                        viewHolder.tvVisitType.setText("Type : " + (Integer.parseInt(appointment.getVisit_type()) ==
                                Constants.TYPE_VIDEO_VISIT ? getString(R.string.video_visit) : getString(R.string.in_person_visit)));
                        linearParent.addView(view);
                    }
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
        getAppointmentsForToday();
    }

    public void onEvent(CommonEvent ce) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                linearParent.removeAllViews();
                getAppointmentsForToday();
                break;
        }

    }

    private void getAppointmentsForToday() {
        RequestData request = new RequestData();
        request.request = RequestName.GET_TODAY_APPOINTMENTS;
        request.type = Constants.TYPE_PATIENT;
        request.parameters.pat_id = loggedInUser.user_id;
        makeRequest(request);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

    class ViewHolder {
        @Bind(R.id.tvTime)
        TextView tvTime;
        @Bind(R.id.tvClinic)
        TextView tvClinic;
        @Bind(R.id.tvDoctor)
        TextView tvDoctor;
        @Bind(R.id.tvVisitType)
        TextView tvVisitType;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
