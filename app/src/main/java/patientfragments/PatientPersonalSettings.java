package patientfragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.DoctaHomeActivity;
import com.sanguinebits.docta.MainActivity;
import com.sanguinebits.docta.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Constants;
import utils.WebConstants;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.CommonPojo;
import webservices.pojos.User;

public class PatientPersonalSettings extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.etFirstName)
    EditText etFirstName;
    @Bind(R.id.etLastName)
    EditText etLastName;
    @Bind(R.id.tvDob)
    TextView tvDob;
    @Bind(R.id.rbMale)
    RadioButton rbMale;
    @Bind(R.id.rbFemale)
    RadioButton rbFemale;
    @Bind(R.id.rgGender)
    RadioGroup rgGender;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.etPhone)
    EditText etPhone;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    private String date = "2016-01-01";

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

// TODO: Rename and change types of parameters


    public PatientPersonalSettings() {
        // Required empty public constructor
    }

    public static PatientPersonalSettings newInstance(User user) {
        PatientPersonalSettings fragment = new PatientPersonalSettings();
        Bundle extras = new Bundle();
        extras.putString(ARG_PARAM1, new Gson().toJson(user));
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick({R.id.tvSubmit, R.id.tvBack, R.id.tvDob})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvDob:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMaxDate(now);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.tvSubmit:
                etFirstName.clearFocus();
                etLastName.clearFocus();
                hideKeyBoard(etPhone);
                String sex = R.id.rbMale == rgGender.getCheckedRadioButtonId() ?
                        Constants.MALE : Constants.FEMALE;
                loggedInUser.fname = etFirstName.getText().toString();
                loggedInUser.lname = etLastName.getText().toString();
                loggedInUser.phone = etPhone.getText().toString();
                loggedInUser.dob = tvDob.getText().toString();
                loggedInUser.sex = sex;
                updateUser();
                break;
            case R.id.tvBack:
                getFragmentManager().popBackStackImmediate();
                break;
        }
    }


    private void updateUser() {
        showProgress();
        String type, request;
        if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
            type = Constants.PATIENT_UPDATE;
            request = RequestName.UPDATE_PROFILE;
        } else {
            type = RequestName.UPDATE_DOCTOR_PROFILE;
            request = RequestName.UPDATE_DOCTOR_PROFILE;
        }
        String imageUrl = "";


        RequestBody typeBody = RequestBody.create(MediaType.parse("text/plain"), type);
        Call<CommonPojo> call = RestClient.get().update(null, typeBody,
                request, loggedInUser.password, WebConstants.ANDROID, loggedInUser.fname,
                loggedInUser.lname, loggedInUser.user_id, loggedInUser.phone.replace("+233 ", ""),
                loggedInUser.age, loggedInUser.sex,
                userPrefsManager.getRegId(), loggedInUser.dob, loggedInUser.passcode, null, imageUrl);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    CommonPojo pojo = response.body();
                    showToast(pojo.getMsg());
                    if (pojo.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                        userPrefsManager.saveLoginUser(loggedInUser);
                        if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT))
                            startActivity(new Intent(getActivity(), MainActivity.class));
                        else
                            startActivity(new Intent(getActivity(), DoctaHomeActivity.class));
                        getActivity().finish();
                    }
                } else {
                    showToast(R.string.there_might_be_some_problem);
                }
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    hideProgress();
                    handleException(t);
                } catch (Exception e) {

                }
            }

        });
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.personal_settings));
        etFirstName.setText(loggedInUser.fname);
        etLastName.setText(loggedInUser.lname);
        etPhone.setText(loggedInUser.phone);
        tvDob.setText(loggedInUser.dob);
        if (loggedInUser.sex.equals(Constants.MALE))
            rbMale.setChecked(true);
        else rbFemale.setChecked(true);

        etPhone.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains(Constants.DEFAULT_COUNTRY_CODE)) {
                    etPhone.setText(Constants.DEFAULT_COUNTRY_CODE);
                    Selection.setSelection(etPhone.getText(), etPhone.getText().length());
                }

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_personal_settings, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void displayDate() {
        date = year + "-" + (month + 1) + "-" + day;
        tvDob.setText(date);
    }

// TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int y, int m, int d) {
        year = y;
        month = m;
        day = d;
        displayDate();
    }
}
