package patientfragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import utils.GeneralFunctions;
import utils.GetSampledImage;
import webservices.pojos.User;

public class SignUpStep2Fragment extends BaseFragment implements GetSampledImage.SampledImageAsyncResp,
        DatePickerDialog.OnDateSetListener {
    private static final String ARG_PARAM1 = "param1";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final int REQUEST_WRITE_STORAGE = 112;
    @Bind(R.id.profilePic)
    SimpleDraweeView profilePic;
    @Bind(R.id.etFirstName)
    EditText etFirstName;
    @Bind(R.id.etLastName)
    EditText etLastName;
    @Bind(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @Bind(R.id.tvDob)
    TextView tvDob;
    @Bind(R.id.etAge)
    EditText etAge;
    @Bind(R.id.rgGender)
    RadioGroup rgGender;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    private String picturePath;
    private File imageFile;
    private ProgressDialog mProgressDialog;
    private User user;
    private String date = "2016-01-01";

    // TODO: Rename and change types of parameters


    /*@SuppressLint("SimpleDateFormat")
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @SuppressLint("SimpleDateFormat")
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            displayDate();
        }
    };*/

    public SignUpStep2Fragment() {
        // Required empty public constructor
    }

    public static SignUpStep2Fragment newInstance(User user) {
        SignUpStep2Fragment fragment = new SignUpStep2Fragment();
        Bundle extras = new Bundle();
        extras.putString(ARG_PARAM1, new Gson().toJson(user));
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick({R.id.tvNext, R.id.tvBack, R.id.profilePic, R.id.tvDob})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvDob:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMaxDate(now);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.profilePic:
                if (imageFile != null && imageFile.exists()) {
                    imageFile.delete();
                }
                boolean permission = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                if (Build.VERSION.SDK_INT >= 23 && !permission) {
                    //if (permission != PackageManager.PERMISSION_GRANTED) {
//                            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    FragmentCompat.requestPermissions(SignUpStep2Fragment.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);

                } else {
                    showPicOptionDialog();
                }
                break;
            case R.id.tvNext:
                if (TextUtils.isEmpty(etFirstName.getText()))
                    showToast(R.string.first_name_empty);
                else if (TextUtils.isEmpty(etLastName.getText()))
                    showToast(R.string.last_name_empty);
                else if (TextUtils.isEmpty(etPhoneNumber.getText().toString().
                        substring(Constants.DEFAULT_COUNTRY_CODE.length(), etPhoneNumber.getText().length())))
                    showToast(R.string.phone_number_empty);
                else if (TextUtils.isEmpty(tvDob.getText()))
                    showToast(R.string.dob_empty);
                else if (R.id.rbMale != rgGender.getCheckedRadioButtonId() &&
                        R.id.rbFemale != rgGender.getCheckedRadioButtonId())
                    showToast(R.string.empty_gender);
                else {
                    etFirstName.clearFocus();
                    etLastName.clearFocus();
                    etPhoneNumber.clearFocus();
                    etAge.clearFocus();
                    hideKeyBoard(etAge);
                    String sex = R.id.rbMale == rgGender.getCheckedRadioButtonId() ?
                            Constants.MALE : Constants.FEMALE;
                    user.photo = imageFile == null ? "" : imageFile.getAbsolutePath();
                    user.fname = etFirstName.getText().toString();
                    user.lname = etLastName.getText().toString();
                    user.phone = etPhoneNumber.getText().toString().
                            substring(Constants.DEFAULT_COUNTRY_CODE.length(), etPhoneNumber.getText().length());
                    user.dob = tvDob.getText().toString();
                    user.age = etAge.getText().toString();
                    user.gender = sex;
                    GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                            SignUpStep3Fragment.newInstance(user), R.id.flFragmentContainer);
                }
                break;
            case R.id.tvBack:
                getFragmentManager().popBackStackImmediate();
                break;
        }
    }

    private void showPicOptionDialog() {
        new MaterialDialog.Builder(getActivity())
                .theme(Theme.LIGHT)
                .items(R.array.media_options)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                Intent i = new Intent(
                                        Intent.ACTION_PICK,
                                        MediaStore.Images.Media
                                                .EXTERNAL_CONTENT_URI);
                                startActivityForResult(i,
                                        Constants.GALLERY_REQUEST);
                                break;
                            case 1:
                                Intent takePictureIntent = new Intent(
                                        MediaStore.ACTION_IMAGE_CAPTURE);
                                File f = null;
                                try {
                                    f = GeneralFunctions.setUpImageFile(Constants
                                            .LOCAL_STORAGE_BASE_PATH_FOR_USER_PHOTOS);
                                    picturePath = f.getAbsolutePath();
                                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                            Uri.fromFile(f));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    f = null;
                                    picturePath = null;
                                }
                                startActivityForResult(takePictureIntent,
                                        Constants.CAMERA_REQUEST);
                                break;
                            case 2:
                                dialog.dismiss();
                                break;
                            default:
                                break;
                        }
                    }
                })
                .show();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        etPhoneNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains(Constants.DEFAULT_COUNTRY_CODE)) {
                    etPhoneNumber.setText(Constants.DEFAULT_COUNTRY_CODE);
                    Selection.setSelection(etPhoneNumber.getText(), etPhoneNumber.getText().length());
                }

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up_step2, container, false);
        ButterKnife.bind(this, view);
        user = new Gson().fromJson(getArguments().getString(ARG_PARAM1), User.class);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                boolean isGalleryImage = false;
                if (requestCode == Constants.GALLERY_REQUEST) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    isGalleryImage = true;
                }
                showProgressDialog();
                new GetSampledImage(this).execute(picturePath,
                        Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_PHOTOS,
                        String.valueOf(isGalleryImage),
                        String.valueOf((int) getResources().getDimension(R.dimen.h_256)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayDate() {
        date = year + "-" + (month + 1) + "-" + day;
        tvDob.setText(date);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
        }
        mProgressDialog.setMessage(getString(R.string.processing_image));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {

                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    // Log.i(TAG, "Permission has been denied by user");

                } else {

                    //Log.i(TAG, "Permission has been granted by user");
                    showPicOptionDialog();
                }
                return;
            }
        }
    }

    @Override
    public void onSampledImageAsyncPostExecute(File file) {
        imageFile = file;
        if (imageFile != null) {
            profilePic.setImageURI(Uri.parse(Constants.LOCAL_FILE_PREFIX +
                    imageFile));
        }
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int y, int m, int d) {
        year = y;
        month = m;
        day = d;
        displayDate();
    }
}
