package patientfragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sanguinebits.docta.MainActivity;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AppointmentTutorialFragment extends BaseFragment implements Animation.AnimationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTut1)
    TextView tvTut1;
    @Bind(R.id.tvTut2)
    TextView tvTut2;
    @Bind(R.id.tvTut3)
    TextView tvTut3;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.iv1)
    ImageView iv1;
    @Bind(R.id.iv2)
    ImageView iv2;
    @Bind(R.id.iv3)
    ImageView iv3;
    @Bind(R.id.iv4)
    ImageView iv4;
    @Bind(R.id.iv5)
    ImageView iv5;
    @Bind(R.id.iv6)
    ImageView iv6;
    @Bind(R.id.animParent1)
    LinearLayout animParent1;
    @Bind(R.id.animParent2)
    LinearLayout animParent2;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private int count = 1;

    public AppointmentTutorialFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AppointmentTutorialFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AppointmentTutorialFragment newInstance(String param1, String param2) {
        AppointmentTutorialFragment fragment = new AppointmentTutorialFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_tutorial, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        tvTut1.setText("Hi " + loggedInUser.fname + ", " + getString(R.string.tut1));
        tvTut1.setVisibility(View.VISIBLE);
        Animation anim = AnimationUtils.loadAnimation(getActivity(),
                R.anim.fade_in);
        anim.setAnimationListener(this);
        tvTut1.startAnimation(anim);
        SpannableString ss = new SpannableString(tvTut3.getText() + " a");
        Drawable d = getDrawable(R.drawable.ic_appointment_float);
        d.setBounds(0, 0, 30, 30);
        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
        ss.setSpan(span, ss.length() - 1, ss.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvTut3.setText(ss);
    }


    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        try {
            switch (count) {
                case 1:
                    count = 2;
                    Animation zoom_anim = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.zoom_in);
                    zoom_anim.setAnimationListener(this);
                    iv1.setVisibility(View.VISIBLE);
                    iv1.startAnimation(zoom_anim);
                    break;
                case 2:
                    count = 3;
                    Animation zoom_anim1 = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.zoom_in);
                    zoom_anim1.setAnimationListener(this);
                    iv2.setVisibility(View.VISIBLE);
                    iv2.startAnimation(zoom_anim1);
                    break;
                case 3:
                    count = 4;
                    Animation zoom_anim2 = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.zoom_in);
                    zoom_anim2.setAnimationListener(this);
                    iv3.setVisibility(View.VISIBLE);
                    iv3.startAnimation(zoom_anim2);
                    break;
                case 4:
                    count = 5;
                    Animation anim = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.fade_in);
                    anim.setAnimationListener(this);


                    animParent1.setVisibility(View.INVISIBLE);
                    tvTut2.startAnimation(anim);
                    break;
                case 5:
                    count = 6;
                    Animation zoom_anim3 = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.zoom_in);
                    zoom_anim3.setAnimationListener(this);
                    iv4.setVisibility(View.VISIBLE);
                    iv4.startAnimation(zoom_anim3);
                    break;
                case 6:
                    count = 7;
                    Animation zoom_anim4 = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.zoom_in);
                    zoom_anim4.setAnimationListener(this);
                    iv4.setVisibility(View.VISIBLE);
                    iv5.startAnimation(zoom_anim4);
                    break;
                case 7:
                    count = 8;
                    Animation zoom_anim5 = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.zoom_in);
                    zoom_anim5.setAnimationListener(this);
                    iv6.setVisibility(View.VISIBLE);
                    iv6.startAnimation(zoom_anim5);
                    break;
                case 8:
                    count = 9;
                    Animation anim_ = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.fade_in);
                    anim_.setAnimationListener(this);
                    animParent2.setVisibility(View.INVISIBLE);
                    tvTut3.startAnimation(anim_);
                    break;
                case 9:
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                getFragmentManager().popBackStack();
                                ((MainActivity) getActivity()).showAppointment();
                            } catch (Exception e) {

                            }
                        }
                    }, 2000);
                    break;

            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
