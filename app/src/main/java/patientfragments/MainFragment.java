package patientfragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import interfaces.DotInterface;
import utils.Constants;
import utils.DotView;
import webservices.pojos.PojoAppointmentRequest;


public class MainFragment extends BaseFragment implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvGreeting)
    DotView tvGreeting;
    @Bind(R.id.tvNext)
    TextView tvNext;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private PojoAppointmentRequest pojoAppointmentRequest;


    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment MyHealthFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(
                    ARG_PARAM1), PojoAppointmentRequest.class);
            pojoAppointmentRequest.setPatient(loggedInUser);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        tvNext.setVisibility(View.GONE);
        showGreeting();
    }

    private void showGreeting() {
        Calendar rightNow = Calendar.getInstance();
        //if (userPrefsManager.showGreeting()) {
        int hour = rightNow.get(Calendar.HOUR);
        String greeting = "";
        if (getTime().toUpperCase().contains(Constants.AM))        // gets hour in 12h format
            greeting = getString(R.string.greeting_good_morning);
        else if (getTime().toUpperCase().contains(Constants.PM) && hour > 3)
            greeting = getString(R.string.greeting_good_evening);
        else
            greeting = getString(R.string.greeting_good_afternoon);
        tvGreeting.setText(greeting + " " + loggedInUser.fname + ", " +
                getString(R.string.request_a_doctor_now));
        tvGreeting.post(tvGreeting.run);
//        } else {
//            tvGreeting.post(tvGreeting.run);
//        }
    }

    @OnClick({R.id.tvBack, R.id.tvGeneral, R.id.tvSkinCare, R.id.tvChildCare, R.id.tvPregnancyAndNewBorn})
    public void click(View view) {
        PojoAppointmentRequest pojoAppointmentRequest = new PojoAppointmentRequest();
        switch (view.getId()) {
            case R.id.tvBack:
                tvGreeting.stopHandler();
                getActivity().onBackPressed();
                break;
            case R.id.tvGeneral:
                pojoAppointmentRequest.setServiceId(Constants.GENERAL);
                pojoAppointmentRequest.setServiceName(getString(R.string.general));
                EventBus.getDefault().post(pojoAppointmentRequest);
                break;
            case R.id.tvSkinCare:
                pojoAppointmentRequest.setServiceId(Constants.SKIN_CARE);
                pojoAppointmentRequest.setServiceName(getString(R.string.skin_care));
                EventBus.getDefault().post(pojoAppointmentRequest);
                break;
            case R.id.tvChildCare:
                pojoAppointmentRequest.setServiceId(Constants.CHILD_CARE);
                pojoAppointmentRequest.setServiceName(getString(R.string.child_care));
                EventBus.getDefault().post(pojoAppointmentRequest);
                break;
            case R.id.tvPregnancyAndNewBorn:
                pojoAppointmentRequest.setServiceId(Constants.PREGNANCY_NEW_BORN);
                pojoAppointmentRequest.setServiceName(getString(R.string.pregnancy_n_new_boen));
                EventBus.getDefault().post(pojoAppointmentRequest);
                break;
        }
        if (view.getId() != R.id.tvBack) {
            tvGreeting.stopHandler();
            getActivity().onBackPressed();
        }
//        if (loggedInUser.child_count.equals("0")) {
//            pojoAppointmentRequest.setPatient(loggedInUser);
//            GeneralFunctions.addFragmentFromRight(getFragmentManager()
//                    , DisclaimerFragment.newInstance(new Gson().toJson(
//                            pojoAppointmentRequest)), R.id.flAppointmentParent);
//        } else {
//            GeneralFunctions.addFragmentFromRight(getFragmentManager()
//                    , PatientSelectionFragment.newInstance(new Gson().toJson(
//                            pojoAppointmentRequest)), R.id.flAppointmentParent);
//        }
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showDot() {
        tvGreeting.resume();
    }

    @Override
    public void stopDot() {
        tvGreeting.stopHandler();
    }
}
