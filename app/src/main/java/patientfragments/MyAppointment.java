package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.TabsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class MyAppointment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvNoData)
    TextView tvNoData;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public MyAppointment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MyAppointment newInstance(String param1, String param2) {
        MyAppointment fragment = new MyAppointment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pBaseBar = pBar;
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.my_appoinrments));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                tvNoData.setVisibility(View.GONE);
                String jsonResponse = new Gson().toJson(result);
                TabsAdapter tabsAdapter = new TabsAdapter(getChildFragmentManager());
                tabsAdapter.addFragment(UpcomingAppointmentFragment.newInstance(jsonResponse),
                        getString(R.string.upcoming));
                tabsAdapter.addFragment(CancelledAppointmentFragment.newInstance(jsonResponse),
                        getString(R.string.cancelled));
                pager.setAdapter(tabsAdapter);
                tabLayout.setupWithViewPager(pager);
            }

            @Override
            public void onFailure() {
                tvNoData.setVisibility(View.VISIBLE);
            }
        });
        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_PATIENT;
        requestData.request = RequestName.GET_APPOINTMENTS;
        requestData.parameters.pat_id = loggedInUser.user_id;
        makeRequest(requestData);
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.commonpager, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
