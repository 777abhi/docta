package patientfragments;


import android.Manifest;
import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.mpowerpayments.mpower.MPowerCheckoutInvoice;
import com.mpowerpayments.mpower.MPowerCheckoutStore;
import com.mpowerpayments.mpower.MPowerSetup;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;


public class VideoRequestFragment extends BaseFragment implements View.OnClickListener {


    private static final String ARG_PARAM1 = "param1";
    private static final int REQUEST_CAMERA_PERMISSION = 111;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvWhoIsPatient)
    TextView tvWhoIsPatient;
    @Bind(R.id.tvIAmSearchingFor)
    TextView tvIAmSearchingFor;
    @Bind(R.id.tvPrimaryConcern)
    TextView tvPrimaryConcern;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    MPowerSetup setup = new MPowerSetup();
    private EditText etCouponCode;
    private TextView tvCancel;
    private TextView tvCheckOut;
    private TextView tvApplyCoupon;
    private TextView tvAmountToPay;
    private MaterialDialog dialog;
    private int amountToPay = 40;
    private PojoAppointmentRequest pojoAppointmentRequest;

    public VideoRequestFragment() {
        // Required empty public constructor
    }

    public static VideoRequestFragment newInstance(String param1) {
        VideoRequestFragment fragment = new VideoRequestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video_request, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }


    @OnClick({R.id.tvWhoIsPatient, R.id.tvBack, R.id.tvIAmSearchingFor,
            R.id.tvPrimaryConcern, R.id.tvRequestNow})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvWhoIsPatient:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , PatientSelectionFragment.newInstance(new Gson().toJson(
                                pojoAppointmentRequest)), R.id.flAppointmentParent);
                break;
            case R.id.tvIAmSearchingFor:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , MainFragment.newInstance(new Gson().toJson(pojoAppointmentRequest)),
                        R.id.flAppointmentParent);
                break;
            case R.id.tvPrimaryConcern:
                if (pojoAppointmentRequest.getServiceId().equals(Constants.GENERAL) ||
                        pojoAppointmentRequest.getServiceId().equals(Constants.CHILD_CARE)) {
                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), PrimaryHealthConcernFragment.
                                    newInstance(new Gson().toJson(pojoAppointmentRequest)),
                            R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
                } else if (pojoAppointmentRequest.getServiceId().equals(Constants.SKIN_CARE)) {
                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), SkinCarePrimaryConcernFragment.
                                    newInstance(new Gson().toJson(pojoAppointmentRequest)),
                            R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
                } else {
                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), PatientQuestionStep1Fragment.
                                    newInstance(new Gson().toJson(pojoAppointmentRequest),
                                            getString(R.string.please_describe_your_primary_health_concern)),
                            R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
                }
                break;
            case R.id.tvRequestNow:
                boolean permission = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                if (Build.VERSION.SDK_INT >= 23 && !permission) {
                    FragmentCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION);

                } else {
                    openPaymentDialog();
                }

                break;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openPaymentDialog();
                }
                break;
        }
    }

    private void openPaymentDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                .customView(R.layout.checkout_dialog_view, true);
        dialog = builder.build();
        dialog.show();
        View dialogView = dialog.getView();
        etCouponCode = (EditText) dialogView.findViewById(R.id.etCouponCode);
        tvCancel = (TextView) dialogView.findViewById(R.id.tvCancel);
        tvCheckOut = (TextView) dialogView.findViewById(R.id.tvCheckOut);
        tvApplyCoupon = (TextView) dialogView.findViewById(R.id.tvApplyCoupon);
        tvAmountToPay = (TextView) dialogView.findViewById(R.id.tvAmountToPay);

        tvCancel.setOnClickListener(this);
        tvCheckOut.setOnClickListener(this);
        tvApplyCoupon.setOnClickListener(this);
    }

    public void onEvent(User selectedUser) {
        pojoAppointmentRequest.setPatient(selectedUser);

        SpannableStringBuilder spannableStringBuilder = null;
        if (selectedUser.user_id.equals(loggedInUser.user_id)) {
            spannableStringBuilder = new SpannableStringBuilder(
                    getString(R.string.who_is_the_patient) + "\n" + getString(R.string.me));
        } else {
            spannableStringBuilder = new SpannableStringBuilder(
                    getString(R.string.who_is_the_patient) + "\n" + selectedUser.getFullName());
        }
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.who_is_the_patient).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.who_is_the_patient).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvWhoIsPatient.setText(spannableStringBuilder);
    }

    public void onEvent(PojoAppointmentRequest pojoAppointmentRequest) {
        this.pojoAppointmentRequest = pojoAppointmentRequest;

        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(
                getString(R.string.i_am_searching_for) + "\n" +
                        pojoAppointmentRequest.getServiceName() + " " + getString(R.string.service));
        spannableStringBuilder2.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.i_am_searching_for).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder2.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.i_am_searching_for).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvIAmSearchingFor.setText(spannableStringBuilder2);

        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(
                getString(R.string.my_primary_concern_is) + "\n" +
                        pojoAppointmentRequest.getPrimaryHealthConcern());
        spannableStringBuilder3.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.my_primary_concern_is).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder3.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.my_primary_concern_is).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvPrimaryConcern.setText(spannableStringBuilder3);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(
                    ARG_PARAM1), PojoAppointmentRequest.class);
            pojoAppointmentRequest.setPatient(loggedInUser);
            pojoAppointmentRequest.setServiceId(Constants.GENERAL);
            pojoAppointmentRequest.setServiceName(getString(R.string.general));
            pojoAppointmentRequest.primaryHealthConcern =
                    getString(R.string.malaria);
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(
                getString(R.string.who_is_the_patient) + "\n" + getString(R.string.me));
        spannableStringBuilder.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.who_is_the_patient).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.who_is_the_patient).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvWhoIsPatient.setText(spannableStringBuilder);

        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(
                getString(R.string.i_am_searching_for) + "\n" + getString(R.string.general)
                        + " " + getString(R.string.service));
        spannableStringBuilder2.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.i_am_searching_for).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder2.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.i_am_searching_for).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvIAmSearchingFor.setText(spannableStringBuilder2);

        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(
                getString(R.string.my_primary_concern_is) + "\n" + getString(R.string.malaria));
        spannableStringBuilder3.setSpan(new RelativeSizeSpan(0.9f), 0,
                getString(R.string.my_primary_concern_is).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableStringBuilder3.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
                getString(R.string.my_primary_concern_is).length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvPrimaryConcern.setText(spannableStringBuilder3);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvCheckOut:
                if (amountToPay == 0) {
                    dialog.dismiss();
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(),
                            LoaderFragment.newInstance(new Gson().toJson(
                                    pojoAppointmentRequest)), R.id.flAppointmentParent,
                            Constants.LOADER_FRAGMENT);

                } else {
                    dialog.dismiss();
                    showProgress();
                    setup.setMasterKey(Constants.MASTER_KEY);
                    setup.setPrivateKey(Constants.PRIVATE_KEY);
                    setup.setPublicKey(Constants.PUBLIC_KEY);
                    setup.setToken(Constants.TOKEN);
                    setup.setMode("live");
                    new PaymentAsynTask().execute("");
                }
                break;
            case R.id.tvCancel:
                dialog.dismiss();
                break;
            case R.id.tvApplyCoupon:
                if (TextUtils.isEmpty(etCouponCode.getText()))
                    return;
                if (etCouponCode.getText().toString().equals("HelloDOCTA")) {
                    amountToPay = 0;
                    tvAmountToPay.setText("Amount to pay : 0 Ghc");
                } else {
                    showToast("Invalid Coupon");
                    tvAmountToPay.setText("Amount to pay : 40 Ghc");
                }
                break;
        }
    }

    private class PaymentAsynTask extends AsyncTask<String, String, Boolean> {
        String invoiceUrl;

        protected Boolean doInBackground(String... urls) {
            MPowerCheckoutStore store = new MPowerCheckoutStore();
            store.setName("Docta Group");
            store.setWebsiteUrl("http://doctagroup.com/");
            MPowerCheckoutInvoice invoice = new MPowerCheckoutInvoice(setup, store);
            invoice.addItem("Video fees", 1, 40, 40);
            invoice.setReturnUrl("/com.codesnickers.docta");
            invoice.setDescription("Video consultation with doctor");
            invoice.setTotalAmount(40);
            boolean a = invoice.create();
            invoiceUrl = a ? invoice.getInvoiceUrl() : "";
            return a;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            hideProgress();
            if (s) {
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(invoiceUrl));
//                startActivity(i);
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        PaymentFragment.newInstance(invoiceUrl,
                                new Gson().toJson(pojoAppointmentRequest)), R.id.flAppointmentParent);
            } else {
                showToast("Something wrong happened");
            }
        }
    }
}
