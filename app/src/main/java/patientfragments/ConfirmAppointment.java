package patientfragments;

import android.app.FragmentManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.mpowerpayments.mpower.MPowerCheckoutInvoice;
import com.mpowerpayments.mpower.MPowerCheckoutStore;
import com.mpowerpayments.mpower.MPowerSetup;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import interfaces.DotInterface;
import utils.CommonEvent;
import utils.Constants;
import utils.DotView;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;

public class ConfirmAppointment extends BaseFragment implements DotInterface, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_CAMERA_PERMISSION = 111;
    @Bind(R.id.tvDate)
    TextView tvDate;
    @Bind(R.id.tvDay)
    TextView tvDay;
    @Bind(R.id.tvMonth)
    TextView tvMonth;
    @Bind(R.id.tvTime)
    TextView tvTime;
    @Bind(R.id.tvCost)
    TextView tvCost;
    @Bind(R.id.tvAppointmentFor) TextView tvAppointmentFor;
    @Bind(R.id.tvClinicName)
    TextView tvClinicName;
    @Bind(R.id.tvProviderName)
    TextView tvProviderName;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvConfirmationText)
    DotView tvConfirmationText;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    MPowerSetup setup = new MPowerSetup();
    private int amountToPay = 40;
    private MaterialDialog dialog;

    private EditText etCouponCode;
    private TextView tvCancel;
    private TextView tvCheckOut;
    private TextView tvApplyCoupon;
    private TextView tvAmountToPay;
    private boolean fromHIstroy = false;
    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;

    public ConfirmAppointment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ConfirmAppointment newInstance(String param1, boolean fromHistory) {
        ConfirmAppointment fragment = new ConfirmAppointment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2, fromHistory);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(
                    ARG_PARAM1), PojoAppointmentRequest.class);
            fromHIstroy = getArguments().getBoolean(ARG_PARAM2);
        }
        tvAppointmentFor.setText("Appointment For " + pojoAppointmentRequest.primaryHealthConcern);
        tvClinicName.setText(pojoAppointmentRequest.getClinic().getClinic_name());
        if (userPrefsManager.getUserType().equals(Constants.TYPE_DOCTOR)) {
            tvProviderName.setText("Dr. " + pojoAppointmentRequest.getFollowUpDoctor().getFullName());
        } else {
            tvProviderName.setText("Dr. " + pojoAppointmentRequest.getDoctor().getFullName());
        }
        if (fromHIstroy)
            tvConfirmationText.setVisibility(View.GONE);
        else {
            tvConfirmationText.setText(getString(R.string.ok) + " " +
                    loggedInUser.fname + ", " + getString(R.string.your_appointment_is_scheduled_for));
            //tvConfirmationText.post(tvConfirmationText.run);
        }
        String d = getConfirmationDateFromString(pojoAppointmentRequest.getAppointment_date());
        String[] date = d.split("\n");
        tvDate.setText(date[0]);
        tvMonth.setText(date[1]);
        tvDay.setText(date[2]);
        tvCost.setText(pojoAppointmentRequest.visitType.equals(
                getString(R.string.video_visit)) ? "Cost: 40 GHC" : "Cost: 0 GHC");
        tvTime.setText(getConfirmationDate(pojoAppointmentRequest.getAppointment_date())
                + "\n" + pojoAppointmentRequest.getTime_slot() + " - " +
                getNextTimeSlot(pojoAppointmentRequest.getTime_slot()));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                if (!fromHIstroy) {
                    userPrefsManager.setShowGreeting(true);
                    EventBus.getDefault().post(new CommonEvent(CommonEvent.SET_TAB_INDICATOR));
                    EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
//                    GeneralFunctions.addFragmentFromRight(getFragmentManager(),
//                            new MainFragment(), R.id.flFragmentContainer);
                }
            }

            @Override
            public void onFailure() {
                //tvNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm_appointment, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.tvConfirmAppointment, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvConfirmAppointment:
                if (pojoAppointmentRequest.visitType.
                        equals(getString(R.string.video_visit))) {
                    openPaymentDialog();
                } else {
                    makeAppointment();
                }
                break;
        }
    }

    private void openPaymentDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                .customView(R.layout.checkout_dialog_view, true);
        dialog = builder.build();
        dialog.show();
        View dialogView = dialog.getView();
        etCouponCode = (EditText) dialogView.findViewById(R.id.etCouponCode);
        tvCancel = (TextView) dialogView.findViewById(R.id.tvCancel);
        tvCheckOut = (TextView) dialogView.findViewById(R.id.tvCheckOut);
        tvApplyCoupon = (TextView) dialogView.findViewById(R.id.tvApplyCoupon);
        tvAmountToPay = (TextView) dialogView.findViewById(R.id.tvAmountToPay);

        tvCancel.setOnClickListener(this);
        tvCheckOut.setOnClickListener(this);
        tvApplyCoupon.setOnClickListener(this);
    }

    private void makeAppointment() {
        tvConfirmationText.stopHandler();
        RequestData requestData = new RequestData();
        requestData.request = RequestName.CONFIRM_APPOINTMENT;
        requestData.type = Constants.TYPE_PATIENT;
        if (userPrefsManager.getUserType().equals(Constants.TYPE_DOCTOR)) {
            requestData.parameters.user_id = pojoAppointmentRequest.getPatient().user_id;
            requestData.parameters.doc_id = pojoAppointmentRequest.getFollowUpDoctor().user_id;
        } else {
            requestData.parameters.user_id = loggedInUser.user_id;
            requestData.parameters.doc_id = pojoAppointmentRequest.getDoctor().user_id;
        }
        requestData.parameters.health_concern = pojoAppointmentRequest.getPrimaryHealthConcern();
        requestData.parameters.symptom_json = new Gson().toJson(pojoAppointmentRequest.sympotoms);
        requestData.parameters.ttime = pojoAppointmentRequest.getTime_slot();
        requestData.parameters.visit_type = pojoAppointmentRequest.visitType.
                equals(getString(R.string.in_person_visit)) ? Constants.TYPE_CLINIC_VISIT : Constants.TYPE_VIDEO_VISIT;
        requestData.parameters.notification_time = getTime();
        requestData.parameters.full_symptom_json = new Gson().toJson(pojoAppointmentRequest);
        requestData.parameters.clinic_id = pojoAppointmentRequest.getClinic().getClinic_id();
        requestData.parameters.appointment_date = pojoAppointmentRequest.getAppointment_date();
        makeRequest(requestData);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvCheckOut:
                if (amountToPay == 0) {
                    dialog.dismiss();
                    makeAppointment();
                } else {
                    dialog.dismiss();
                    showProgress();
                    setup.setMasterKey(Constants.MASTER_KEY);
                    setup.setPrivateKey(Constants.PRIVATE_KEY);
                    setup.setPublicKey(Constants.PUBLIC_KEY);
                    setup.setToken(Constants.TOKEN);
                    setup.setMode("live");
                    new PaymentAsynTask().execute("");
                }
                break;
            case R.id.tvCancel:
                dialog.dismiss();
                break;
            case R.id.tvApplyCoupon:
                if (TextUtils.isEmpty(etCouponCode.getText()))
                    return;
                if (etCouponCode.getText().toString().equals("HelloDOCTA")) {
                    amountToPay = 0;
                    tvAmountToPay.setText("Amount to pay : 0 Ghc");
                } else {
                    showToast("Invalid Coupon");
                    tvAmountToPay.setText("Amount to pay : 40 Ghc");
                }
                break;
        }
    }

    @Override
    public void showDot() {
        tvConfirmationText.resume();
    }

    @Override
    public void stopDot() {
        tvConfirmationText.stopHandler();
    }

    private class PaymentAsynTask extends AsyncTask<String, String, Boolean> {
        String invoiceUrl;

        protected Boolean doInBackground(String... urls) {
            MPowerCheckoutStore store = new MPowerCheckoutStore();
            store.setName("Docta Group");
            store.setWebsiteUrl("http://doctagroup.com/");
            MPowerCheckoutInvoice invoice = new MPowerCheckoutInvoice(setup, store);
            invoice.addItem("Video fees", 1, 40, 40);
            invoice.setReturnUrl("/com.codesnickers.docta");
            invoice.setDescription("Video consultation with doctor");
            invoice.setTotalAmount(40);
            boolean a = invoice.create();
            invoiceUrl = a ? invoice.getInvoiceUrl() : "";
            return a;

        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            hideProgress();
            if (s) {
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(invoiceUrl));
//                startActivity(i);
                PaymentFragment paymentFragment = PaymentFragment.newInstance(invoiceUrl,
                        new Gson().toJson(pojoAppointmentRequest));
                paymentFragment.setPaymentConfirmationListener(new PaymentFragment.PaymentConfirmationLiatener() {
                    @Override
                    public void onPayment() {
                        makeAppointment();
                    }
                });
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        paymentFragment, R.id.flAppointmentParent);
            } else {
                showToast("Something wrong happened");
            }
        }
    }
}
