package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.GeneralFunctions;
import webservices.pojos.Appointment;
import webservices.pojos.PojoAppointmentRequest;


public class AppointmentDetail extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvPatientName)
    TextView tvPatientName;
    @Bind(R.id.tvDate)
    TextView tvDate;
    @Bind(R.id.tvReason)
    TextView tvReason;
    @Bind(R.id.tvProvider)
    TextView tvProvider;
    @Bind(R.id.tvDoctor)
    TextView tvDoctor;
    @Bind(R.id.tvVisitType)
    TextView tvVisitType;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvTime)
    TextView tvTime;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Appointment pojoAppointment;
    private PojoAppointmentRequest pojoAppointmentRequest;

    public AppointmentDetail() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AppointmentDetail.
     */
    // TODO: Rename and change types and number of parameters
    public static AppointmentDetail newInstance(String param1, String param2) {
        AppointmentDetail fragment = new AppointmentDetail();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointment = new Gson().fromJson(getArguments().
                    getString(ARG_PARAM1), Appointment.class);
            pojoAppointmentRequest = new Gson().fromJson(
                    pojoAppointment.getApointment_data(), PojoAppointmentRequest.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_detail, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pBaseBar = pBar;
        tvNext.setCompoundDrawables(null, null, null, null);
        tvTitle.setText(getString(R.string.appointment_details));
        tvNext.setText(getString(R.string.done));
        tvPatientName.setText(pojoAppointment.getProfile().getFullName());
        tvProvider.setText(pojoAppointment.getClinic().getClinic_name());
        tvReason.setText(pojoAppointmentRequest.getPrimaryHealthConcern());
        tvDoctor.setText(pojoAppointment.getDoctor() == null ? "" :
                pojoAppointment.getDoctor().getFullName());
        tvDate.setText(GeneralFunctions.getFormatedDate(pojoAppointment.getDdate()));
        tvTime.setText(pojoAppointment.getTtime());
        tvVisitType.setText(pojoAppointmentRequest.visitType.equals(getString(R.string.video_visit)) ?
                "Video Visit" : "Clinic Visit");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
