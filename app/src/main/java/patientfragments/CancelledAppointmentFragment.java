package patientfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.AppointmentAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;


public class CancelledAppointmentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvAppointmentList;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    private List<Appointment> listAppointment = new ArrayList<>();
    private AppointmentAdapter appointmentAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public CancelledAppointmentFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CancelledAppointmentFragment newInstance(String param1) {
        CancelledAppointmentFragment fragment = new CancelledAppointmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            listAppointment = new Gson().fromJson(mParam1, CommonPojo.class).getCancelled();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recycler_only_layout, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        return view;
    }

    public void onEvent(Appointment pojoAppointment) {
        tvNoData.setVisibility(View.GONE);
        listAppointment.add(0, pojoAppointment);
        appointmentAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvAppointmentList.setLayoutManager(new LinearLayoutManager(getActivity()));
        appointmentAdapter = new AppointmentAdapter(getActivity(), listAppointment, false);
        rvAppointmentList.setAdapter(appointmentAdapter);
        if (listAppointment.size() == 0) //{
            tvNoData.setVisibility(View.VISIBLE);
//        } else {
//            appointmentAdapter = new AppointmentAdapter(getActivity(), listAppointment, false);
//            rvAppointmentList.setAdapter(appointmentAdapter);
//        }
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
