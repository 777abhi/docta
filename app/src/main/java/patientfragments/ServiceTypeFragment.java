package patientfragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServiceTypeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiceTypeFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvNext) TextView tvNext;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private PojoAppointmentRequest pojoAppointmentRequest;


    public ServiceTypeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ServiceTypeFragment newInstance(String param1) {
        ServiceTypeFragment fragment = new ServiceTypeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.tvAdult, R.id.tvChild, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvAdult:
                pojoAppointmentRequest.setServiceId(Constants.GENERAL);
                pojoAppointmentRequest.setServiceName(getString(R.string.general));
                getActivity().onBackPressed();
                GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), PrimaryHealthConcernFragment.
                                newInstance(new Gson().toJson(pojoAppointmentRequest)),
                        userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT) ?
                                R.id.flAppointmentParent : R.id.flRequestFragmentContainer, Constants.TAG_PRIMARY_HEALTH_CONCERN);
                break;
            case R.id.tvChild:
                pojoAppointmentRequest.setServiceId(Constants.GENERAL);
                pojoAppointmentRequest.setServiceName(getString(R.string.child_care));
                getActivity().onBackPressed();
                GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), PrimaryHealthConcernFragment.
                                newInstance(new Gson().toJson(pojoAppointmentRequest)),
                        userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT) ?
                                R.id.flAppointmentParent : R.id.flRequestFragmentContainer, Constants.TAG_PRIMARY_HEALTH_CONCERN);
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    PojoAppointmentRequest.class);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_type, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
