package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.AllergiesAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import emrfragments.AddAllergy;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;

public class Allergies extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.rvData)
    RecyclerView rvAllergies;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvPageTitle)
    TextView tvPageTitle;
    @Bind(R.id.divider)
    View divider;
    @Bind(R.id.tvNoData)
    TextView tvNoData;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public Allergies() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static Allergies newInstance(String param1, String param2) {
        Allergies fragment = new Allergies();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        if (mParam1 != null) {
            tvNext.setVisibility(View.GONE);
        }
        tvNoData.setText(getString(R.string.no_data));
        tvNext.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        tvNext.setText(getString(R.string.add));
        tvTitle.setText(getString(R.string.allergies));
        rvAllergies.setLayoutManager(new LinearLayoutManager(getActivity()));

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                if (result.getAllergy().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    rvAllergies.setAdapter(new AllergiesAdapter(getActivity(), result.getAllergy()));
                } else tvNoData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        });
        getAllergis();

    }

    private void getAllergis() {
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.GET_ALLERGIES;
        apiRequest.type = Constants.EMR;
        if (mParam1 != null) {
            apiRequest.parameters.pat_id = loggedInUser.user_id;
            apiRequest.parameters.doc_id = "0";
        } else {
            apiRequest.parameters.pat_id = getAppointment().getUser_id();
            apiRequest.parameters.doc_id = loggedInUser.user_id;
        }
        makeRequest(apiRequest);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void onEvent(CommonEvent e) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getAllergis();
                break;
        }
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new AddAllergy(), R.id.fragment_container);
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);

    }
}
