package patientfragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.Stack;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import utils.GeneralFunctions;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class ChangePasscode extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.iv1)
    ImageView iv1;
    @Bind(R.id.iv2)
    ImageView iv2;
    @Bind(R.id.iv3)
    ImageView iv3;
    @Bind(R.id.iv4)
    ImageView iv4;
    @Bind(R.id.tvCreateAPasscode)
    TextView tvCreateAPasscode;
    @Bind(R.id.tvPasscodeAddsExtraSecurity)
    TextView tvPasscodeAddsExtraSecurity;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    private Stack<String> passcode = new Stack<>();
    private String title;
    private String passCode = "";
    // TODO: Rename parameter arguments, choose names that match

    // TODO: Rename and change types and number of parameters

    public ChangePasscode() {
        // Required empty public constructor
    }

    public static ChangePasscode newInstance(String title) {
        ChangePasscode fragment = new ChangePasscode();
        Bundle extras = new Bundle();
        extras.putString(ARG_PARAM1, title);
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick({R.id.tvBack, R.id.tv0, R.id.tv1, R.id.tv2, R.id.tv3, R.id.tv4, R.id.tv5, R.id.tv6
            , R.id.tv7, R.id.tv8, R.id.tv9, R.id.tvDelete, R.id.tvReset, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.tvNext:
                if (passcode.size() < 4) {
                    showToast(R.string.passcode_should_be_of_length_4);
                    return;
                }
                changePasscode();
                break;
            case R.id.tv0:
                if (passcode.size() < 4) {
                    passcode.push("0");
                    setIndicator();
                }
                break;
            case R.id.tv1:
                if (passcode.size() < 4) {
                    passcode.push("1");
                    setIndicator();
                }
                break;
            case R.id.tv2:
                if (passcode.size() < 4) {
                    passcode.push("2");
                    setIndicator();
                }
                break;
            case R.id.tv3:
                if (passcode.size() < 4) {
                    passcode.push("3");
                    setIndicator();
                }
                break;
            case R.id.tv4:
                if (passcode.size() < 4) {
                    passcode.push("4");
                    setIndicator();
                }
                break;
            case R.id.tv5:
                if (passcode.size() < 4) {
                    passcode.push("5");
                    setIndicator();
                }
                break;
            case R.id.tv6:
                if (passcode.size() < 4) {
                    passcode.push("6");
                    setIndicator();
                }
                break;
            case R.id.tv7:
                if (passcode.size() < 4) {
                    passcode.push("7");
                    setIndicator();
                }
                break;
            case R.id.tv8:
                if (passcode.size() < 4) {
                    passcode.push("8");
                    setIndicator();
                }
                break;
            case R.id.tv9:
                if (passcode.size() < 4) {
                    passcode.push("9");
                    setIndicator();
                }
                break;
            case R.id.tvDelete:
                if (passcode.size() > 0) {
                    passcode.pop();
                    setIndicatorAfterDelete();
                }
                break;
            case R.id.tvReset:
                if (passcode.size() > 0) {
                    passcode.clear();
                    resetIndicator();
                }
                break;
            default:
                break;
        }
    }

    private void changePasscode() {
        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_PATIENT;
        requestData.request = RequestName.PAT_CHANGE_PASSCODE;
        requestData.parameters.pat_id = loggedInUser.user_id;
        requestData.parameters.passcode = passCode;
        makeRequest(requestData);
    }

    private void setIndicatorAfterDelete() {
        int size = passcode.size();
        switch (size) {
            case 0:
                iv1.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                break;
            case 1:
                iv2.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                break;
            case 2:
                iv3.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                break;
            case 3:
                iv4.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
                break;
        }
    }

    private void setIndicator() {
        int size = passcode.size();
        switch (size) {
            case 1:
                iv1.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                break;
            case 2:
                iv2.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                break;
            case 3:
                iv3.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                break;
            case 4:
                iv4.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_on));
                Stack<String> tempList = new Stack<>();
                tempList.addAll(passcode);
                while (!tempList.empty()) {
                    passCode = tempList.pop() + passCode;
                }
                if (title.equals(getString(R.string.enter_your_old_passcode))) {
                    if (!loggedInUser.passcode.equals(passCode))
                        showToast(R.string.wrong_passcode);
                    else {
                        getFragmentManager().popBackStackImmediate();
                        passcode.clear();
                        passCode = "";
                        GeneralFunctions.addFragmentFromRight(getFragmentManager()
                                , ChangePasscode.newInstance(getString(R.string.enter_your_new_passcode)), R.id.flAppointmentParent);
                    }
                }
                break;
        }
    }

    private void resetIndicator() {
        iv1.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
        iv2.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
        iv3.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
        iv4.setImageDrawable(getDrawable(R.drawable.ic_slide_oval_off));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        tvPasscodeAddsExtraSecurity.setVisibility(View.GONE);
        title = getArguments().getString(ARG_PARAM1);
        if (title.equals(getString(R.string.enter_your_old_passcode))) {
            tvNext.setVisibility(View.GONE);
        }
        tvCreateAPasscode.setText(title);
        tvNext.setText(getString(R.string.save_changes));
        pBaseBar = pBar;
        hideProgress();

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                    loggedInUser.passcode = passCode;
                    userPrefsManager.saveLoginUser(loggedInUser);
                    getFragmentManager().popBackStackImmediate();
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up_step3, container, false);
        return view;
    }
    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
