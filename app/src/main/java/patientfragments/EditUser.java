package patientfragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import utils.GetSampledImage;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.CommonPojo;
import webservices.pojos.User;

public class EditUser extends BaseFragment implements GetSampledImage.SampledImageAsyncResp {
    private static final String ARG_PARAM1 = "param1";
    private static final int REQUEST_WRITE_STORAGE = 112;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    @Bind(R.id.etFirstName)
    EditText etFirstName;
    @Bind(R.id.etLastName)
    EditText etLastName;
    @Bind(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @Bind(R.id.tvDob)
    TextView tvDob;
    @Bind(R.id.etAge)
    EditText etAge;
    @Bind(R.id.rgGender)
    RadioGroup rgGender;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.profilePic)
    SimpleDraweeView profilePic;
    @Bind(R.id.tvSave)
    TextView tvSave;
    @Bind(R.id.tvDelete)
    TextView tvDelete;
    @Bind(R.id.llEditSaveParent)
    LinearLayout llEditSaveParent;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    private String picturePath;
    private File imageFile;
    private ProgressDialog mProgressDialog;
    private int year = 1980, month = 0, day = 1;
    private String date = "1980-01-01";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private User user;
    @SuppressLint("SimpleDateFormat")
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @SuppressLint("SimpleDateFormat")
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            Calendar calendar = Calendar.getInstance();
            int curr_year = calendar.get(Calendar.YEAR);
            int curr_month = calendar.get(Calendar.MONTH);
            int curr_day = calendar.get(Calendar.DAY_OF_MONTH);
            if (selectedYear >= curr_year || selectedMonth > curr_month || selectedDay > curr_day) {
                showToast(R.string.invalid_date_of_birth);
            } else {
                year = selectedYear;
                month = selectedMonth;
                day = selectedDay;
                displayDate();
            }
        }
    };

    public EditUser() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EditUser newInstance(String param1) {
        EditUser fragment = new EditUser();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_user, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pBaseBar = pBar;
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            user = new Gson().fromJson(mParam1, User.class);
            profilePic.setImageURI(Uri.parse(user.photo));
            etFirstName.setText(user.fname);
            etLastName.setText(user.lname);
            tvDob.setText(user.dob);
            etAge.setText(user.age);
            etPhoneNumber.setText(user.phone);
            String gender = user.sex.equals(Constants.MALE) ? Constants.MALE : Constants.FEMALE;
            switch (gender) {
                case Constants.MALE:
                    rgGender.check(R.id.rbMale);
                    break;
                case Constants.FEMALE:
                    rgGender.check(R.id.rbFemale);
                    break;
            }
        }
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.edit));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                loggedInUser.child_count = result.getChild_count();
                userPrefsManager.saveLoginUser(loggedInUser);
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                boolean isGalleryImage = false;
                if (requestCode == Constants.GALLERY_REQUEST) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    isGalleryImage = true;
                }
                showProgressDialog();
                new GetSampledImage(this).execute(picturePath,
                        Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_PHOTOS,
                        String.valueOf(isGalleryImage),
                        String.valueOf((int) getResources().getDimension(R.dimen.h_256)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayDate() {
        date = year + "-" + (month + 1) + "-" + day;
        tvDob.setText(date);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
        }
        mProgressDialog.setMessage(getString(R.string.processing_image));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void onSampledImageAsyncPostExecute(File file) {
        imageFile = file;
        if (imageFile != null) {
            profilePic.setImageURI(Uri.parse(Constants.LOCAL_FILE_PREFIX +
                    imageFile));
        }
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
    // TODO: Rename method, update argument and hook method into UI event

    @OnClick({R.id.tvNext, R.id.tvBack, R.id.profilePic, R.id.tvDob, R.id.tvSave, R.id.tvDelete})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvSave:
                if (imageFile == null && TextUtils.isEmpty(user.photo)) {
                    showToast(R.string.please_upload_picture);
                } else if (TextUtils.isEmpty(etFirstName.getText()))
                    showToast(R.string.first_name_empty);
                else if (TextUtils.isEmpty(etLastName.getText()))
                    showToast(R.string.last_name_empty);
                else if (TextUtils.isEmpty(etPhoneNumber.getText()))
                    showToast(R.string.phone_number_empty);
                else if (TextUtils.isEmpty(tvDob.getText()))
                    showToast(R.string.dob_empty);
                else if (TextUtils.isEmpty(etAge.getText()))
                    showToast(R.string.empty_age);
                else if (R.id.rbMale != rgGender.getCheckedRadioButtonId() &&
                        R.id.rbFemale != rgGender.getCheckedRadioButtonId())
                    showToast(R.string.empty_gender);
                else {
                    String sex = R.id.rbMale == rgGender.getCheckedRadioButtonId() ?
                            Constants.MALE : Constants.FEMALE;
                    RequestBody requestBody = new RequestBody() {
                        @Override
                        public MediaType contentType() {
                            return null;
                        }

                        @Override
                        public void writeTo(BufferedSink sink) throws IOException {

                        }
                    };
                    String imageUrl = "";
                    if (imageFile != null) {
                        requestBody = RequestBody.create(
                                MediaType.parse(Constants.MIME_TYPE_IMAGE), new File(imageFile.getAbsolutePath()));
                    } else imageUrl = user.photo;

                    showProgress();
                    RequestBody typeBody = RequestBody.create(MediaType.parse("text/plain"), WebConstants.USER_TYPE_PATIENT_CHILD);
                    Call<CommonPojo> call = RestClient.get().editChild(requestBody, typeBody,
                            WebConstants.REQUEST_CHILD_UPDATE, null,
                            WebConstants.ANDROID, etFirstName.getText().toString(), etLastName.getText().toString(),
                            null, etPhoneNumber.getText().toString(),
                            etAge.getText().toString(), sex,
                            "", tvDob.getText().toString(),
                            null, loggedInUser.user_id, user.user_id, imageUrl);
                    call.enqueue(new Callback<CommonPojo>() {
                        @Override
                        public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                            hideProgress();
                            if (response.isSuccessful() && response.body() != null) {
                                CommonPojo pojo = response.body();
                                showToast(pojo.getMsg());
                                if (pojo.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                                    EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                                    getActivity().onBackPressed();
                                }
                            } else {
                                showToast(R.string.there_might_be_some_problem);
                            }
                        }

                        @Override
                        public void onFailure(Call<CommonPojo> call, Throwable t) {
                            try {
                                hideProgress();
                                handleException(t);
                            } catch (Exception e) {

                            }
                        }

                    });
                }
                break;
            case R.id.tvDelete:
                RequestData requestData = new RequestData();
                requestData.type = Constants.TYPE_PATIENT;
                requestData.request = RequestName.DELETE_CHILD;
                requestData.parameters.user_id = loggedInUser.user_id;
                requestData.parameters.child_id = user.user_id;
                makeRequest(requestData);
                break;
            case R.id.tvDob:
                Dialog datedial = new DatePickerDialog(getActivity(),
                        datePickerListener, year, month, day);
                datedial.show();
                break;
            case R.id.profilePic:
                if (imageFile != null && imageFile.exists()) {
                    imageFile.delete();
                }
                boolean permission = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                if (Build.VERSION.SDK_INT >= 23 && !permission) {
                    //if (permission != PackageManager.PERMISSION_GRANTED) {
//                            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    FragmentCompat.requestPermissions(EditUser.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);

                } else {
                    showPicOptionDialog();
                }
                break;
            case R.id.tvNext:
                llEditSaveParent.setVisibility(View.VISIBLE);
                break;
            case R.id.tvBack:
                getFragmentManager().popBackStackImmediate();
                break;
        }
    }

    private void showPicOptionDialog() {
        new MaterialDialog.Builder(getActivity())
                .theme(Theme.LIGHT)
                .items(R.array.media_options)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                Intent i = new Intent(
                                        Intent.ACTION_PICK,
                                        MediaStore.Images.Media
                                                .EXTERNAL_CONTENT_URI);
                                startActivityForResult(i,
                                        Constants.GALLERY_REQUEST);
                                break;
                            case 1:
                                Intent takePictureIntent = new Intent(
                                        MediaStore.ACTION_IMAGE_CAPTURE);
                                File f = null;
                                try {
                                    f = GeneralFunctions.setUpImageFile(Constants
                                            .LOCAL_STORAGE_BASE_PATH_FOR_USER_PHOTOS);
                                    picturePath = f.getAbsolutePath();
                                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                            Uri.fromFile(f));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    f = null;
                                    picturePath = null;
                                }
                                startActivityForResult(takePictureIntent,
                                        Constants.CAMERA_REQUEST);
                                break;
                            case 2:
                                dialog.dismiss();
                                break;
                            default:
                                break;
                        }
                    }
                })
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {

                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    // Log.i(TAG, "Permission has been denied by user");

                } else {

                    //Log.i(TAG, "Permission has been granted by user");
                    showPicOptionDialog();
                }
                return;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
