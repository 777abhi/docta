package patientfragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;

public class DisclaimerFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rgSelection)
    RadioGroup rgSelection;
    private PojoAppointmentRequest pojoAppointmentRequest;

    // TODO: Rename and change types of parameters
    private String mParam1;

    public DisclaimerFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DisclaimerFragment newInstance(String param1) {
        DisclaimerFragment fragment = new DisclaimerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().getString(
                    ARG_PARAM1), PojoAppointmentRequest.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_disclaimer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                if (rgSelection.getCheckedRadioButtonId() == R.id.rbYes) {
                    pojoAppointmentRequest.setSeriousHealthProblem(true);
                    GeneralFunctions.addFragmentFromRight(getFragmentManager(), RequestAppointment.
                            newInstance(new Gson().toJson(pojoAppointmentRequest)), R.id.flAppointmentParent);
                } else if (rgSelection.getCheckedRadioButtonId() == R.id.rbNo) {
                    pojoAppointmentRequest.setSeriousHealthProblem(false);
                    GeneralFunctions.addFragmentFromRight(getFragmentManager(), VideoRequestFragment.
                                        newInstance(new Gson().toJson(pojoAppointmentRequest)),
                                R.id.flAppointmentParent);
//                    if (pojoAppointmentRequest.getServiceId().equals(Constants.GENERAL) ||
//                            pojoAppointmentRequest.getServiceId().equals(Constants.CHILD_CARE)) {
//                        GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), PrimaryHealthConcernFragment.
//                                        newInstance(new Gson().toJson(pojoAppointmentRequest)),
//                                R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
//                    } else if (pojoAppointmentRequest.getServiceId().equals(Constants.SKIN_CARE)) {
//                        GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), SkinCarePrimaryConcernFragment.
//                                        newInstance(new Gson().toJson(pojoAppointmentRequest)),
//                                R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
//                    } else {
//                        GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(), PatientQuestionStep1Fragment.
//                                        newInstance(new Gson().toJson(pojoAppointmentRequest),
//                                                getString(R.string.please_describe_your_primary_health_concern)),
//                                R.id.flAppointmentParent, Constants.TAG_PRIMARY_HEALTH_CONCERN);
//                    }

                }
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
