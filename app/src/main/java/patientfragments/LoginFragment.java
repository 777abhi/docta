package patientfragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.DoctaHomeActivity;
import com.sanguinebits.docta.MainActivity;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gcm.RegistrationIntentService;
import preferences.UserPrefsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.CommonPojo;

public class LoginFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.tvSignUp)
    TextView tvSignUp;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    private String regId = "";
    private UserPrefsManager mUserPrefsManager;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public LoginFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        if (userPrefsManager.getUserType().equals(Constants.TYPE_DOCTOR))
            tvSignUp.setVisibility(View.GONE);
        hideProgress();
        etEmail.setText(userPrefsManager.getUserId());
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                userPrefsManager.saveUserId(etEmail.getText().toString());
                userPrefsManager.saveFavClinic(result.getClinic());
                userPrefsManager.saveFavPharmacy(result.getPharmacy());
                if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
                    userPrefsManager.saveLoginUser(result.getUser());
                    startActivity(new Intent(getActivity(), MainActivity.class));
                } else {
                    result.getUser().clinic = result.getClinic();
                    userPrefsManager.saveLoginUser(result.getUser());
                    startActivity(new Intent(getActivity(), DoctaHomeActivity.class));
                }
                getActivity().finish();
            }

            @Override
            public void onFailure() {
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick({R.id.tvSignIn, R.id.tvSignUp, R.id.tvForgotPasswd})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvSignIn:
                if (TextUtils.isEmpty(etEmail.getText()))
                    showToast(R.string.empty_email);
                else if (TextUtils.isEmpty(etPassword.getText()))
                    showToast(R.string.empty_password);
                else {
                    RequestData requestData = new RequestData();
                    requestData.parameters.email = etEmail.getText().toString();
                    requestData.parameters.password = etPassword.getText().toString();
                    requestData.parameters.deviceid = userPrefsManager.getRegId();
                    requestData.parameters.login_type = Constants.TYPE_PATIENT;
                    if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
                        requestData.request = RequestName.PATIENT_LOGIN;
                        requestData.type = Constants.TYPE_PATIENT;
                    } else {
                        requestData.request = RequestName.DOC_LOGIN;
                        requestData.type = Constants.TYPE_DOCTOR;
                    }
                    makeRequest(requestData);
                }
                break;
            case R.id.tvSignUp:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new SignUpStep1Fragment(), R.id.flFragmentContainer);
                break;
            case R.id.tvForgotPasswd:
                MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                        .title(R.string.forgot_password_)
                        .content(R.string.enter_your_email_address)
                        .positiveText(R.string.done)
                        .theme(Theme.LIGHT)
                        .autoDismiss(false)
                        .negativeText(R.string.cancel)
                        .inputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                        .input(R.string.your_email_address, 0, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                String email = input.toString();
                                if (email.isEmpty()) {
                                    showToast(getString(R.string.enter_your_email_address));
                                    return;
                                }
                                dialog.dismiss();
                                forgotPassword(email);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // TODO
                                dialog.dismiss();
                            }
                        });
                MaterialDialog dialog = builder.build();
                dialog.show();
                break;
            default:
                break;
        }
    }

    private void forgotPassword(String email) {
        showProgress();
        final RequestData requestData = new RequestData();
        if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
            requestData.request = RequestName.FORGOT_PASSWORD;
            requestData.type = Constants.TYPE_PATIENT;
        } else {
            requestData.request = RequestName.DOCTOR_FORGOT_PASSWORD;
            requestData.type = Constants.TYPE_DOCTOR;
        }
        requestData.parameters.email = email;
        Call<CommonPojo> call = RestClient.get().makeRequest(requestData);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    CommonPojo pojo = response.body();
                    showToast(pojo.getMsg());
                }
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    hideProgress();
                    handleException(t);
                } catch (Exception e) {

                }
            }

        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        mUserPrefsManager = new UserPrefsManager(getActivity());
        //if (getRegistrationId().isEmpty()) {
        registerGCM();
        // }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private String getRegistrationId() {
        regId = mUserPrefsManager.getRegId();
        if (regId.isEmpty()) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        if (mUserPrefsManager.getAppVersion() != getAppVersion(getActivity())) {
            return "";
        }
        return regId;
    }

    private void registerGCM() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                regId = userPrefsManager.getRegId();
                if (regId.isEmpty()) {
                    intent = new Intent(getActivity(), RegistrationIntentService.class);
                    getActivity().startService(intent);
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
            getActivity().startService(intent);
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                showToast(R.string.device_will_not_receive_any_notification);
            }
            return false;
        }
        return true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
