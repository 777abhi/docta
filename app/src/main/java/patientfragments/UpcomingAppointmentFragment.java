package patientfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import adapters.AppointmentAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import utils.CommonEvent;
import webservices.pojos.Appointment;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;

/**
 * Created by OM on 12/7/2015.
 */
public class UpcomingAppointmentFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "upcomingAppointments";
    @Bind(R.id.rvData)
    RecyclerView rvAppointmentList;

    @Bind(R.id.tvNoData)
    TextView tvNoData;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private List<Appointment> listAppointment;

    private AppointmentAdapter adapter;

    private boolean showDeleteIcon = true;

    public UpcomingAppointmentFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static UpcomingAppointmentFragment newInstance(String upcomingAppointments) {
        UpcomingAppointmentFragment fragment = new UpcomingAppointmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, upcomingAppointments);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        rvAppointmentList.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (listAppointment.size() == 0) {
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            adapter = new AppointmentAdapter(getActivity(), listAppointment, showDeleteIcon);
            rvAppointmentList.setAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            listAppointment = new Gson().fromJson(mParam1, CommonPojo.class).getUpcoming();
            if (listAppointment == null) {
                showDeleteIcon = false;
                listAppointment = new Gson().fromJson(mParam1, Common.class).getAppointmentList();
                view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
                TextView tvBack = (TextView) view.findViewById(R.id.tvBack);
                view.findViewById(R.id.tvNext).setVisibility(View.GONE);
                tvBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().onBackPressed();
                    }
                });
            } else
                view = inflater.inflate(R.layout.recycler_only_layout, container, false);

        }
        ButterKnife.bind(this, view);
        return view;
    }

    public void onEvent(CommonEvent commonEvent) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.NO_DATA:
                tvNoData.setVisibility(View.VISIBLE);
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
