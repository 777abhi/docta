package adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import patientfragments.EditUser;
import preferences.UserPrefsManager;
import utils.CommonEvent;
import utils.GeneralFunctions;
import webservices.pojos.User;

/**
 * Created by OM on 12/12/2015.
 */
public class PatientChildAdapter extends BaseAdapter
        implements View.OnClickListener {
    private List<User> userList;
    private Activity mContext;

    public PatientChildAdapter(Activity context, List<User> userList) {
        mContext = context;
        this.userList = userList;
        loggedInUser = new UserPrefsManager(context).getLoggedInUser();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_pat_child, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.itemView.setTag(position);
        User user = userList.get(position);
        holder.tvName.setText(user.getFullName());
        holder.profilePic.setImageURI(Uri.parse(user.photo));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public void onClick(View v) {
        final int pos = (int) v.getTag();
        final UserPrefsManager userPrefsManager = new UserPrefsManager(mContext);
        if (userPrefsManager.getParentUser() != null &&
                !userPrefsManager.getLoggedInUser().user_id.
                        equals(userPrefsManager.getParentUser().user_id)) {
            loggedInUser = userPrefsManager.getParentUser();
            userPrefsManager.saveLoginUser(loggedInUser);
            Toast.makeText(mContext, "Default profile changed to parent profile " +
                    loggedInUser.getFullName(), Toast.LENGTH_SHORT).show();
            EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
            return;
        }
        String[] items = {"Switch Profile", "Edit Profile"};
        AlertDialog.Builder build = new AlertDialog.Builder(mContext);
        build.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //....
                switch (which) {
                    case 0:
                        if (userPrefsManager.getParentUser() == null) {
                            userPrefsManager.saveParentUser(loggedInUser);
                        }
                        loggedInUser = userList.get(pos);
                        userPrefsManager.saveLoginUser(loggedInUser);
                        EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                        Toast.makeText(mContext, "Default profile changed to " +
                                loggedInUser.getFullName(), Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager()
                                , EditUser.newInstance(new Gson().toJson(userList.get(pos))), R.id.flAppointmentParent);
                        break;
                }

            }
        }).create().show();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.profilePic)
        SimpleDraweeView profilePic;
        @Bind(R.id.tvName)
        TextView tvName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}