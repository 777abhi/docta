package adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import views.RoundCornerMultiColorProgressBar;
import webservices.pojos.Vital;

/**
 * Created by OM on 1/18/2016.
 */

public class OverViewAdapter extends BaseExpandableListAdapter {

    private static final int CHILD_TYPE_VITAL = 0;
    private static final int CHILD_TYPE_SIMPLE = 1;
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private List<String> _listDataChild = new ArrayList<>();
    private Vital currentVitals;

    public OverViewAdapter(Context context, List<String> listDataHeader,
                           List<String> listChildData, Vital vital) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.currentVitals = vital;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        return groupPosition == 3 ? CHILD_TYPE_VITAL : CHILD_TYPE_SIMPLE;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, final ViewGroup parent) {

        ChildViewHolder childViewHolder = null;
        VitalViewHolder vitalViewHolder = null;
        switch (getChildType(groupPosition, childPosition)) {
            case CHILD_TYPE_VITAL:
                if (convertView == null) {
                    LayoutInflater infalInflater = (LayoutInflater) this._context
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = infalInflater.inflate(R.layout.row_vital, parent, false);
                    vitalViewHolder = new VitalViewHolder(convertView);
                    convertView.setTag(vitalViewHolder);
                } else vitalViewHolder = (VitalViewHolder) convertView.getTag();

                if (currentVitals == null) {
                    vitalViewHolder.vitalParent.setVisibility(View.GONE);
                    vitalViewHolder.tvNoData.setVisibility(View.VISIBLE);
                } else {
                    vitalViewHolder.vitalParent.setVisibility(View.VISIBLE);
                    vitalViewHolder.tvNoData.setVisibility(View.GONE);
//                    vitalViewHolder.bpValue.setData(
//                            getBpIndication(currentVitals.getBp()), currentVitals.getBp(), Constants.BP);
//                    vitalViewHolder.tempValue.setData(
//                            getTempIndication("" + currentVitals.getTemp()), "" + currentVitals.getTemp(), Constants.TEMP);
//                    vitalViewHolder.pulseValue.setData(
//                            getPulseIndication("" + currentVitals.getPulse()), "" + currentVitals.getPulse(), Constants.PULSE);
//                    vitalViewHolder.osatValue.setData(
//                            getOsatIndication("" + currentVitals.getO2sat()), "" + currentVitals.getO2sat(), Constants.OSAT);

                    if (TextUtils.isEmpty(currentVitals.getBp()) ||
                            currentVitals.getBp().equals("0/0"))
                        vitalViewHolder.bpParent.setVisibility(View.GONE);
                    else {
                        vitalViewHolder.bpParent.setVisibility(View.VISIBLE);
                        vitalViewHolder.bpValue.setData(
                                getBpIndication(currentVitals.getBp()), currentVitals.getBp(), Constants.BP);
                    }

                    if (TextUtils.isEmpty(currentVitals.getTemp()) ||
                            currentVitals.getTemp().equals("0"))
                        vitalViewHolder.tempParent.setVisibility(View.GONE);
                    else {
                        vitalViewHolder.tempParent.setVisibility(View.VISIBLE);
                        vitalViewHolder.tempValue.setData(
                                getTempIndication("" + currentVitals.getTemp()), "" + currentVitals.getTemp(), Constants.TEMP);
                    }

                    if (TextUtils.isEmpty(currentVitals.getPulse()) ||
                            currentVitals.getPulse().equals("0"))
                        vitalViewHolder.pulseParent.setVisibility(View.GONE);
                    else {
                        vitalViewHolder.pulseParent.setVisibility(View.VISIBLE);
                        vitalViewHolder.pulseValue.setData(
                                getPulseIndication("" + currentVitals.getPulse()), "" + currentVitals.getPulse(), Constants.PULSE);
                    }

                    if (TextUtils.isEmpty(currentVitals.getO2sat()) ||
                            currentVitals.getO2sat().equals("0"))
                        vitalViewHolder.o2satParent.setVisibility(View.GONE);
                    else {
                        vitalViewHolder.o2satParent.setVisibility(View.VISIBLE);
                        vitalViewHolder.osatValue.setData(
                                getOsatIndication("" + currentVitals.getO2sat()), "" + currentVitals.getO2sat(), Constants.OSAT);
                    }
                }
                break;
            default:
                final String childText = (String) getChild(groupPosition, childPosition);
                if (convertView == null) {
                    LayoutInflater infalInflater = (LayoutInflater) this._context
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = infalInflater.inflate(R.layout.notes_expandable_lis_item, parent, false);
                    childViewHolder = new ChildViewHolder(convertView);
                    convertView.setTag(childViewHolder);
                } else childViewHolder = (ChildViewHolder) convertView.getTag();
                childViewHolder.etChild.setText(childText);
                break;
        }
        return convertView;
    }


    private int getBpIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        String[] bp = value.split("/");
        float bp1 = Float.parseFloat(bp[0].isEmpty() ? "0" : bp[0]);
        float bp2 = Float.parseFloat(bp[1].isEmpty() ? "0" : bp[1]);

        int colorCode1, colorCode2;
        if (bp1 >= 90 && bp1 <= 120)
            colorCode1 = Constants.GREEN;
        else if (bp1 > 121 && bp1 <= 140)
            colorCode1 = Constants.YELLOW;
        else
            colorCode1 = Constants.RED;

        if (bp2 >= 60 && bp2 <= 80)
            colorCode2 = Constants.GREEN;
        else if (bp1 > 81 && bp1 <= 90)
            colorCode2 = Constants.YELLOW;
        else
            colorCode2 = Constants.RED;
        int returnType = colorCode1 > colorCode2 ? colorCode1 : colorCode2;
        return returnType;
    }

    private int getTempIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 35 && val <= 37.5)
            return Constants.GREEN;
        else if (val > 37.5 && val <= 38.3)
            return Constants.YELLOW;
        return Constants.RED;
    }


    private int getPulseIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 50 && val <= 90)
            return Constants.GREEN;
        else if (val > 90 && val <= 100)
            return Constants.YELLOW;
        return Constants.RED;
    }

    private int getOsatIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 95 && val <= 100)
            return Constants.GREEN;
        else if (val >= 80 && val <= 95)
            return Constants.YELLOW;
        return Constants.RED;
    }

    @Override
    public int getChildTypeCount() {
        return 2;
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        HeaderViewHolder headerViewHolder;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.notes_expandable_list_header, null);
            headerViewHolder = new HeaderViewHolder(convertView);
            convertView.setTag(headerViewHolder);
        } else
            headerViewHolder = (HeaderViewHolder) convertView.getTag();

        headerViewHolder.tvGroupName.setText(headerTitle);
        return convertView;

    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    static class HeaderViewHolder {
        @Bind(R.id.tvGroupName)
        TextView tvGroupName;
        @Bind(R.id.cbCheck)
        CheckBox cbCheck;

        HeaderViewHolder(View view) {
            ButterKnife.bind(this, view);
            cbCheck.setVisibility(View.GONE);
        }
    }

    static class ChildViewHolder {
        @Bind(R.id.etChild)
        TextView etChild;

        ChildViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class VitalViewHolder {
        @Bind(R.id.bpValue) RoundCornerMultiColorProgressBar bpValue;
        @Bind(R.id.bpParent) LinearLayout bpParent;
        @Bind(R.id.tempValue) RoundCornerMultiColorProgressBar tempValue;
        @Bind(R.id.tempParent) LinearLayout tempParent;
        @Bind(R.id.pulseValue) RoundCornerMultiColorProgressBar pulseValue;
        @Bind(R.id.pulseParent) LinearLayout pulseParent;
        @Bind(R.id.osatValue) RoundCornerMultiColorProgressBar osatValue;
        @Bind(R.id.o2satParent) LinearLayout o2satParent;
        @Bind(R.id.vitalParent) LinearLayout vitalParent;
        @Bind(R.id.tvNoData) TextView tvNoData;


        VitalViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}