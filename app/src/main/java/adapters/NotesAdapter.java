package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import emrfragments.NoteDetailFragment;
import utils.GeneralFunctions;
import webservices.pojos.PojoNotes;
import webservices.pojos.User;

/**
 * Created by OM on 5/17/2016.
 */
public class NotesAdapter extends BaseAdapter implements View.OnClickListener {


    private Activity mContext;
    private List<PojoNotes> commonPojoList;

    public NotesAdapter(Activity context, List<PojoNotes> commonPojoList) {
        this.mContext = context;
        this.commonPojoList = commonPojoList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notes, parent, false);
        view.setOnClickListener(this);
        DataViewHolder viewHolder = new DataViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DataViewHolder dataViewHolder = (DataViewHolder) holder;
        dataViewHolder.itemView.setTag(position);
        PojoNotes commonPojo = commonPojoList.get(position);
        User doctor = commonPojo.profile;
        dataViewHolder.tvDoctorName.setText(doctor.getFullName());
        dataViewHolder.tvSpeciality.setText(doctor.speciality);
        dataViewHolder.tvDate.setText(commonPojo.ddate);
        dataViewHolder.tvTime.setText(commonPojo.ttime);
        dataViewHolder.cbNoteCompleted.setChecked(commonPojo.completed);
    }

    @Override
    public int getItemCount() {
        return commonPojoList.size();
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag();
        GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager(),
                NoteDetailFragment.newInstance(
                        new Gson().toJson(commonPojoList.get(pos)), ""), R.id.fragment_container);
    }

    static class DataViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvDoctorName)
        TextView tvDoctorName;
        @Bind(R.id.tvSpeciality)
        TextView tvSpeciality;
        @Bind(R.id.cbNoteCompleted)
        CheckBox cbNoteCompleted;
        @Bind(R.id.tvDate)
        TextView tvDate;
        @Bind(R.id.tvTime)
        TextView tvTime;

        DataViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            cbNoteCompleted.setClickable(false);
        }
    }
}
