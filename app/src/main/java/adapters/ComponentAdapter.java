package adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import webservices.pojos.Common;

/**
 * Created by OM on 12/12/2015.
 */
public class ComponentAdapter extends RecyclerView.Adapter<ComponentAdapter.ViewHolder>
        implements View.OnClickListener {
    public int selectedPosition = -1;
    private List<Common> medicationList;
    private Activity mContext;

    public ComponentAdapter(Activity context, List<Common> medicationList) {
        mContext = context;
        this.medicationList = medicationList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_medication, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        Common common = medicationList.get(position);
        holder.tvMedicationame.setText(common.getC_name());
        if (position == selectedPosition) {
            holder.tvMedicationame.setTextColor(ContextCompat.getColor(mContext, R.color.back_text_color));
            holder.tvMedicationame.setBackgroundColor(ContextCompat.getColor(mContext, R.color.grey_stroke_color));
        } else {
            holder.tvMedicationame.setTextColor(ContextCompat.getColor(mContext, R.color.disclaimer_text_color));
            holder.tvMedicationame.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparent));
        }
    }

    @Override
    public int getItemCount() {
        return medicationList.size();
    }

    @Override
    public void onClick(View v) {
        selectedPosition = (int) v.getTag();
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvMedicationame)
        TextView tvMedicationame;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            tvMedicationame.setCompoundDrawables(null, null, null, null);
        }
    }
}