package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import doctorfragments.FollowUpAppointmentFragment;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;

/**
 * Created by OM on 12/28/2015.
 */
public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.ViewHolder>
        implements View.OnClickListener {
    private List<User> listDoctors;
    private Activity mContext;
    private int checkedPosition = 0;

    private PojoAppointmentRequest pojoAppointmentRequest;

    public DoctorsAdapter(Activity context, List<User> listDoctors, PojoAppointmentRequest pojoAppointmentRequest) {
        mContext = context;
        this.listDoctors = listDoctors;
        this.pojoAppointmentRequest = pojoAppointmentRequest;
    }

    public DoctorsAdapter(Activity context, List<User> listDoctors) {
        mContext = context;
        this.listDoctors = listDoctors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_single_textview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvOptionName.setTag(position);
        User doctor = listDoctors.get(position);
        holder.tvOptionName.setText("Dr. " + doctor.getFullName());
        holder.tvOptionName.setOnClickListener(this);
    }

    public User getDoctor() {
        return listDoctors.get(checkedPosition);
    }

    @Override
    public int getItemCount() {
        return listDoctors.size();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvOptionName:
                int pos = (int) v.getTag();
                pojoAppointmentRequest.setFollowUpDoctor(listDoctors.get(pos));
                pojoAppointmentRequest.setFollowUp(true);
                pojoAppointmentRequest.primaryHealthConcern = mContext.getString(R.string.follow_up_appointment);
                GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager()
                        , FollowUpAppointmentFragment.newInstance(
                                new Gson().toJson(pojoAppointmentRequest), ""),
                        R.id.flRequestFragmentContainer);
                break;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvOptionName)
        TextView tvOptionName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
