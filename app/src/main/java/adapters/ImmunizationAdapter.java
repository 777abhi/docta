package adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import webservices.pojos.Common;

/**
 * Created by OM on 12/12/2015.
 */
public class ImmunizationAdapter extends BaseAdapter
        implements View.OnClickListener {
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    DateFormat datFormat = new SimpleDateFormat("yyyy-MM-dd");
    private List<Common> immunizationList;
    private Activity mContext;
    private Date date = new Date();

    public ImmunizationAdapter(Activity context, List<Common> immunizationList) {
        mContext = context;
        this.immunizationList = immunizationList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_immunization, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.itemView.setTag(position);
        Common vaccine = immunizationList.get(position);
        holder.tvName.setText(vaccine.getName());
        holder.tvLastDone.setText(getDateFromString(vaccine.getLastdone()));
        try {
            if (date.before(datFormat.parse(vaccine.getNextdue())))
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(
                        ContextCompat.getDrawable(mContext, R.drawable.green_circle_small), null, null, null);
            else
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(
                        ContextCompat.getDrawable(mContext, R.drawable.red_circle_small), null, null, null);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvNextDue.setText("Next Due : " + getDateFromString(vaccine.getNextdue()));

    }

    protected String getDateFromString(String date) {
        try {
            return dateFormat.format(datFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


    @Override
    public int getItemCount() {
        return immunizationList.size();
    }

    @Override
    public void onClick(View v) {
//        int pos = (int) v.getTag();
//        GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager(),
//                new MedicationDetails(), R.id.flAppointmentParent);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvLastDone)
        TextView tvLastDone;
        @Bind(R.id.tvNextDue)
        TextView tvNextDue;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}