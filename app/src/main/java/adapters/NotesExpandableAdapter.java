package adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import emrfragments.ChooseSymptoms;
import emrfragments.LabResult;
import utils.Constants;
import utils.GeneralFunctions;
import views.RoundCornerMultiColorProgressBar;
import webservices.pojos.Assessment;
import webservices.pojos.Common;
import webservices.pojos.Vital;

/**
 * Created by OM on 1/18/2016.
 */

public class NotesExpandableAdapter extends BaseExpandableListAdapter
        implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private static final int CHILD_TYPE_VITAL = 0;
    private static final int CHILD_TYPE_EDITTEXT = 1;
    private static final int CHILD_TYPE_DIAGNOSE_ASSESSMENT = 2;
    private static final int CHILD_TYPE_SYMPTOMS = 3;
    private static final int CHILD_TYPE_LABS = 4;

    public Common selectedDiagnoses;
    public Activity _context;
    public int checkCounter = 0;
    private List<String> _listDataHeader; // header titles
    private List<Object> _listDataChild = new ArrayList<>();
    private List<Common> diagnosisList = new ArrayList<>();
    private String symptomJson = "";
    private boolean isCompleted;

    public NotesExpandableAdapter(Activity context, List<String> listDataHeader, List<Common> diagnosisList,
                                  List<Object> listChildData, String symptomJson, boolean isCompleted) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.diagnosisList = diagnosisList;
        this.symptomJson = symptomJson;
        this.isCompleted = isCompleted;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(groupPosition);
    }

    public void setSymptomsJson(String symptomJson) {
        this.symptomJson = symptomJson;
    }

    public void setSymptoms(String symptomJson) {
        this.symptomJson = symptomJson;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildTypeCount() {
        return 5;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        switch (groupPosition) {
            case 2:
                return CHILD_TYPE_VITAL;
            case 4:
                return CHILD_TYPE_DIAGNOSE_ASSESSMENT;
            case 1:
                return CHILD_TYPE_SYMPTOMS;
            case 3:
                return CHILD_TYPE_LABS;
            default:
                return CHILD_TYPE_EDITTEXT;
        }
    }

    protected void hideKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void showKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, final ViewGroup parent) {
        VitalViewHolder vitalViewHolder = null;
        ChildViewHolder childViewHolder = null;
        ViewHolder viewHolder = null;
        SymptomsViewHolder symptomsViewHolder = null;

        LayoutInflater infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (getChildType(groupPosition, childPosition)) {
            case CHILD_TYPE_VITAL:
                if (convertView == null) {
                    convertView = infalInflater.inflate(R.layout.row_vital, parent, false);
                    vitalViewHolder = new VitalViewHolder(convertView);
                    convertView.setTag(vitalViewHolder);
                } else vitalViewHolder = (VitalViewHolder) convertView.getTag();
                if (_listDataChild.get(groupPosition) == null) {
                    vitalViewHolder.vitalParent.setVisibility(View.GONE);
                    vitalViewHolder.tvNoData.setVisibility(View.VISIBLE);
                    return convertView;
                }
                Vital currentVitals = (Vital) _listDataChild.get(groupPosition);
                if (TextUtils.isEmpty(currentVitals.getBp()) ||
                        currentVitals.getBp().equals("0/0") || currentVitals.getBp().equals("0"))
                    vitalViewHolder.bpParent.setVisibility(View.GONE);
                else {
                    vitalViewHolder.bpParent.setVisibility(View.VISIBLE);
                    vitalViewHolder.bpValue.setData(
                            getBpIndication(currentVitals.getBp()), currentVitals.getBp(), Constants.BP);
                }

                if (TextUtils.isEmpty(currentVitals.getTemp()) ||
                        currentVitals.getTemp().equals("0"))
                    vitalViewHolder.tempParent.setVisibility(View.GONE);
                else {
                    vitalViewHolder.tempParent.setVisibility(View.VISIBLE);
                    vitalViewHolder.tempValue.setData(
                            getTempIndication("" + currentVitals.getTemp()), "" + currentVitals.getTemp(), Constants.TEMP);
                }

                if (TextUtils.isEmpty(currentVitals.getPulse()) ||
                        currentVitals.getPulse().equals("0"))
                    vitalViewHolder.pulseParent.setVisibility(View.GONE);
                else {
                    vitalViewHolder.pulseParent.setVisibility(View.VISIBLE);
                    vitalViewHolder.pulseValue.setData(
                            getPulseIndication("" + currentVitals.getPulse()), "" + currentVitals.getPulse(), Constants.PULSE);
                }

                if (TextUtils.isEmpty(currentVitals.getO2sat()) ||
                        currentVitals.getO2sat().equals("0"))
                    vitalViewHolder.o2satParent.setVisibility(View.GONE);
                else {
                    vitalViewHolder.o2satParent.setVisibility(View.VISIBLE);
                    vitalViewHolder.osatValue.setData(
                            getOsatIndication("" + currentVitals.getO2sat()), "" + currentVitals.getO2sat(), Constants.OSAT);
                }
                break;
            case CHILD_TYPE_DIAGNOSE_ASSESSMENT:
                //if (convertView == null) {
                convertView = infalInflater.inflate(R.layout.notes_expandable_lis_item_diagnosis, parent, false);
                viewHolder = new ViewHolder(convertView);
                if (!isCompleted)
                    viewHolder.etComment.setOnClickListener(this);
                // convertView.setTag(viewHolder);
                // } else viewHolder = (ViewHolder) convertView.getTag();

                ArrayAdapter<Common> adapter =
                        new ArrayAdapter<Common>(_context, R.layout.row_drop_down, diagnosisList);

                viewHolder.tvDiagnosis.setAdapter(adapter);

                final Assessment assessment = (Assessment) _listDataChild.get(groupPosition);
                viewHolder.etComment.setText(assessment.getCommment());
                viewHolder.etComment.setTag(groupPosition);
                viewHolder.tvDiagnosisName.setText("Diagnosis : " +
                        (assessment.getDiagnose() == null ? "" : assessment.getDiagnose().getName()));
                final ViewHolder finalViewHolder = viewHolder;
                viewHolder.tvDiagnosis.setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position,
                                                    long id) {
                                String text = ((TextView) view).getText().toString();
                                for (int i = 0; i < diagnosisList.size(); i++) {
                                    if (text.equals(diagnosisList.get(i).getName())) {
                                        selectedDiagnoses = diagnosisList.get(i);
                                        assessment.setDiagnose(diagnosisList.get(i));
                                        break;
                                    }
                                }
                                finalViewHolder.tvDiagnosisName.setText("Diagnosis : " +
                                        assessment.getDiagnose().getName());
                                hideKeyBoard(finalViewHolder.tvDiagnosis);
                                finalViewHolder.tvDiagnosis.setText("");
                            }
                        }

                );
                break;

            case CHILD_TYPE_SYMPTOMS:
                if (convertView == null)

                {
                    convertView = infalInflater.inflate(R.layout.notes_expandable_symptoms_list_item, parent, false);
                    symptomsViewHolder = new SymptomsViewHolder(convertView);
                    convertView.setTag(symptomsViewHolder);
                } else symptomsViewHolder = (SymptomsViewHolder) convertView.getTag();
                symptomsViewHolder.tvSymptoms.setText((String) _listDataChild.get(groupPosition));
                symptomsViewHolder.tvAdd.setOnClickListener(this);
                break;
            case CHILD_TYPE_LABS:
                if (convertView == null)

                {
                    convertView = infalInflater.inflate(R.layout.notes_expandable_symptoms_list_item, parent, false);
                    symptomsViewHolder = new SymptomsViewHolder(convertView);
                    convertView.setTag(symptomsViewHolder);
                } else symptomsViewHolder = (SymptomsViewHolder) convertView.getTag();
                symptomsViewHolder.tvSymptoms.setText((String) _listDataChild.get(groupPosition));

                symptomsViewHolder.tvAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GeneralFunctions.addFragmentFromRight(_context.getFragmentManager(),
                                new LabResult(), R.id.fragment_container);
                    }
                });
                break;
            case CHILD_TYPE_EDITTEXT:
                convertView = infalInflater.inflate(R.layout.notes_expandable_lis_item, parent, false);
                childViewHolder = new

                        ChildViewHolder(convertView);

                if (groupPosition == 3)
                    childViewHolder.tvChild.setEnabled(false);
                else childViewHolder.tvChild.setEnabled(true);

                if (!isCompleted)
                    childViewHolder.tvChild.setOnClickListener(this);
                if (groupPosition == 5) {
                    childViewHolder.tvChild.setHint(_context.getString(R.string.enter_plan));
                } else {
                    childViewHolder.tvChild.setHint("");
                }

                childViewHolder.tvChild.setTag(groupPosition);
                childViewHolder.tvChild.setText((String) _listDataChild.get(groupPosition));
                break;
        }

        return convertView;
    }

    private int getBpIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        String[] bp = value.split("/");
        float bp1 = Float.parseFloat(bp[0].isEmpty() ? "0" : bp[0]);
        float bp2 = Float.parseFloat(bp[1].isEmpty() ? "0" : bp[1]);

        int colorCode1, colorCode2;
        if (bp1 >= 90 && bp1 <= 120)
            colorCode1 = Constants.GREEN;
        else if (bp1 > 121 && bp1 <= 140)
            colorCode1 = Constants.YELLOW;
        else
            colorCode1 = Constants.RED;

        if (bp2 >= 60 && bp2 <= 80)
            colorCode2 = Constants.GREEN;
        else if (bp1 > 81 && bp1 <= 90)
            colorCode2 = Constants.YELLOW;
        else
            colorCode2 = Constants.RED;
        int returnType = colorCode1 > colorCode2 ? colorCode1 : colorCode2;
        return returnType;
    }

    private int getTempIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 35 && val <= 37.5)
            return Constants.GREEN;
        else if (val > 37.5 && val <= 38.3)
            return Constants.YELLOW;
        return Constants.RED;
    }


    private int getPulseIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 50 && val <= 90)
            return Constants.GREEN;
        else if (val > 90 && val <= 100)
            return Constants.YELLOW;
        return Constants.RED;
    }

    private int getOsatIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 95 && val <= 100)
            return Constants.GREEN;
        else if (val >= 80 && val <= 95)
            return Constants.YELLOW;
        return Constants.RED;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        HeaderViewHolder headerViewHolder;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.notes_expandable_list_header, null);
            headerViewHolder = new HeaderViewHolder(convertView);
            convertView.setTag(headerViewHolder);
        } else
            headerViewHolder = (HeaderViewHolder) convertView.getTag();

        headerViewHolder.tvGroupName.setText(headerTitle);
        headerViewHolder.cbCheck.setOnCheckedChangeListener(this);
        return convertView;

    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) checkCounter++;
        else checkCounter--;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAdd:
                GeneralFunctions.addFragmentFromRight(_context.getFragmentManager(),
                        ChooseSymptoms.newInstance(symptomJson),
                        R.id.fragment_container);
                break;
            case R.id.etChild:
                int groupPosition = (int) v.getTag();
                switch (groupPosition) {
                    case 0:
                        openInputDialog(groupPosition, _context.getString(R.string.chief_complaint),
                                _context.getString(R.string.enter_your_chief_complain));
                        break;
                    case 5:
                        openInputDialog(groupPosition, _context.getString(R.string.plan),
                                _context.getString(R.string.enter_plan));
                        break;
                }
                break;
            case R.id.etComment:
                int gp = (int) v.getTag();
                openInputDialog(gp, _context.getString(R.string.assessment),
                        _context.getString(R.string.type_your_comment_here));
                break;
        }
    }

    private void openInputDialog(final int groupPosition, String title, String content) {
        String text = "";
        if (groupPosition == 4) {
            text = ((Assessment) _listDataChild.get(groupPosition)).getCommment();
        } else text = _listDataChild.get(groupPosition).toString();
        MaterialDialog.Builder builder = new MaterialDialog.Builder(_context)
                .title(title)
                .content(content)
                .positiveText(R.string.done)
                .theme(Theme.LIGHT)
                .autoDismiss(false)
                .negativeText(R.string.cancel)
                .input(content, text, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                        String text = input.toString();
                        if (text.isEmpty())
                            return;
                        if (groupPosition == 4) {
                            final Assessment assessment = (Assessment) _listDataChild.get(groupPosition);
                            assessment.setCommment(text);
                        } else
                            _listDataChild.set(groupPosition, text);

                        try {
                            _context.getWindow().getCurrentFocus().clearFocus();
                        } catch (Exception e) {

                        }
                        dialog.dismiss();
                        notifyDataSetChanged();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        dialog.dismiss();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.show();
    }


    class HeaderViewHolder {
        @Bind(R.id.tvGroupName)
        TextView tvGroupName;
        @Bind(R.id.cbCheck)
        CheckBox cbCheck;

        HeaderViewHolder(View view) {
            ButterKnife.bind(this, view);
            cbCheck.setClickable(!isCompleted);
            cbCheck.setChecked(isCompleted);
        }
    }

    class ChildViewHolder {
        @Bind(R.id.etChild)
        TextView tvChild;

        ChildViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    class ViewHolder {
        @Bind(R.id.tvDiagnosis)
        AutoCompleteTextView tvDiagnosis;
        @Bind(R.id.tvDiagnosisName)
        TextView tvDiagnosisName;
        @Bind(R.id.etComment)
        TextView etComment;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
            ArrayAdapter<Common> adapter =
                    new ArrayAdapter<Common>(_context, R.layout.row_drop_down, diagnosisList);
            tvDiagnosis.setAdapter(adapter);
            tvDiagnosis.setEnabled(!isCompleted);
            etComment.setEnabled(!isCompleted);
        }
    }

    class VitalViewHolder {
        @Bind(R.id.bpValue) RoundCornerMultiColorProgressBar bpValue;
        @Bind(R.id.bpParent) LinearLayout bpParent;
        @Bind(R.id.tempValue) RoundCornerMultiColorProgressBar tempValue;
        @Bind(R.id.tempParent) LinearLayout tempParent;
        @Bind(R.id.pulseValue) RoundCornerMultiColorProgressBar pulseValue;
        @Bind(R.id.pulseParent) LinearLayout pulseParent;
        @Bind(R.id.osatValue) RoundCornerMultiColorProgressBar osatValue;
        @Bind(R.id.o2satParent) LinearLayout o2satParent;
        @Bind(R.id.vitalParent) LinearLayout vitalParent;
        @Bind(R.id.tvNoData) TextView tvNoData;

        VitalViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    class SymptomsViewHolder {
        @Bind(R.id.tvSymptoms)
        TextView tvSymptoms;
        @Bind(R.id.tvAdd)
        TextView tvAdd;

        SymptomsViewHolder(View view) {
            ButterKnife.bind(this, view);
            if (isCompleted)
                tvAdd.setVisibility(View.GONE);
        }
    }


}