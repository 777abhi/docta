package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import webservices.pojos.Common;

/**
 * Created by OM on 12/12/2015.
 */
public class PatientLabTestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements View.OnClickListener {
    private static final int TYPE_CURRENT = 1;
    private static final int TYPE_PAST = 2;
    private List<Common> data;
    private Activity mContext;
    private int viewType;

    public PatientLabTestAdapter(Activity context, String viewType, List<Common> data) {
        mContext = context;
        this.data = data;
        this.viewType = viewType.equals(Constants.COMMON) ? TYPE_CURRENT : TYPE_PAST;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = null;
        switch (this.viewType) {
            case TYPE_CURRENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_patient_lab_test, parent, false);
                ViewHolder viewHolder = new ViewHolder(view);
                view.setOnClickListener(this);
                return viewHolder;
            case TYPE_PAST:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_patient__past_lab_test, parent, false);
                PastViewHolder pastViewHolder = new PastViewHolder(view);
                view.setOnClickListener(this);
                return pastViewHolder;
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (viewType) {
            case TYPE_CURRENT:
                ViewHolder viewHolder = (ViewHolder) holder;
                holder.itemView.setTag(position);
                Common common = data.get(position);
                viewHolder.tvMedicationame.setText(common.getName());
                viewHolder.tvOrderBy.setText(mContext.getString(R.string.order_by) + common.getDoc_name());
                break;
            case TYPE_PAST:
                PastViewHolder pastViewHolder = (PastViewHolder) holder;
                pastViewHolder.itemView.setTag(position);
                Common common1 = data.get(position);
                pastViewHolder.tvMedicationame.setText(common1.getName());
                break;
        }

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onClick(View v) {
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'patient__past_lab_test.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */


    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvMedicationame)
        TextView tvMedicationame;
        @Bind(R.id.tvOrderBy)
        TextView tvOrderBy;
        @Bind(R.id.chk)
        CheckBox chk;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class PastViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvMedicationame)
        TextView tvMedicationame;
        @Bind(R.id.ivMore)
        ImageView ivMore;
        @Bind(R.id.ivIndicator)
        ImageView ivIndicator;

        PastViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}