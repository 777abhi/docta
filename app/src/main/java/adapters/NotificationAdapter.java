package adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import webservices.pojos.Common;

/**
 * Created by OM on 12/12/2015.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>
        implements View.OnClickListener {
    private static final int VIEW_TYPE_HEADER = 1;
    private static final int VIEW_TYPE_ROW_ITEM = 2;
    public INotification notificationProcessor;
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat monthDateFormat = new SimpleDateFormat("MMM d ,yyyy");
    private List<Common> notificationList;
    private Activity mContext;

    public NotificationAdapter(Activity context, List<Common> notificationList) {
        mContext = context;
        this.notificationList = notificationList;
    }

    public void setNotificationProcessor(INotification notificationProcessor) {
        this.notificationProcessor = notificationProcessor;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        ViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                View headerView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_header_view, parent, false);
                viewHolder = new ViewHolder(headerView);
                viewHolder.ivDelete.setOnClickListener(this);
                break;
            case VIEW_TYPE_ROW_ITEM:
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_notification, parent, false);
                viewHolder = new ViewHolder(itemView);
                viewHolder.ivDelete.setOnClickListener(this);
                break;
        }
        return viewHolder;

    }

    @Override
    public int getItemViewType(int position) {
        if (notificationList.get(position).isHeader())
            return VIEW_TYPE_HEADER;
        return VIEW_TYPE_ROW_ITEM;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.ivDelete.setTag(position);
        Common notification = notificationList.get(position);
        switch (getItemViewType(position)) {
            case VIEW_TYPE_HEADER:
                if (notification.isToday())
                    holder.tvName.setText("Today");
                else if (notification.isYesterDay())
                    holder.tvName.setText("Yesterday");
                else
                    holder.tvName.setText(getDateFromString(notification.getDdate()));
                break;
            case VIEW_TYPE_ROW_ITEM:
                SpannableString styledString
                        = new SpannableString(notification.getTtime() + "\n" + notification.getMsg());
                styledString.setSpan(new RelativeSizeSpan(1.3f),
                        notification.getTtime().length(), styledString.length(), 0);
                styledString.setSpan(new ForegroundColorSpan(
                                ContextCompat.getColor(mContext, R.color.black)),
                        notification.getTtime().length(), styledString.length(), 0);
                holder.tvName.setText(notification.getTtime() + "\n" + notification.getMsg());
                break;
        }
    }

    protected String getDateFromString(String date) {
        Date startDate = null;
        try {
            startDate = dateFormat.parse(date);
            return monthDateFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        if (notificationList.get(position).isHeader()) {
            notificationProcessor.deleteDateNotifications(position);
        } else {
            notificationProcessor.deleteANotification(position);
        }
    }

    public interface INotification {
        void deleteANotification(int pos);

        void deleteDateNotifications(int pos);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.ivDelete)
        ImageView ivDelete;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}