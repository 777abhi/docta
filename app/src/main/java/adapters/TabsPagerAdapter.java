package adapters;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OM on 12/7/2015.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragmemtList = new ArrayList<>();
    private List<String> tabTitleList = new ArrayList<>();

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragmemt, String title) {
        fragmemtList.add(fragmemt);
        tabTitleList.add(title);
    }

    public void addFragment(Fragment fragmemt) {
        fragmemtList.add(fragmemt);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmemtList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitleList.size() > 0 ? tabTitleList.get(position) : null;
    }

    @Override
    public int getCount() {
        return fragmemtList.size();
    }
}
