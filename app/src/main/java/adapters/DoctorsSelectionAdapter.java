package adapters;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import doctorfragments.DoctorProfile;
import patientfragments.ConfirmAppointment;
import patientfragments.DoctorSelectionFragmentCopy;
import preferences.UserPrefsManager;
import utils.Constants;
import utils.DataHolder;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;

/**
 * Created by OM on 12/28/2015.
 */
public class DoctorsSelectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements View.OnClickListener {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEE_MORE = 1;
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private List<User> listDoctors;
    private Fragment mContext;
    private int checkedPosition = 0;
    private int limit;
    private PojoAppointmentRequest pojoAppointmentRequest;

    private boolean showMore = true;

    public DoctorsSelectionAdapter(Fragment context, List<User> listDoctors,
                                   PojoAppointmentRequest pojoAppointmentRequest) {
        mContext = context;
        this.listDoctors = listDoctors;
        this.pojoAppointmentRequest = pojoAppointmentRequest;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        switch (viewType) {
            case TYPE_ITEM:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_doctor_selection_copy, parent, false);
                return new ViewHolder(view);
            case TYPE_SEE_MORE:
                View view1 = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_only_textview, parent, false);
                return new SeeMoreViewHolder(view1);
        }
        return null;

    }


    @Override
    public int getItemViewType(int position) {
        return position == listDoctors.size() ? TYPE_SEE_MORE : TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
//        holder.tvProfle.setTag(position);
        switch (getItemViewType(position)) {
            case TYPE_ITEM:
                ViewHolder holder = (ViewHolder) viewHolder;
                holder.relativeParent.setTag(position);
                holder.relativeParent.setOnClickListener(this);
                final User doctor = listDoctors.get(position);
                String doctorName = "Dr. " + doctor.getFullName();
                String speciality = "";//= doctor.speciality;
                switch (doctor.speciality) {
                    case Constants.GENERAL:
                        speciality = mContext.getString(R.string.general);
                        break;
                    case Constants.PREGNANCY_NEW_BORN:
                        speciality = mContext.getString(R.string.pregnancy_n_new_boen);
                        break;
                    case Constants.SKIN_CARE:
                        speciality = mContext.getString(R.string.skin_care);
                        break;
                    case Constants.CHILD_CARE:
                        speciality = mContext.getString(R.string.child_care);
                        break;
                }
                String clinicName = doctor.clinic.getClinic_name();
                SpannableStringBuilder spannableStringBuilder =
                        new SpannableStringBuilder(doctorName + "\n" +
                                speciality + "\n\n" + clinicName);
//                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0,
//                        doctorName.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableStringBuilder.setSpan(new RelativeSizeSpan(1.3f), 0,
                        doctorName.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.BLACK), 0,
                        doctorName.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                holder.tvName.setText(spannableStringBuilder);
                holder.sdvPic.setImageURI(Uri.parse(doctor.photo));
                holder.tvDistance.setText(doctor.distance + " km");
                holder.llTimeParent.removeAllViews();
                for (int i = 0; i < doctor.time_slot.size(); i++) {
                    TextView view = (TextView) LayoutInflater.from(holder.llTimeParent.getContext())
                            .inflate(R.layout.textview, holder.llTimeParent, false);
                    view.setText(doctor.time_slot.get(i));
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            UserPrefsManager userPrefsManager = new UserPrefsManager(mContext.getActivity());
                            if (userPrefsManager.getUserType().equals(Constants.TYPE_DOCTOR)) {
                                pojoAppointmentRequest.setFollowUpDoctor(doctor);
                                pojoAppointmentRequest.setFollowUp(true);
                                pojoAppointmentRequest.setDoctor(userPrefsManager.getLoggedInUser());
                            } else {
                                pojoAppointmentRequest.setDoctor(doctor);
                            }
                            pojoAppointmentRequest.setClinic(doctor.clinic);
                            pojoAppointmentRequest.setAppointment_date(
                                    dateFormat.format(DataHolder.selectedDate.getTime()));
                            pojoAppointmentRequest.setTime_slot(((TextView) view).getText().toString());
                            GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager()
                                    , ConfirmAppointment.newInstance(
                                            new Gson().toJson(pojoAppointmentRequest), false),
                                    new UserPrefsManager(mContext.getActivity()).getUserType().equals(Constants.TYPE_PATIENT) ?
                                            R.id.flAppointmentParent : R.id.flRequestFragmentContainer);
                        }
                    });
                    holder.llTimeParent.addView(view);
                }
                break;
            case TYPE_SEE_MORE:
                SeeMoreViewHolder seeMoreViewHolder = (SeeMoreViewHolder) viewHolder;
                if (!showMore) {
                    seeMoreViewHolder.tvOptionName.setText(mContext.getString(R.string.no_more_data));
                    return;
                } else
                    seeMoreViewHolder.tvOptionName.setText(mContext.getString(R.string.see_more));
                seeMoreViewHolder.tvOptionName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (showMore) {
                            limit += Constants.LIMIT;
                            ((DoctorSelectionFragmentCopy) mContext).getDoctors(limit);
                        }
                    }
                });
                break;
        }

    }

    public void setShowMore(boolean showMore) {
        this.showMore = showMore;
        notifyDataSetChanged();
    }

    public User getDoctor() {
        return listDoctors.get(checkedPosition);
    }

    @Override
    public int getItemCount() {
        return listDoctors.size() == 0 ? listDoctors.size() : listDoctors.size() + 1;
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag();
        switch (v.getId()) {
            case R.id.relativeParent:
                GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager(),
                        DoctorProfile.newInstance(
                                new Gson().toJson(listDoctors.get(pos))), R.id.flAppointmentParent);
                break;
        }
    }

    static class SeeMoreViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvOptionName)
        TextView tvOptionName;

        SeeMoreViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            tvOptionName.setGravity(Gravity.CENTER);
            tvOptionName.setTypeface(Typeface.DEFAULT_BOLD);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.sdvPic)
        SimpleDraweeView sdvPic;
        @Bind(R.id.tvDistance)
        TextView tvDistance;
        @Bind(R.id.relativeParent)
        RelativeLayout relativeParent;
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.llTimeParent)
        LinearLayout llTimeParent;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
