package adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListPopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.sanguinebits.docta.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import emrfragments.MedicationList;
import utils.GeneralFunctions;
import webservices.pojos.PojoMedication;

/**
 * Created by OM on 12/12/2015.
 */
public class AddMedicationAdapter extends RecyclerView.Adapter<AddMedicationAdapter.ViewHolder>
        implements View.OnClickListener {
    public int selectedPosition;
    protected DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    @SuppressLint("SimpleDateFormat")

    MyDateListener obj = new MyDateListener();
    DatePickerDialog datedial;
    private List<PojoMedication> medicationList;
    private Activity mContext;
    private int year = 2016, month = 1, day = 1;
    private String date = "2016-01-01";

    public AddMedicationAdapter(Activity context, List<PojoMedication> userList) {
        mContext = context;
        datedial = new DatePickerDialog(mContext,
                obj, year, month, day);

        this.medicationList = userList;
    }

    private void displayDate(TextView textView) {
        date = year + "-" + (month + 1) + "-" + day;
        textView.setText(date);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order_medication, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.tvQty.setOnClickListener(this);
        viewHolder.tvDoseType.setOnClickListener(this);
        viewHolder.tvTime1.setOnClickListener(this);
        viewHolder.tvTime2.setOnClickListener(this);

        viewHolder.tvSTart.setOnClickListener(this);
        viewHolder.tvEnd.setOnClickListener(this);
        return viewHolder;

    }

    private void showPopUpWindow(final TextView tv, int arrayId, final int rowPosition) {
        final ListPopupWindow listPopupWindow = new ListPopupWindow(
                mContext);
        final String[] arr = mContext.getResources().getStringArray(arrayId);
        ArrayAdapter<String> adapter = new ArrayAdapter(
                mContext,
                R.layout.row_drop_down, arr);
        listPopupWindow.setAdapter(adapter);
        listPopupWindow.setAnchorView(tv);
        listPopupWindow.setWidth(300);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tv.setText(arr[position]);
                switch (tv.getId()) {
                    case R.id.tvDoseType:
                        medicationList.get(rowPosition).dose = arr[position];
                        break;
                    case R.id.tvQty:
                        medicationList.get(rowPosition).quantity = arr[position];
                        break;
                    case R.id.tvTime1:
                        medicationList.get(rowPosition).time1 = arr[position];
                        break;
                    case R.id.tvTime2:
                        medicationList.get(rowPosition).time2 = arr[position];
                        break;
                }
                listPopupWindow.dismiss();
            }
        });
        listPopupWindow.show();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.itemView.setTag(position);

        holder.tvQty.setTag(position);
        holder.tvDoseType.setTag(position);
        holder.tvTime1.setTag(position);
        holder.tvTime2.setTag(position);
        holder.tvSTart.setTag(position);
        holder.tvEnd.setTag(position);

        PojoMedication pojoAddMedication = medicationList.get(position);
        holder.etName.setText(pojoAddMedication.name);
        holder.etDose.setText(pojoAddMedication.dose);
        holder.tvSTart.setText(pojoAddMedication.startDate);
        holder.tvEnd.setText(pojoAddMedication.endDate);

        holder.tvDoseType.setText(pojoAddMedication.dosetype);
        holder.tvQty.setText(pojoAddMedication.quantity);
        holder.tvTime1.setText(pojoAddMedication.time1);
        holder.tvTime2.setText(pojoAddMedication.time2);
        holder.etName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = holder.getAdapterPosition();
                GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager(),
                        new MedicationList(), R.id.fragment_container);
            }
        });
        holder.etDose.addTextChangedListener(new MyTextWatchet() {
            @Override
            public void onText(String s) {
                medicationList.get(holder.getAdapterPosition()).dose = s;
            }
        });
        holder.etQuantity.addTextChangedListener(new MyTextWatchet() {
            @Override
            public void onText(String s) {
                medicationList.get(holder.getAdapterPosition()).medication_quantity = s;
            }
        });
    }

    @Override
    public int getItemCount() {
        return medicationList.size();
    }


    @Override
    public void onClick(View v) {
        int arrayId = 0;
        int rowPosition = (int) v.getTag();
        switch (v.getId()) {
            case R.id.tvDoseType:
                arrayId = R.array.doseTypeArray;
                showPopUpWindow((TextView) v, arrayId, rowPosition);
                break;
            case R.id.tvQty:
                arrayId = R.array.qtyArray;
                showPopUpWindow((TextView) v, arrayId, rowPosition);
                break;
            case R.id.tvTime1:
                arrayId = R.array.timeArrayMorning;
                showPopUpWindow((TextView) v, arrayId, rowPosition);
                break;
            case R.id.tvTime2:
                arrayId = R.array.timeArrayEvening;
                showPopUpWindow((TextView) v, arrayId, rowPosition);
                break;
            case R.id.tvStart:
                obj.setTextView(v);
                datedial.getDatePicker().setMinDate(0);
                datedial.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datedial.show();
                break;
            case R.id.tvEnd:
                PojoMedication pojoAddMedication = medicationList.get(rowPosition);
                if (pojoAddMedication.startDate.isEmpty()) {
                    Toast.makeText(mContext, "Please select start date first", Toast.LENGTH_SHORT).show();
                    return;
                }
                obj.setTextView(v);
                datedial.getDatePicker().setMinDate(0);
                datedial.getDatePicker().setMinDate(
                        getDateFromString(pojoAddMedication.startDate).getTime());
                datedial.show();
                break;
        }
    }

    protected Date getDateFromString(String date) {
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.etName)
        TextView etName;
        @Bind(R.id.etDose)
        EditText etDose;
        @Bind(R.id.tvQty)
        TextView tvQty;
        @Bind(R.id.etQuantity)
        EditText etQuantity;
        @Bind(R.id.tvDoseType)
        TextView tvDoseType;
        @Bind(R.id.tvTime1)
        TextView tvTime1;
        @Bind(R.id.tvTime2)
        TextView tvTime2;
        @Bind(R.id.tvStart)
        TextView tvSTart;
        @Bind(R.id.tvEnd)
        TextView tvEnd;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private abstract class MyTextWatchet implements TextWatcher {

        public abstract void onText(String s);

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            onText(s.toString());
        }
    }

    private class MyDateListener implements DatePickerDialog.OnDateSetListener {

        private TextView tv;

        public void setTextView(View v) {
            tv = (TextView) v;
        }

        @Override
        public void onDateSet(DatePicker view, int y, int monthOfYear, int dayOfMonth) {
            year = y;
            month = monthOfYear;
            day = dayOfMonth;
            String d, m;
            if (day < 9) {
                d = "0" + day;
            } else d = day + "";
            if (month < 9) {
                m = "0" + (month + 1);
            } else m = (month + 1) + "";
            int pos = (int) tv.getTag();
            if (tv.getId() == R.id.tvStart)
                medicationList.get(pos).startDate = date = d + "-" + m + "-" + year;
            else
                medicationList.get(pos).endDate = date = d + "-" + m + "-" + year;
            displayDate(tv);
        }
    }
}