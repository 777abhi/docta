package adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import webservices.pojos.Vital;

/**
 * Created by OM on 3/30/2016.
 */
public class PastBmiAdapter extends BaseAdapter {
    private List<Vital> vitalList;
    private Activity context;
    private int type;

    public PastBmiAdapter(Activity context, List<Vital> vitalList, int type) {
        this.context = context;
        this.vitalList = vitalList;
        this.type = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_bmi, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Vital vital = vitalList.get(position);
        viewHolder.tvDate.setText(getDateFromString(vital.getDdate()));
        viewHolder.tvOrderBy.setText(context.getString(R.string.order_by) + vital.getDoc_name());
        viewHolder.tvWeight.setText(context.getString(R.string.weight) + " " + vital.getWeight() + context.getString(R.string.weight_unit));
        setColorIndicator(getBmiIndication(vital.getBmi_value()), viewHolder.tvValue);
        viewHolder.tvValue.setText(vital.getBmi_value() + "\n"
                + context.getString(R.string.bmi_unit));
        viewHolder.tvValue.setCompoundDrawables(null,
                ContextCompat.getDrawable(context, R.drawable.red_circle), null, null);
    }

    @Override
    public int getItemCount() {
        return vitalList.size();
    }

    private int getBmiIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 18.5 && val <= 24.9)
            return Constants.GREEN;
        else if (val >= 25 && val <= 29.9)
            return Constants.YELLOW;
        return Constants.RED;
    }

    private void setColorIndicator(final int type, TextView iv) {
        switch (type) {
            case Constants.GREEN:
                iv.setBackground(ContextCompat.getDrawable(context, R.drawable.green_circle));
                break;
            case Constants.YELLOW:
                iv.setBackground(ContextCompat.getDrawable(context, R.drawable.yellos_circle));
                break;
            case Constants.RED:
                iv.setBackground(ContextCompat.getDrawable(context, R.drawable.red_circle));
                break;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvDate)
        TextView tvDate;
        @Bind(R.id.tvOrderBy)
        TextView tvOrderBy;
        @Bind(R.id.tvValue)
        TextView tvValue;
        @Bind(R.id.tvWeight)
        TextView tvWeight;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
