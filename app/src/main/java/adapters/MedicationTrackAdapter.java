package adapters;

import android.app.Fragment;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.sanguinebits.docta.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import webservices.pojos.AvailableTime;
import webservices.pojos.Common;

/**
 * Created by OM on 12/2/2015.
 */
public class MedicationTrackAdapter extends RecyclerView.Adapter<MedicationTrackAdapter.ViewHolder>
        implements View.OnClickListener {

    public static List<String> day_string;
    public GregorianCalendar pmonth;
    public int defaultSelection = -1;
    /**
     * calendar instance for previous month for getting complete view
     */
    public GregorianCalendar pmonthmaxset;
    public View previousView;
    int firstDay;
    int maxWeeknumber;
    int maxP;
    int calMaxP;
    int mnthlength;
    String itemvalue, curentDateString;
    DateFormat df;
    private Context context;
    private String dateSelected = "";
    private Calendar month;
    private GregorianCalendar selectedDate;
    private ArrayList<String> items;
    private int currentDay;

    private List<Common> medicationTrackList = new ArrayList<>();

    public MedicationTrackAdapter(Fragment context, GregorianCalendar monthCalendar) {
        MedicationTrackAdapter.day_string = new ArrayList<String>();
        Locale.setDefault(Locale.US);
        month = monthCalendar;
        selectedDate = (GregorianCalendar) monthCalendar.clone();
        this.context = context.getActivity();
        month.set(GregorianCalendar.DAY_OF_MONTH, 1);
        this.items = new ArrayList<String>();
        df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        curentDateString = df.format(selectedDate.getTime());
        refreshDays();

    }

    public List<Common> getTrackList() {
        return medicationTrackList;
    }

    public void setTrackList(List<Common> medicationTrackList) {
        this.medicationTrackList = medicationTrackList;
    }

    public void setCurrentDay(int cd) {
        currentDay = cd;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cal_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        vh.date.setOnClickListener(this);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String[] separatedTime = day_string.get(position).split("-");
        String gridvalue = separatedTime[2].replaceFirst("^0*", "");
        Common dayTrackdata = medicationTrackList.size() > 0 ?
                medicationTrackList.get(Integer.parseInt(gridvalue) - 1) : null;
        holder.date.setChecked(false);
        holder.date.setBackground(null);
        if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
            holder.date.setTextColor(ContextCompat.getColor(context, R.color.grey_stroke_color));
            holder.date.setClickable(false);
            holder.date.setFocusable(false);
        } else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
            holder.date.setTextColor(ContextCompat.getColor(context, R.color.grey_stroke_color));
            holder.date.setClickable(false);
            holder.date.setFocusable(false);
        } else if (dayTrackdata != null && dayTrackdata.getColor().equals(Constants.MEDICATION_TAKEN)) {
            holder.date.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.date.setBackgroundResource(R.drawable.blue_circle_cal_bck);
        } else if (dayTrackdata != null && dayTrackdata.getColor().equals(Constants.MEDICATION_NOT_TAKEN)) {
            holder.date.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.date.setBackgroundResource(R.drawable.red_circle_cal_bck);
        } else {
            holder.date.setTextColor(ContextCompat.getColor(context, R.color.disclaimer_text_color));
            holder.date.setClickable(true);
            holder.date.setFocusable(true);
        }
        holder.date.setText(gridvalue);
        holder.date.setTag(position);

    }


    private boolean isSatSun(int position) {
        return (position + 1) % 7 == 0 || (position + 1) % 7 == 1;
    }

    @Override
    public int getItemCount() {
        return day_string.size();
    }

    public View setSelected(View view, int pos) {
        if (previousView != null) {
            ((CheckedTextView) previousView).setChecked(false);
            ((CheckedTextView) previousView).setTextColor(
                    ContextCompat.getColor(context, R.color.disclaimer_text_color));
        }
        ((CheckedTextView) view).setChecked(true);
        ((CheckedTextView) view).setTextColor(ContextCompat.getColor(context, R.color.white));
        int len = day_string.size();
        if (len > pos) {
            previousView = view;
        }
        return view;
    }

    public void refreshDays() {
        // clear items
        items.clear();
        day_string.clear();
        Locale.setDefault(Locale.US);
        pmonth = (GregorianCalendar) month.clone();
        // month start day. ie; sun, mon, etc
        firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);
        // finding number of weeks in current month.
        maxWeeknumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
        // allocating maximum row number for the gridview.
        mnthlength = maxWeeknumber * 7;
        maxP = getMaxP(); // previous month maximum day 31,30....
        calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...
        /**
         * Calendar instance for getting a complete gridview including the three
         * month's (previous,current,next) dates.
         */
        pmonthmaxset = (GregorianCalendar) pmonth.clone();
        /**
         * setting the start date as previous month's required date.
         */
        pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);

        /**
         * filling calendar gridview.
         */
        for (int n = 0; n < mnthlength; n++) {

            itemvalue = df.format(pmonthmaxset.getTime());
            pmonthmaxset.add(GregorianCalendar.DATE, 1);
            day_string.add(itemvalue);

        }
    }

    private int getMaxP() {
        int maxP;
        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            pmonth.set((month.get(GregorianCalendar.YEAR) - 1),
                    month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            pmonth.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) - 1);
        }
        maxP = pmonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        return maxP;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.date:
//                int position = (int) v.getTag();
//                defaultSelection = position;
//                setSelected(v, position);
//                String selectedGridDate = MedicationTrackAdapter.day_string
//                        .get(position);
//                String[] temp = selectedGridDate.split("-");
//                dateSelected = temp[2] + "-" + temp[1] + "-" + temp[0];
//                String[] separatedTime = selectedGridDate.split("-");
//                String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
//                int gridvalue = Integer.parseInt(gridvalueString) - 1;

                break;
        }
    }

    public String getDateSelected() {
        return dateSelected;
    }


    public interface FragmentAdapterCommunicator {
        void setPreviousMonth();

        void refreshCalendar();

        void setNextMonth();

        void setTimeSlot(List<AvailableTime> availableTimeSlotList);
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'cal_item.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.date)
        CheckedTextView date;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
