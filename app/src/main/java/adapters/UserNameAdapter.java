package adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import webservices.pojos.User;

/**
 * Created by OM on 12/12/2015.
 */
public class UserNameAdapter extends RecyclerView.Adapter<UserNameAdapter.ViewHolder>
        implements View.OnClickListener {
    private List<User> userList;
    private Activity mContext;

    private int selectedPosition = -1;

    public UserNameAdapter(Activity context, List<User> userList) {
        mContext = context;
        this.userList = userList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_user_name, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        User user = userList.get(position);
        if (position == 0)
            holder.tvName.setText("Me");
        else
            holder.tvName.setText(user.getFullName());
        if (position == selectedPosition) {
            holder.tvName.setTextColor(ContextCompat.getColor(mContext, R.color.back_text_color));
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.grey_stroke_color));
        } else {
            holder.tvName.setTextColor(ContextCompat.getColor(mContext, R.color.disclaimer_text_color));
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparent));
        }

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public void onClick(View v) {
        selectedPosition = (int) v.getTag();
        EventBus.getDefault().post(userList.get(selectedPosition));
        mContext.getFragmentManager().popBackStack();
        //notifyDataSetChanged();
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}