package adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by OM on 12/2/2015.
 */
public class SymptomOptionsAdapter extends BaseAdapter
        implements View.OnClickListener {
    private String[] dataSource;
    private Context mContext;
    private int selectedPosition = -1;

    public SymptomOptionsAdapter(Context context, String[] dataSource) {
        mContext = context;
        this.dataSource = dataSource;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_single_textview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.tvOption.setOnClickListener(this);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.tvOption.setText(dataSource[position]);
        holder.tvOption.setTag(position);
        if (position == selectedPosition) {
            holder.tvOption.setTextColor(ContextCompat.getColor(mContext, R.color.back_text_color));
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.grey_stroke_color));
        } else {
            holder.tvOption.setTextColor(ContextCompat.getColor(mContext, R.color.disclaimer_text_color));
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.transparent));
        }
    }

    @Override
    public void onClick(View v) {
        selectedPosition = (int) v.getTag();
        notifyDataSetChanged();
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    @Override
    public int getItemCount() {
        return dataSource.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvOptionName)
        TextView tvOption;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}