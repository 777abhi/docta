package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import patientfragments.ConfirmAppointment;
import utils.DataHolder;
import utils.GeneralFunctions;
import webservices.pojos.PojoAppointmentRequest;

/**
 * Created by OM on 12/12/2015.
 */
public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.ViewHolder>
        implements View.OnClickListener {
    public int selectedPosition = -1;
    private List<String> timeSlotList;
    private Activity mContext;
    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private PojoAppointmentRequest pojoAppointmentRequest;

    public TimeSlotAdapter(Activity context, List<String> timeSlotList, PojoAppointmentRequest pojoAppointmentRequest) {
        mContext = context;
        this.timeSlotList = timeSlotList;
        this.pojoAppointmentRequest = pojoAppointmentRequest;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_slot_text_view, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        holder.tvTimeSlot.setText(timeSlotList.get(position));
        holder.tvTimeSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pojoAppointmentRequest.setAppointment_date(
                        dateFormat.format(DataHolder.selectedDate.getTime()));
                pojoAppointmentRequest.setTime_slot(((TextView) view).getText().toString());
                GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager()
                        , ConfirmAppointment.newInstance(
                                new Gson().toJson(pojoAppointmentRequest), true), R.id.flRequestFragmentContainer);

            }
        });
    }

    @Override
    public int getItemCount() {
        return timeSlotList.size();
    }

    @Override
    public void onClick(View v) {
        selectedPosition = (int) v.getTag();
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvTimeSlot)
        TextView tvTimeSlot;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}