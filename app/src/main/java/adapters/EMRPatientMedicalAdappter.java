package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import webservices.pojos.Common;

/**
 * Created by OM on 12/12/2015.
 */
public class EMRPatientMedicalAdappter extends RecyclerView.Adapter<EMRPatientMedicalAdappter.ViewHolder>
        implements View.OnClickListener {
    private List<Common> medicationList;
    private Activity mContext;

    public EMRPatientMedicalAdappter(Activity context, List<Common> medicationList) {
        mContext = context;
        this.medicationList = medicationList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_emr_patient_medical, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        Common common = medicationList.get(position);
        Common data = new Gson().fromJson(common.getContent(), Common.class);
        holder.tvName.setText(data.getDiagnose().getName());
        holder.tvDate.setText(common.getDdate());

    }

    @Override
    public int getItemCount() {
        return medicationList.size();
    }

    @Override
    public void onClick(View v) {

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvName)
        TextView tvName;
        @Bind(R.id.tvDate)
        TextView tvDate;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}