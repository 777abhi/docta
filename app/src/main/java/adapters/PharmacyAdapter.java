package adapters;

import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Pharmacy;

/**
 * Created by OM on 4/15/2016.
 */
public class PharmacyAdapter extends BaseAdapter implements View.OnClickListener {
    public Pharmacy selectedPharmacy;
    public int selectedClinicFavPosition;
    private List<Pharmacy> pharmacyList;
    private BaseFragment context;

    public PharmacyAdapter(Fragment context, List<Pharmacy> pharmacyList) {
        init(context.getActivity());
        this.pharmacyList = pharmacyList;
        this.context = (BaseFragment) context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_fav_clinic, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.ivFav.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.ivFav.setTag(position);
        Pharmacy pharmacy = pharmacyList.get(position);
        holder.tvClinicName.setText(pharmacy.getName());
        if (pharmacy.getIsFav().equals("0")) {
            holder.ivFav.setImageResource(R.drawable.heart_off_small);
        } else holder.ivFav.setImageResource(R.drawable.heart_on_small);

    }

    @Override
    public int getItemCount() {
        return pharmacyList.size();
    }

    @Override
    public void onClick(View v) {
        final int pos = (int) v.getTag();
        switch (v.getId()) {
            case R.id.ivFav:
                final String content = context.getString(R.string.do_you_want_to_add_this_pharmacy_to_favorite);
                MaterialDialog.Builder builder = new MaterialDialog.Builder(context.getActivity())
                        .content(content)
                        .positiveText(R.string.yes)
                        .theme(Theme.LIGHT)
                        .title(context.getString(R.string.add_to_fav_pharmacy))
                        .cancelable(false)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // TODO
                                RequestData requestData = new RequestData();
                                requestData.request = RequestName.ADD_FAV_PHARMACY;
                                requestData.type = Constants.TYPE_PATIENT;
                                requestData.parameters.user_id = loggedInUser.user_id;
                                selectedClinicFavPosition = pos;
                                requestData.parameters.pharm_id = pharmacyList.get(pos).getId();
                                selectedPharmacy = pharmacyList.get(pos);
                                context.makeRequest(requestData);
                            }
                        })
                        .negativeText(R.string.no)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // TODO
                                dialog.dismiss();
                            }
                        });
                MaterialDialog dialog = builder.build();
                dialog.show();
                break;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvClinicName)
        TextView tvClinicName;
        @Bind(R.id.ivFav)
        ImageView ivFav;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
