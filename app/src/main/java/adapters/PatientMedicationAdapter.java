package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import patientfragments.MedicationDetails;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RestClient;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoMedication;

/**
 * Created by OM on 12/12/2015.
 */
public class PatientMedicationAdapter extends RecyclerView.Adapter<PatientMedicationAdapter.ViewHolder>
        implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    public String type = "";
    public boolean showToggle;
    private List<PojoMedication> pojoMedicationList;
    private Activity mContext;
    private ProgressWheel pBaseBar;
    private String patientId;
    private String source;

    public PatientMedicationAdapter(Activity context, List<PojoMedication> pojoMedicationList, String type,
                                    boolean showToggle, String source, ProgressWheel pBaseBar, String pat_id) {
        mContext = context;
        this.pojoMedicationList = pojoMedicationList;
        this.type = type;
        this.showToggle = showToggle;
        this.pBaseBar = pBaseBar;
        this.patientId = pat_id;
        this.source = source;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_patient_medication, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.toggleButton1.setOnCheckedChangeListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        holder.toggleButton1.setTag(position);
        PojoMedication medication = pojoMedicationList.get(position);
        holder.tvMedicationame.setText(medication.name);
        holder.tvOrderBy.setText(mContext.getString(R.string.order_by) + medication.doc_name);

    }

    @Override
    public int getItemCount() {
        return pojoMedicationList.size();
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag();
        GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager(),
                MedicationDetails.newInstance(
                        new Gson().toJson(pojoMedicationList.get(pos)), ""),
                source.equals(Constants.TYPE_PATIENT) ? R.id.flAppointmentParent : R.id.fragment_container);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int pos = (int) buttonView.getTag();
        PojoMedication pojoMedication = pojoMedicationList.get(pos);
        RequestData requestData = new RequestData();
        requestData.request = Constants.ACTIVATE_DEACTIVATE_MEDICATION;
        requestData.type = Constants.EMR;
        requestData.parameters.pat_id = patientId;
        requestData.parameters.list_id = pojoMedication.id;
        if (isChecked) {
            requestData.parameters.active = "2";
        } else {
            requestData.parameters.active = "0";
        }
        makeRequest(requestData);
    }

    public void makeRequest(final RequestData requestData) {
        if (pBaseBar != null)
            pBaseBar.setVisibility(View.VISIBLE);
        Call<CommonPojo> call = RestClient.get().makeRequest(requestData);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                if (pBaseBar != null)
                    pBaseBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {
                    CommonPojo data = response.body();
                    if (data.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                        EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    if (pBaseBar != null)
                        pBaseBar.setVisibility(View.GONE);
                } catch (Exception e) {

                }
            }

        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvMedicationame)
        TextView tvMedicationame;
        @Bind(R.id.tvOrderBy)
        TextView tvOrderBy;
        @Bind(R.id.toggleButton1)
        ToggleButton toggleButton1;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            if (showToggle)
                toggleButton1.setVisibility(View.VISIBLE);
            else
                toggleButton1.setVisibility(View.GONE);

            if (type.equals(Constants.COMMON)) {
                toggleButton1.setChecked(true);
            } else toggleButton1.setChecked(false);

        }
    }
}