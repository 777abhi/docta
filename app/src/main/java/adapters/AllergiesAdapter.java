package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import webservices.pojos.Common;

/**
 * Created by OM on 12/12/2015.
 */
public class AllergiesAdapter extends RecyclerView.Adapter<AllergiesAdapter.ViewHolder>
        implements View.OnClickListener {
    private List<Common> allergiesList;
    private Activity mContext;

    public AllergiesAdapter(Activity context, List<Common> allergiesList) {
        mContext = context;
        this.allergiesList = allergiesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_single_textview, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        Common allergy = allergiesList.get(position);
        holder.tvAllergy.setText(allergy.getName());
    }

    @Override
    public int getItemCount() {
        return allergiesList.size();
    }

    @Override
    public void onClick(View v) {

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvOptionName)
        TextView tvAllergy;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}