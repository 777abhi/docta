package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import patientfragments.AppointmentDetail;
import utils.GeneralFunctions;
import webservices.pojos.Appointment;

/**
 * Created by OM on 12/9/2015.
 */

public class ClinicAppointmentAdapter extends RecyclerView.Adapter<ClinicAppointmentAdapter.ViewHolder>
        implements View.OnClickListener {
    private List<Appointment> listAppointment;
    private Activity mContext;

    public ClinicAppointmentAdapter(Activity context, List<Appointment> listAppointment) {
        mContext = context;
        this.listAppointment = listAppointment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_appointment, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        Appointment pojoAppointment = listAppointment.get(position);
        holder.tvAppointmentName.setText(pojoAppointment.getClinic().getClinic_name());
        holder.tvAppointmentDate.setText(GeneralFunctions.getFormatedDate(pojoAppointment.getDdate()));
    }


    @Override
    public int getItemCount() {
        return listAppointment.size();
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager()
                , AppointmentDetail.newInstance(new Gson().toJson(
                listAppointment.get(position)), ""), R.id.flAppointmentParent);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvAppointmentDate)
        TextView tvAppointmentDate;
        @Bind(R.id.tvAppointmentName)
        TextView tvAppointmentName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}