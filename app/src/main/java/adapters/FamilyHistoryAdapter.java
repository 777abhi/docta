package adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import webservices.pojos.Common;

/**
 * Created by OM on 12/12/2015.
 */
public class FamilyHistoryAdapter extends RecyclerView.Adapter<FamilyHistoryAdapter.ViewHolder>
        implements View.OnClickListener {
    private List<Common> userList;
    private Activity mContext;

    public FamilyHistoryAdapter(Activity context, List<Common> userList) {
        mContext = context;
        this.userList = userList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_family_history, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        Common data = userList.get(position);
        Common content = new Gson().fromJson(data.getContent(), Common.class);
        holder.tvAge.setText(mContext.getString(R.string.age_of_onset) + content.getAgeOfOnset());
        holder.tvRelation.setText(content.getRelation());
        holder.tvMedicalProblem.setText(content.getMedicalProblem());
        holder.tvStatus.setText(content.getStatus());

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public void onClick(View v) {
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvMedicalProblem)
        TextView tvMedicalProblem;
        @Bind(R.id.tvRelation)
        TextView tvRelation;
        @Bind(R.id.tvAge)
        TextView tvAge;
        @Bind(R.id.tvStatus)
        TextView tvStatus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}