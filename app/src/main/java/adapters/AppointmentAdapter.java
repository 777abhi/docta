package adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import patientfragments.AppointmentDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;

/**
 * Created by OM on 12/9/2015.
 */

public class AppointmentAdapter extends BaseAdapter
        implements View.OnClickListener {
    private List<Appointment> listAppointment;
    private Activity mContext;
    private boolean showDeleteButton;

    public AppointmentAdapter(Activity context, List<Appointment> listAppointment, boolean showDeleteButton) {
        mContext = context;
        init(mContext);
        this.listAppointment = listAppointment;
        this.showDeleteButton = showDeleteButton;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_appointment, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.ivDelete.setOnClickListener(this);
        viewHolder.linearLayout.setOnClickListener(this);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vHolder, int position) {
        ViewHolder holder = (ViewHolder) vHolder;
        holder.ivDelete.setTag(position);
        holder.linearLayout.setTag(position);
        Appointment pojoAppointment = listAppointment.get(position);
        if (Integer.parseInt(pojoAppointment.getVisit_type()) == Constants.TYPE_VIDEO_VISIT)
            holder.ivVideo.setVisibility(View.VISIBLE);
        else holder.ivVideo.setVisibility(View.GONE);
        PojoAppointmentRequest pojoAppointmentRequest = new Gson().fromJson(
                pojoAppointment.getApointment_data(), PojoAppointmentRequest.class);
        holder.tvAppointmentName.setText(pojoAppointment.getClinic().getClinic_name());
        holder.tvAppointmentDate.setText(GeneralFunctions.getFormatedDate(pojoAppointmentRequest.getAppointment_date()));
    }

    @Override
    public int getItemCount() {
        return listAppointment.size();
    }

    @Override
    public void onClick(View v) {
        final int position = (int) v.getTag();
        switch (v.getId()) {
            case R.id.ivDelete:
                showDeleteConfirmation(position);
                break;
            default:
                GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager()
                        , AppointmentDetail.newInstance(new Gson().toJson(
                                listAppointment.get(position)), ""), R.id.flAppointmentParent);
                break;
        }
    }

    public void delete(final int position) {
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.CANCEL_APPOINTMENT;
        apiRequest.type = Constants.TYPE_PATIENT;
        apiRequest.parameters.pat_id = loggedInUser.user_id;
        apiRequest.parameters.ttime = getTime();

        apiRequest.parameters.apoint_id = listAppointment.get(position).getApoint_id();
        Call<CommonPojo> call = RestClient.get().makeRequest(apiRequest);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                try {
                    if (response.isSuccessful() && response.body().getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                        showToast(response.body().getMsg());
                        EventBus.getDefault().post(listAppointment.get(position));
                        listAppointment.remove(position);
                        if (listAppointment.size() == 0) {
                            EventBus.getDefault().post(new CommonEvent(CommonEvent.NO_DATA));
                        }
                        notifyDataSetChanged();
                    } else {
                        showToast(R.string.there_might_be_some_problem);
                    }
                } catch (Exception e) {
                    showToast(e + "");
                }
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    hideProgress();
                    handleException(t);
                } catch (Exception e) {

                }
            }
        });
    }

    private void showDeleteConfirmation(final int position) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(mContext)
                .content("Are you sure you want to cancel this appointment?")
                .positiveText(R.string.yes)
                .theme(Theme.LIGHT)
                .negativeText(R.string.no)
                .autoDismiss(true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        delete(position);
                    }

                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO

                    }

                });
        MaterialDialog dialog = builder.build();
        dialog.show();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvAppointmentDate)
        TextView tvAppointmentDate;
        @Bind(R.id.tvAppointmentName)
        TextView tvAppointmentName;
        @Bind(R.id.ivDelete)
        ImageView ivDelete;
        @Bind(R.id.ivVideo)
        ImageView ivVideo;
        @Bind(R.id.linearParent)
        LinearLayout linearLayout;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            if (showDeleteButton)
                ivDelete.setVisibility(View.VISIBLE);
        }
    }
}