package adapters;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import emrfragments.PastLabDetail;
import patientfragments.TestDetailFragment;
import preferences.UserPrefsManager;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.pojos.Common;
import webservices.pojos.Vital;

/**
 * Created by OM on 12/12/2015.
 */
public class PatientLabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements View.OnClickListener {
    private static final int TYPE_CURRENT = 1;
    private static final int TYPE_PAST = 2;
    private static final int TYPE_DETAIL = 3;
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat monthDateFormat = new SimpleDateFormat("MMM d ,yyyy");
    private List<Vital> data;
    private Activity mContext;
    private int adapterType;
    private int indicator = -1;
    private GraphInterface grpGraphInterface;

    public PatientLabAdapter(String source, Fragment context, String viewType, List<Vital> data) {
        mContext = context.getActivity();
        grpGraphInterface = context instanceof GraphInterface ? (GraphInterface) context : null;
        this.data = data;
        this.adapterType = viewType.equals(Constants.PAST) ? TYPE_DETAIL :
                viewType.equals(Constants.COMMON) ? TYPE_CURRENT : TYPE_PAST;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = null;
        ViewHolder viewHolder = null;
        PastViewHolder pastViewHolder = null;
        DetailViewHolder detailViewHolder = null;
        switch (adapterType) {
            case TYPE_CURRENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_lab_test, parent, false);
                viewHolder = new ViewHolder(view);
                view.setOnClickListener(this);
                return viewHolder;
            case TYPE_PAST:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_past_lab_test, parent, false);
                pastViewHolder = new PastViewHolder(view);
                pastViewHolder.ivMore.setOnClickListener(this);
                view.setOnClickListener(this);
                return pastViewHolder;
            case TYPE_DETAIL:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_vital_detail, parent, false);
                detailViewHolder = new DetailViewHolder(view);
                return detailViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Vital vital = data.get(position);
        Common component = vital.getComp_detail();
        switch (adapterType) {
            case TYPE_CURRENT:
                ViewHolder viewHolder = (ViewHolder) holder;
                indicator = -1;
                holder.itemView.setTag(position);
                viewHolder.tvMedicationame.setText(component.getC_name());
                if (vital.getUnit().equals(mContext.getString(R.string.no_unit))) {
                } else {
                    simplyfyRange(component.getRed(), Constants.RED, vital);
                    if (indicator == -1) {
                        simplyfyRange(component.getGreen(), Constants.GREEN, vital);
                        if (indicator == -1)
                            simplyfyRange(component.getYellow(), Constants.YELLOW, vital);
                    }
                }
                switch (indicator) {
                    case Constants.GREEN:
                        viewHolder.tvOrderBy.setTextColor(Color.GREEN);
                        break;
                    case Constants.RED:
                        viewHolder.tvOrderBy.setTextColor(Color.RED);
                        break;
                    case Constants.YELLOW:
                        viewHolder.tvOrderBy.setTextColor(Color.YELLOW);
                        break;
                    default:
                        viewHolder.tvOrderBy.setTextColor(ContextCompat.getColor(mContext, R.color.disclaimer_text_color));
                        break;
                }
                if (vital.getComp_value().equalsIgnoreCase(mContext.getString(R.string.positive))) {
                    viewHolder.tvOrderBy.setTextColor(Color.RED);
                } else if (vital.getComp_value().equalsIgnoreCase(mContext.getString(R.string.negative))) {
                    viewHolder.tvOrderBy.setTextColor(Color.GREEN);
                }
                viewHolder.tvOrderBy.setText(vital.getComp_value() + " " +
                        (vital.getUnit().equals(mContext.getString(R.string.no_unit)) ? "" : vital.getUnit()));
                break;
            case TYPE_PAST:
                PastViewHolder pastViewHolder = (PastViewHolder) holder;
                pastViewHolder.ivMore.setTag(position);
                pastViewHolder.itemView.setTag(position);
                pastViewHolder.tvName.setText(component.getC_name());
                if (vital.getUnit().equals(mContext.getString(R.string.no_unit))) {
                    pastViewHolder.ivMore.setVisibility(View.INVISIBLE);
                } else pastViewHolder.ivMore.setVisibility(View.VISIBLE);

                break;
            case TYPE_DETAIL:
                DetailViewHolder detailViewHolder = (DetailViewHolder) holder;
                indicator = -1;
                holder.itemView.setTag(position);
                detailViewHolder.tvDate.setText(getDateFromString(vital.getDdate()));
                if (vital.getUnit().equals(mContext.getString(R.string.no_unit))) {
                } else {
                    simplyfyRange(component.getRed(), Constants.RED, vital);
                    if (indicator == -1) {
                        simplyfyRange(component.getGreen(), Constants.GREEN, vital);
                        if (indicator == -1)
                            simplyfyRange(component.getYellow(), Constants.YELLOW, vital);
                    }
                }
                detailViewHolder.tvValue.setText(vital.getComp_value() + "\n" + vital.getComp_detail().getUnit());
                setColorIndicator(indicator, detailViewHolder.tvValue);
                detailViewHolder.tvOrderBy.setText(mContext.getString(R.string.order_by)
                        + vital.getProfile().getFullName());
                break;
        }
    }

    private void setColorIndicator(final int type, TextView iv) {
        switch (type) {
            case Constants.GREEN:
                iv.setBackground(ContextCompat.getDrawable(mContext, R.drawable.green_circle));
                break;
            case Constants.YELLOW:
                iv.setBackground(ContextCompat.getDrawable(mContext, R.drawable.yellos_circle));
                break;
            case Constants.RED:
                iv.setBackground(ContextCompat.getDrawable(mContext, R.drawable.red_circle));
                break;
        }
    }

    protected String getDateFromString(String date) {
        Date startDate = null;
        try {
            startDate = dateFormat.parse(date);
            return monthDateFormat.format(startDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void simplyfyRange(String rangeColor, int colorCode, Vital labComponent) {
        if (TextUtils.isEmpty(rangeColor)) {
            //tvResultValue.setText(labComponent.getComp_value()); //
        } else if (rangeColor.toLowerCase().contains(Constants.OR)) {
            String arr[] = rangeColor.toLowerCase().split(Constants.OR);
            findRange(arr[0], colorCode, labComponent);
            if (indicator == -1)
                findRange(arr[1], colorCode, labComponent);

        } else if (rangeColor.contains(Constants.DASH)) {
            findRange(rangeColor, colorCode, labComponent);
        }
    }

    private int findRange(String cv, int colorCode, Vital labComponent) {
        float low = Float.parseFloat(cv.split(Constants.DASH)[0]);
        float high = Float.parseFloat(cv.split(Constants.DASH)[1]);
        float temp = Float.parseFloat(labComponent.getComp_value());
        if (temp >= low && temp < high) {
            indicator = colorCode;
        }
        return indicator;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        UserPrefsManager userPrefsManager = new UserPrefsManager(mContext);
        int containerId = userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT) ?
                R.id.flAppointmentParent : R.id.fragment_container;
        switch (v.getId()) {
            case R.id.ivMore:
                grpGraphInterface.moreClicked(position);
                break;
            default:
                switch (adapterType) {
                    case TYPE_CURRENT:
                        GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager(),
                                TestDetailFragment.newInstance(new Gson().toJson(data.get(position))), containerId);
                        break;
                    case TYPE_PAST:
                        GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager(),
                                PastLabDetail.newInstance(data.get(position).getComp_detail().getId()), containerId);
                        break;
                }
                break;
        }
    }

    public interface GraphInterface {
        void moreClicked(int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvName)
        TextView tvMedicationame;
        @Bind(R.id.tvOrderBy)
        TextView tvOrderBy;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class PastViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ivMore)
        ImageView ivMore;
        @Bind(R.id.tvName)
        TextView tvName;

        PastViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class DetailViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvDate) TextView tvDate;
        @Bind(R.id.tvOrderBy) TextView tvOrderBy;
        @Bind(R.id.tvValue) TextView tvValue;

        DetailViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}