package adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import webservices.pojos.Vital;

/**
 * Created by OM on 3/30/2016.
 */
public class VitalAdapter extends BaseAdapter {
    private List<Vital> vitalList;
    private Activity context;
    private int type;

    public VitalAdapter(Activity context, List<Vital> vitalList, int type) {
        this.context = context;
        this.vitalList = vitalList;
        this.type = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_vital_detail, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Vital vital = vitalList.get(position);
        switch (type) {
            case Constants.BP:
                viewHolder.tvDate.setText(getDateFromString(vital.getDdate()));
                viewHolder.tvOrderBy.setText(context.getString(R.string.order_by) + vital.getDoc_name());
                setColorIndicator(getBpIndication(vital.getBp()), viewHolder.tvValue);
                viewHolder.tvValue.setText(vital.getBp() + "\n" + context.getString(R.string.bp_unit));
                break;
            case Constants.TEMP:
                viewHolder.tvDate.setText(getDateFromString(vital.getDdate()));
                viewHolder.tvOrderBy.setText(context.getString(R.string.order_by) + vital.getDoc_name());
                setColorIndicator(getTempIndication(vital.getTemp()), viewHolder.tvValue);
                viewHolder.tvValue.setText(vital.getTemp() + context.getString(R.string.temp_unit));
                break;
            case Constants.PULSE:
                viewHolder.tvDate.setText(getDateFromString(vital.getDdate()));
                viewHolder.tvOrderBy.setText(context.getString(R.string.order_by) + vital.getDoc_name());
                setColorIndicator(getPulseIndication(vital.getPulse()), viewHolder.tvValue);
                viewHolder.tvValue.setText(vital.getPulse() + "\n" + context.getString(R.string.pulse_unit));
                break;
            case Constants.OSAT:
                viewHolder.tvDate.setText(getDateFromString(vital.getDdate()));
                viewHolder.tvOrderBy.setText(context.getString(R.string.order_by) + vital.getDoc_name());
                setColorIndicator(getOsatIndication(vital.getO2sat()), viewHolder.tvValue);
                viewHolder.tvValue.setText(vital.getO2sat() + "\n" + context.getString(R.string.osat_unit));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return vitalList.size();
    }

    private int getBpIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        String[] bp = value.split("/");
        float bp1 = Float.parseFloat(bp[0].isEmpty() ? "0" : bp[0]);
        float bp2 = Float.parseFloat(bp[1].isEmpty() ? "0" : bp[1]);

        int colorCode1, colorCode2;
        if (bp1 >= 90 && bp1 <= 120)
            colorCode1 = Constants.GREEN;
        else if (bp1 > 121 && bp1 <= 140)
            colorCode1 = Constants.YELLOW;
        else
            colorCode1 = Constants.RED;

        if (bp2 >= 60 && bp2 <= 80)
            colorCode2 = Constants.GREEN;
        else if (bp1 > 81 && bp1 <= 90)
            colorCode2 = Constants.YELLOW;
        else
            colorCode2 = Constants.RED;
        int returnType = colorCode1 > colorCode2 ? colorCode1 : colorCode2;
        return returnType;
    }

    private int getTempIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 35 && val <= 37.5)
            return Constants.GREEN;
        else if (val > 37.5 && val <= 38.3)
            return Constants.YELLOW;
        return Constants.RED;
    }


    private int getPulseIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 50 && val <= 90)
            return Constants.GREEN;
        else if (val > 90 && val <= 100)
            return Constants.YELLOW;
        return Constants.RED;
    }

    private int getOsatIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 95 && val <= 100)
            return Constants.GREEN;
        else if (val >= 80 && val <= 95)
            return Constants.YELLOW;
        return Constants.RED;
    }

    private void setColorIndicator(final int type, TextView iv) {
        switch (type) {
            case Constants.GREEN:
                iv.setBackground(ContextCompat.getDrawable(context, R.drawable.green_circle));
                break;
            case Constants.YELLOW:
                iv.setBackground(ContextCompat.getDrawable(context, R.drawable.yellos_circle));
                break;
            case Constants.RED:
                iv.setBackground(ContextCompat.getDrawable(context, R.drawable.red_circle));
                break;
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvDate)
        TextView tvDate;
        @Bind(R.id.tvOrderBy)
        TextView tvOrderBy;
        @Bind(R.id.tvValue)
        TextView tvValue;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
