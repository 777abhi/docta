package adapters;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sanguinebits.docta.DoctaHomeActivity;
import com.sanguinebits.docta.EMRActivity;
import com.sanguinebits.docta.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import doctorfragments.TodaysAppointment;
import patientfragments.ServiceTypeFragment;
import preferences.UserPrefsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.CommonEvent;
import utils.Constants;
import utils.DataHolder;
import utils.GeneralFunctions;
import utils.PushNotificationSender;
import webservices.RequestData;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;

/**
 * Created by OM on 12/9/2015.
 */

public class DoctaAppointmentAdapter extends BaseAdapter
        implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener,
        DatePickerDialog.OnDateChangedListener {
    private final int TODAY_APPOINTMENT = 0;
    private final int UPCOMING_APPOINTMENT = 1;
    private final int PAST_APPOINTMENT = 2;
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private List<Appointment> listAppointment;
    private Fragment mContext;
    private boolean showIcon;
    private DatePickerDialog dpd;
    private int appointmentType;
    private int color;
    private int selectedPosition;

    public DoctaAppointmentAdapter(Fragment context, List<Appointment> listAppointment,
                                   boolean showIcon, int appointmentType) {
        init(context.getActivity());
        mContext = context;
        this.listAppointment = listAppointment;
        this.showIcon = showIcon;
        this.appointmentType = appointmentType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_docta_appointment, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.llParent.setOnClickListener(this);
        viewHolder.ivFollowUpAppointment.setOnClickListener(this);
        viewHolder.ivCall.setOnClickListener(this);
        return viewHolder;

    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        try {
            holder.llParent.setTag(position);
            holder.ivFollowUpAppointment.setTag(position);
            holder.ivCall.setTag(position);

            Appointment doctaAppointments = listAppointment.get(position);
            PojoAppointmentRequest data = new Gson().fromJson(
                    doctaAppointments.getApointment_data(), PojoAppointmentRequest.class);
            User profile = doctaAppointments.getProfile();
            holder.tvPatientName.setText(profile.getFullName());
            if (appointmentType == TODAY_APPOINTMENT) {
                holder.ivCall.setPadding(0, 0, 0, 0);
            } else {
                int padding = dpToPx(16);
            }

            if (data.visitType.equals(mContext.getString(R.string.video_visit))) {
                holder.ivCall.setVisibility(View.VISIBLE);
            } else {
                holder.ivCall.setVisibility(View.GONE);
            }
            holder.tvChiefComplaint.setText(
                    mContext.getString(R.string.chief_complaint) + " : " + data.primaryHealthConcern);
            holder.tvDateTime.setText(
                    mContext.getString(R.string.date_time) + getDateFromString(
                            data.getAppointment_date()) + " - " + data.getTime_slot());
            if (data.isFollowUp()) {
                holder.tvRequestedBy.setVisibility(View.VISIBLE);
                holder.tvRequestedBy.setText("Requested By: Dr." + data.getDoctor().getFullName());
            } else {
                holder.tvRequestedBy.setVisibility(View.GONE);
            }
            if (doctaAppointments.getNotes_complete().equals(Constants.INCOMPLETE))
                holder.cbNoteCompleted.setChecked(false);
            else holder.cbNoteCompleted.setChecked(true);
            switch (doctaAppointments.getColor_code()) {
                case Constants.YELLOW_COLOR:
                    holder.tvStatus.setCompoundDrawablesWithIntrinsicBounds(
                            0, R.drawable.yellos_circle, 0, 0);
                    holder.tvStatus.setText(doctaAppointments.getColor_text());
                    break;
                case Constants.RED_COLOR:
                    holder.tvStatus.setCompoundDrawablesWithIntrinsicBounds(
                            0, R.drawable.red_circle, 0, 0);
                    holder.tvStatus.setText(String.format("%.0f",
                            Float.parseFloat(doctaAppointments.getLateTime())) + " minutes late");
                    break;
                default:
                    holder.tvStatus.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.green_circle, 0, 0);
                    holder.tvStatus.setText(doctaAppointments.getColor_text());
                    break;
            }
        } catch (Exception e) {
            Toast.makeText(mContext.getActivity(), "exception " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return listAppointment == null ? 0 : listAppointment.size();
    }

    @Override
    public void onClick(View v) {
        selectedPosition = (int) v.getTag();
        switch (v.getId()) {
            case R.id.ivFollowUpAppointment:
//                GeneralFunctions.addFragmentFromRight(mContext.getFragmentManager(),
//                        ScheduleAppointment.newInstance(appointment.getApointment_data(),
//                                Constants.TAG_CONSULT_REQUEST), R.id.flRequestFragmentContainer);
                Appointment doctaAppointments = listAppointment.get(selectedPosition);
                User profile = doctaAppointments.getProfile();
                new UserPrefsManager(mContext.getActivity()).savePatientProfile(profile);

                Calendar now = Calendar.getInstance();
                if (now.get(Calendar.DAY_OF_WEEK) ==
                        Calendar.SUNDAY) {
                    now.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH) + 1);
                } else if (now.get(Calendar.DAY_OF_WEEK) ==
                        Calendar.SATURDAY) {
                    now.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH) + 2);
                }
                dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMinDate(now);
                dpd.registerOnDateChangedListener(this);
                dpd.show(mContext.getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.ivCall:
                if (appointmentType == TODAY_APPOINTMENT) {

                    String[] items = {"Make Call", "Mark Visit Complete"};
                    AlertDialog.Builder build = new AlertDialog.Builder(mContext.getActivity());
                    build.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //....
                            switch (which) {
                                case 0:
                                    ((TodaysAppointment) mContext).requestCameraPermission();
                                    break;
                                case 1:
                                    visitCompleted(listAppointment.get(selectedPosition).getApoint_id());
                                    break;
                            }

                        }
                    }).create().show();
                }
                break;
            default:
                if (appointmentType != UPCOMING_APPOINTMENT) {
                    Intent intent = new Intent(mContext.getActivity(), EMRActivity.class);
                    intent.putExtra("type", Constants.EMR);
                    intent.putExtra("requestData", new Gson().toJson(listAppointment.get(selectedPosition)));
                    mContext.startActivityForResult(intent, Constants.CALL_ACTIVITY_CLOSE);
                }
                break;
        }

    }

    public void makeCall() {
        DataHolder.appointmentData = new Gson().toJson(listAppointment.get(selectedPosition));
        Appointment doctaAppointments = listAppointment.get(selectedPosition);
        User profile = doctaAppointments.getProfile();


        String patient=profile.getFullName();
        int profileId= Integer.parseInt(doctaAppointments.getUser_id());
        String doctor= String.valueOf(doctaAppointments.getDoctor());
        //  String  doctor=doc.getFullName();
        Log.e("Create","Starting push notification");
        PushNotificationSender.sendPushMessage(profileId,doctor);
        Log.e("Done"," push notification done ,making call");

        ((DoctaHomeActivity) mContext.getActivity()).callUser(Integer.parseInt(profile.qbid));
    }

    private void visitCompleted(String appointmentId) {
        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.request = RequestName.COMPLETE_VISIT;
        requestData.parameters.pat_id = loggedInUser.user_id;
        requestData.parameters.apoint_id = appointmentId;

        Call<CommonPojo> call = RestClient.get().makeRequest(requestData);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                showToast("Successful");
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                showToast("something wrong happened");
            }

        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.YEAR, year);
        now.set(Calendar.MONTH, monthOfYear);
        now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        DataHolder.selectedDate = now;
        // Appointment appointment = listAppointment.get(selectedPosition);

        PojoAppointmentRequest pojoAppointmentRequest = new PojoAppointmentRequest();
        pojoAppointmentRequest.visitType = mContext.getString(R.string.in_person_visit);
        pojoAppointmentRequest.setPatient(new UserPrefsManager(mContext.getActivity()).getPatientProfile());
        GeneralFunctions.addFragmentFromRight(mContext.getActivity().getFragmentManager()
                , ServiceTypeFragment.newInstance(new Gson().toJson(pojoAppointmentRequest)),
                R.id.flRequestFragmentContainer);
//        GeneralFunctions.addFragmentFromRight(mContext.getActivity().getFragmentManager()
//                , DoctorSelectionFragment.newInstance(
//                        appointment.getApointment_data(), dateFormat.format(now.getTime())),
//                R.id.flRequestFragmentContainer);
    }

    @Override
    public void onDateChanged() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, dpd.getSelectedDay().getDay());
        calendar.set(Calendar.MONTH, dpd.getSelectedDay().getMonth());
        calendar.set(Calendar.YEAR, dpd.getSelectedDay().getYear());
        if (color == 0)
            color = ((AppCompatButton) dpd.getView().findViewById(R.id.ok)).getCurrentTextColor();

        if (calendar.get(Calendar.DAY_OF_WEEK) ==
                Calendar.SUNDAY) {
            ((AppCompatButton) dpd.getView().findViewById(R.id.ok)).setTextColor(Color.LTGRAY);
            dpd.getView().findViewById(R.id.ok).setEnabled(false);
        } else {
            ((AppCompatButton) dpd.getView().findViewById(R.id.ok)).setTextColor(color);
            dpd.getView().findViewById(R.id.ok).setEnabled(true);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvDateTime)
        TextView tvDateTime;
        @Bind(R.id.tvStatus)
        TextView tvStatus;
        @Bind(R.id.tvRequestedBy)
        TextView tvRequestedBy;
        @Bind(R.id.ivFollowUpAppointment)
        ImageView ivFollowUpAppointment;
        @Bind(R.id.ivCall)
        ImageView ivCall;
        @Bind(R.id.tvPatientName)
        TextView tvPatientName;
        @Bind(R.id.tvChiefComplaint)
        TextView tvChiefComplaint;
        @Bind(R.id.cbNoteCompleted)
        CheckBox cbNoteCompleted;
        @Bind(R.id.llParent)
        LinearLayout llParent;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            if (showIcon) {
                ivFollowUpAppointment.setVisibility(View.VISIBLE);
                tvStatus.setVisibility(View.VISIBLE);
            } else {
                ivFollowUpAppointment.setVisibility(View.GONE);
                tvStatus.setVisibility(View.INVISIBLE);
            }

        }
    }

}