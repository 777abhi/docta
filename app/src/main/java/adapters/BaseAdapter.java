package adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import preferences.UserPrefsManager;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.User;

/**
 * Created by OM on 2/25/2016.
 */

public abstract class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected User loggedInUser;
    //protected String userId;
    protected PojoAppointmentRequest pojoAppointmentRequest;
    ProgressWheel pBaseBar;
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat timeFormat = new SimpleDateFormat("hh:mm a");
    DateFormat monthDateFormat = new SimpleDateFormat("MMM d ,yyyy");
    private UserPrefsManager userPrefsManager;
    private Context context;

    public void init(Context context) {
        this.context = context;
        userPrefsManager = new UserPrefsManager(context);
        loggedInUser = userPrefsManager.getLoggedInUser();
    }


    protected PojoAppointmentRequest getAppointmentDeatil() {
        return pojoAppointmentRequest;
    }

    protected String getDate() {
        Date date = new Date();
        return dateFormat.format(date);
    }

    protected String getDateFromString(String date) {
        Date startDate = null;
        try {
            startDate = dateFormat.parse(date);
            return monthDateFormat.format(startDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    protected String getTime() {
        Date date = new Date();
        return timeFormat.format(date);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    protected void showProgress() {
        pBaseBar.setVisibility(View.VISIBLE);
    }

    protected void hideProgress() {
        pBaseBar.setVisibility(View.INVISIBLE);
    }

    protected void showToast(int messageId) {
        try {
            Toast.makeText(context, context.getString(messageId), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    protected Drawable getDrawable(int drawableId) {
        return ContextCompat.getDrawable(context, drawableId);
    }

    protected int getColor(int colorId) {
        return ContextCompat.getColor(context, colorId);
    }

    protected void showToast(String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }


    protected void hideKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void handleException(Throwable e) {
        if (e instanceof IOException) {
            showToast(R.string.no_internet);
        } else {
            showToast(R.string.there_might_be_some_problem);
        }
    }
}