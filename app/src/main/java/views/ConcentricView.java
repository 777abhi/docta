package views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.util.Calendar;

/**
 * Created by OM on 6/14/2016.
 */
public class ConcentricView extends View {
    Paint paint = new Paint();

    private float outerCircleRadius = 170;
    private float innerCircleRadius = 130;
    private float angle_at_8_30_AM = 255;
    private float angle_at_8_PM = 240;
    private float targetAngle;
    private float current_angle_by_time = 90;
    private int targetLineColor;

    private boolean isAllMedicationTaken = false;
    private Context context;

    public ConcentricView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.concentric,
                0, 0);
        //Reading values from the XML layout
        try {
            outerCircleRadius = typedArray.getDimension(R.styleable.concentric_outer_circle_radius, 170);
            innerCircleRadius = typedArray.getDimension(R.styleable.concentric_inner_circle_radius, 130);
            targetLineColor = typedArray.getColor(R.styleable.concentric_target_line_color, Color.RED);
        } finally {
            typedArray.recycle();
        }

        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setAntiAlias(true);
        //setTime();
    }

    public void setAllTaken(boolean isAllMedicationTaken) {
        this.isAllMedicationTaken = isAllMedicationTaken;
        invalidate();
    }

    public void setTime(TextView tv1, TextView tv2) {
        Calendar rightNow = Calendar.getInstance();
        float hour = rightNow.get(Calendar.HOUR_OF_DAY);
        float minutes = rightNow.get(Calendar.MINUTE);
        float h = 0;
        if (hour > 12) h = hour - 12;
        else h = hour;
        current_angle_by_time = h * 30 + minutes / 2;
        if (isAllMedicationTaken) {
            tv1.setText("");
            tv2.setText("");
        } else if ((hour < 12 && current_angle_by_time <= angle_at_8_30_AM) ||
                (hour > 12 && current_angle_by_time > angle_at_8_PM)) {
            targetAngle = angle_at_8_30_AM;
            tv1.setText("8:30 AM");
            tv2.setVisibility(GONE);
        } else {
            targetAngle = angle_at_8_PM;
            tv2.setText("8:00 PM");
            tv1.setVisibility(GONE);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(getWidth() / 2, getWidth() / 2, outerCircleRadius, paint);
        canvas.save();
        canvas.rotate(-90, getWidth() / 2, getHeight() / 2);
        float startX = getWidth() / 2 - outerCircleRadius;
        paint.setColor(targetLineColor);
        if (isAllMedicationTaken) {
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(ContextCompat.getColor(context, R.color.back_text_color));
            canvas.drawArc(new RectF(startX, startX, outerCircleRadius * 2 + startX,
                    outerCircleRadius * 2 + startX), 0, 360, true, paint);
        } else {
            canvas.drawArc(new RectF(startX, startX,
                    outerCircleRadius * 2 + startX, outerCircleRadius * 2 + startX), targetAngle, 0, true, paint);
            paint.setColor(Color.BLACK);
            canvas.drawArc(new RectF(startX, startX, outerCircleRadius * 2 + startX,
                    outerCircleRadius * 2 + startX), current_angle_by_time, 0, true, paint);
        }
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, innerCircleRadius, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.BLACK);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, innerCircleRadius, paint);
        canvas.restore();

    }
}
