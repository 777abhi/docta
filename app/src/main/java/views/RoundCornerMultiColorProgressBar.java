package views;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import com.sanguinebits.docta.R;

import java.util.ArrayList;

import utils.Constants;

/**
 * Created by OM on 11/20/2015.
 */
public class RoundCornerMultiColorProgressBar extends View {
    private static int RECT_HEIGHT = 20;
    private int type = -1;
    private int referenceTextSize;
    private String value = " ";

    private String safeRange = "";
    private String notSafeRange = "";
    private String dangerRange = "";

    public RoundCornerMultiColorProgressBar(Context context) {
        super(context);
    }

    public RoundCornerMultiColorProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RoundCornerMultiColorProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);

    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.roundProgressBar,
                0, 0);
        try {
            RECT_HEIGHT = a.getDimensionPixelSize(R.styleable.roundProgressBar_rect_height, 20);
            referenceTextSize = a.getDimensionPixelSize(R.styleable.roundProgressBar_reference_text_size, 12);
        } finally {
            a.recycle();
        }
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setData(int type, String value, int measurementType) {
        switch (measurementType) {
            case Constants.BP:
                safeRange = "90/60-120/80";
                notSafeRange = "121/81-140/90";
                dangerRange = ">140/90 or <90/60";
                break;
            case Constants.TEMP:
                safeRange = "37-37.5";
                notSafeRange = "37.5-38.5";
                dangerRange = ">38.3 or <35";
                break;
            case Constants.PULSE:
                safeRange = "50-90";
                notSafeRange = "90-100";
                dangerRange = ">100";
                break;
            case Constants.OSAT:
                safeRange = "95-100";
                notSafeRange = "80-95";
                dangerRange = "<80";
                break;
        }
        this.type = type;
        this.value = value;
        invalidate();
    }

    public void setData(int type, String value, ArrayList<String> rangeList) {
        safeRange = rangeList.get(0);
        notSafeRange = rangeList.get(1);
        dangerRange = rangeList.get(2);
        this.type = type;
        this.value = value;
        invalidate();
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    protected void onDraw(android.graphics.Canvas canvas) {
        Paint paint = new Paint();

        paint.setAlpha(255);
        // canvas.translate(0, 30);
        paint.setColor(Color.BLUE);
        Path mPath = new Path();
        float startY = getHeight() / 2 - RECT_HEIGHT / 2;
        float stopY = getHeight() / 2 - RECT_HEIGHT / 2 + RECT_HEIGHT - 1;
        paint.setColor(Color.BLACK);
        paint.setTextSize(getPixels(14));

        Rect textBounds;
        int textWidth;
        switch (type) {
            case Constants.GREEN:
                textBounds = new Rect();
                paint.getTextBounds(value, 0, value.length(), textBounds);
                canvas.drawText(value, getWidth() / 6 - textBounds.width() / 2, textBounds.height(), paint);
                canvas.drawLine(getWidth() / 6, startY,
                        getWidth() / 6, textBounds.height() + 5, paint);
                break;
            case Constants.YELLOW:
                textBounds = new Rect();
                paint.getTextBounds(value, 0, value.length(), textBounds);
                canvas.drawText(value, getWidth() / 2 - textBounds.width() / 2,
                        textBounds.height(), paint);
                canvas.drawLine(getWidth() / 2, startY,
                        getWidth() / 2, textBounds.height() + 5, paint);
                break;
            case Constants.RED:
                textBounds = new Rect();
                paint.getTextBounds(value, 0, value.length(), textBounds);
                canvas.drawText(value, getWidth() - getWidth() / 6 - textBounds.width() / 2,
                        textBounds.height(), paint);
                canvas.drawLine(getWidth() - getWidth() / 6, startY,
                        getWidth() - getWidth() / 6, textBounds.height() + 5, paint);
                break;
        }
        paint.setTextSize(referenceTextSize);
        paint.setTextSize(referenceTextSize);
        textBounds = new Rect();
        int offset = dpToPx(3);

        paint.getTextBounds(safeRange, 0, safeRange.length(), textBounds);
        textWidth = textBounds.width();
        canvas.drawText(safeRange, getWidth() / 6 - textWidth / 2, offset + stopY + textBounds.height(), paint);
        textBounds = new Rect();
        paint.getTextBounds(notSafeRange, 0, notSafeRange.length(), textBounds);
        textWidth = textBounds.width();
        canvas.drawText(notSafeRange, getWidth() / 2 - textWidth / 2, offset + stopY + textBounds.height(), paint);


        textBounds = new Rect();
        paint.getTextBounds(dangerRange, 0, dangerRange.length(), textBounds);
        textWidth = textBounds.width();
        canvas.drawText(dangerRange, 5 * getWidth() / 6 - textWidth / 2, offset + stopY + textBounds.height(), paint);

        mPath.addRoundRect(new RectF(0, startY, getWidth(), stopY),
                RECT_HEIGHT / 2, RECT_HEIGHT / 2, Path.Direction.CCW);
        setLayerType(LAYER_TYPE_SOFTWARE, null);
        canvas.clipPath(mPath, Region.Op.INTERSECT);
        paint.setColor(Color.GREEN);
        paint.setAntiAlias(true);
        if (type != Constants.GREEN)
            paint.setAlpha(50);
        canvas.drawRect(0, startY, getWidth() / 3, stopY, paint);

        paint.setAlpha(255);
        paint.setColor(Color.YELLOW);
        if (type != Constants.YELLOW)
            paint.setAlpha(50);
        canvas.drawRect(getWidth() / 3, startY, getWidth() / 3 * 2, stopY, paint);

        paint.setAlpha(255);

        paint.setColor(Color.RED);
        if (type != Constants.RED)
            paint.setAlpha(50);
        canvas.drawRect(getWidth() / 3 * 2, startY, getWidth(), stopY, paint);
    }

    private int getPixels(float size) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, size, metrics);
    }
}