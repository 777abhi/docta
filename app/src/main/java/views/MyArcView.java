package views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by OM on 11/28/2015.
 */
public class MyArcView extends View {
    public MyArcView(Context context) {
        super(context);
    }

    public MyArcView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyArcView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void onDraw(android.graphics.Canvas canvas) {

        Paint p = new Paint();
        RectF rectDown = new RectF(0, dpToPx(3), dpToPx(240), dpToPx(237));
        p.setColor(Color.GRAY);
        canvas.drawArc(rectDown, 90, 180, true, p);
        p.setColor(Color.GRAY);
        RectF rectInner = new RectF(dpToPx(3), dpToPx(3), dpToPx(237), dpToPx(237));
        p.setColor(Color.WHITE);
        canvas.translate(1, -1);
        canvas.drawArc(rectInner, 90, 180, true, p);

    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return (int) ((dp * displayMetrics.density) + 0.5);
    }
}