package views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.util.AttributeSet;
import android.view.View;


/**
 * Created by OM on 11/20/2015.
 */
public class RoundCornerProgressBar extends View {
    private float progressFactor = 0.25f;

    public RoundCornerProgressBar(Context context) {
        super(context);
    }

    public RoundCornerProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundCornerProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setProgress(float progress) {
        progressFactor = progress;
        invalidate();
    }

    protected void onDraw(android.graphics.Canvas canvas) {
        final Paint paint = new Paint();
        setLayerType(LAYER_TYPE_SOFTWARE, null);
        paint.setAlpha(255);
        // canvas.translate(0, 30);
        paint.setColor(Color.BLUE);
        Path mPath = new Path();
        mPath.addRoundRect(new RectF(0, 0, getWidth(), getHeight()), getHeight() / 2, getHeight() / 2, Path.Direction.CCW);
        canvas.clipPath(mPath, Region.Op.INTERSECT);
        paint.setColor(Color.LTGRAY);
        paint.setAntiAlias(true);
        canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
        paint.setColor(Color.GREEN);
        canvas.drawRoundRect(new RectF(0, 0, getWidth() * progressFactor, getHeight()), getHeight() / 2, getHeight() / 2, paint);

    }
}