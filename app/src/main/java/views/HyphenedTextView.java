package views;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.CheckedTextView;

import com.sanguinebits.docta.R;

import emrfragments.ChooseSymptoms;


public class HyphenedTextView extends CheckedTextView implements View.OnClickListener {

    private String hyphenText = " - ";
    private HtvClickListener radioClickListener;

    private float textSize;

    public HyphenedTextView(Context context) {
        super(context);
    }

    public HyphenedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.hyphen_text_view,
                0, 0);

        try {
            boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
            if (tabletSize) {
                // do something
                textSize = a.getDimension(R.styleable.hyphen_text_view_text_size, 22);
            } else {
                // do something else
                textSize = a.getDimension(R.styleable.hyphen_text_view_text_size, 16);
            }
        } finally {
            a.recycle();
        }
        init(context);
    }

    public HyphenedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    private void init(Context context) {
        Fragment fragment = ((Activity) context).getFragmentManager().
                findFragmentByTag(ChooseSymptoms.class.getName());
        if (fragment != null && fragment instanceof HtvClickListener)
            radioClickListener = (HtvClickListener) fragment;
        setPadding(0, 0, dpToPx(12), dpToPx(4));
        setOnClickListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint textPaint = new Paint();
        if (isChecked())
            ContextCompat.getColor(this.getContext(), R.color.white);
        else
            ContextCompat.getColor(this.getContext(), R.color.disclaimer_text_color);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        textPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, textSize, getResources().getDisplayMetrics()));
        Rect bounds = new Rect();
        Paint tvPaint = getPaint();
        tvPaint.getTextBounds(hyphenText, 0, hyphenText.length(), bounds);
        int textWidth = bounds.width() * 3;
        int lineCount = getHeight() / getLineHeight();
        canvas.drawText(hyphenText, 0, dpToPx(4) + getLineHeight() - (getHeight() -
                getLineHeight() * lineCount), textPaint);
        canvas.translate(textWidth, 0);
        super.onDraw(canvas);
    }

    @Override
    public void onClick(View v) {
        setChecked(!isChecked());
        CheckedTextView ctv = (CheckedTextView) v;
        if (radioClickListener != null)
            radioClickListener.onClick(0, ctv.getText().toString());
    }

    public interface HtvClickListener {
        void onClick(int parentId, String text);
    }
}
