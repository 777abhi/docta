package views;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.CheckedTextView;

import com.sanguinebits.docta.R;

import utils.Constants;


public class MyRadioButton extends CheckedTextView implements View.OnClickListener {


    private static final int PADDING_RIGHT = 16;
    private static final int INNER_CIRCLE_RADIUS = 5;
    private static final int OUTER_CIRCLE_RADIUS = 8;
    private static int CIRCLE_XY_OFFSET = 14;
    private RadioClickListener radioClickListener;
    private Context mContext;

    public MyRadioButton(Context context) {
        super(context);
        this.mContext = context;
    }

    public MyRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init(context);
    }

    public MyRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init(context);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    private void init(Context context) {
        Fragment fragment = ((Activity) context).getFragmentManager().
                findFragmentByTag(Constants.TAG_PRIMARY_HEALTH_CONCERN);
        if (fragment != null && fragment instanceof RadioClickListener)
            radioClickListener = (RadioClickListener) fragment;
        setOnClickListener(this);
       // CIRCLE_XY_OFFSET = getHeight() / 2;
        int totalRightPadding = PADDING_RIGHT + CIRCLE_XY_OFFSET;
        setPadding(0, 0, dpToPx(totalRightPadding), 0);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        //canvas.drawRGB(2, 2, 200);
        Paint textPaint = new Paint();
        textPaint.setColor(ContextCompat.getColor(mContext, R.color.disclaimer_text_color));
        textPaint.setTextSize(getTextSize());
        int lineCount = getHeight() / getLineHeight();
        Paint circlePaint = new Paint();
        circlePaint.setStrokeWidth(dpToPx(2));
        if (isChecked()) {
            circlePaint.setStyle(Paint.Style.FILL);
            circlePaint.setColor(Color.BLACK);
            if (lineCount > 1)
                canvas.drawCircle(dpToPx(CIRCLE_XY_OFFSET), dpToPx(CIRCLE_XY_OFFSET),
                        dpToPx(INNER_CIRCLE_RADIUS), circlePaint);
            else
                canvas.drawCircle(dpToPx(CIRCLE_XY_OFFSET), dpToPx(CIRCLE_XY_OFFSET),
                        dpToPx(INNER_CIRCLE_RADIUS), circlePaint);

        }
        circlePaint.setColor(ContextCompat.getColor(mContext, R.color.back_text_color));
        circlePaint.setStyle(Paint.Style.STROKE);
        if (lineCount > 1)
            canvas.drawCircle(dpToPx(CIRCLE_XY_OFFSET), dpToPx(CIRCLE_XY_OFFSET),
                    dpToPx(OUTER_CIRCLE_RADIUS), circlePaint);
        else
            canvas.drawCircle(dpToPx(CIRCLE_XY_OFFSET), dpToPx(CIRCLE_XY_OFFSET),
                    dpToPx(OUTER_CIRCLE_RADIUS), circlePaint);
        int translateOffset = PADDING_RIGHT + CIRCLE_XY_OFFSET;
        canvas.translate(dpToPx(translateOffset), 0);
        super.onDraw(canvas);
    }

    @Override
    public void onClick(View v) {
        GridLayout gl = (GridLayout) v.getParent();
        int childIndex = gl.indexOfChild(v);
        for (int i = 0; i < gl.getChildCount(); i++) {
            if (i != childIndex) {
                CheckedTextView ct = (CheckedTextView) gl.getChildAt(i);
                ct.setChecked(false);
                ct.invalidate();
            }
        }
        CheckedTextView ctv = (CheckedTextView) v;
        ctv.setChecked(!ctv.isChecked());
        v.invalidate();
        if (radioClickListener != null) {
            radioClickListener.onClick(gl.getId());
            radioClickListener.onViewClick(v.getId());

        }
    }

    public interface RadioClickListener {
        void onClick(int parentId);

        void onViewClick(int viewId);

    }
}
