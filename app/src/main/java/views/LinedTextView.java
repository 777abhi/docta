package views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.sanguinebits.docta.R;


/**
 * Created by OM on 12/7/2015.
 */
public class LinedTextView extends TextView {

    private static final int DEFAULT_PADDING = 8;
    private int color;

    public LinedTextView(Context context) {
        super(context);
    }

    public LinedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.myView,
                0, 0);

        try {
            color = a.getColor(R.styleable.myView_color1, 0);
        } finally {
            a.recycle();
        }
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        Rect bounds = new Rect();
        Paint textPaint = getPaint();
        textPaint.getTextBounds(getText().toString(), 0, getText().length(), bounds);
        int textWidth = bounds.width();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(dpToPx(1));
        paint.setAntiAlias(true);
        paint.setColor(color);
        float lineEnd = getWidth() / 2 - textWidth / 2 - dpToPx(DEFAULT_PADDING);
        canvas.drawLine(0, getHeight() / 2, lineEnd, getHeight() / 2, paint);
        float lineStart = getWidth() / 2 + textWidth / 2 + dpToPx(DEFAULT_PADDING);
        canvas.drawLine(lineStart, getHeight() / 2, getWidth(), getHeight() / 2, paint);
    }
}
