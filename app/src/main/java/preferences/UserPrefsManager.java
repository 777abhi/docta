package preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import webservices.pojos.Appointment;
import webservices.pojos.Clinic;
import webservices.pojos.Pharmacy;
import webservices.pojos.PrefsNotes;
import webservices.pojos.User;

/**
 * Created by OM on 11/27/2015.
 */
public class UserPrefsManager {
    public static final String PREFS_FILENAME = "DoctaUsers";
    public static final String PREFS_NOTES = "notes";
    public static final int PREFS_MODE = 0;
    public static final String PREFS_REG_ID = "regId";
    public static final String APPOINTMENT_ID = "appointmentId";

    public static final String PREFS_LOGGED_IN_USER_NAME = "userId";
    public static final String PREFS_DOC_ID = "doctorId";

    public static final String PREFS_USER_TYPE = "userType";

    public static final String PREFS_LOGGED_USER = "prefsProfile";
    public static final String PREFS_PARENT_USER = "prefsParentProfile";

    public static final String PREFS_APPOINTMENT = "prefsAppointment";

    public static final String PREFS_FAV_CLINIC = "prefsFavClinic";
    public static final String PREFS_FAV_PHARMACY = "prefsFavPharmacy";
    public static final String PREFS_SHOW_TUTORIAL = "prefsShowTutorial";
    public static final String PREFS_SHOW_GREETINGS = "prefsShowGreetings";

    public static final String PREFS_DOCTOR_PROFILE = "prefsDoctorProfile";
    public static final String PREFS_PATIENT_PROFILE = "prefsPatientProfile";

    public static final String PREFS_APP_VERSION = "appVersion";
    private final SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;


    public UserPrefsManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFS_FILENAME,   PREFS_MODE);
        mEditor = mSharedPreferences.edit();
    }

    public void clearUserPrefs() {
        mEditor.clear();
        mEditor.apply();
    }

    public User getPatientProfile() {
        return new Gson().fromJson(
                mSharedPreferences.getString(PREFS_PATIENT_PROFILE, null), User.class);
    }

    public void savePatientProfile(User patientProfile) {
        mEditor.putString(PREFS_PATIENT_PROFILE, new Gson().toJson(patientProfile));
        mEditor.apply();
    }

    public String getRegId() {
        return mSharedPreferences.getString(PREFS_REG_ID, "");
    }

    public int getAppVersion() {
        return mSharedPreferences.getInt(PREFS_APP_VERSION, Integer.MIN_VALUE);
    }

    public void saveAppVersion(int appVersion) {
        mEditor.putInt(PREFS_APP_VERSION, appVersion);
        mEditor.apply();
    }

    public String getUserId() {
        return mSharedPreferences.getString(PREFS_LOGGED_IN_USER_NAME, "");
    }

    public void saveUserId(String userId) {
        mEditor.putString(PREFS_LOGGED_IN_USER_NAME, userId);
        mEditor.apply();
    }

    public String getUserType() {
        return mSharedPreferences.getString(PREFS_USER_TYPE, "");
    }

    public void saveUserType(String userType) {
        mEditor.putString(PREFS_USER_TYPE, userType);
        mEditor.apply();
    }

    public String getDoctorId() {
        return mSharedPreferences.getString(PREFS_DOC_ID, "");
    }

    public void saveDoctorId(String doctorId) {
        mEditor.putString(PREFS_DOC_ID, doctorId);
        mEditor.apply();
    }

    public boolean showTutorial() {
        return mSharedPreferences.getBoolean(PREFS_SHOW_TUTORIAL, true);
    }

    public void setShowTutorial(boolean showGreeting) {
        mEditor.putBoolean(PREFS_SHOW_TUTORIAL, showGreeting);
        mEditor.apply();
    }

    public boolean showGreeting() {
        return mSharedPreferences.getBoolean(PREFS_SHOW_GREETINGS, false);
    }

    public void setShowGreeting(boolean showGreeting) {
        mEditor.putBoolean(PREFS_SHOW_GREETINGS, showGreeting);
        mEditor.apply();
    }

    public User getLoggedInUser() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_LOGGED_USER, null), User.class);
    }

    public void saveLoginUser(User profile) {
        mEditor.putString(PREFS_LOGGED_USER, new Gson().toJson(profile));
        mEditor.apply();
    }

    public User getParentUser() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_PARENT_USER, null), User.class);
    }

    public void saveParentUser(User user) {
        mEditor.putString(PREFS_PARENT_USER, new Gson().toJson(user));
        mEditor.apply();
    }

    public Appointment getAppointment() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_APPOINTMENT, null), Appointment.class);
    }

    public void saveAppointment(Appointment appointment) {
        mEditor.putString(PREFS_APPOINTMENT, new Gson().toJson(appointment));
        mEditor.apply();
    }

    public Clinic getFavClinic() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_FAV_CLINIC, null), Clinic.class);
    }

    public void saveFavClinic(Clinic clinic) {
        mEditor.putString(PREFS_FAV_CLINIC, new Gson().toJson(clinic));
        mEditor.apply();
    }

    public Pharmacy getFavPharmacy() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_FAV_PHARMACY, null), Pharmacy.class);
    }

    public void saveFavPharmacy(Pharmacy pharmacy) {
        mEditor.putString(PREFS_FAV_PHARMACY, new Gson().toJson(pharmacy));
        mEditor.apply();
    }

    public User getAssignedDoctorProfile() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_DOCTOR_PROFILE, null), User.class);
    }

    public void saveAssignedDoctorProfile(User profile) {
        mEditor.putString(PREFS_DOCTOR_PROFILE, new Gson().toJson(profile));
        mEditor.apply();
    }

    public User getAssignedPatientProfile() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_PATIENT_PROFILE, null), User.class);
    }

    public void saveAssignedPatientProfile(User profile) {
        mEditor.putString(PREFS_PATIENT_PROFILE, new Gson().toJson(profile));
        mEditor.apply();
    }

    public PrefsNotes getNotes() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_NOTES, null), PrefsNotes.class);
    }

    public void saveNotes(PrefsNotes notes) {
        mEditor.putString(PREFS_NOTES, new Gson().toJson(notes));
        mEditor.apply();
    }

    /*  public VisitRequest getVisitRequest() {
        return new Gson().fromJson(mSharedPreferences.getString(
                PREFS_VISIT_REQUEST, null), VisitRequest.class);
    }

    public void saveVisitrequest(VisitRequest visitRequest) {
        mEditor.putString(PREFS_VISIT_REQUEST, new Gson().toJson(visitRequest));
        mEditor.apply();
    }

    public QBUser getQbUser() {
        return new Gson().fromJson(mSharedPreferences.getString(PREFS_QBUSER, ""), QBUser.class);
    }*/

    public void saveRegId(String regId) {
        mEditor.putString(PREFS_REG_ID, regId);
        mEditor.apply();
    }

    public void saveAppointmentId(String appointId) {
        mEditor.putString(APPOINTMENT_ID, appointId);
        mEditor.apply();
    }

    public String getAppointmentId() {
        return mSharedPreferences.getString(APPOINTMENT_ID, null);
    }
}
