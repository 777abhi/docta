package services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSignaling;
import com.quickblox.chat.QBWebRTCSignaling;
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCConfig;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacks;
import com.sanguinebits.docta.MainActivity;

import java.util.List;
import java.util.Map;

import preferences.UserPrefsManager;
import utils.Constants;
import utils.DataHolder;

/**
 * Created by anirudhnanda on 11/10/2016 AD.
 */

public class VideoCallService extends Service implements
        QBRTCClientSessionCallbacks {
    private static QBChatService chatService;
    private IBinder mBinder = new MyBinder();
    private QBRTCClient rtcClient;

    private QBRTCClientSessionCallbacks qbrtcClientSessionCallbacks;
  //  public static void start(Context context, QBUser qbUser, PendingIntent pendingIntent) {
  public static void start(Context context) {
        Intent intent = new Intent(context, VideoCallService.class);

//        intent.putExtra(Consts.EXTRA_COMMAND_TO_SERVICE, Consts.COMMAND_LOGIN);
//        intent.putExtra(Consts.EXTRA_QB_USER, qbUser);
//        intent.putExtra(Consts.EXTRA_PENDING_INTENT, pendingIntent);

        context.startService(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        //countDownTimer.start();
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //stopSelf();
        login();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (rtcClient != null) rtcClient.removeSessionsCallbacksListener(this);
        super.onDestroy();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        QBSettings.getInstance().fastConfigInit(Constants.APP_ID, Constants.AUTH_KEY, Constants.AUTH_SECRET);
        QBChatService.setDebugEnabled(true);
    }

    private void initQBRTCClient() {
        rtcClient = QBRTCClient.getInstance(this);
        // Add signalling manager
        QBChatService.getInstance().getVideoChatWebRTCSignalingManager().
                addSignalingManagerListener(new QBVideoChatSignalingManagerListener() {
                    @Override
                    public void signalingCreated(QBSignaling qbSignaling, boolean createdLocally) {
                        if (!createdLocally) {
                            rtcClient.addSignaling((QBWebRTCSignaling) qbSignaling);
                        }
                    }
                });

        // Configure
        //
        QBRTCConfig.setMaxOpponentsCount(6);
        QBRTCConfig.setDisconnectTime(30);
        QBRTCConfig.setAnswerTimeInterval(30l);
        QBRTCConfig.setDebugEnabled(true);


        // Add activity as callback to RTCClient
        rtcClient.addSessionCallbacksListener(this);
        rtcClient.prepareToProcessCalls();
    }

    private void login() {
        final QBUser user = new UserPrefsManager(this).getLoggedInUser().getQbUser();
        QBAuth.createSession(user.getLogin(), user.getPassword(), new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle bundle) {//8347861
                user.setId(session.getUserId());
                if (!QBChatService.isInitialized()) {
                    QBChatService.init(VideoCallService.this);
                }
                chatService = QBChatService.getInstance();
                if (chatService.isLoggedIn()) {
                    initQBRTCClient();
                } else {
                    chatService.login(user, new QBEntityCallbackImpl<QBUser>() {
                        @Override
                        public void onSuccess() {
                            initQBRTCClient();
                        }

                        @Override
                        public void onError(List errors) {
                            Log.v("error", errors + "");
                        }
                    });
                }

            }

            @Override
            public void onError(List<String> errors) {
                try {
                    Log.v("error", errors + "");
                } catch (Exception e) {
                    Log.v("error", errors + "");
                }

            }
        });
    }

    public void wakePhone() {
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wakeLock.acquire();
    }

    @Override
    public void onReceiveNewSession(QBRTCSession qbrtcSession) {
        wakePhone();
        DataHolder.qbrtcSession = qbrtcSession;
        DataHolder.appointmentData = qbrtcSession.getUserInfo().get(Constants.APPOINTMENT_DATA);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("video_call", "");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onUserNotAnswer(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onCallRejectByUser(QBRTCSession qbrtcSession, Integer integer, Map<String, String> map) {

    }

    @Override
    public void onCallAcceptByUser(QBRTCSession qbrtcSession, Integer integer, Map<String, String> map) {

    }

    @Override
    public void onReceiveHangUpFromUser(QBRTCSession qbrtcSession, Integer integer, Map<String, String> map) {
        qbrtcClientSessionCallbacks.onReceiveHangUpFromUser(qbrtcSession, integer,map);
    }

//    @Override
//    public void onReceiveHangUpFromUser(QBRTCSession qbrtcSession, Integer integer) {
//        qbrtcClientSessionCallbacks.onReceiveHangUpFromUser(qbrtcSession, integer);
//    }

    @Override
    public void onUserNoActions(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onSessionClosed(QBRTCSession qbrtcSession) {

    }

    @Override
    public void onSessionStartClose(QBRTCSession qbrtcSession) {

    }

    public class MyBinder extends Binder {
        public void setClentSessionListener(QBRTCClientSessionCallbacks qbrtcClientSessionCallbacks) {
            VideoCallService.this.qbrtcClientSessionCallbacks = qbrtcClientSessionCallbacks;
        }
    }
}
