package webservices;


import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import webservices.pojos.CommonPojo;

/**
 * Created by OM on 4/10/2016.
 */
public interface API {
    @Multipart
    @POST("json_imgUpload.php")
    Call<CommonPojo> update(@Part("image\"; filename=\"test.jpg") RequestBody file, @Part("type") RequestBody type,
                            @Query("request") String request, @Query("password") String password,
                            @Query("devicetype") String deviceType, @Query("fname") String firstName,
                            @Query("lname") String lastName, @Query("user_id") String userId,
                            @Query("phone") String phone, @Query("age") String age,
                            @Query("sex") String sex, @Query("deviceid") String deviceId,
                            @Query("dob") String dateOfBirth, @Query("passcode") String passcode,
                            @Query("parent_id") String parentId, @Query("image_url") String imageUrl);

    @Multipart
    @POST("json_imgUpload.php")
    Call<CommonPojo> updateImage(@Part("image\"; filename=\"test.jpg") RequestBody file, @Part("type") RequestBody type,
                                 @Query("request") String request, @Query("user_id") String userId);

    @Multipart
    @POST("json_imgUpload.php")
    Call<CommonPojo> register(@Part("image\"; filename=\"test.jpg") RequestBody file, @Part("type") RequestBody type,
                              @Query("request") String request, @Query("password") String password,
                              @Query("devicetype") String deviceType, @Query("fname") String firstName,
                              @Query("lname") String lastName, @Query("email") String email,
                              @Query("phone") String phone, @Query("age") String age,
                              @Query("sex") String sex, @Query("deviceid") String deviceId,
                              @Query("dob") String dateOfBirth, @Query("passcode") String passcode,
                              @Query("parent_id") String parentId, @Query("child_id") String childId);

    @Multipart
    @POST("json_imgUpload.php")
    Call<CommonPojo> editChild(@Part("image\"; filename=\"test.jpg") RequestBody file, @Part("type") RequestBody type,
                               @Query("request") String request, @Query("password") String password,
                               @Query("devicetype") String deviceType, @Query("fname") String firstName,
                               @Query("lname") String lastName, @Query("email") String email,
                               @Query("phone") String phone, @Query("age") String age,
                               @Query("sex") String sex, @Query("deviceid") String deviceId,
                               @Query("dob") String dateOfBirth, @Query("passcode") String passcode,
                               @Query("parent_id") String parentId, @Query("child_id") String childId,
                               @Query("image_url") String imageUrl);

    @POST("index.php")
    Call<CommonPojo> makeRequest(@Body RequestData requestData);
}
