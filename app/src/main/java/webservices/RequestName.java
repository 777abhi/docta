package webservices;

/**
 * Created by OM on 4/10/2016.
 */
public class RequestName {
    public static final String REQUEST_CHILD_REGISTER = "pat_child_reg";
    public static final String GET_PAT_CHILD = "pat_get_allchild_users";
    public static final String PAT_LOGOUT = "pat_Logout";
    public static final String PATIENT_LOGIN = "pat_login";
    public static final String DOC_LOGIN = "doc_login";
    public static final String PATIENT_REGISTER = "pat_reg";
    public static final String FORGOT_PASSWORD = "pat_forgot_password";
    public static final String REMOVE_FAV_CLINIC = "pat_delete_favclinic";
    public static final String REMOVE_FAV_PHARMACY = "pat_delete_favpharmacy";

    public static final String GET_ALL_CLINIC = "pat_search_clinic";
    public static final String GET_ALL_PHARMACY = "pat_get_allPharmacy";

    public static final String ADD_FAV_CLINIC = "pat_add_favclinic";
    public static final String ADD_FAV_PHARMACY = "pat_add_favpharmacy";

    public static final String GET_NEAR_BY_CLINIC = "pat_get_near_byclinics";
    public static final String GET_TIME_SLOTS = "pat_get_doctor_timing_for_apointment";
    public static final String CONFIRM_APPOINTMENT = "pat_confirm_apointment";
    public static final String DOC_HISTORY = "doc_getHistory";
    public static final String GET_VITALS = "emr_getPatVital";
    public static final String DOC_INSERT_VITAL = "emr_docInsertVital";
    public static final String GET_BMI = "get_patBMI";
    public static final String INSERT_BMI = "emr_insertPatBMIByDoc";
    public static final String GET_LAB = "emr_getPatientLabComponentsForPatient";
    public static final String PAT_GET_MEDICAL = "get_MedicalTabData";
    public static final String GET_MEDICAL_CATEGORIES = "emr_getAllMedicalCategories";
    public static final String GET_DIAGNOSES = "emr_getAllDiagnosys";
    public static final String INSERT_MEDICAL_HIST = "insert_MedicalHisDiag";
    public static final String GET_FAMILY_HISTORY = "emr_GetFamilyHistory";
    public static final String INSERT_FAMILY_HISTORY = "insert_MedicalFamilyHistory";
    public static final String GET_SURGICAL_DATA = "get_SergicalTabData";
    public static final String GET_SURGICAL_CATEGORIES = "emr_getAllSergicalCategories";
    public static final String GET_SURGICAL_SUBCATEGORY = "emr_getAllSergicalSubcat";
    public static final String GET_SOCIAL_HOSTORY = "emr_GetSocialHistory";
    public static final String INSERT_SOCIAL_HISTORY = "insert_SocialHIstory";
    public static final String GET_ALLERGIES = "emr_getAlergies";
    public static final String INSERT_SURGICAL = "insert_SergicalHIstory";
    public static final String ADD_ALLERGY = "emr_addAlergies";
    public static final String GET_MEDICATION = "get_PaTePriscription";
    public static final String GET_MEDICATION_LIST = "emr_getMedicatinList";
    public static final String GET_IMMUNIZATION = "get_PaTImmunization";
    public static final String INSERT_IMMUNIZATION = "emr_docInsertImmunization";
    public static final String GET_ALL_VACCINE = "emr_GetAllVaccines";
    public static final String GET_OVERVIEW = "emr_getOverview";
    public static final String INSERT_PRESCRIPTION = "insert_epriscription";
    public static final String GET_LAB_COMPONENTS = "emr_ListLabComponents";
    public static final String ADD_LAB_COMPONENT = "emr_addLabComponent";
    public static final String ADD_LAB_COMPONENTS = "emr_addLabComponentForPatient";
    public static final String GO_ONLINE = "doc_goOnline";
    public static final String GO_OFFLINE = "doc_goOffline";
    public static final String GET_VISIT_REQUEST = "doc_get_pending_visit_req_list";
    public static final String ACCEPT_REQUEST = "doc_appAccept";
    public static final String REQUEST_VIDEO_VISIT = "pat_save_video_visit";
    public static final String GET_AVAIL_DOC = "pat_getAvailDoc";
    public static final String CANCEl_VISIT = "pat_cancelVisit";
    public static final String GET_APPOINTMENTS = "pat_getAppointments";
    public static final String COMPLETE_VISIT = "doc_completedVisit";
    public static final String CANCEL_APPOINTMENT = "pat_CancelApointment";
    public static final String GET_VISIT_HISTORY = "pat_History";
    public static final String RATE_VIDEO_VISIT = "pat_addRating";
    public static final String DOCTOR_LOGOUT = "doc_Logout";
    public static final String GET_INSERTED_NOTES = "emr_getInsertedNotes";
    public static final String GET_DOC_RATING = "doc_getRating";

    public static final String GET_NOTE_DATA = "emr_GetNotes";
    public static final String INSERT_NOTES = "emr_insertNotes";
    public static final String EMR_PROGRESS = "pat_incrementReportViewwer";
    public static final String UPDATE_PROFILE = "pat_update";
    public static final String UPDATE_DOCTOR_PROFILE = "doctor_update_profile";
    public static final String RATE_APP = "pat_clinicrating";
    public static final String DOCTOR_FORGOT_PASSWORD = "doc_forgot_password";
    public static final String PATIENT_UPDATE_PASSWORD = "pat_update_password";
    public static final String DOC_UPDATE_PERSONAL_INFO = "doc_update_personelinfo";
    public static final String PAT_CHANGE_PASSCODE = "pat_update_passcode";
    public static final String DOCTOR_UPDATE_PASSWORD = "doc_updatePassword";
    public static final String PROFILE_PIC_UPDATE = "profilePicUpload";
    public static final String GET_TODAY_APPOINTMENTS = "pat_getcurrent_dayAppointments";
    public static final String GET_MEDICATION_CALANDER = "get_medication_calender";
    public static final String TAKE_MEDICATION = "mark_medication_taken";
    public static final String DELETE_CHILD = "pat_delete_child_user";
    public static final String DELETE_NOTIFICATION = "pat_read_notification";
    public static final String GET_DOCTOR_NOTIFICATIONS = "get_doc_notificationList";
    public static final String GET_TODAY_MEDICATION = "get_current_medication";
    public static final String GET_TODAY_VACCINE = "get_current_dayPaTImmunization";
    public static final String UPDATE_PROFILE_ = "nurse_updatePatientProfile";
    public static final String GET_APPOINTMENT_CALANDER = "get_apointment_calender";
    public static final String GET_VACCINE_CALANDER = "get_vaccine_calender";
    public static final String GET_PATIENT_NOTIFICATIONS = "get_pat_notificationList";
    public static final String DELETE_DOC_NOTIFICATION = "doc_read_notification";
    public static final String PAST_LAB_COMPONENTS = "emr_getHistoryLabComponentsForPatient";
    public static final String GET_APPOINTMENT_CLINIC = "get_appointment_clinic";
    public static final String GET_TIME_SLOTS_FOR_FOLLOW_APPOONTMENTS = "get_doc_timeslot";
    public static final String GET_FOLLOW_UP_APPOINTMENT_DOCTORS = "get_docOfClinic";
}
