package webservices;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import webservices.pojos.Common;
import webservices.pojos.PojoMedication;

/**
 * Created by OM on 4/11/2016.
 */
public class Param {
    public String email;
    public String password;
    public String deviceid;
    public String devicetype = "android";
    public String user_id;
    public String clinic_id;
    public double latitude;
    public double longitude;
    public String pharm_id;
    public String service_id;
    public String health_concern;
    public String symptom_json;
    public String ttime;
    public String doc_id;
    public String full_symptom_json;
    public String pat_id;
    public String bp;
    public String temp;
    public String pulse;
    public String o2sat;
    public String bmi;
    public String weight;
    public String height;
    public String cat_name;
    public String data;
    public String imu_name;
    public String lastdone;
    public String nextdue;
    public String c_name = "";
    public String green = "";
    public String yellow = "";
    public String red = "";
    public String unit;
    public String comp_id;
    public String comp_value;
    public List<Common> allergy = new ArrayList<>();
    public List<PojoMedication> medication = new ArrayList<>();
    public String login_side = "patient";
    public String apoint_id;
    public String status;
    public String type;
    public String appointment_date;
    public String active;
    public String list_id;
    public String msg;
    public String seconds;
    public int ccount;
    public int rec_rating;
    public int nurse_rating;
    public int doc_rating;
    public int overall_rating;
    public Object notes;
    public String login_type;
    public String new_password;
    public String fname;
    public String lname;
    public String sex;
    public String dob;
    public String speciality;
    public String medical_school;
    public String yop;
    public String aoi;
    public String hobby;
    public String passcode;
    public String old_password;
    public Object daytime;
    public Object medication_id;
    public String ethnic_group;
    public String address;
    public String marital_status;
    public String occupation;
    public String language;
    public String compare_date;
    public String child_id;
    public String notification_id;
    public String clearall;
    public String limit = "0";
    public String notification_time;
    public String diagnoses_data;
    public int visit_type;
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public String date = dateFormat.format(new Date());
    public String ddate = date;
}
