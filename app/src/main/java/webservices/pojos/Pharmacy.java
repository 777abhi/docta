package webservices.pojos;

/**
 * Created by OM on 4/16/2016.
 */
public class Pharmacy {
    private String id;
    private String name;
    private String location;
    private String lati;
    private String longi;
    private String phone;
    private String is_fav = "0";

    private String others;
    private String open_time;
    private String close_time;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return The lati
     */
    public String getLati() {
        return lati;
    }

    /**
     * @param lati The lati
     */
    public void setLati(String lati) {
        this.lati = lati;
    }

    /**
     * @return The longi
     */
    public String getLongi() {
        return longi;
    }

    /**
     * @param longi The longi
     */
    public void setLongi(String longi) {
        this.longi = longi;
    }

    /**
     * @return The others
     */
    public String getOthers() {
        return others;
    }

    /**
     * @param others The others
     */
    public void setOthers(String others) {
        this.others = others;
    }

    /**
     * @return The open_time
     */
    public String getOpen_time() {
        return open_time;
    }

    /**
     * @param open_time The open_time
     */
    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    /**
     * @return The close_time
     */
    public String getClose_time() {
        return close_time;
    }

    /**
     * @param close_time The close_time
     */
    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIsFav() {
        return is_fav;
    }

    public void setIsFav(String is_fav) {
        this.is_fav = is_fav;
    }
}
