package webservices.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OM on 5/17/2016.
 */
public class PojoNotes {
    public String id;
    public String ddate;
    public String ttime;
    public String content;
    public String doc_id;
    public String apoint_id;
    public boolean completed = true;
    public User profile;
    public List<String> listHeader = new ArrayList<>();
    public List<Object> listChild = new ArrayList<>();
    public String primary_concern;
    public String symptoms;
    public Vital vital;
    public String labs;
    public Assessment assesment;
    public String plan;
    public String symptom_json;
    public List<Common> commonList;
    public String pojoAppointmentRequest;
    public String pojoSympotoms;
}
