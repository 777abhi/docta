package webservices.pojos;

/**
 * Created by OM on 1/20/2016.
 */
public class NoteBean {
    public String primary_concern;
    public String symptoms;
    public Vital vital;
    public String labs;
    public Assessment assesment;
    public String plan;
}
