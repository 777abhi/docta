package webservices.pojos;

/**
 * Created by OM on 5/17/2016.
 */
public class Assessment {
    private String commment;
    private Common diagnose;

    public String getCommment() {
        return commment;
    }

    public void setCommment(String commment) {
        this.commment = commment;
    }

    public Common getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(Common diagnose) {
        this.diagnose = diagnose;
    }
}
