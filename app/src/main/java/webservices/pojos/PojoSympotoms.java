package webservices.pojos;

import java.util.ArrayList;

/**
 * Created by OM on 4/14/2016.
 */
public class PojoSympotoms {
    public ArrayList<String> generalSympotoms = new ArrayList<>();
    public ArrayList<String> headNeckSympotoms = new ArrayList<>();
    public ArrayList<String> chestSympotoms = new ArrayList<>();
    public ArrayList<String> gastrointestinalSympotoms = new ArrayList<>();
    public ArrayList<String> urinarySympotoms = new ArrayList<>();
    public ArrayList<String> musculoskeletalSympotoms = new ArrayList<>();
    public ArrayList<String> skinSympotoms = new ArrayList<>();

    public String otherSymptoms = "";
    public ArrayList<Integer> generalSympotomsPositions = new ArrayList<>();
    public ArrayList<Integer> headNeckSympotomsPositions = new ArrayList<>();
    public ArrayList<Integer> chestSympotomsPositions = new ArrayList<>();
    public ArrayList<Integer> gastrointestinalSympotomsPositions = new ArrayList<>();
    public ArrayList<Integer> urinarySympotomsPositions = new ArrayList<>();
    public ArrayList<Integer> musculoskeletalSympotomsPositions = new ArrayList<>();
    public ArrayList<Integer> skinSympotomsPositions = new ArrayList<>();

    public ArrayList<String> getAllSymptoms() {
        ArrayList<String> allSymptoms = new ArrayList<>();
        allSymptoms.addAll(generalSympotoms);
        allSymptoms.addAll(headNeckSympotoms);
        allSymptoms.addAll(chestSympotoms);
        allSymptoms.addAll(gastrointestinalSympotoms);
        allSymptoms.addAll(urinarySympotoms);
        allSymptoms.addAll(musculoskeletalSympotoms);
        allSymptoms.addAll(skinSympotoms);
        return allSymptoms;
    }
}
