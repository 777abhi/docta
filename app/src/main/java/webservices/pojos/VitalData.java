package webservices.pojos;

/**
 * Created by OM on 2/2/2016.
 */
public class VitalData {
    public static String bp = "";
    public static String temp = "";
    public static String pulseRate = "";
    public static String o2sat = "";
    public static String bmi = "";
    public static String sys = "";
    public static String dias = "";
    public static String weight = "";

    public static String height = "0";

    public static void clearData() {
        bp = "";
        temp = "";
        pulseRate = "";
        o2sat = "";
        bmi = "";
        sys = "";
        dias = "";
        weight = "";
        height = "0";
    }
}
