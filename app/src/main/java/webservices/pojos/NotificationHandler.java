package webservices.pojos;

/**
 * Created by OM on 5/11/2016.
 */
public class NotificationHandler {
    private String msg;
    private String msg_display;
    private String apoint_id;

    private boolean isRead = false;
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg_display() {
        return msg_display;
    }

    public void setMsg_display(String msg_display) {
        this.msg_display = msg_display;
    }

    public String getAppoint_id() {
        return apoint_id;
    }

    public void setAppoint_id(String apoint_id) {
        this.apoint_id = apoint_id;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
