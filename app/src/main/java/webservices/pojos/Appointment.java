package webservices.pojos;

/**
 * Created by OM on 5/2/2016.
 */
public class Appointment {

    private String apoint_id;
    private String medical_id;
    private String type;

    private String ddate;
    private String ttime;
    private String user_id;
    private String clinic_id;
    private String apointment_data;

    private String color_code = "";
    private String color_text;
    private String lateTime;

    private String visit_type;
    private String notes_complete;
    private User profile;
    private User doctor;// for primry doctor

    private Clinic clinic;
    private Pharmacy pharmacy;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDdate() {
        return ddate;
    }

    public void setDdate(String ddate) {
        this.ddate = ddate;
    }

    public String getTtime() {
        return ttime;
    }

    public void setTtime(String ttime) {
        this.ttime = ttime;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


    public String getApoint_id() {
        return apoint_id;
    }

    public void setApoint_id(String apoint_id) {
        this.apoint_id = apoint_id;
    }

    public String getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(String clinic_id) {
        this.clinic_id = clinic_id;
    }

    public String getApointment_data() {
        return apointment_data;
    }

    public void setApointment_data(String apointment_data) {
        this.apointment_data = apointment_data;
    }

    public User getProfile() {
        return profile;
    }

    public void setProfile(User profile) {
        this.profile = profile;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    public Pharmacy getPharmacy() {
        return pharmacy;
    }

    public void setPharmacy(Pharmacy pharmacy) {
        this.pharmacy = pharmacy;
    }

    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }

    public String getColor_code() {
        return color_code;
    }

    public void setColor_code(String color_code) {
        this.color_code = color_code;
    }

    public String getColor_text() {
        return color_text;
    }

    public void setColor_text(String color_text) {
        this.color_text = color_text;
    }

    public String getLateTime() {
        return lateTime;
    }

    public void setLateTime(String lateTime) {
        this.lateTime = lateTime;
    }

    public String getNotes_complete() {
        return notes_complete;
    }

    public void setNotes_complete(String notes_complete) {
        this.notes_complete = notes_complete;
    }

    public String getMedical_id() {
        return medical_id;
    }

    public void setMedical_id(String medical_id) {
        this.medical_id = medical_id;
    }

    public String getVisit_type() {
        return visit_type;
    }

    public void setVisit_type(String visit_type) {
        this.visit_type = visit_type;
    }
}
