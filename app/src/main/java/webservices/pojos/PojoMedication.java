package webservices.pojos;

/**
 * Created by OM on 5/10/2016.
 */
public class PojoMedication {

    public String id = "";
    public Common medication;
    public String medication_id = "";
    public String startDate = "";
    public String endDate = "";
    public String dose = "";
    public String name = "";
    public String quantity = "1";
    public String medication_quantity = "";
    public String start = "";
    public String end = "";
    public String doc_name = "";
    public String time1 = "am";
    public String time2 = "pm";
    public String dosetype = "Tablet";
    public String taken = "";
    public String doc_id;
}
