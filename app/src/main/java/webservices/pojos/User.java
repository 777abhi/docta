package webservices.pojos;

import com.quickblox.users.model.QBUser;

import java.util.List;

/**
 * Created by OM on 4/10/2016.
 */
public class User {
    public String user_id = "";
    public String parent_id = "";
    public String fname = "";
    public String lname = "";
    public String email = "";
    public String password = "";

    public String medical_id;
    public String phone = "";
    public String photo = "";
    public String medical_school = null;
    public String speciality = null;
    public String yop = null;
    public String aoi = null;
    public String hobby = null;

    public String sex = "";
    public String height = "";

    public String distance;
    public String dob = "";
    public String age = "";
    public String gender = "";

    public String clinic_id = "1";
    public String qbid = "";
    public String qb_password;
    public String occupation;
    public String marital_status;
    public String address;
    public String ethnic_group;

    public Clinic clinic;
    public String child_count = "0";
    public String passcode = "";

    public List<String> time_slot;

    public QBUser getQbUser() {
        QBUser qbUser = new QBUser();
        qbUser.setPassword(qb_password);
        qbUser.setEmail(email);
        qbUser.setLogin(email);
        qbUser.setCustomData(photo);
        qbUser.setId(Integer.parseInt(qbid));
        return qbUser;
    }

    public String getFullName() {
        return fname + " " + lname;
    }
}
