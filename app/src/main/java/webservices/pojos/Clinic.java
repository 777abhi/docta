package webservices.pojos;

/**
 * Created by OM on 12/11/2015.
 */
public class Clinic {
    private String clinic_name;
    private String pat_id;
    private String clinic_id;
    private String town;
    private String doc_id;
    private String latitude;
    private String longitude;
    private String ownership;
    private String is_favourite = "0";
    private String phone;
    private String email;

    private String clinic_open_time;
    private String clinic_close_time;

    /**
     * @return The clinic_id
     */
    public String getClinic_id() {
        return clinic_id;
    }

    /**
     * @param clinic_id The clinic_id
     */
    public void setClinic_id(String clinic_id) {
        this.clinic_id = clinic_id;
    }


    /**
     * @return The location
     */
    public String getLocation() {
        return town;
    }

    /**
     * @param town The location
     */
    public void setLocation(String town) {
        this.town = town;
    }

    /**
     * @return The doc_id
     */
    public String getDoc_id() {
        return doc_id;
    }

    /**
     * @param doc_id The doc_id
     */
    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    /**
     * @return The latitud
     */
    public String getLatitud() {
        return latitude;
    }

    /**
     * @param latitude The latitud
     */
    public void setLatitud(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The longitude
     */
    public String getLongitud() {
        return longitude;
    }

    /**
     * @param longitud The longitud
     */
    public void setLongitud(String longitud) {
        this.longitude = longitude;
    }

    /**
     * @return The open_time
     */
    public String getOpen_time() {
        return clinic_open_time;
    }

    /**
     * @param clinic_open_time The open_time
     */
    public void setOpen_time(String clinic_open_time) {
        this.clinic_open_time = clinic_open_time;
    }

    /**
     * @return The close_time
     */
    public String getClose_time() {
        return clinic_close_time;
    }

    /**
     * @param clinic_close_time The close_time
     */
    public void setClose_time(String clinic_close_time) {
        this.clinic_close_time = clinic_close_time;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public String getPat_id() {
        return pat_id;
    }

    public void setPat_id(String pat_id) {
        this.pat_id = pat_id;
    }


    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIsFav() {
        return is_favourite;
    }

    public void setIsFav(String is_favourite) {
        this.is_favourite = is_favourite;
    }
}
