package webservices.pojos;

import java.util.List;

/**
 * Created by OM on 5/8/2016.
 */
public class Common {
    //    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//    Date date = new Date();

    public Common(String name) {
        this.name = name;
    }

    public Common() {
    }

    private String id;
    private String name = "";
    private String msg;
    private String ttime;
    private boolean selected;
    private boolean isDiagnoseCategory = true;
    private boolean isHeader = false;
    private boolean isToday = false;
    private boolean isYesterDay = false;
    private boolean isFalse = false;
    private Common diagnose;
    private String ddate;//= dateFormat.format(date);
    private String medicalProblem = "";
    private String relation = "";
    private String ageOfOnset = "";
    private String nameOptional = "";
    private String status = "";
    private String comp_value;
    private Integer avg;
    private String c_name;
    private String unit;
    private String last_done;

    private String tobaccoUse = "";
    private String alcoholUse = "";
    private String drugUse = "";
    private String sexualActivity = "";

    private String doc_id;
    private String doc_name;
    private String lastdone = "";
    private String nextdue = "";
    private String pat_id;
    private String time;

    private String qty = "";
    private String time1 = "";
    private String time2 = "";
    private String dosetype = "";

    private String dose = "";
    private String start = "";
    private String end = "";

    private String green;
    private String yellow;
    private String red;
    private Common medication;
    private String content;

    private String color = "";
    private List<Common> medicationList;
    private List<Common> vaccineList;
    private List<Appointment> apointmentList;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Common> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Common> medicationList) {
        this.medicationList = medicationList;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isDiagnoseCategory() {
        return isDiagnoseCategory;
    }

    public void setIsDiagnoseCategory(boolean isDiagnoseCategory) {
        this.isDiagnoseCategory = isDiagnoseCategory;
    }

    public String getDdate() {
        return ddate;
    }

    public void setDdate(String ddate) {
        this.ddate = ddate;
    }

    public Common getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(Common diagnose) {
        this.diagnose = diagnose;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMedicalProblem() {
        return medicalProblem;
    }

    public void setMedicalProblem(String medicalProblem) {
        this.medicalProblem = medicalProblem;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getAgeOfOnset() {
        return ageOfOnset;
    }

    public void setAgeOfOnset(String ageOfOnset) {
        this.ageOfOnset = ageOfOnset;
    }

    public String getNameOptional() {
        return nameOptional;
    }

    public void setNameOptional(String nameOptional) {
        this.nameOptional = nameOptional;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTobaccoUse() {
        return tobaccoUse;
    }

    public void setTobaccoUse(String tobaccoUse) {
        this.tobaccoUse = tobaccoUse;
    }

    public String getAlcoholUse() {
        return alcoholUse;
    }

    public void setAlcoholUse(String alcoholUse) {
        this.alcoholUse = alcoholUse;
    }

    public String getDrugUse() {
        return drugUse;
    }

    public void setDrugUse(String drugUse) {
        this.drugUse = drugUse;
    }

    public String getSexualActivity() {
        return sexualActivity;
    }

    public void setSexualActivity(String sexualActivity) {
        this.sexualActivity = sexualActivity;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getPat_id() {
        return pat_id;
    }

    public void setPat_id(String pat_id) {
        this.pat_id = pat_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getDosetype() {
        return dosetype;
    }

    public void setDosetype(String dosetype) {
        this.dosetype = dosetype;
    }

    public Common getMedication() {
        return medication;
    }

    public void setMedication(Common medication) {
        this.medication = medication;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getLastdone() {
        return lastdone;
    }

    public void setLastdone(String lastdone) {
        this.lastdone = lastdone;
    }

    public String getNextdue() {
        return nextdue;
    }

    public void setNextdue(String nextdue) {
        this.nextdue = nextdue;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getComp_value() {
        return comp_value;
    }

    public void setComp_value(String comp_value) {
        this.comp_value = comp_value;
    }

    public Integer getAvg() {
        return avg;
    }

    public void setAvg(Integer avg) {
        this.avg = avg;
    }

    public String getGreen() {
        return green;
    }

    public void setGreen(String green) {
        this.green = green;
    }

    public String getYellow() {
        return yellow;
    }

    public void setYellow(String yellow) {
        this.yellow = yellow;
    }

    public String getRed() {
        return red;
    }

    public void setRed(String red) {
        this.red = red;
    }

    @Override
    public String toString() {
        return name;
    }

    public List<Appointment> getAppointmentList() {
        return apointmentList;
    }

    public void setAppointmentList(List<Appointment> apointmentList) {
        this.apointmentList = apointmentList;
    }

    public List<Common> getVaccineList() {
        return vaccineList;
    }

    public void setVaccineList(List<Common> vaccineList) {
        this.vaccineList = vaccineList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDdtae() {
        return ddate;
    }

    public void setDdtae(String ddtae) {
        this.ddate = ddtae;
    }

    public String getTtime() {
        return ttime;
    }

    public void setTtime(String ttime) {
        this.ttime = ttime;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setIsHeader(boolean isHeader) {
        this.isHeader = isHeader;
    }

    public boolean isToday() {
        return isToday;
    }

    public void setIsToday(boolean isToday) {
        this.isToday = isToday;
    }

    public boolean isFalse() {
        return isFalse;
    }

    public void setIsFalse(boolean isFalse) {
        this.isFalse = isFalse;
    }

    public boolean isYesterDay() {
        return isYesterDay;
    }

    public void setIsYesterDay(boolean isYesterDay) {
        this.isYesterDay = isYesterDay;
    }
}
