package webservices.pojos;

import java.util.HashMap;

/**
 * Created by OM on 5/17/2016.
 */
public class PrefsNotes {
    private HashMap<String, PojoNotes> prefsNotes = new HashMap<>();

    public HashMap<String, PojoNotes> getPrefsNotes() {
        return prefsNotes;
    }

    public void setPrefsNotes(HashMap<String, PojoNotes> prefsNotes) {
        this.prefsNotes = prefsNotes;
    }
}
