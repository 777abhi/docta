package webservices.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OM on 4/10/2016.
 */
public class CommonPojo {
    public String requestName = "";
    public List<Common> allergy = new ArrayList<>();
    private String status = "";
    private String user_id = "";
    private String qbid = "";
    private String qb_password = "";
    private String image = "";
    private String apoint_id;
    private String ddate;
    private String child_count = "";
    private String incomplete_notes;
    private String doc_rating;
    private List<Common> notification_list;
    private String ttime;
    private String taken = "";
    private boolean completed = true;
    private String msg;

    private float rating;
    private String maxbp = "0";
    private String minbp = "0";
    private String maxtemp = "0";
    private String mintemp = "0";
    private String min = "0";
    private String max = "0";
    private String maxpulse = "0";
    private String minpulse = "0";
    private String maxo2sat = "0";
    private String mino2sat = "0";
    private User profile;
    private List<User> users;
    private List<Pharmacy> pharmacy_list;
    private List<Pharmacy> near_pharmacy;
    private List<Clinic> clinic_list;
    private List<List<AvailableTime>> time;
    private List<PojoNotes> notes_list;
    private List<Appointment> upcoming;
    private List<Appointment> history;
    private List<Appointment> cancelled;
    private List<Appointment> today;
    private List<Appointment> clinic_visit;
    private List<Appointment> video_visit;
    private List<String> time_slots;

    private Appointment appointment;

    private Vital current_vital;
    private String primary_concern;
    private String health_concern;
    private List<Vital> history_vital;//
    private List<Vital> current_vital_list;//

    private List<Common> common_list;
    private List<Common> current_list;
    private List<Common> history_list;

    private List<PojoMedication> current_medication;
    private List<PojoMedication> history_medication;
    private Common common;

    private List<Clinic> near_clinic;
    private List<User> child_list;

    private Clinic clinic;
    private Pharmacy pharmacy;
    private String symptom_json;
    private String apointment_data;
    private String symptoms;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public User getUser() {
        return profile;
    }

    public void setUser(User profile) {
        this.profile = profile;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    public String getQbid() {
        return qbid;
    }

    public void setQbid(String qbid) {
        this.qbid = qbid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Clinic> getClinicList() {
        return clinic_list;
    }

    public void setClinicList(List<Clinic> clinic_list) {
        this.clinic_list = clinic_list;
    }

    public List<Clinic> getNear_clinic() {
        return near_clinic;
    }

    public void setNear_clinic(List<Clinic> near_clinic) {
        this.near_clinic = near_clinic;
    }

    public List<User> getChild_list() {
        return child_list;
    }

    public void setChild_list(List<User> child_list) {
        this.child_list = child_list;
    }

    public Pharmacy getPharmacy() {
        return pharmacy;
    }

    public void setPharmacy(Pharmacy pharmacy) {
        this.pharmacy = pharmacy;
    }

    public List<Pharmacy> getPharmacy_list() {
        return pharmacy_list;
    }

    public void setPharmacy_list(List<Pharmacy> pharmacy_list) {
        this.pharmacy_list = pharmacy_list;
    }

    public List<Pharmacy> getNear_pharmacy() {
        return near_pharmacy;
    }

    public void setNear_pharmacy(List<Pharmacy> near_pharmacy) {
        this.near_pharmacy = near_pharmacy;
    }

    public List<List<AvailableTime>> getTime() {
        return time;
    }

    public void setTime(List<List<AvailableTime>> time) {
        this.time = time;
    }

    public List<Appointment> getUpcoming() {
        return upcoming;
    }

    public void setUpcoming(List<Appointment> upcoming) {
        this.upcoming = upcoming;
    }

    public List<Appointment> getHistory() {
        return history;
    }

    public void setHistory(List<Appointment> history) {
        this.history = history;
    }

    public List<Appointment> getToday() {
        return today;
    }

    public void setToday(List<Appointment> today) {
        this.today = today;
    }

    public Vital getCurrent_vital() {
        return current_vital;
    }

    public void setCurrent_vital(Vital current_vital) {
        this.current_vital = current_vital;
    }

    public List<Vital> getHistory_vital() {
        return history_vital;
    }

    public void setHistory_vital(List<Vital> history_vital) {
        this.history_vital = history_vital;
    }

    public List<Common> getCommmon_list() {
        return common_list;
    }

    public void setCommmon_list(List<Common> common_list) {
        this.common_list = common_list;
    }

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }


    public List<Common> getAllergy() {
        return allergy;
    }

    public void setAllergy(List<Common> allergy) {
        this.allergy = allergy;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getPrimary_concern() {
        return primary_concern;
    }

    public void setPrimary_concern(String primary_concern) {
        this.primary_concern = primary_concern;
    }

    public List<Common> getCurrent_list() {
        return current_list;
    }

    public void setCurrent_list(List<Common> current_list) {
        this.current_list = current_list;
    }

    public List<Common> getHistory_list() {
        return history_list;
    }

    public void setHistory_list(List<Common> history_list) {
        this.history_list = history_list;
    }

    public List<PojoMedication> getCurrent_medication() {
        return current_medication;
    }

    public void setCurrent_medication(List<PojoMedication> current_medication) {
        this.current_medication = current_medication;
    }

    public List<PojoMedication> getHistory_medication() {
        return history_medication;
    }

    public void setHistory_medication(List<PojoMedication> history_medication) {
        this.history_medication = history_medication;
    }

    public List<Vital> getCurrent_vital_list() {
        return current_vital_list;
    }

    public void setCurrent_vital_list(List<Vital> current_vital_list) {
        this.current_vital_list = current_vital_list;
    }

    public String getAppoint_id() {
        return apoint_id;
    }

    public void setAppoint_id(String apoint_id) {
        this.apoint_id = apoint_id;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public List<Appointment> getCancelled() {
        return cancelled;
    }

    public void setCancelled(List<Appointment> cancelled) {
        this.cancelled = cancelled;
    }

    public List<Appointment> getClinic_visit() {
        return clinic_visit;
    }

    public void setClinic_visit(List<Appointment> clinic_visit) {
        this.clinic_visit = clinic_visit;
    }

    public List<Appointment> getVideo_visit() {
        return video_visit;
    }

    public void setVideo_visit(List<Appointment> video_visit) {
        this.video_visit = video_visit;
    }

    public List<PojoNotes> getNotes_list() {
        return notes_list;
    }

    public void setNotes_list(List<PojoNotes> notes_list) {
        this.notes_list = notes_list;
    }

    public String getHealth_concern() {
        return health_concern;
    }

    public void setHealth_concern(String health_concern) {
        this.health_concern = health_concern;
    }

    public String getSymptom_json() {
        return symptom_json;
    }

    public void setSymptom_json(String symptom_json) {
        this.symptom_json = symptom_json;
    }

    public String getDdate() {
        return ddate;
    }

    public void setDdate(String ddate) {
        this.ddate = ddate;
    }

    public String getTtime() {
        return ttime;
    }

    public void setTtime(String ttime) {
        this.ttime = ttime;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getIncomplete_notes() {
        return incomplete_notes;
    }

    public void setIncomplete_notes(String incomplete_notes) {
        this.incomplete_notes = incomplete_notes;
    }

    public String getApointment_data() {
        return apointment_data;
    }

    public void setApointment_data(String apointment_data) {
        this.apointment_data = apointment_data;
    }

    public String getTaken() {
        return taken;
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }

    public String getChild_count() {
        return child_count;
    }

    public void setChild_count(String child_count) {
        this.child_count = child_count;
    }

    public List<Common> getNotification_list() {
        return notification_list;
    }

    public void setNotification_list(List<Common> notification_list) {
        this.notification_list = notification_list;
    }

    public String getDoctorRating() {
        return doc_rating;
    }

    public void setDoctorRating(String rating) {
        this.doc_rating = rating;
    }

    public List<String> getFollowUpAppointmentTimeSlot() {
        return time_slots;
    }

    public void setTime_slot(List<String> time_slots) {
        this.time_slots = time_slots;
    }

    public String getMaxbp() {
        return maxbp;
    }

    public void setMaxbp(String maxbp) {
        this.maxbp = maxbp;
    }

    public String getMinbp() {
        return minbp;
    }

    public void setMinbp(String minbp) {
        this.minbp = minbp;
    }

    public String getMaxtemp() {
        return maxtemp;
    }

    public void setMaxtemp(String maxtemp) {
        this.maxtemp = maxtemp;
    }

    public String getMintemp() {
        return mintemp;
    }

    public void setMintemp(String mintemp) {
        this.mintemp = mintemp;
    }

    public String getMaxpulse() {
        return maxpulse;
    }

    public void setMaxpulse(String maxpulse) {
        this.maxpulse = maxpulse;
    }

    public String getMinpulse() {
        return minpulse;
    }

    public void setMinpulse(String minpulse) {
        this.minpulse = minpulse;
    }

    public String getMaxo2sat() {
        return maxo2sat;
    }

    public void setMaxo2sat(String maxo2sat) {
        this.maxo2sat = maxo2sat;
    }

    public String getMino2sat() {
        return mino2sat;
    }

    public void setMino2sat(String mino2sat) {
        this.mino2sat = mino2sat;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getQb_password() {
        return qb_password;
    }

    public void setQb_password(String qb_password) {
        this.qb_password = qb_password;
    }
}
