package webservices.pojos;

import java.util.List;

/**
 * Created by OM on 6/19/2016.
 */
public class PojoMedicationCalander {
    private String color;
    private List<Common> medicationList;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Common> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Common> medicationList) {
        this.medicationList = medicationList;
    }
}
