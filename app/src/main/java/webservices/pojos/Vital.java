package webservices.pojos;

import android.text.TextUtils;

/**
 * Created by OM on 5/7/2016.
 */
public class Vital {

    private String id;
    private String user_id;
    private String apoint_id;
    private String temp;
    private String name = "";
    private boolean isSelected = false;

    private String c_name;
    private String unit;
    private String pulse;
    private String o2sat;
    private String bp;
    private String nurse_id;
    private String nurse_name;
    private String doc_id;
    private String doc_name;
    private String qbid;
    private String ddate;
    private String pat_id;
    private String bmi_value;
    private String height;
    private String weight;
    private String sex_age_specific;
    private String green;
    private String yellow;
    private String red;
    private String comp_value;
    private String avg;
    private Common comp_detail;

    private User profile;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id The user_id
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return The apoint_id
     */
    public String getApoint_id() {
        return apoint_id;
    }

    /**
     * @param apoint_id The apoint_id
     */
    public void setApoint_id(String apoint_id) {
        this.apoint_id = apoint_id;
    }

    /**
     * @return The temp
     */
    public String getTemp() {
        return temp;
    }

    /**
     * @param temp The temp
     */
    public void setTemp(String temp) {
        this.temp = temp;
    }

    /**
     * @return The pulse
     */
    public String getPulse() {
        return TextUtils.isEmpty(pulse) ? "0" : pulse;
    }

    /**
     * @param pulse The pulse
     */
    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    /**
     * @return The o2sat
     */
    public String getO2sat() {
        return TextUtils.isEmpty(o2sat) ? "0" : o2sat;
    }

    /**
     * @param o2sat The o2sat
     */
    public void setO2sat(String o2sat) {
        this.o2sat = o2sat;
    }

    /**
     * @return The bp
     */
    public String getBp() {
        return TextUtils.isEmpty(bp) ? "0/0" : bp;
    }

    /**
     * @param bp The bp
     */
    public void setBp(String bp) {
        this.bp = bp;
    }

    /**
     * @return The nurse_id
     */
    public String getNurse_id() {
        return nurse_id;
    }

    /**
     * @param nurse_id The nurse_id
     */
    public void setNurse_id(String nurse_id) {
        this.nurse_id = nurse_id;
    }

    /**
     * @return The nurse_name
     */
    public String getNurse_name() {
        return nurse_name;
    }

    /**
     * @param nurse_name The nurse_name
     */
    public void setNurse_name(String nurse_name) {
        this.nurse_name = nurse_name;
    }

    /**
     * @return The doc_id
     */
    public String getDoc_id() {
        return doc_id;
    }

    /**
     * @param doc_id The doc_id
     */
    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    /**
     * @return The doc_name
     */
    public String getDoc_name() {
        return doc_name;
    }

    /**
     * @param doc_name The doc_name
     */
    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    /**
     * @return The qbid
     */
    public String getQbid() {
        return qbid;
    }

    /**
     * @param qbid The qbid
     */
    public void setQbid(String qbid) {
        this.qbid = qbid;
    }

    /**
     * @return The ddate
     */
    public String getDdate() {
        return ddate;
    }

    /**
     * @param ddate The ddate
     */
    public void setDdate(String ddate) {
        this.ddate = ddate;
    }

    public String getPat_id() {
        return pat_id;
    }

    public void setPat_id(String pat_id) {
        this.pat_id = pat_id;
    }

    public String getBmi_value() {
        return TextUtils.isEmpty(bmi_value) ? "0" : String.format("%.1f", Float.parseFloat(bmi_value));
    }

    public void setBmi_value(String bmi_value) {
        this.bmi_value = bmi_value;
    }

    public String getHeight() {
        return TextUtils.isEmpty(height) ? "0" : height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return TextUtils.isEmpty(weight) ? "0" : weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSex_age_specific() {
        return sex_age_specific;
    }

    public void setSex_age_specific(String sex_age_specific) {
        this.sex_age_specific = sex_age_specific;
    }

    public String getGreen() {
        return green;
    }

    public void setGreen(String green) {
        this.green = green;
    }

    public String getYellow() {
        return yellow;
    }

    public void setYellow(String yellow) {
        this.yellow = yellow;
    }

    public String getRed() {
        return red;
    }

    public void setRed(String red) {
        this.red = red;
    }

    public String getComp_value() {
        return comp_value;
    }

    public void setComp_value(String comp_value) {
        this.comp_value = comp_value;
    }

    public String getAvg() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg = avg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Common getComp_detail() {
        return comp_detail;
    }

    public void setComp_detail(Common comp_detail) {
        this.comp_detail = comp_detail;
    }

    public User getProfile() {
        return profile;
    }

    public void setProfile(User profile) {
        this.profile = profile;
    }
}