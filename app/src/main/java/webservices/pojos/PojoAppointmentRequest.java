package webservices.pojos;

/**
 * Created by OM on 12/30/2015.
 */
public class PojoAppointmentRequest {
    public String primaryHealthConcern;
    public PojoSympotoms sympotoms = new PojoSympotoms();
    public String visitType;
    private String serviceId;
    private String serviceName;
    private String time_slot;

    private String appointment_date;
    private String user_id;
    private Clinic clinic;
    private User doctor;
    private User followUpDoctor;
    private String answerStoreId;
    private boolean isFollowUp;
    private String appointmentId;
    private String visitId;
    private String whenDidYouStartFeelingThisWay;
    private boolean isSeriousHealthProblem;
    private User patient;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

    public boolean isSeriousHealthProblem() {
        return isSeriousHealthProblem;
    }

    public void setSeriousHealthProblem(boolean seriousHealthProblem) {
        isSeriousHealthProblem = seriousHealthProblem;
    }

    public String getWhenDidYouStartFeelingThisWay() {
        return whenDidYouStartFeelingThisWay;
    }

    public void setWhenDidYouStartFeelingThisWay(String whenDidYouStartFeelingThisWay) {
        this.whenDidYouStartFeelingThisWay = whenDidYouStartFeelingThisWay;
    }

    public String getTime_slot() {
        return time_slot;
    }

    public void setTime_slot(String time_slot) {
        this.time_slot = time_slot;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }

    public String getPrimaryHealthConcern() {
        return primaryHealthConcern;
    }

    public void setPrimaryHealthConcern(String primaryHealthConcern) {
        this.primaryHealthConcern = primaryHealthConcern;
    }

    public String getAnswerStoreId() {
        return answerStoreId;
    }

    public void setAnswerStoreId(String answerStoreId) {
        this.answerStoreId = answerStoreId;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public boolean isFollowUp() {
        return isFollowUp;
    }

    public void setFollowUp(boolean followUp) {
        isFollowUp = followUp;
    }

    public User getFollowUpDoctor() {
        return followUpDoctor;
    }

    public void setFollowUpDoctor(User followUpDoctor) {
        this.followUpDoctor = followUpDoctor;
    }

}
