package webservices.pojos;

import java.util.List;

/**
 * Created by OM on 4/29/2016.
 */
public class AvailableTime {
    private String time_slot;
    private List<User> doctors;

    public String getTime_slot() {
        return time_slot;
    }

    public void setTime_slot(String time_slot) {
        this.time_slot = time_slot;
    }

    public List<User> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<User> doctors) {
        this.doctors = doctors;
    }
}
