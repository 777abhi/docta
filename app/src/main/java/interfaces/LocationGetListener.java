package interfaces;

/**
 * Created by anirudhnanda on 28/11/2016 AD.
 */

public interface LocationGetListener {
    void onLocationRetrieved();
}
