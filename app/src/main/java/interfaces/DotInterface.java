package interfaces;

/**
 * Created by anirudhnanda on 13/07/16 AD.
 */
public interface DotInterface {
    void showDot();

    void stopDot();

}
