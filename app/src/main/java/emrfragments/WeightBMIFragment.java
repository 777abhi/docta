package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.TabsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class WeightBMIFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pager)
    ViewPager viewpager;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public WeightBMIFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static WeightBMIFragment newInstance(String param1, String param2) {
        WeightBMIFragment fragment = new WeightBMIFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.commonpager, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    public void onEvent(CommonEvent commonEvent) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getBmi();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        tvTitle.setText(getString(R.string.weight_bmi));
        tvNext.setText(R.string.add);
        tvNext.setCompoundDrawables(null, null, null, null);

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                String data = new Gson().toJson(result);
                TabsAdapter tabsAdapter = new TabsAdapter(getChildFragmentManager());
                tabsAdapter.addFragment(CurrentBMI.newInstance(data),
                        getString(R.string.current));
                tabsAdapter.addFragment(PastBMI.newInstance(data),
                        getString(R.string.past));
                viewpager.setAdapter(tabsAdapter);
                tabLayout.setupWithViewPager(viewpager);
            }

            @Override
            public void onFailure() {

            }
        });
        getBmi();

    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new AddBMI(), R.id.fragment_container);
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);

    }


    public void getBmi() {
        RequestData requestData = new RequestData();
        requestData.type = Constants.EMR;
        requestData.request = RequestName.GET_BMI;
        requestData.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(requestData);
    }

}
