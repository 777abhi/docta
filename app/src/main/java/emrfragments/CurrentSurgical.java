package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.EMRPatientMedicalAdappter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class CurrentSurgical extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvPageTitle)
    TextView tvPageTitle;
    @Bind(R.id.divider)
    View divider;
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;

    // TODO: Rename and change types of parameters


    private EMRPatientMedicalAdappter currentAdapter;

    public CurrentSurgical() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CurrentMedicalRecord.
     */
    // TODO: Rename and change types and number of parameters
    public static CurrentSurgical newInstance(String param1, String param2) {
        CurrentSurgical fragment = new CurrentSurgical();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                GeneralFunctions.addFragmentFromRight(
                        getFragmentManager(), new SurgicalCategoryFragment(), R.id.fragment_container);
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void onEvent(CommonEvent commonEvent) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getSurgicalData();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        tvNoData.setText(getString(R.string.no_data));
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.add));
        tvTitle.setText(getString(R.string.surgical));

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
                if (result.getCommmon_list().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    currentAdapter = new EMRPatientMedicalAdappter(getActivity(), result.getCommmon_list());
                    rvData.setAdapter(currentAdapter);
                } else tvNoData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        });
        getSurgicalData();

    }

    private void getSurgicalData() {
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.GET_SURGICAL_DATA;
        apiRequest.type = Constants.EMR;
        apiRequest.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(apiRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);

    }
}
