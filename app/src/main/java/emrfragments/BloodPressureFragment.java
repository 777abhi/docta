package emrfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import views.CircularSeekBar;
import webservices.pojos.VitalData;


public class BloodPressureFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.ivClose)
    ImageView ivClose;
    @Bind(R.id.ivCheck)
    ImageView ivCheck;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.rbSys)
    RadioButton rbSys;
    @Bind(R.id.rbDias)
    RadioButton rbDias;
    @Bind(R.id.rgSelection)
    RadioGroup rgSelection;
    @Bind(R.id.tvValue)
    TextView tvValue;
    @Bind(R.id.tvUnit)
    TextView tvUnit;
    @Bind(R.id.circularSeekBar)
    CircularSeekBar circularSeekBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private int sysBp;
    private int diasBp;

    public BloodPressureFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BloodPressureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BloodPressureFragment newInstance(String param1, String param2) {
        BloodPressureFragment fragment = new BloodPressureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @OnClick({R.id.ivCheck, R.id.ivClose})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.ivCheck:
                VitalData.bp = (sysBp + Constants.MIN_SYS) + "/" + (diasBp + Constants.MIN_DIAS);
                VitalData.sys = sysBp + Constants.MIN_SYS + "";
                VitalData.dias = diasBp + Constants.MIN_DIAS + "";
                getActivity().onBackPressed();
                break;
            case R.id.ivClose:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blood_pressure, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle.setText(getString(R.string.blood_pressure));
        sysBp = VitalData.sys.isEmpty() ? 0 : Integer.parseInt(VitalData.sys) - Constants.MIN_SYS;
        diasBp = VitalData.dias.isEmpty() ? 0 : Integer.parseInt(VitalData.dias) - Constants.MIN_DIAS;
        tvValue.setText((sysBp + Constants.MIN_SYS) + "/" + (diasBp + Constants.MIN_DIAS));
        circularSeekBar.setProgress((int) sysBp);
        circularSeekBar.setOnSeekBarChangeListener(new CircleSeekBarListener());
        rgSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbSys:
                        circularSeekBar.setMax((int) Constants.MAX_SYS);
                        circularSeekBar.setProgress((int) sysBp);
                        break;
                    case R.id.rbDias:
                        circularSeekBar.setMax((int) Constants.MAX_DIAS);
                        circularSeekBar.setProgress((int) diasBp);
                        break;
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public class CircleSeekBarListener implements CircularSeekBar.OnCircularSeekBarChangeListener {
        @Override
        public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
            // TODO Insert your code here
            switch (rgSelection.getCheckedRadioButtonId()) {
                case R.id.rbSys:
                    sysBp = progress;
                    tvValue.setText((sysBp + Constants.MIN_SYS) + "/" + (diasBp + Constants.MIN_DIAS));
                    break;
                case R.id.rbDias:
                    diasBp = progress;
                    tvValue.setText((sysBp + Constants.MIN_SYS) + "/" + (diasBp + Constants.MIN_DIAS));
                    break;
            }
        }

        @Override
        public void onStopTrackingTouch(CircularSeekBar seekBar) {

        }

        @Override
        public void onStartTrackingTouch(CircularSeekBar seekBar) {

        }
    }
}
