package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.VaccineListAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;


public class VaccinationList extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvAppointmentList)
    RecyclerView rvAppointmentList;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.etSearch)
    EditText etSearch;
    @Bind(R.id.tvDone)
    TextView tvDone;
    private VaccineListAdapter adapter;
    private List<Common> vaccineList;
    private List<Common> allList = new ArrayList<>();

    // TODO: Rename and change types of parameters

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            applyFilter(s.toString());
        }
    };

    public VaccinationList() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static VaccinationList newInstance(String param1, String param2) {
        VaccinationList fragment = new VaccinationList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvDone.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        rvAppointmentList.setLayoutManager(new LinearLayoutManager(getActivity()));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                vaccineList = result.getCommmon_list();
                allList.addAll(vaccineList);
                adapter =
                        new VaccineListAdapter(getActivity(), vaccineList);
                rvAppointmentList.setAdapter(adapter);
                etSearch.addTextChangedListener(textWatcher);
            }

            @Override
            public void onFailure() {

            }
        });

        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_ALL_VACCINE;
        requestData.type = Constants.EMR;
        requestData.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(requestData);
    }


    private void applyFilter(String s) {
        vaccineList.clear();
        if (s.isEmpty()) {
            vaccineList.addAll(allList);
            adapter.notifyDataSetChanged();
            return;
        }
        for (int i = 0; i < allList.size(); i++) {
            if (allList.get(i).getName().startsWith(s))
                vaccineList.add(allList.get(i));
            adapter.notifyDataSetChanged();

        }
    }

    @OnClick({R.id.tvDone, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvDone:
                if (adapter.selectedPosition != -1) {
                    EventBus.getDefault().post(vaccineList.get(adapter.selectedPosition));
                    getActivity().onBackPressed();
                }
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medication_list, container, false);
        ButterKnife.bind(this, view);
        tvNext.setVisibility(View.GONE);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
