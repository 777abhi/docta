package emrfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CurrentImaging extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam2;


    public CurrentImaging() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CurrentImaging newInstance(String param1, String param2) {
        CurrentImaging fragment = new CurrentImaging();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           /* vitals = new Gson().fromJson(getArguments().getString(ARG_PARAM1), Vitals.class);
            mParam2 = getArguments().getString(ARG_PARAM2);

            List<Vital> commons = new ArrayList<>();
            if (mParam2.equals(Constants.COMMON))
                commons = vitals.getCurrent();
            else commons = vitals.getHistory();
            for (int i = 0; i < commons.size(); i++) {
                Vital vital = commons.get(i);
                try {
                    JSONArray jsonArray = new JSONArray(vital.getContent());
                    for (int k = 0; k < jsonArray.length(); k++) {
                        data.add(new Gson().fromJson(jsonArray.get(k).
                                toString(), Common.class));
                        data.get(data.size() - 1).
                                setDoc_name(vital.getDoc_name());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }*/
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));

//        if (mParam2.equals(Constants.COMMON))
//            rvMedication.setAdapter(new PatientLabTestAdapter(getActivity(), mParam2, data));
//        else
//            rvMedication.setAdapter(new PatientLabTestAdapter(getActivity(), mParam2, data));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recycler_only_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


}
