package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.EMRPatientMedicalAdappter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class CurrentMedicalRecord extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvPageTitle)
    TextView tvPageTitle;
    @Bind(R.id.divider)
    View divider;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    private String mParam1;

    // TODO: Rename and change types of parameters
    private EMRPatientMedicalAdappter medicalAdapter;

    public CurrentMedicalRecord() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CurrentMedicalRecord newInstance(String param1, String param2) {
        CurrentMedicalRecord fragment = new CurrentMedicalRecord();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                GeneralFunctions.addFragmentFromRight(
                        getFragmentManager(), new EMRMedicalCategory(), R.id.fragment_container);
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNoData.setText(getString(R.string.no_data));
        tvNext.setText(getString(R.string.add));
        tvTitle.setText(getString(R.string.medical));
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            tvNext.setVisibility(View.GONE);
        }
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
                if (result.getCommmon_list().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    medicalAdapter = new EMRPatientMedicalAdappter(getActivity(), result.getCommmon_list());
                    rvData.setAdapter(medicalAdapter);
                } else tvNoData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        });
        getMedicalRecord();
    }

    private void getMedicalRecord() {
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.PAT_GET_MEDICAL;
        apiRequest.type = Constants.EMR;
        if (mParam1 != null)
            apiRequest.parameters.pat_id = loggedInUser.user_id;
        else
            apiRequest.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(apiRequest);
    }

    public void onEvent(CommonEvent e) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getMedicalRecord();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
