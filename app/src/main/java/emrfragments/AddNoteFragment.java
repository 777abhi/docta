package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.NotesExpandableAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Assessment;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;
import webservices.pojos.NoteBean;
import webservices.pojos.PojoAppointmentRequest;
import webservices.pojos.PojoNotes;
import webservices.pojos.PojoSympotoms;
import webservices.pojos.PrefsNotes;
import webservices.pojos.Vital;


public class AddNoteFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvDoctorName)
    TextView tvDoctorName;
    @Bind(R.id.tvDoctorDesignationAndClinic)
    TextView tvDoctorDesignationAndClinic;
    @Bind(R.id.elvNotesOptions)
    ExpandableListView elvNotesOptions;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    private NotesExpandableAdapter notesExpandableAdapter;
    private PojoSympotoms pojoSympotoms;
    private List<String> listHeader;
    private List<Object> listChild = new ArrayList<>();
    private PojoAppointmentRequest pojoAppointmentRequest;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CommonPojo commonPojo;

    public AddNoteFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AddNoteFragment newInstance(String param1, String param2) {
        AddNoteFragment fragment = new AddNoteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @OnClick({R.id.tvBack, R.id.tvSave, R.id.tvDone})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvSave:
                PrefsNotes prefsNotes = userPrefsManager.getNotes();
                if (prefsNotes == null)
                    prefsNotes = new PrefsNotes();

                if (prefsNotes.getPrefsNotes().get(getAppointment().getUser_id()) != null) {
                    prefsNotes.getPrefsNotes().remove(getAppointment().getUser_id());
                }
                PojoNotes pojoNotes = new PojoNotes();
                pojoNotes.listChild = listChild;
                pojoNotes.listHeader = listHeader;
                pojoNotes.pojoAppointmentRequest = new Gson().toJson(pojoAppointmentRequest);
                pojoNotes.pojoSympotoms = new Gson().toJson(pojoSympotoms);
                pojoNotes.commonList = commonPojo.getCommmon_list();
                pojoNotes.profile = loggedInUser;
                pojoNotes.completed = false;
                pojoNotes.ddate = getDate();
                pojoNotes.ttime = getTime().toLowerCase();
                prefsNotes.getPrefsNotes().put(getAppointment().getUser_id(), pojoNotes);
                userPrefsManager.saveNotes(prefsNotes);
                showToast(getString(R.string.saved));
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                getActivity().onBackPressed();
                break;
            case R.id.tvDone:
                if (notesExpandableAdapter.checkCounter != listHeader.size()) {
                    showToast(R.string.please_select_all);
                    return;
                }
                NoteBean noteBean = new NoteBean();
                noteBean.primary_concern = (String) listChild.get(0);
                noteBean.symptoms = (String) listChild.get(1);
                noteBean.vital = (Vital) listChild.get(2);
                noteBean.labs = (String) listChild.get(3);
                noteBean.assesment = (Assessment) listChild.get(4);
                noteBean.plan = (String) listChild.get(5);

                pojoAppointmentRequest.primaryHealthConcern = noteBean.primary_concern;
                pojoAppointmentRequest.sympotoms = pojoSympotoms;
                RequestData apiRequest = new RequestData();
                apiRequest.type = Constants.EMR;
                apiRequest.request = RequestName.INSERT_NOTES;
                apiRequest.parameters.pat_id = getAppointment().getUser_id();
                apiRequest.parameters.health_concern = noteBean.primary_concern;
                apiRequest.parameters.doc_id = loggedInUser.user_id;
                apiRequest.parameters.ttime = getTime().toLowerCase();
                apiRequest.parameters.data = new Gson().toJson(noteBean);
                if (notesExpandableAdapter.selectedDiagnoses != null) {
                    Common category = new Common();
                    category.setDiagnose(notesExpandableAdapter.selectedDiagnoses);
                    apiRequest.parameters.diagnoses_data = new Gson().toJson(category);
                } else {
                    apiRequest.parameters.diagnoses_data = "";
                }
                apiRequest.parameters.ttime = getTime();
                apiRequest.parameters.type = getAppointment().getType();
                apiRequest.parameters.symptom_json = new Gson().toJson(pojoSympotoms);
                apiRequest.parameters.full_symptom_json = new Gson().toJson(pojoAppointmentRequest);
                apiRequest.parameters.apoint_id = getAppointment().getApoint_id();
                makeRequest(apiRequest);
                break;
        }
    }

    public void onEvent(PojoSympotoms pojoSympotoms) {
        this.pojoSympotoms = pojoSympotoms;
        String symptomsJson = getSymptoms(pojoSympotoms);
        listChild.set(1, symptomsJson);
        notesExpandableAdapter.setSymptomsJson(symptomsJson);
        notesExpandableAdapter.setSymptoms(new Gson().toJson(pojoSympotoms));
        notesExpandableAdapter.notifyDataSetChanged();
    }

    public void onEvent(Vital vital) {
        Common component = vital.getComp_detail();
        String lab = listChild.get(3) == null ? "" : listChild.get(3).toString();
        lab += "\n\n" + component.getC_name() + "\n" + vital.getComp_value() + " " +
                (vital.getUnit().equals(getString(R.string.no_unit)) ?
                        " " : " " + vital.getUnit());
        listChild.set(3, lab);
        notesExpandableAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        EventBus.getDefault().register(this);

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                switch (result.requestName) {
                    case RequestName.INSERT_NOTES:
                        showToast(R.string.note_added);
                        EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                        getActivity().onBackPressed();
                        break;
                    case RequestName.GET_NOTE_DATA:

                        commonPojo = result;
                        pojoAppointmentRequest = new Gson().fromJson(commonPojo.getApointment_data(), PojoAppointmentRequest.class);
                        listHeader = new ArrayList<>();
                        listHeader.add(getString(R.string.chief_complaint));//et
                        listHeader.add(getString(R.string.current_symptoms));//et
                        listHeader.add(getString(R.string.vitals));//vital
                        listHeader.add(getString(R.string.labs_imaging));//et
                        listHeader.add(getString(R.string.assessment));//dignose
                        listHeader.add(getString(R.string.plan));//et


                        listChild.add(result.getHealth_concern());
                        pojoSympotoms = new Gson().fromJson(result.getSymptom_json(), PojoSympotoms.class);
                        listChild.add(pojoSympotoms == null ? "" : getSymptoms(pojoSympotoms));
                        listChild.add(result.getCurrent_vital());
                        String lab = "";
                        for (int i = 0; i < result.getCurrent_vital_list().size(); i++) {

                            Vital vital = result.getCurrent_vital_list().get(i);
                            Common component = vital.getComp_detail();
                            if (i == result.getCurrent_vital_list().size() - 1)
                                lab += component.getC_name() + "\n" + vital.getComp_value() + " " +
                                        (vital.getUnit().equals(getString(R.string.no_unit)) ?
                                                " " : " " + vital.getUnit());
                            else
                                lab += component.getC_name() + "\n" + vital.getComp_value() +
                                        (vital.getUnit().equals(getString(R.string.no_unit)) ?
                                                "" : " " + vital.getUnit()) + "\n\n";
                        }
                        listChild.add(lab);
                        Assessment assessment = new Assessment();
//                        if (result.getCommmon_list().size() > 0)
//                            assessment.setDiagnose(result.getCommmon_list().get(0)); // modified
                        listChild.add(assessment);
                        listChild.add("");
                        notesExpandableAdapter = new NotesExpandableAdapter(getActivity(), listHeader
                                , result.getCommmon_list(), listChild, result.getSymptom_json(), false);
                        elvNotesOptions.setAdapter(notesExpandableAdapter);

                        break;
                }
            }

            @Override
            public void onFailure() {

            }
        });

        elvNotesOptions.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    elvNotesOptions.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        tvDoctorName.setText(loggedInUser.getFullName() + "," + loggedInUser.speciality);
        tvDoctorDesignationAndClinic.setText(loggedInUser.clinic.getClinic_name());
        elvNotesOptions.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        tvBack.setText(getString(R.string.back));
        tvNext.setText(getString(R.string.save));
        tvNext.setCompoundDrawables(null, null, null, null);

        RequestData requestData = new RequestData();
        requestData.type = Constants.EMR;
        requestData.request = RequestName.GET_NOTE_DATA;
        requestData.parameters.apoint_id = getAppointment().getApoint_id();
        requestData.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(requestData);
    }


    private String getSymptoms(PojoSympotoms pojoSympotoms) {
        String symptoms = "";
        if (!TextUtils.isEmpty(pojoSympotoms.otherSymptoms)) {
            return pojoSympotoms.otherSymptoms;
        }

        ArrayList<String> symptomList = pojoSympotoms.generalSympotoms;
        for (int i = 0; i < symptomList.size(); i++) {
            symptoms += symptomList.get(i) + "\n";
        }
        symptomList = pojoSympotoms.headNeckSympotoms;
        for (int i = 0; i < symptomList.size(); i++) {
            symptoms += symptomList.get(i) + "\n";
        }
        symptomList = pojoSympotoms.chestSympotoms;
        for (int i = 0; i < symptomList.size(); i++) {
            symptoms += symptomList.get(i) + "\n";
        }
        symptomList = pojoSympotoms.gastrointestinalSympotoms;
        for (int i = 0; i < symptomList.size(); i++) {
            symptoms += symptomList.get(i) + "\n";
        }
        symptomList = pojoSympotoms.urinarySympotoms;
        for (int i = 0; i < symptomList.size(); i++) {
            symptoms += symptomList.get(i) + "\n";
        }
        symptomList = pojoSympotoms.musculoskeletalSympotoms;
        for (int i = 0; i < symptomList.size(); i++) {
            symptoms = symptomList.get(i) + "\n";
        }
        symptomList = pojoSympotoms.skinSympotoms;
        for (int i = 0; i < symptomList.size(); i++) {
            symptoms += symptomList.get(i) + "\n";
        }
        return symptoms;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_note, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
