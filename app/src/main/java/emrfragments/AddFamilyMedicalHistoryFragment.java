package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;

public class AddFamilyMedicalHistoryFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.etMidicalProblem)
    EditText etMidicalProblem;
    @Bind(R.id.erRelation)
    EditText erRelation;
    @Bind(R.id.etAge)
    EditText etAge;
    @Bind(R.id.etName)
    EditText etName;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.rgSelection)
    RadioGroup rgSelection;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private Common data;

    public AddFamilyMedicalHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddFamilyMedicalHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddFamilyMedicalHistoryFragment newInstance(String param1, String param2) {
        AddFamilyMedicalHistoryFragment fragment = new AddFamilyMedicalHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_family_medical_history, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setText(getString(R.string.done));
        tvNext.setCompoundDrawables(null, null, null, null);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                showToast(R.string.successful);
                etMidicalProblem.setText("");
                etAge.setText("");
                etName.setText("");
                erRelation.setText("");
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                if (etMidicalProblem.getText().toString().isEmpty()) {
                    showToast(R.string.please_medical_problem);
                    return;
                } else if (erRelation.getText().toString().isEmpty()) {
                    showToast(R.string.please_enter_relation);
                    return;
                } else if (etAge.getText().toString().isEmpty()) {
                    showToast(R.string.please_enter_age);
                    return;
                }
                Common data = new Common();
                data.setMedicalProblem(etMidicalProblem.getText().toString());
                data.setRelation(erRelation.getText().toString());
                data.setAgeOfOnset(etAge.getText().toString());
                data.setNameOptional(etName.getText().toString());
                data.setDdate(getDate());
                switch (rgSelection.getCheckedRadioButtonId()) {
                    case R.id.rbAlive:
                        data.setStatus(getString(R.string.alive));
                        break;
                    case R.id.rbDeceased:
                        data.setStatus(getString(R.string.deceased));
                        break;
                }
                hideKeyBoard(etAge);
                RequestData apiRequest = new RequestData();
                apiRequest.request = RequestName.INSERT_FAMILY_HISTORY;
                apiRequest.type = Constants.EMR;
                apiRequest.parameters.pat_id = getAppointment().getUser_id();
                apiRequest.parameters.doc_id = loggedInUser.user_id;
                apiRequest.parameters.data = new Gson().toJson(data);
                makeRequest(apiRequest);
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
