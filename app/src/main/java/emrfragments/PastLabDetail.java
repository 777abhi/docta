package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.PatientLabAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;

public class PastLabDetail extends BaseFragment implements PatientLabAdapter.GraphInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String componentId;


    public PastLabDetail() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PastLabDetail newInstance(String componentId) {
        PastLabDetail fragment = new PastLabDetail();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, componentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            componentId = getArguments().getString(ARG_PARAM1);
        }
        tvNoData.setText(getString(R.string.no_data));
        tvNext.setVisibility(View.INVISIBLE);
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.getCurrent_vital_list().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    rvData.setAdapter(new PatientLabAdapter(Constants.EMR, PastLabDetail.this,
                            Constants.PAST, result.getCurrent_vital_list()));
                } else tvNoData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
        RequestData requestData = new RequestData();
        requestData.request = RequestName.PAST_LAB_COMPONENTS;
        requestData.type = Constants.EMR;
        requestData.parameters.pat_id = userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT) ?
                loggedInUser.user_id : getAppointment().getUser_id();
        requestData.parameters.comp_id = componentId;
        makeRequest(requestData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @Override
    public void moreClicked(int position) {

    }
}
