package emrfragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;


public class AddImmunizationFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvVaccine)
    TextView tvVaccine;
    @Bind(R.id.tvLastDone)
    TextView tvLastDone;
    @Bind(R.id.tvNextDue)
    TextView tvNextDue;

    private String vaccineName = "";
    private int viewId;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private int year = 2016, month = 0, day = 1;
    private String date = "1980-01-01";


    @SuppressLint("SimpleDateFormat")
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @SuppressLint("SimpleDateFormat")
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            displayDate();

        }
    };

    public AddImmunizationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddVitalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddImmunizationFragment newInstance(String param1, String param2) {
        AddImmunizationFragment fragment = new AddImmunizationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void displayDate() {
        //date = day + "-" + (month + 1) + "-" + year;
        date = year + "-" + (month + 1) + "-" + day;

        switch (viewId) {
            case R.id.tvLastDone:
                tvLastDone.setText(date);
                break;
            case R.id.tvNextDue:
                tvNextDue.setText(date);
                break;

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        tvNext.setVisibility(View.GONE);
        pBaseBar = pBar;

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_immunization, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public void onEvent(Common medication) {
        vaccineName = medication.getName();
        tvVaccine.setText(vaccineName);
    }

    @OnClick({R.id.tvDone, R.id.tvBack, R.id.tvVaccine, R.id.tvLastDone, R.id.tvNextDue})
    public void click(View view) {
        DatePickerDialog datedial;
        switch (view.getId()) {
            case R.id.tvVaccine:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new VaccinationList(), R.id.fragment_container);
                break;
            case R.id.tvLastDone:
                viewId = R.id.tvLastDone;
                datedial = new DatePickerDialog(getActivity(),
                        datePickerListener, year, month, day);
                datedial.getDatePicker().setMinDate(0);
                datedial.show();
                break;
            case R.id.tvNextDue:
                viewId = R.id.tvNextDue;
                datedial = new DatePickerDialog(getActivity(),
                        datePickerListener, year, month, day);
                datedial.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datedial.show();
                break;
            case R.id.tvDone:
                if (tvVaccine.getText().toString().isEmpty() || tvLastDone.getText().toString().isEmpty() ||
                        tvNextDue.getText().toString().isEmpty()) {
                    showToast(R.string.fill_all_fields);
                    return;
                }
                hideKeyBoard(tvVaccine);

                RequestData requestData = new RequestData();
                requestData.request = RequestName.INSERT_IMMUNIZATION;
                requestData.type = Constants.EMR;
                requestData.parameters.pat_id = getAppointment().getUser_id();
                requestData.parameters.doc_id = loggedInUser.user_id;
                requestData.parameters.imu_name = tvVaccine.getText().toString();
                requestData.parameters.nextdue = tvNextDue.getText().toString();
                requestData.parameters.lastdone = tvLastDone.getText().toString();
                makeRequest(requestData);
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
