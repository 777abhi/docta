package emrfragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.facebook.drawee.view.SimpleDraweeView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;
import webservices.pojos.User;

public class EMRPatientProfile extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.profilePic)
    SimpleDraweeView profilePic;
    @Bind(R.id.tvPatientName)
    TextView tvPatientName;
    @Bind(R.id.tvMedicalId)
    TextView tvMedicalId;
    @Bind(R.id.tvSex)
    TextView tvSex;
    @Bind(R.id.etHeight)
    EditText etHeight;
    @Bind(R.id.etLanguage)
    EditText etLanguage;
    @Bind(R.id.etOccupation)
    EditText etOccupation;
    @Bind(R.id.etMaritalStatus)
    EditText etMaritalStatus;
    @Bind(R.id.etEthnicGroup)
    EditText etEthnicGroup;
    @Bind(R.id.etAddress)
    EditText etAddress;
    @Bind(R.id.tvBirthday)
    TextView tvBirthday;
    @Bind(R.id.tvPhone)
    TextView tvPhone;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.llRootClinic)
    LinearLayout llRootClinic;
    @Bind(R.id.llRootPersonal)
    LinearLayout llRootPersonal;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    @Bind(R.id.tvDoctorName)
    TextView tvDoctorName;
    @Bind(R.id.tvPrimaryClinic)
    TextView tvPrimaryClinic;
    @Bind(R.id.tvPrimaryPharmacy)
    TextView tvPrimaryPharmacy;

    private User patientDetail;
    TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId,
                                      KeyEvent event) {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /* handle action here */
                handled = true;
                updateProfile();
            }
            return handled;
        }
    };
    private Appointment appointment;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public EMRPatientProfile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EMRPatientProfile.
     */
    // TODO: Rename and change types and number of parameters
    public static EMRPatientProfile newInstance(String param1, String param2) {
        EMRPatientProfile fragment = new EMRPatientProfile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_emrpatient_profile, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @OnClick({R.id.cbUpdateHeight, R.id.cbUpdateAddress, R.id.cbUpdateEthnicGroup, R.id.cbUpdateMaritalStatus,
            R.id.cbUpdateOccupation, R.id.ivCallPrimaryClinic, R.id.ivCallPrimaryDoctor, R.id.ivCallPrimaryPharmacy,
            R.id.ivEmail, R.id.ivUpdateLanguage})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.cbUpdateHeight:
                etHeight.setSelection(etHeight.getText().length());
                etHeight.setEnabled(true);
                etHeight.requestFocus();
                showKeyBoard(etHeight);
                break;
            case R.id.ivUpdateLanguage:
                etLanguage.setSelection(etLanguage.getText().length());
                etLanguage.setEnabled(true);
                etLanguage.requestFocus();
                showKeyBoard(etLanguage);
                break;
            case R.id.cbUpdateAddress:
                etAddress.setSelection(etAddress.getText().length());
                etAddress.setEnabled(true);
                etAddress.requestFocus();
                showKeyBoard(etAddress);
                break;
            case R.id.cbUpdateEthnicGroup:
                etEthnicGroup.setSelection(etEthnicGroup.getText().length());
                etEthnicGroup.setEnabled(true);
                etEthnicGroup.requestFocus();
                showKeyBoard(etEthnicGroup);
                break;
            case R.id.cbUpdateMaritalStatus:
                etMaritalStatus.setSelection(etMaritalStatus.getText().length());
                etMaritalStatus.setEnabled(true);
                etMaritalStatus.requestFocus();
                showKeyBoard(etMaritalStatus);
                break;
            case R.id.cbUpdateOccupation:
                etOccupation.setSelection(etOccupation.getText().length());
                etOccupation.setEnabled(true);
                etOccupation.requestFocus();
                showKeyBoard(etOccupation);
                break;
            case R.id.ivCallPrimaryClinic:
                call(appointment.getClinic() != null ?
                        appointment.getClinic().getPhone() : "");
                break;
            case R.id.ivCallPrimaryDoctor:
                call(appointment.getDoctor() != null ?
                        appointment.getDoctor().phone : "");
                break;
            case R.id.ivCallPrimaryPharmacy:
                call(appointment.getPharmacy() != null ?
                        appointment.getPharmacy().getPhone() : "");
                break;
            case R.id.ivEmail:
                sendEmail(appointment.getDoctor() != null ?
                        appointment.getDoctor().email : "");
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        appointment = getAppointment();
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.personal)), true);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.clinical)));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                if (pos == 0) {
                    llRootPersonal.setVisibility(View.VISIBLE);
                    llRootClinic.setVisibility(View.GONE);
                } else {
                    llRootPersonal.setVisibility(View.GONE);
                    llRootClinic.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
       /* try {
            ((CallActivity) getActivity()).pojoPatientProfile = response.body();
        } catch (Exception e) {
            ((EMRActivity) getActivity()).pojoPatientProfile = response.body();
        }*/
        patientDetail = appointment.getProfile();
        tvPatientName.setText(patientDetail.getFullName());
        tvMedicalId.setText(getString(R.string.medical_id) + " " +
                patientDetail.medical_id);
        profilePic.setImageURI(Uri.parse(patientDetail.photo));
        tvSex.setText(patientDetail.sex);
        etHeight.setText("");
        etHeight.invalidate();
        etHeight.setText(patientDetail.height + " m");
        etHeight.invalidate();
        tvBirthday.setText(patientDetail.dob);
        etOccupation.setText(patientDetail.occupation);
        etMaritalStatus.setText(patientDetail.marital_status);
        etEthnicGroup.setText(patientDetail.ethnic_group);
        etAddress.setText(patientDetail.address);
        tvPhone.setText(patientDetail.phone);
        tvDoctorName.setText("dobut here");
        tvPrimaryClinic.setText(appointment.getClinic() == null ? null : appointment.getClinic().getClinic_name());
        tvPrimaryPharmacy.setText(appointment.getPharmacy() == null ? null : appointment.getPharmacy().getName());

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                Appointment appointment = getAppointment();
                appointment.setProfile(patientDetail);
                userPrefsManager.saveAppointment(appointment);
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
            }

            @Override
            public void onFailure() {

            }
        });

        etLanguage.setOnEditorActionListener(editorActionListener);
        etHeight.setOnEditorActionListener(editorActionListener);
        etOccupation.setOnEditorActionListener(editorActionListener);
        etMaritalStatus.setOnEditorActionListener(editorActionListener);
        etEthnicGroup.setOnEditorActionListener(editorActionListener);
        etAddress.setOnEditorActionListener(editorActionListener);

    }

    @Override
    public void onResume() {
        super.onResume();
        etHeight.setText(patientDetail.height + " m");
    }

    private void updateProfile() {
        patientDetail.height = etHeight.getText().toString();
        patientDetail.address = etAddress.getText().toString();
        patientDetail.ethnic_group = etEthnicGroup.getText().toString();
        patientDetail.marital_status = etMaritalStatus.getText().toString();
        patientDetail.occupation = etOccupation.getText().toString();

        Appointment appointment_ = getAppointment();
        appointment_.setProfile(patientDetail);
        userPrefsManager.saveAppointment(appointment_);
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.UPDATE_PROFILE_;
        apiRequest.type = Constants.USER_TYPE_NURSE;

        apiRequest.parameters.clinic_id = loggedInUser.clinic_id;
        apiRequest.parameters.user_id = loggedInUser.user_id;
        apiRequest.parameters.pat_id = patientDetail.user_id;

        apiRequest.parameters.height = TextUtils.isEmpty(etHeight.getText()) ?
                "0" : etHeight.getText().toString().replace(" m", "");
        apiRequest.parameters.language = etLanguage.getText().toString();

        apiRequest.parameters.ethnic_group = etEthnicGroup.getText().toString();
        apiRequest.parameters.address = etAddress.getText().toString();
        apiRequest.parameters.marital_status = etMaritalStatus.getText().toString();
        apiRequest.parameters.occupation = etOccupation.getText().toString();

        makeRequest(apiRequest);
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void call(final String phone) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.make_call)
                .theme(Theme.LIGHT)
                .content(getString(R.string.are_you_sure_you_wanr_to_call) + " " + phone)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        Intent in = new Intent(Intent.ACTION_CALL, Uri.parse(phone));
                        try {
                            startActivity(in);
                        } catch (ActivityNotFoundException ex) {
                        }
                    }
                })
                .show();

    }

    protected void sendEmail(String email) {
        String[] TO = {email};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Query");
        //emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (ActivityNotFoundException ex) {
        }
    }
}
