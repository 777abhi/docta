package emrfragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import views.HyphenedTextView;
import webservices.pojos.PojoSympotoms;

public class ChooseSymptoms extends BaseFragment implements HyphenedTextView.HtvClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.gridGeneralSymptomms)
    GridLayout gridGeneralSymptomms;
    @Bind(R.id.gridHeadNeck)
    GridLayout gridHeadNeck;
    @Bind(R.id.gridChest)
    GridLayout gridChest;
    @Bind(R.id.gridGastrointestinal)
    GridLayout gridGastrointestinal;
    @Bind(R.id.gridUrinary)
    GridLayout gridUrinary;
    @Bind(R.id.gridMusculoskeletal)
    GridLayout gridMusculoskeletal;
    @Bind(R.id.gridSkin)
    GridLayout gridSkin;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.etOtherSymptom)
    EditText etOtherSymptom;
    private int[] arr;
    private int max;
    // TODO: Rename and change types of parameters
    private PojoSympotoms pojoSympotoms;

    public ChooseSymptoms() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ChooseSymptoms newInstance(String param1) {
        ChooseSymptoms fragment = new ChooseSymptoms();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public static int getMax(int[] inputArray) {
        int maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_choose_symptoms, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        arr = new int[]{gridGeneralSymptomms.getChildCount(),
                gridHeadNeck.getChildCount(),
                gridChest.getChildCount(),
                gridGastrointestinal.getChildCount(),
                gridUrinary.getChildCount(),
                gridMusculoskeletal.getChildCount(),
                gridSkin.getChildCount()};
        max = getMax(arr);
        return view;
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:

                pojoSympotoms.skinSympotoms.clear();
                pojoSympotoms.musculoskeletalSympotoms.clear();
                pojoSympotoms.urinarySympotoms.clear();
                pojoSympotoms.gastrointestinalSympotoms.clear();
                pojoSympotoms.chestSympotoms.clear();
                pojoSympotoms.generalSympotoms.clear();
                pojoSympotoms.headNeckSympotoms.clear();


                pojoSympotoms.skinSympotomsPositions.clear();
                pojoSympotoms.musculoskeletalSympotomsPositions.clear();
                pojoSympotoms.urinarySympotomsPositions.clear();
                pojoSympotoms.gastrointestinalSympotomsPositions.clear();
                pojoSympotoms.chestSympotomsPositions.clear();
                pojoSympotoms.generalSympotomsPositions.clear();
                pojoSympotoms.headNeckSympotomsPositions.clear();

                if (!TextUtils.isEmpty(etOtherSymptom.getText())) {
                    pojoSympotoms.otherSymptoms = etOtherSymptom.getText().toString();
                    EventBus.getDefault().post(pojoSympotoms);
                    getFragmentManager().beginTransaction().remove(this).commit();
                    return;
                }
                pojoSympotoms.otherSymptoms = "";
                for (int i = 0; i < max; i++) {
                    if (i < arr[0] && ((CheckedTextView) gridGeneralSymptomms.getChildAt(i)).isChecked()) {
                        pojoSympotoms.generalSympotoms.add(
                                ((CheckedTextView) gridGeneralSymptomms.getChildAt(i)).getText().toString());
                        pojoSympotoms.generalSympotomsPositions.add(i);
                    }
                    if (i < arr[1] && ((CheckedTextView) gridHeadNeck.getChildAt(i)).isChecked()) {
                        pojoSympotoms.headNeckSympotoms.add(
                                ((CheckedTextView) gridHeadNeck.getChildAt(i)).getText().toString());
                        pojoSympotoms.headNeckSympotomsPositions.add(i);
                    }
                    if (i < arr[2] && ((CheckedTextView) gridChest.getChildAt(i)).isChecked()) {
                        pojoSympotoms.chestSympotoms.add(
                                ((CheckedTextView) gridChest.getChildAt(i)).getText().toString());
                        pojoSympotoms.chestSympotomsPositions.add(i);
                    }
                    if (i < arr[3] && ((CheckedTextView) gridGastrointestinal.getChildAt(i)).isChecked()) {
                        pojoSympotoms.gastrointestinalSympotoms.add(
                                ((CheckedTextView) gridGastrointestinal.getChildAt(i)).getText().toString());
                        pojoSympotoms.gastrointestinalSympotomsPositions.add(i);
                    }

                    if (i < arr[4] && ((CheckedTextView) gridUrinary.getChildAt(i)).isChecked()) {
                        pojoSympotoms.urinarySympotoms.add(
                                ((CheckedTextView) gridUrinary.getChildAt(i)).getText().toString());
                        pojoSympotoms.urinarySympotomsPositions.add(i);
                    }

                    if (i < arr[5] && ((CheckedTextView) gridMusculoskeletal.getChildAt(i)).isChecked()) {
                        pojoSympotoms.musculoskeletalSympotoms.add(
                                ((CheckedTextView) gridMusculoskeletal.getChildAt(i)).getText().toString());
                        pojoSympotoms.musculoskeletalSympotomsPositions.add(i);
                    }
                    if (i < arr[6] && ((CheckedTextView) gridSkin.getChildAt(i)).isChecked()) {
                        pojoSympotoms.skinSympotoms.add(
                                ((CheckedTextView) gridSkin.getChildAt(i)).getText().toString());
                        pojoSympotoms.skinSympotomsPositions.add(i);
                    }
                }
                EventBus.getDefault().post(pojoSympotoms);
                getFragmentManager().beginTransaction().remove(this).commit();
                break;
        }
    }

    private void addSymptoms(PojoSympotoms pojoSympotoms) {
        ArrayList<Integer> symptomList = pojoSympotoms.generalSympotomsPositions;
        for (int i = 0; i < symptomList.size(); i++) {
            CheckedTextView checkedTextView = (CheckedTextView) gridGeneralSymptomms.getChildAt(symptomList.get(i));
            checkedTextView.setChecked(true);
        }

        symptomList = pojoSympotoms.headNeckSympotomsPositions;
        for (int i = 0; i < symptomList.size(); i++) {
            CheckedTextView checkedTextView = (CheckedTextView) gridHeadNeck.getChildAt(symptomList.get(i));
            checkedTextView.setChecked(true);
        }

        symptomList = pojoSympotoms.chestSympotomsPositions;
        for (int i = 0; i < symptomList.size(); i++) {
            CheckedTextView checkedTextView = (CheckedTextView) gridChest.getChildAt(symptomList.get(i));
            checkedTextView.setChecked(true);
        }


        symptomList = pojoSympotoms.gastrointestinalSympotomsPositions;
        for (int i = 0; i < symptomList.size(); i++) {
            CheckedTextView checkedTextView = (CheckedTextView) gridGastrointestinal.getChildAt(symptomList.get(i));
            checkedTextView.setChecked(true);
        }

        symptomList = pojoSympotoms.urinarySympotomsPositions;
        for (int i = 0; i < symptomList.size(); i++) {
            CheckedTextView checkedTextView = (CheckedTextView) gridUrinary.getChildAt(symptomList.get(i));
            checkedTextView.setChecked(true);
        }

        symptomList = pojoSympotoms.musculoskeletalSympotomsPositions;
        for (int i = 0; i < symptomList.size(); i++) {
            CheckedTextView checkedTextView = (CheckedTextView) gridMusculoskeletal.getChildAt(symptomList.get(i));
            checkedTextView.setChecked(true);
        }

        symptomList = pojoSympotoms.skinSympotomsPositions;
        for (int i = 0; i < symptomList.size(); i++) {
            CheckedTextView checkedTextView = (CheckedTextView) gridSkin.getChildAt(symptomList.get(i));
            checkedTextView.setChecked(true);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setText(getString(R.string.done));
        tvNext.setCompoundDrawables(null, null, null, null);
        if (getArguments() != null) {
            pojoSympotoms = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    PojoSympotoms.class);
        }

        etOtherSymptom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty())
                    return;
                for (int i = 0; i < gridGeneralSymptomms.getChildCount(); i++) {
                    CheckedTextView checkedTextView = (CheckedTextView) gridGeneralSymptomms.getChildAt(i);
                    checkedTextView.setChecked(false);
                }

                for (int i = 0; i < gridHeadNeck.getChildCount(); i++) {
                    CheckedTextView checkedTextView = (CheckedTextView) gridHeadNeck.getChildAt(i);
                    checkedTextView.setChecked(false);
                }

                for (int i = 0; i < gridChest.getChildCount(); i++) {
                    CheckedTextView checkedTextView = (CheckedTextView) gridChest.getChildAt(i);
                    checkedTextView.setChecked(false);
                }

                for (int i = 0; i < gridGastrointestinal.getChildCount(); i++) {
                    CheckedTextView checkedTextView = (CheckedTextView) gridGastrointestinal.getChildAt(i);
                    checkedTextView.setChecked(false);
                }

                for (int i = 0; i < gridUrinary.getChildCount(); i++) {
                    CheckedTextView checkedTextView = (CheckedTextView) gridUrinary.getChildAt(i);
                    checkedTextView.setChecked(false);
                }

                for (int i = 0; i < gridMusculoskeletal.getChildCount(); i++) {
                    CheckedTextView checkedTextView = (CheckedTextView) gridMusculoskeletal.getChildAt(i);
                    checkedTextView.setChecked(false);
                }

                for (int i = 0; i < gridSkin.getChildCount(); i++) {
                    CheckedTextView checkedTextView = (CheckedTextView) gridSkin.getChildAt(i);
                    checkedTextView.setChecked(false);
                }


            }
        });
        if (!TextUtils.isEmpty(pojoSympotoms.otherSymptoms)) {
            etOtherSymptom.setText(pojoSympotoms.otherSymptoms);
            return;
        }

        addSymptoms(pojoSympotoms == null ? new PojoSympotoms() : pojoSympotoms);
        tvNext.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        tvNext.setText(getString(R.string.done));
        //getFavClinic();
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(int parentId, String text) {
        etOtherSymptom.setText(null);
    }
}
