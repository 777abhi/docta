package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.PatientMedicationAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoMedication;


public class PatientCurrentMedication extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.linearParent)
    LinearLayout linearParent;
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;


    private String mParam2;
    private String mParam4;

    private boolean mParam3;
    // TODO: Rename and change types of parameters
    private List<PojoMedication> commons = new ArrayList<>();


    public PatientCurrentMedication() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PatientCurrentMedication newInstance(String param1, String param2,
                                                       boolean showToggle, String source) {
        PatientCurrentMedication fragment = new PatientCurrentMedication();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putBoolean(ARG_PARAM3, showToggle);
        args.putString(ARG_PARAM4, source);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            CommonPojo data = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    CommonPojo.class);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getBoolean(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);

            if (mParam2.equals(Constants.COMMON))
                commons = data.getCurrent_medication();
            else commons = data.getHistory_medication();
        }
        ((FrameLayout.LayoutParams) linearParent.getLayoutParams()).topMargin = 0;
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        frTopBar.setVisibility(View.GONE);
        tvNoData.setText(getString(R.string.no_data));
        if (commons.size() > 0) {
            tvNoData.setVisibility(View.GONE);
            String patId = mParam4.equals(Constants.TYPE_PATIENT) ? loggedInUser.user_id : getAppointment().getUser_id();
            PatientMedicationAdapter patientMedicationAdapter = new PatientMedicationAdapter(
                    getActivity(), commons, mParam2, mParam3, mParam4, pBar, patId);
            rvData.setAdapter(patientMedicationAdapter);
        } else tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


}
