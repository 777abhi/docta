package emrfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.ImmunizationAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import webservices.pojos.CommonPojo;


public class CurrentImmunization extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam2;

    private CommonPojo data;

    public CurrentImmunization() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CurrentImmunization newInstance(String param1, String type) {
        CurrentImmunization fragment = new CurrentImmunization();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            data = new Gson().fromJson(getArguments().getString(ARG_PARAM1), CommonPojo.class);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvNoData.setText(getString(R.string.no_data));
        if (mParam2.equals(Constants.COMMON)) {
            if (data.getCurrent_list().size() > 0) {
                tvNoData.setVisibility(View.GONE);
                rvData.setAdapter(new ImmunizationAdapter(getActivity(), data.getCurrent_list()));
            } else tvNoData.setVisibility(View.VISIBLE);
        } else {
            if (data.getHistory_list().size() > 0) {
                tvNoData.setVisibility(View.GONE);
                rvData.setAdapter(new ImmunizationAdapter(getActivity(), data.getHistory_list()));
            } else tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recycler_only_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


}
