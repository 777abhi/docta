package emrfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import java.text.DecimalFormat;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import utils.Constants;
import views.CircularSeekBar;
import webservices.pojos.VitalData;


public class CommonMeasurement extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvValue)
    TextView tvValue;
    @Bind(R.id.tvUnit)
    TextView tvUnit;
    @Bind(R.id.circularSeekBar)
    CircularSeekBar circularSeekBar;

    private float bmiValue = 0;
    // TODO: Rename and change types of parameters
    private String title;
    private String mParam2;
    private float height;
    private DecimalFormat df = new DecimalFormat();

    public CommonMeasurement() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param title  Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BloodPressureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CommonMeasurement newInstance(String title, String param2) {
        CommonMeasurement fragment = new CommonMeasurement();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, title);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @OnClick({R.id.ivCheck, R.id.ivClose})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.ivCheck:
                if (title.equals(getString(R.string.temperature))) {
                    VitalData.temp = circularSeekBar.getProgress() + "";
                } else if (title.equals(getString(R.string.pulse_rate))) {
                    VitalData.pulseRate = circularSeekBar.getProgress() + "";
                } else if (title.equals(getString(R.string.o2_saturation))) {
                    VitalData.o2sat = circularSeekBar.getProgress() + "";
                } else if (title.equals(getString(R.string.weight))) {
                    VitalData.bmi = bmiValue + "";
                    VitalData.weight = circularSeekBar.getProgress() + "";
                } else if (title.equals(getString(R.string.height))) {
                    VitalData.height = circularSeekBar.getProgress() + "";
                }
                getActivity().onBackPressed();
                break;
            case R.id.ivClose:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_common_measurement, container, false);
        ButterKnife.bind(this, view);
        height = Float.parseFloat(VitalData.height);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        df.setMaximumFractionDigits(3);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        if (title.equals(getString(R.string.temperature))) {
            tvUnit.setText(" \u2103");
            circularSeekBar.setMax((int) Constants.MAX_TEMP);
            float defaultTemp = VitalData.temp.isEmpty() ? Constants.MIN_TEMP : Float.parseFloat(VitalData.temp);
            tvValue.setText(defaultTemp / 10 + "");
            circularSeekBar.setProgress((int) defaultTemp);
        } else if (title.equals(getString(R.string.pulse_rate))) {
            tvUnit.setText(getString(R.string.bpm));
            circularSeekBar.setMax((int) Constants.MAX_PULSE);
            float defaultPulseRate = VitalData.pulseRate.isEmpty() ? Constants.MIN_PULSE : Float.parseFloat(VitalData.pulseRate);
            tvValue.setText(defaultPulseRate + "");
            circularSeekBar.setProgress((int) defaultPulseRate);
        } else if (title.equals(getString(R.string.o2_saturation))) {
            tvUnit.setMinHeight(100);
            tvUnit.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            tvUnit.setText(Html.fromHtml("S<sub><small>p</small></sub>O<sub><small>2</small></sub>"));
            circularSeekBar.setMax((int) Constants.MAX_OSAT);
            float o2sat = VitalData.o2sat.isEmpty() ? Constants.MIN_OSAT : Float.parseFloat(VitalData.o2sat);
            tvValue.setText(o2sat + "");
            circularSeekBar.setProgress((int) o2sat);
        } else if (title.equals(getString(R.string.weight))) {
            circularSeekBar.setMax((int) Constants.MAX_WEIGHT);
            float weight = VitalData.weight.isEmpty() ? Constants.MIN_WEIGHT : Float.parseFloat(VitalData.weight);
            float bmi = VitalData.bmi.isEmpty() ? Constants.MIN_WEIGHT : Float.parseFloat(VitalData.bmi);
            tvUnit.setText("BMI : " + bmi + getString(R.string.bmi_unit));
            circularSeekBar.setProgress((int) weight);
            tvValue.setText(weight + getString(R.string.weight_unit));
        } else if (title.equals(getString(R.string.height))) {
            circularSeekBar.setMax((int) Constants.MAX_HEIGHT);
            float height = VitalData.height.isEmpty() ? Constants.MIN_HEIGHT :
                    Float.parseFloat(VitalData.height);
            // tvUnit.setText("in meters");
            circularSeekBar.setProgress((int) height);
            tvValue.setText((height / 100.0) + getString(R.string.height_unit));
        }
        tvTitle.setText(Html.fromHtml(title));
        circularSeekBar.setOnSeekBarChangeListener(new CircleSeekBarListener());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public class CircleSeekBarListener implements CircularSeekBar.OnCircularSeekBarChangeListener {
        @Override
        public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
            // TODO Insert your code here
            if (title.equals(getString(R.string.weight))) {
                tvValue.setText(progress + getString(R.string.weight_unit));
                float h = height / 100;
                bmiValue = (h == 0) ? 0 : progress / (h * h);
                bmiValue = Float.parseFloat(df.format(bmiValue));
                tvUnit.setText("BMI : " + bmiValue + getString(R.string.bmi_unit));
            } else if (title.equals(getString(R.string.height))) {
                tvValue.setText((progress / 100.0) + getString(R.string.height_unit));
            } else if (title.equals(getString(R.string.temperature))) {
                tvValue.setText((progress / 10.0) + getString(R.string.temp_unit));
            } else
                tvValue.setText(progress + "");
        }

        @Override
        public void onStopTrackingTouch(CircularSeekBar seekBar) {

        }

        @Override
        public void onStartTrackingTouch(CircularSeekBar seekBar) {

        }
    }
}
