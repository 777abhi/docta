package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.TabsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class EMRMedication extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvOrder)
    TextView tvOrder;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;


    // TODO: Rename and change types of parameters
    private String source = "";
    private String mParam2;


    public EMRMedication() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Medication.
     */
    // TODO: Rename and change types and number of parameters
    public static EMRMedication newInstance(String param1, String param2) {
        EMRMedication fragment = new EMRMedication();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.tvBack, R.id.tvOrder})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvOrder:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new OrderMedication(), R.id.fragment_container);
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            source = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.commonpager, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.medication));
        if (!source.equals(Constants.TYPE_PATIENT))
            tvOrder.setVisibility(View.VISIBLE);
        pBaseBar = pBar;
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                String data = new Gson().toJson(result);
                boolean showToggle = source.equals(Constants.TYPE_PATIENT) ? false : true;
                TabsAdapter tabsAdapter = new TabsAdapter(getChildFragmentManager());
                tabsAdapter.addFragment(PatientCurrentMedication.newInstance(
                        data, Constants.COMMON, showToggle, source), getString(R.string.current));
                tabsAdapter.addFragment(PatientCurrentMedication.newInstance(
                        data, "", showToggle, source), getString(R.string.past));
                pager.setAdapter(tabsAdapter);
                tabLayout.setupWithViewPager(pager);
            }

            @Override
            public void onFailure() {

            }
        });

        getMedication();
    }

    public void onEvent(CommonEvent ce) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getMedication();
                break;
        }
    }

    private void getMedication() {
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_MEDICATION;
        requestData.type = Constants.EMR;
        if (source.equals(Constants.TYPE_PATIENT))
            requestData.parameters.pat_id = loggedInUser.user_id;
        else requestData.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(requestData);
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
