package emrfragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.List;

import adapters.PatientLabAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import patientfragments.BaseFragment;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.Vital;

public class CurrentLab extends BaseFragment implements PatientLabAdapter.GraphInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private CommonPojo data;
    private String mParam2;
    private List<Vital> historyVitalList;

    public CurrentLab() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CurrentLab newInstance(String param1, String type) {
        CurrentLab fragment = new CurrentLab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = new Gson().fromJson(getArguments().getString(ARG_PARAM1), CommonPojo.class);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNoData.setText(getString(R.string.no_data));
        pBaseBar = pBar;
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (mParam2.equals(Constants.COMMON)) {
            if (data.getCurrent_vital_list().size() > 0) {
                tvNoData.setVisibility(View.GONE);
                rvData.setAdapter(new PatientLabAdapter(Constants.EMR,
                        CurrentLab.this, mParam2, data.getCurrent_vital_list()));
            } else tvNoData.setVisibility(View.VISIBLE);
        } else {
            if (data.getCurrent_vital_list().size() > 0) {
                tvNoData.setVisibility(View.GONE);
                historyVitalList = data.getHistory_vital();
                rvData.setAdapter(new PatientLabAdapter(Constants.EMR,
                        CurrentLab.this, mParam2, historyVitalList));
            } else tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recycler_only_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @Override
    public void moreClicked(final int position) {
        String[] items = {"Graph", "Cancel"};
        if (historyVitalList.get(position).getUnit().
                equals(getString(R.string.no_unit)))
            return;
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
        build.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //....
                switch (which) {
                    case 0:
                        String componentId = historyVitalList == null ? "" :
                                historyVitalList.get(position).getComp_detail().getId();
                        RequestData requestData = new RequestData();
                        requestData.request = RequestName.PAST_LAB_COMPONENTS;
                        requestData.type = Constants.EMR;
                        requestData.parameters.pat_id = userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT) ?
                                loggedInUser.user_id : getAppointment().getUser_id();
                        requestData.parameters.comp_id = componentId;
                        makeRequest(requestData);
                        break;
                }

            }
        }).create().show();
        setServiceCallbackListener(new BaseFragment.ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.getCurrent_vital_list().size() > 0)
                    GeneralFunctions.addFragmentFromRight(
                            getActivity().getFragmentManager(),
                            GraphFragment.newInstance(Constants.LAB_COMPONENT_GRAPH, new Gson().toJson(result)),
                            userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT) ?
                                    R.id.flAppointmentParent : R.id.fragment_container);
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
    }
}
