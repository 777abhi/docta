package emrfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.Allergies;
import utils.GeneralFunctions;

public class EMRMedicalRecordFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvVitals)
    TextView tvVitals;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public EMRMedicalRecordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EMRPatientProfile.
     */
    // TODO: Rename and change types and number of parameters
    public static EMRMedicalRecordFragment newInstance(String param1, String param2) {
        EMRMedicalRecordFragment fragment = new EMRMedicalRecordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_emr_medical_record, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.tvVitals, R.id.tvWeightBmi, R.id.tvMedication, R.id.tvImmunizations,
            R.id.tvLabs, R.id.tvImaging, R.id.tvMedicalHistory, R.id.tvAllergies})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvVitals:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new VitalFragment(), R.id.fragment_container);
                break;
            case R.id.tvWeightBmi:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new WeightBMIFragment(), R.id.fragment_container);
                break;
            case R.id.tvAllergies:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new Allergies(), R.id.fragment_container);
                break;
            case R.id.tvMedication:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new EMRMedication(), R.id.fragment_container);
                break;
            case R.id.tvImmunizations:
//                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
//                        new ImmunizationFragment(), R.id.fragment_container);
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new CurrentImmunizationCopy(), R.id.fragment_container);
                break;
            case R.id.tvLabs:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new LabFrament(), R.id.fragment_container);
                break;
            case R.id.tvMedicalHistory:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new EMRMedicalHistory(), R.id.fragment_container);
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
