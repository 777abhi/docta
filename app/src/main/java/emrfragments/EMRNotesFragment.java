package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.List;

import adapters.NotesAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoNotes;
import webservices.pojos.PrefsNotes;

public class EMRNotesFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvNotes)
    RecyclerView rvNotes;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public EMRNotesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EMRPatientProfile.
     */
    // TODO: Rename and change types and number of parameters
    public static EMRNotesFragment newInstance(String param1, String param2) {
        EMRNotesFragment fragment = new EMRNotesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notes, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        rvNotes.setLayoutManager(new LinearLayoutManager(getActivity()));
        setServiceCallbackListener(
                new ServiceCallback() {
                    @Override
                    public void onResult(CommonPojo result) throws Exception {
                        PrefsNotes prefsNotes = userPrefsManager.getNotes();
                        List<PojoNotes> pojoNotesList = result.getNotes_list();
                        if (prefsNotes != null && prefsNotes.getPrefsNotes().get(getAppointment().getUser_id()) != null) {
                            PojoNotes pojoNotes = prefsNotes.getPrefsNotes().get(getAppointment().getUser_id());
                            pojoNotesList.add(0, pojoNotes);
                        }
                        if (pojoNotesList.size() == 0) {
                            tvNoData.setVisibility(View.VISIBLE);
                        } else {
                            tvNoData.setVisibility(View.GONE);
                        }
                        NotesAdapter adapter = new NotesAdapter(getActivity(), pojoNotesList);
                        rvNotes.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure() throws Exception {

                    }
                }

        );

        getNotes();
    }

    public void onEvent(CommonEvent ce) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getNotes();
                break;
        }
    }

    private void getNotes() {
        RequestData requestData = new RequestData();
        requestData.type = Constants.EMR;
        requestData.request = RequestName.GET_INSERTED_NOTES;
        requestData.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(requestData);
    }

    @OnClick({R.id.tvNewNote, R.id.tvFilter})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvNewNote:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        AddNoteFragment.newInstance("", ""), R.id.fragment_container);
                break;
            case R.id.tvFilter:
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
