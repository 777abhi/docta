package emrfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import adapters.PastBmiAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import webservices.pojos.CommonPojo;
import webservices.pojos.Vital;


public class PastBMI extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Vital> vitalList;


    public PastBMI() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PastBMI newInstance(String param1) {
        PastBMI fragment = new PastBMI();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            vitalList = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    CommonPojo.class).getHistory_vital();

        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        tvNoData.setText(getString(R.string.no_data));
        if (vitalList.size() > 0) {
            tvNoData.setVisibility(View.GONE);
            rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvData.setAdapter(new PastBmiAdapter(getActivity(), vitalList, 0));
        } else {
            tvNoData.setVisibility(View.VISIBLE);
            rvData.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recycler_only_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
