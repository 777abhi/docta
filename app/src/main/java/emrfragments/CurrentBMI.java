package emrfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import views.RoundCornerMultiColorProgressBar;
import webservices.pojos.CommonPojo;
import webservices.pojos.Vital;


public class CurrentBMI extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @Bind(R.id.tvDate)
    TextView tvDate;
    @Bind(R.id.tvCompletedBy)
    TextView tvCompletedBy;
    @Bind(R.id.weightValue)
    RoundCornerMultiColorProgressBar weightValue;
    @Bind(R.id.heightValue)
    RoundCornerMultiColorProgressBar heightValue;
    @Bind(R.id.bmiValue)
    RoundCornerMultiColorProgressBar bmiValue;
    @Bind(R.id.tvWeight)
    TextView tvWeight;
    @Bind(R.id.tvHeight)
    TextView tvHeight;
    @Bind(R.id.bmiParent)
    LinearLayout bmiParent;
    @Bind(R.id.tvNoData)
    TextView tvNoData;

    // TODO: Rename and change types of parameters
    private Vital currentViral;
    private String mParam2;


    public CurrentBMI() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CurrentBMI newInstance(String param1) {
        CurrentBMI fragment = new CurrentBMI();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNoData.setText(getString(R.string.no_data));
        if (getArguments() != null) {
            currentViral = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    CommonPojo.class).getCurrent_vital();
        }
        if (currentViral == null) {
            bmiParent.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
            return;
        }
        bmiParent.setVisibility(View.VISIBLE);
        tvNoData.setVisibility(View.GONE);
        tvWeight.setText(currentViral.getWeight().isEmpty() ? "" : currentViral.getWeight() +
                getString(R.string.weight_unit));
        tvHeight.setText(currentViral.getHeight().isEmpty() ? "" : currentViral.getHeight() +
                getString(R.string.height_unit));
        tvCompletedBy.setText(getString(R.string.completed_by) + (currentViral.getDoc_name().isEmpty() ?
                currentViral.getNurse_name() : currentViral.getDoc_name()));
        tvDate.setText(getString(R.string.date) + currentViral.getDdate());
        bmiValue.setData(getBmiIndication(currentViral.getBmi_value()),
                currentViral.getBmi_value() + getString(R.string.bmi_unit), Constants.BMI);
    }


    private int getBmiIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 18.5 && val <= 24.9)
            return Constants.GREEN;
        else if (val >= 25 && val <= 29.9)
            return Constants.YELLOW;
        return Constants.RED;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_current_bmi, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
