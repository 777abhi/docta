package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.EMRMedicalCategoriesAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;


public class EMRMedicalCategory extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.etSearch)
    EditText etSearch;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Common category = new Common();

    private EMRMedicalCategoriesAdapter allAdapter;

    private List<Common> commonList = new ArrayList<>();
    private List<Common> allList = new ArrayList<>();

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            applyFilter(s.toString());
        }
    };

    public EMRMedicalCategory() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EMRMedicalCategory newInstance(String param1, String param2) {
        EMRMedicalCategory fragment = new EMRMedicalCategory();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        etSearch.setVisibility(View.VISIBLE);
        pBaseBar = pBar;
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvTitle.setText(getString(R.string.select_category_for_medical_issue));
        tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        tvNext.setText("");
        tvNext.setCompoundDrawables(ContextCompat.getDrawable(getActivity(), R.drawable.ic_cart), null, null, null);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                if (result.requestName.equals(RequestName.INSERT_MEDICAL_HIST)) {
                    showToast(R.string.successful);
                    EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                    getActivity().onBackPressed();
                } else {
                    commonList = result.getCommmon_list();
                    allList.addAll(commonList);
                    allAdapter = new EMRMedicalCategoriesAdapter(getActivity(), commonList);
                    rvData.setAdapter(allAdapter);
                    etSearch.addTextChangedListener(textWatcher);
                }
            }

            @Override
            public void onFailure() {

            }
        });
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.GET_MEDICAL_CATEGORIES;
        apiRequest.type = Constants.EMR;
        apiRequest.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(apiRequest);
    }

    public void onEvent(Common data) {
        if (!data.isDiagnoseCategory())
            category = data;
        else {
            category.setDiagnose(data);
            RequestData apiRequest = new RequestData();
            apiRequest.request = RequestName.INSERT_MEDICAL_HIST;
            apiRequest.type = Constants.EMR;
            apiRequest.parameters.pat_id = getAppointment().getUser_id();
            apiRequest.parameters.doc_id = loggedInUser.user_id;
            apiRequest.parameters.data = new Gson().toJson(category);
            makeRequest(apiRequest);
        }


    }

    private void applyFilter(String s) {
        commonList.clear();
        if (s.isEmpty()) {
            commonList.addAll(allList);
            allAdapter.notifyDataSetChanged();
            return;
        }
        for (int i = 0; i < allList.size(); i++) {
            if (allList.get(i).getName().toLowerCase().contains(s.toLowerCase()))
                commonList.add(allList.get(i));
            allAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

}
