package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;

public class AddComponentFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.etComponentName)
    EditText etComponentName;
    @Bind(R.id.rbResultType)
    RadioGroup rbResultType;
    @Bind(R.id.etUnits)
    EditText etUnits;
    @Bind(R.id.etAbNormalStart)
    EditText etAbNormalStart;
    @Bind(R.id.etAbNormalEnd)
    EditText etAbNormalEnd;
    @Bind(R.id.etNormalStart)
    EditText etNormalStart;
    @Bind(R.id.etNormalEnd)
    EditText etNormalEnd;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.rangeParent)
    LinearLayout rangeParent;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public AddComponentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddComponentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddComponentFragment newInstance(String param1, String param2) {
        AddComponentFragment fragment = new AddComponentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_component, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        pBaseBar = pBar;
        rbResultType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbPN) {
                    etUnits.setText(getString(R.string.no_unit));
                    etUnits.setEnabled(false);
                    rangeParent.setVisibility(View.GONE);
                } else {
                    etUnits.setEnabled(true);
                    etUnits.setText("");
                    rangeParent.setVisibility(View.VISIBLE);
                }
            }
        });

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                etUnits.setText("");
                etComponentName.setText("");
                etNormalStart.setText("");
                etNormalEnd.setText("");
                etAbNormalStart.setText("");
                etAbNormalEnd.setText("");
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @OnClick({R.id.tvBack, R.id.tvCancel, R.id.tvSave})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvCancel:
                getActivity().onBackPressed();
                break;
            case R.id.tvSave:
                if (etComponentName.getText().toString().isEmpty()) {
                    showToast(R.string.please_add_component_name);
                    return;
                } else if (rangeParent.getVisibility() == View.VISIBLE &&
                        (etAbNormalStart.getText().toString().isEmpty() ||
                                etAbNormalEnd.getText().toString().isEmpty())) {
                    showToast(R.string.please_add_range);
                    return;
                }
                hideKeyBoard(etComponentName);
                RequestData apiRequest = new RequestData();
                apiRequest.request = RequestName.ADD_LAB_COMPONENT;
                apiRequest.type = Constants.EMR;
                apiRequest.parameters.pat_id = getAppointment().getUser_id();
                apiRequest.parameters.c_name = etComponentName.getText().toString();
                apiRequest.parameters.green = rbResultType.getCheckedRadioButtonId() == R.id.rbPN ? getString(R.string.negative) :
                        etNormalStart.getText().toString() +
                                getString(R.string.component_separator) + etNormalEnd.getText().toString();
                apiRequest.parameters.red = rbResultType.getCheckedRadioButtonId() == R.id.rbPN ? getString(R.string.positive) :
                        etAbNormalStart.getText().toString() +
                                getString(R.string.component_separator) + etAbNormalEnd.getText().toString();
                apiRequest.parameters.unit = etUnits.getText().toString();
                makeRequest(apiRequest);
                break;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
