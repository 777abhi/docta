package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.MedicationListAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;


public class MedicationList extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvAppointmentList)
    RecyclerView rvAppointmentList;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.etSearch)
    EditText etSearch;
    @Bind(R.id.tvDone)
    TextView tvDone;
    // TODO: Rename and change types of parameters
    private String mParam2;

    private MedicationListAdapter allAdapter;

    private List<Common> medicationList;
    private List<Common> allList = new ArrayList<>();

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            applyFilter(s.toString());
        }
    };

    public MedicationList() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MedicationList newInstance(String param1, String param2) {
        MedicationList fragment = new MedicationList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            medication = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
//                    Medication.class);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvDone.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.medication_list));
        tvNext.setVisibility(View.GONE);

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                rvAppointmentList.setLayoutManager(new LinearLayoutManager(getActivity()));
                medicationList = result.getCommmon_list();
                allList.addAll(medicationList);
                allAdapter = new MedicationListAdapter(getActivity(), medicationList);
                rvAppointmentList.setAdapter(allAdapter);
                etSearch.addTextChangedListener(textWatcher);
            }

            @Override
            public void onFailure() {

            }
        });

        RequestData requestData = new RequestData();
        requestData.type = Constants.EMR;
        requestData.request = RequestName.GET_MEDICATION_LIST;
        requestData.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(requestData);
    }

    @OnClick({R.id.tvDone, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvDone:
                if (allAdapter.selectedPosition != -1)
                    EventBus.getDefault().post(medicationList.get(allAdapter.selectedPosition));
                getActivity().onBackPressed();
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    private void applyFilter(String s) {
        medicationList.clear();
        if (s.isEmpty()) {
            medicationList.addAll(allList);
            allAdapter.notifyDataSetChanged();
            return;
        }
        for (int i = 0; i < allList.size(); i++) {
            if (allList.get(i).getName().toLowerCase().startsWith(s.toLowerCase()))
                medicationList.add(allList.get(i));
            allAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_medication_list, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
