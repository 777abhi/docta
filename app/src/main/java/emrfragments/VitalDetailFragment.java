package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import adapters.VitalAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.pojos.CommonPojo;
import webservices.pojos.Vital;


public class VitalDetailFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData) TextView tvNoData;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private int mParam2;
    private List<Vital> vitals;


    public VitalDetailFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static VitalDetailFragment newInstance(String param1, int type) {
        VitalDetailFragment fragment = new VitalDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mParam2 = getArguments().getInt(ARG_PARAM2);
            vitals = new Gson().fromJson(getArguments().getString(ARG_PARAM1),
                    CommonPojo.class).getHistory_vital();
            switch (mParam2) {
                case Constants.BP:
                    for (int i = vitals.size() - 1; i >= 0; i--) {
                        if (vitals.get(i).getBp().equals("0"))
                            vitals.remove(i);
                    }
                    break;
                case Constants.TEMP:
                    for (int i = vitals.size() - 1; i >= 0; i--) {
                        if (vitals.get(i).getTemp().equals("0"))
                            vitals.remove(i);
                    }
                    break;
                case Constants.PULSE:
                    for (int i = vitals.size() - 1; i >= 0; i--) {
                        if (vitals.get(i).getPulse().equals("0"))
                            vitals.remove(i);
                    }
                    break;
                case Constants.OSAT:
                    for (int i = vitals.size() - 1; i >= 0; i--) {
                        if (vitals.get(i).getO2sat().equals("0"))
                            vitals.remove(i);
                    }
                    break;
            }
        }
        tvNext.setVisibility(View.GONE);
        if (vitals.size() == 0) {
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvData.setAdapter(new VitalAdapter(getActivity(), vitals, mParam2));
        }
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
