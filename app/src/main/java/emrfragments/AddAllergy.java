package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;


public class AddAllergy extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.allergyParent)
    LinearLayout allergyParent;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    private List<EditText> editTextList = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public AddAllergy() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddAllergy.
     */
    // TODO: Rename and change types and number of parameters
    public static AddAllergy newInstance(String param1, String param2) {
        AddAllergy fragment = new AddAllergy();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_allergy, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
        EditText allergy = (EditText) LayoutInflater.from(getActivity())
                .inflate(R.layout.row_allergy, allergyParent, false);
        allergyParent.addView(allergy);
        editTextList.add(allergy);
        pBaseBar = pBar;
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                for (int k = 0; k < editTextList.size(); k++) {
                    editTextList.get(k).setText("");
                }
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event


    @OnClick({R.id.tvAddMore, R.id.tvBack, R.id.tvCancel, R.id.tvSave})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                hideKeyBoard(editTextList.get(0));
                getActivity().onBackPressed();
                break;
            case R.id.tvAddMore:
                hideKeyBoard(editTextList.get(0));
                EditText allergy = (EditText) LayoutInflater.from(getActivity())
                        .inflate(R.layout.row_allergy, allergyParent, false);
                allergyParent.addView(allergy);
                editTextList.add(allergy);
                break;
            case R.id.tvCancel:
                hideKeyBoard(editTextList.get(0));
                getActivity().onBackPressed();
                break;
            case R.id.tvSave:
                if (TextUtils.isEmpty(editTextList.get(0).getText()))
                    return;
                hideKeyBoard(editTextList.get(0));
                RequestData apiRequest = new RequestData();

                for (int i = 0; i < editTextList.size(); i++) {
                    if (!TextUtils.isEmpty(editTextList.get(i).getText()))
                        apiRequest.parameters.allergy.add(new Common(editTextList.get(i).getText().toString()));
                }
                apiRequest.request = RequestName.ADD_ALLERGY;
                apiRequest.type = Constants.EMR;
                apiRequest.parameters.pat_id = getAppointment().getUser_id();
                apiRequest.parameters.doc_id = loggedInUser.user_id;
                makeRequest(apiRequest);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static class AllergyModel {
        String name;

        public AllergyModel(String name) {
            this.name = name;
        }
    }
}
