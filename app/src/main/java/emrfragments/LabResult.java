package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;
import webservices.pojos.Vital;

public class LabResult extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.etName)
    TextView etName;
    @Bind(R.id.etResult)
    EditText etResult;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvUnit)
    TextView tvUnit;
    @Bind(R.id.resultParent)
    LinearLayout resultParent;
    @Bind(R.id.rbPositive)
    RadioButton rbPositive;
    @Bind(R.id.rbNegative)
    RadioButton rbNegative;
    @Bind(R.id.rgParent)
    RadioGroup rgParent;

    private Common labComponent;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public LabResult() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LabResult.
     */
    // TODO: Rename and change types and number of parameters
    public static LabResult newInstance(String param1, String param2) {
        LabResult fragment = new LabResult();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void onEvent(Common labComponent) {
        etName.setText(labComponent.getC_name());
        this.labComponent = labComponent;
        if (labComponent.getUnit().equals(getString(R.string.no_unit))) {
            rgParent.setVisibility(View.VISIBLE);
            resultParent.setVisibility(View.GONE);
        } else {
            rgParent.setVisibility(View.GONE);
            resultParent.setVisibility(View.VISIBLE);
            tvUnit.setText(labComponent.getUnit());
        }

    }

    @OnClick({R.id.etName, R.id.tvBack, R.id.tvSave, R.id.tvCancel})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.etName:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new LabComponentFragment(), R.id.fragment_container);
                break;
            case R.id.tvCancel:
                getActivity().onBackPressed();
                break;
            case R.id.tvSave:
                if (labComponent == null) {
                    showToast(R.string.please_add_component_name);
                    return;
                }
                hideKeyBoard(etResult);
                RequestData apiRequest = new RequestData();
                apiRequest.request = RequestName.ADD_LAB_COMPONENTS;
                apiRequest.type = Constants.EMR;
                apiRequest.parameters.pat_id = getAppointment().getUser_id();
                apiRequest.parameters.doc_id = loggedInUser.user_id;
                apiRequest.parameters.comp_id = labComponent.getId();
                if (rgParent.getVisibility() == View.VISIBLE) {
                    apiRequest.parameters.comp_value = rgParent.getCheckedRadioButtonId() ==
                            R.id.rbPositive ? getString(R.string.positive) : getString(R.string.negative);
                } else {
                    if (TextUtils.isEmpty(etResult.getText())) {
                        showToast(R.string.please_enter_result_value);
                        return;
                    }
                    apiRequest.parameters.comp_value = etResult.getText().toString();
                }
                apiRequest.parameters.unit = labComponent.getUnit();
                makeRequest(apiRequest);
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pBaseBar = pBar;
        EventBus.getDefault().register(this);
        tvNext.setVisibility(View.GONE);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                Vital vital = new Vital();
                vital.setComp_detail(labComponent);
                vital.setUnit(labComponent.getUnit());
                if (rgParent.getVisibility() == View.VISIBLE) {
                    vital.setComp_value(rgParent.getCheckedRadioButtonId() ==
                            R.id.rbPositive ? getString(R.string.positive) : getString(R.string.negative));
                } else
                    vital.setComp_value(etResult.getText().toString());
                EventBus.getDefault().post(vital);
                labComponent = null;
                etName.setText("");
                etResult.setText("");
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lab_result, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);

    }
}
