package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.AddMedicationAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoMedication;

public class OrderMedication extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;

    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.rvAddMedication)
    RecyclerView rvAddMedication;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    private AddMedicationAdapter adapter;
    private List<PojoMedication> medicationList = new ArrayList<>();
    // TODO: Rename and change types of parameters
    private String mParam1 = "";
    private String mParam2 = "";


    public OrderMedication() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrderMedication.
     */
    // TODO: Rename and change types and number of parameters
    public static OrderMedication newInstance(String param1, String param2) {
        OrderMedication fragment = new OrderMedication();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_medication, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        medicationList.clear();
        tvTitle.setVisibility(View.GONE);
        tvNext.setVisibility(View.GONE);
        if (mParam1.equals(Constants.HIDE))
            frTopBar.setVisibility(View.GONE);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                showToast(R.string.medication_prescribed_to_patient);
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() {

            }
        });

        rvAddMedication.setLayoutManager(new LinearLayoutManager(getActivity()));
        medicationList.add(new PojoMedication());
        adapter = new AddMedicationAdapter(getActivity(), medicationList);
        rvAddMedication.setAdapter(adapter);
    }

    public void onEvent(Common medication) {
        medicationList.get(adapter.selectedPosition).medication_id = medication.getId();
        medicationList.get(adapter.selectedPosition).name = medication.getName();
        medicationList.get(adapter.selectedPosition).dose = medication.getDose();
        medicationList.get(adapter.selectedPosition).doc_name = loggedInUser.getFullName();
        medicationList.get(adapter.selectedPosition).doc_id = loggedInUser.user_id;
        adapter.notifyDataSetChanged();
    }

    @OnClick({R.id.tvAddMore, R.id.tvSend, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvAddMore:
                medicationList.add(new PojoMedication());
                adapter.notifyDataSetChanged();
                break;
            case R.id.tvSend:
                if (medicationList.size() > 0 &&
                        medicationList.get(0).name.isEmpty()) {
                    showToast(getString(R.string.fill_all_fields));
                    return;
                }
                RequestData requestData = new RequestData();
                requestData.type = Constants.EMR;
                requestData.request = RequestName.INSERT_PRESCRIPTION;
                requestData.parameters.pat_id = getAppointment().getUser_id();
                requestData.parameters.doc_id = loggedInUser.user_id;

                boolean isOK = true;
                for (int i = 0; i < medicationList.size(); i++) {
                    if (TextUtils.isEmpty(medicationList.get(i).startDate) ||
                            TextUtils.isEmpty(medicationList.get(i).endDate)) {
                        isOK = false;
                        showToast("Please enter start date and end date for all medications");
                        break;
                    }
                    if (!TextUtils.isEmpty(medicationList.get(i).name)) {
                        requestData.parameters.medication.add(medicationList.get(i));
//                        if (medicationList.get(i).time1.equalsIgnoreCase(getString(R.string.morning)))
//                            medicationList.get(i).time1 = "am";
//                        else if (medicationList.get(i).time1.equalsIgnoreCase(getString(R.string.evening)))
//                            medicationList.get(i).time1 = "pm";
//                        else medicationList.get(i).time1 = "n/a";
//
//                        if (medicationList.get(i).time2.equalsIgnoreCase(getString(R.string.morning)))
//                            medicationList.get(i).time2 = "am";
//                        else if (medicationList.get(i).time2.equalsIgnoreCase(getString(R.string.evening)))
//                            medicationList.get(i).time2 = "pm";
//                        else medicationList.get(i).time2 = "n/a";
                    }
                }
                if (isOK)
                    makeRequest(requestData);
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);

    }
}
