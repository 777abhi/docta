package emrfragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import views.RoundCornerMultiColorProgressBar;
import webservices.pojos.CommonPojo;
import webservices.pojos.Vital;

public class CurrentVital extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.bpValue)
    RoundCornerMultiColorProgressBar bpValue;
    @Bind(R.id.tempValue)
    RoundCornerMultiColorProgressBar tempValue;
    @Bind(R.id.pulseValue)
    RoundCornerMultiColorProgressBar pulseValue;
    @Bind(R.id.osatValue)
    RoundCornerMultiColorProgressBar osatValue;
    @Bind(R.id.tvDate)
    TextView tvDate;
    @Bind(R.id.tvCompletedBy)
    TextView tvCompletedBy;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.vitalParent)
    LinearLayout vitalParent;

    // TODO: Rename and change types of parameters
    private Vital currentVitals;


    public CurrentVital() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CurrentVital newInstance(String param1) {
        CurrentVital fragment = new CurrentVital();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNoData.setText(getString(R.string.no_data));
        if (getArguments() != null) {
            CommonPojo commonPojo = new Gson().fromJson(getArguments().getString(
                    ARG_PARAM1), CommonPojo.class);
            currentVitals = commonPojo.getCurrent_vital();
        }

        if (currentVitals == null) {
            tvNoData.setVisibility(View.VISIBLE);
            vitalParent.setVisibility(View.GONE);
            return;
        }
        tvNoData.setVisibility(View.GONE);
        vitalParent.setVisibility(View.VISIBLE);
        bpValue.setData(getBpIndication(currentVitals.getBp()), currentVitals.getBp(), Constants.BP);
        tempValue.setData(getTempIndication("" + currentVitals.getTemp()), "" + currentVitals.getTemp(), Constants.TEMP);
        pulseValue.setData(getPulseIndication("" + currentVitals.getPulse()), "" + currentVitals.getPulse(), Constants.PULSE);
        osatValue.setData(getOsatIndication("" + currentVitals.getO2sat()), "" + currentVitals.getO2sat(), Constants.OSAT);
        tvCompletedBy.setText(getString(R.string.completed_by) + (currentVitals.getDoc_name().isEmpty() ?
                currentVitals.getNurse_name() : currentVitals.getDoc_name()));
        tvDate.setText(getString(R.string.date) + currentVitals.getDdate());
    }

    private int getBpIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        String[] bp = value.split("/");
        float bp1 = Float.parseFloat(bp[0].isEmpty() ? "0" : bp[0]);
        float bp2 = Float.parseFloat(bp[1].isEmpty() ? "0" : bp[1]);

        int colorCode1, colorCode2;
        if (bp1 >= 90 && bp1 <= 120)
            colorCode1 = Constants.GREEN;
        else if (bp1 > 121 && bp1 <= 140)
            colorCode1 = Constants.YELLOW;
        else
            colorCode1 = Constants.RED;

        if (bp2 >= 60 && bp2 <= 80)
            colorCode2 = Constants.GREEN;
        else if (bp2 > 81 && bp2 <= 90)
            colorCode2 = Constants.YELLOW;
        else
            colorCode2 = Constants.RED;
        int returnType = colorCode1 > colorCode2 ? colorCode1 : colorCode2;
        return returnType;
    }

    private int getTempIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 35 && val <= 37.5)
            return Constants.GREEN;
        else if (val > 37.5 && val <= 38.3)
            return Constants.YELLOW;
        return Constants.RED;
    }


    private int getPulseIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 50 && val <= 90)
            return Constants.GREEN;
        else if (val > 90 && val <= 100)
            return Constants.YELLOW;
        return Constants.RED;
    }

    private int getOsatIndication(String value) {
        if (value == null || value.isEmpty())
            return Constants.RED;
        float val = Float.parseFloat(value);
        if (val >= 95 && val <= 100)
            return Constants.GREEN;
        else if (val >= 80 && val <= 95)
            return Constants.YELLOW;
        return Constants.RED;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_current_vital, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
