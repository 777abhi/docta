package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.EMRDiagnosisAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;


public class EMRDiagnosisCategory extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.etSearch)
    EditText etSearch;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Common> commonList = new ArrayList<>();
    private List<Common> allList = new ArrayList<>();

    private EMRDiagnosisAdapter allAdapter;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            applyFilter(s.toString());
        }
    };

    public EMRDiagnosisCategory() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EMRDiagnosisCategory newInstance(String param1) {
        EMRDiagnosisCategory fragment = new EMRDiagnosisCategory();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etSearch.setVisibility(View.VISIBLE);
//        EventBus.getDefault().register(this);
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (getArguments() != null) {
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        pBaseBar = pBar;
        tvTitle.setText(getString(R.string.select_diagnosis));
        tvNext.setText("");
        tvNext.setCompoundDrawables(getDrawable(R.drawable.ic_cart), null, null, null);

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {

                if (result.getCommmon_list().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    commonList = result.getCommmon_list();
                    allList.addAll(commonList);
                    allAdapter = new EMRDiagnosisAdapter(
                            getActivity(), commonList);
                    rvData.setAdapter(allAdapter);
                    etSearch.addTextChangedListener(textWatcher);
                } else {
                    tvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure() {

            }
        });

        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.GET_DIAGNOSES;
        apiRequest.type = Constants.EMR;
        apiRequest.parameters.pat_id = getAppointment().getUser_id();
        apiRequest.parameters.cat_name = mParam1;

        makeRequest(apiRequest);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @OnClick({R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }

    private void applyFilter(String s) {
        commonList.clear();
        if (s.isEmpty()) {
            commonList.addAll(allList);
            allAdapter.notifyDataSetChanged();
            return;
        }
        for (int i = 0; i < allList.size(); i++) {
            if (allList.get(i).getName().toLowerCase().contains(s.toLowerCase()))
                commonList.add(allList.get(i));
            allAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
