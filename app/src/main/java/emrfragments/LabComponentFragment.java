package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.ComponentAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;

public class LabComponentFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.etSearch)
    EditText etSearch;
    @Bind(R.id.rvComponents)
    RecyclerView rvComponents;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ComponentAdapter componentAdapter;

    private List<Common> labComponentList;
    private List<Common> filterComponentsList = new ArrayList<>();


    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            applyFilter(s.toString());
        }
    };

    public LabComponentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LabComponentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LabComponentFragment newInstance(String param1, String param2) {
        LabComponentFragment fragment = new LabComponentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void applyFilter(String s) {
        filterComponentsList.clear();
        if (s.isEmpty()) {
            filterComponentsList.addAll(labComponentList);
            componentAdapter.notifyDataSetChanged();
            return;
        }
        for (int i = 0; i < labComponentList.size(); i++) {
            if (labComponentList.get(i).getC_name().toLowerCase().contains(s.toLowerCase()))
                filterComponentsList.add(labComponentList.get(i));
            componentAdapter.selectedPosition = -1;
            componentAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        pBaseBar = pBar;
        etSearch.addTextChangedListener(textWatcher);
        rvComponents.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.done));

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                labComponentList = result.getCommmon_list();
                filterComponentsList.addAll(labComponentList);
                componentAdapter = new ComponentAdapter(getActivity(), filterComponentsList);
                rvComponents.setAdapter(componentAdapter);
            }

            @Override
            public void onFailure() {

            }
        });
        getLabComponents();
    }

    public void onEvent(CommonEvent ce) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getLabComponents();
                break;
        }
    }

    private void getLabComponents() {
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.GET_LAB_COMPONENTS;
        apiRequest.type = Constants.EMR;
        apiRequest.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(apiRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lab_component, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.tvBack, R.id.tvNext, R.id.tvAddNew})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                hideKeyBoard(etSearch);
                getActivity().onBackPressed();
                break;
            case R.id.tvAddNew:
                hideKeyBoard(etSearch);
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        new AddComponentFragment(), R.id.fragment_container);
                break;
            case R.id.tvNext:
                if (componentAdapter.selectedPosition == -1) {
                    showToast(R.string.please_select_component);
                    return;
                }
                hideKeyBoard(etSearch);
                EventBus.getDefault().post(filterComponentsList.get(componentAdapter.selectedPosition));
                getActivity().onBackPressed();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
