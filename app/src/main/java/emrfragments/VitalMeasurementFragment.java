package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.VitalData;


public class VitalMeasurementFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBloodPressure)
    TextView tvBloodPressure;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public VitalMeasurementFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VitalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VitalFragment newInstance(String param1, String param2) {
        VitalFragment fragment = new VitalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vital_measurement, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.done));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                showToast(R.string.vitals_added);
                VitalData.clearData();
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @OnClick({R.id.tvBloodPressure, R.id.tvO2Saturation, R.id.tvTemprature,
            R.id.tvPulseRate, R.id.tvNext, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvBloodPressure:
                getFragmentManager().beginTransaction().add(R.id.fragment_container,
                        new BloodPressureFragment()).addToBackStack(null).commit();
                break;
            case R.id.tvTemprature:
                getFragmentManager().beginTransaction().add(R.id.fragment_container,
                        CommonMeasurement.newInstance(getString(R.string.temperature), "")).
                        addToBackStack(null).commit();
                break;
            case R.id.tvPulseRate:
                getFragmentManager().beginTransaction().add(R.id.fragment_container,
                        CommonMeasurement.newInstance(getString(R.string.pulse_rate), "")).
                        addToBackStack(null).commit();
                break;
            case R.id.tvO2Saturation:
                getFragmentManager().beginTransaction().add(R.id.fragment_container,
                        CommonMeasurement.newInstance(getString(R.string.o2_saturation), "")).
                        addToBackStack(null).commit();
                break;
            case R.id.tvNext:
                showProgress();
//                if (VitalData.bp.isEmpty() && VitalData.temp.isEmpty() &&
//                        VitalData.pulseRate.isEmpty() && VitalData.o2sat.isEmpty()) {
//                    showToast(R.string.fill_all_fields);
//                    return;
//                }
                RequestData requestData = new RequestData();
                requestData.request = RequestName.DOC_INSERT_VITAL;
                requestData.type = Constants.EMR;
                requestData.parameters.pat_id = getAppointment().getUser_id();
                requestData.parameters.apoint_id = getAppointment().getApoint_id();
                requestData.parameters.doc_id = loggedInUser.user_id;
                requestData.parameters.bp = VitalData.bp;
                requestData.parameters.temp = TextUtils.isEmpty(VitalData.temp) ? VitalData.temp :
                        Float.parseFloat(VitalData.temp) / 10 + "";
                requestData.parameters.pulse = VitalData.pulseRate;
                requestData.parameters.o2sat = VitalData.o2sat;
                makeRequest(requestData);

                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
