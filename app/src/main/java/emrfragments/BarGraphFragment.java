package emrfragments;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.pojos.CommonPojo;
import webservices.pojos.Vital;


public class BarGraphFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.chart)
    LinearLayout chart;

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;
    private float minValue, maxValue;

    private ArrayList<Vital> all = new ArrayList<Vital>();


    public BarGraphFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BarGraphFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BarGraphFragment newInstance(int param1, String param2) {
        BarGraphFragment fragment = new BarGraphFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bar_graph, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            CommonPojo commonPojo = new Gson().fromJson(getArguments().getString(ARG_PARAM2),
                    CommonPojo.class);
            switch (mParam1) {
                case Constants.BP:
                    minValue = Float.parseFloat(commonPojo.getMinbp());
                    maxValue = Float.parseFloat(commonPojo.getMaxbp());
                    for (int i = all.size() - 1; i >= 0; i--) {
                        if (all.get(i).getBp().equals("0"))
                            all.remove(i);
                    }
                    break;
                case Constants.PULSE:
                    minValue = Float.parseFloat(commonPojo.getMinpulse());
                    maxValue = Float.parseFloat(commonPojo.getMaxpulse());
                    for (int i = all.size() - 1; i >= 0; i--) {
                        if (all.get(i).getPulse().equals("0"))
                            all.remove(i);
                    }
                    break;
                case Constants.OSAT:
                    minValue = Float.parseFloat(commonPojo.getMino2sat());
                    maxValue = Float.parseFloat(commonPojo.getMaxo2sat());
                    for (int i = all.size() - 1; i >= 0; i--) {
                        if (all.get(i).getO2sat().equals("0"))
                            all.remove(i);
                    }
                    break;
                case Constants.TEMP:
                    minValue = Float.parseFloat(commonPojo.getMintemp());
                    maxValue = Float.parseFloat(commonPojo.getMaxtemp());
                    for (int i = all.size() - 1; i >= 0; i--) {
                        if (all.get(i).getTemp().equals("0"))
                            all.remove(i);
                    }
                    break;
                default:
                    minValue = Float.parseFloat(commonPojo.getMin());
                    maxValue = Float.parseFloat(commonPojo.getMax());
                    break;


            }
            all = (ArrayList<Vital>) commonPojo.getHistory_vital();
        }


        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setColor(ContextCompat.getColor(getActivity(), R.color.back_text_color));

        XYSeriesRenderer diasystolicSeriesRenderer = new XYSeriesRenderer();
        diasystolicSeriesRenderer.setColor(ContextCompat.getColor(getActivity(), R.color.black));

// Include low and max value
        // renderer.setDisplayBoundingPoints(true);
// we add point markers
        // renderer.setPointStyle(PointStyle.CIRCLE);
        // renderer.setPointStrokeWidth(1);
        XYSeries series = null;
        XYSeries diastolicSeries = null;

        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
        mRenderer.setLabelsTextSize(getPixels(10));
        mRenderer.setLegendTextSize(getPixels(12));
        mRenderer.setLegendHeight(dpToPx(50));
        mRenderer.setMargins(new int[]{dpToPx(16), dpToPx(16), dpToPx(40), dpToPx(16)});
        mRenderer.setFitLegend(true);


        mRenderer.setYLabelsAlign(Paint.Align.RIGHT);
        mRenderer.setYLabelsPadding(2);
        mRenderer.setXAxisMin(-0.5);
        mRenderer.setAxesColor(Color.BLACK);
        mRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
        mRenderer.setXLabels(0);
        // mRenderer.setYLabels(0);
        mRenderer.setPanEnabled(false, false);
        mRenderer.setShowGrid(false);
        mRenderer.setBarWidth(40);
        mRenderer.setBarSpacing(0);
        mRenderer.setYLabelsColor(0, Color.BLACK);
        mRenderer.setLabelsColor(Color.BLACK);
        mRenderer.setChartTitleTextSize(getPixels(16));
        mRenderer.setXLabelsColor(Color.BLACK);
        mRenderer.addSeriesRenderer(renderer);
        mRenderer.setXAxisMax(5);


        mRenderer.setXLabelsAngle(-30);
        mRenderer.setXLabelsAlign(Paint.Align.RIGHT);

        mRenderer.setMargins(new int[]{dpToPx(20), 30, 50, 20});

        switch (mParam1) {
            case Constants.BP:
                mRenderer.addSeriesRenderer(diasystolicSeriesRenderer);
                mRenderer.setBarWidth(25);
                //  mRenderer.setBarSpacing(2);

                mRenderer.setYAxisMax(maxValue);
                mRenderer.setYAxisMin(minValue);
                //diasystolicSeriesRenderer.setYAxisMax(Constants.MAX_SYS);
                //mRenderer.setYAxisMin(Constants.MIN_TEMP);
                series = new XYSeries(getString(R.string.systolic_blood_pressure));
                diastolicSeries = new XYSeries(getString(R.string.diastolic_blood_pressure));

                for (int i = 0; i < all.size(); i++) {
                    String[] bp = all.get(i).getBp().split("/");
                    float bp1 = Float.parseFloat(bp[0].isEmpty() ? "0" : bp[0]);
                    if (bp.length > 1) {
                        float bp2 = Float.parseFloat(bp[1].isEmpty() ? "0" : bp[1]);
                        diastolicSeries.add(i, bp2);
                    } else {
                        diastolicSeries.add(i, 0);
                    }
                    series.add(i, bp1);
                    mRenderer.addXTextLabel(i, all.get(i).getDdate());
                }
                mRenderer.setChartTitle(getString(R.string.blood_pressure));
                break;
            case Constants.TEMP:
                mRenderer.setYAxisMax(maxValue);
                mRenderer.setYAxisMin(minValue);
                series = new XYSeries(getString(R.string.temperature));
                for (int i = 0; i < all.size(); i++) {
                    float y = Float.parseFloat(all.get(i).getTemp());
                    series.add(i, y);
                    mRenderer.addXTextLabel(i, all.get(i).getDdate());
                    // mRenderer.addYTextLabel(i, all.get(i).getTemp());
                }
                mRenderer.setChartTitle(getString(R.string.temperature));
                break;
            case Constants.PULSE:
                mRenderer.setYAxisMax(maxValue);
                mRenderer.setYAxisMin(minValue);
                series = new XYSeries(getString(R.string.pulse));
                for (int i = 0; i < all.size(); i++) {
                    int y = Integer.parseInt(all.get(i).getPulse());
                    series.add(i, y);
                    mRenderer.addXTextLabel(i, all.get(i).getDdate());
                }
                mRenderer.setChartTitle(getString(R.string.pulse));
                break;
            case Constants.OSAT:
                mRenderer.setYAxisMax(maxValue);
                mRenderer.setYAxisMin(minValue);
                series = new XYSeries(getString(R.string.o2_saturation));
                for (int i = 0; i < all.size(); i++) {
                    int y = Integer.parseInt(all.get(i).getO2sat());
                    series.add(i, y);
                    mRenderer.addXTextLabel(i, all.get(i).getDdate());
                }
                mRenderer.setChartTitle(getString(R.string.osat));
                break;
            case Constants.LAB_COMPONENT_GRAPH:
                all = (ArrayList<Vital>) new Gson().fromJson(getArguments().getString(ARG_PARAM2),
                        CommonPojo.class).getCurrent_vital_list();
                mRenderer.setYAxisMax(maxValue);
                mRenderer.setYAxisMin(minValue);
                series = new XYSeries(all.get(0).getComp_detail().getC_name());

                for (int i = 0; i < all.size(); i++) {
                    int y = Integer.parseInt(all.get(i).getComp_value());
                    series.add(i, y);
                    mRenderer.addXTextLabel(i, all.get(i).getDdate());
                }
                mRenderer.setChartTitle(all.get(0).getComp_detail().getC_name());
                break;
        }


        //renderer.setStroke(BasicStroke.DOTTED);


        // mRenderer.setLabelsTextSize(18);
        // mRenderer.setXTitle("Hello World");
        // mRenderer.setXLabelsColor(Color.BLACK);
        //mRenderer.setYLabelsColor(0, Color.BLACK);
        //mRenderer.setXLabelsAngle(-45);

        // transparent margins
// Disable Pan on two axis
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(series);
        if (diastolicSeries != null)
            dataset.addSeries(diastolicSeries);

        GraphicalView chartView = ChartFactory.getBarChartView(getActivity(), dataset, mRenderer, BarChart.Type.DEFAULT);
        chart.addView(chartView, 0);
        //openChart();
    }

    // TODO: Rename method, update argument and hook method into UI event
    private int getPixels(float size) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, size, metrics);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
