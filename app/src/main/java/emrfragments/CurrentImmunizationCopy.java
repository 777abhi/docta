package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.ImmunizationAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class CurrentImmunizationCopy extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvBack) TextView tvBack;
    @Bind(R.id.tvTitle) TextView tvTitle;
    @Bind(R.id.linearParent) LinearLayout linearParent;
    // TODO: Rename and change types of parameters
    private String mParam1;


    public CurrentImmunizationCopy() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CurrentImmunizationCopy newInstance(String param1, String type) {
        CurrentImmunizationCopy fragment = new CurrentImmunizationCopy();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        pBaseBar = pBar;
        if (mParam1 != null) {
            tvNext.setVisibility(View.GONE);
        } else {
            tvNext.setText(R.string.add);
            tvNext.setCompoundDrawables(null, null, null, null);
        }
        if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
            tvNext.setVisibility(View.INVISIBLE);
        }
        tvTitle.setText(getString(R.string.vaccination));

        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvNoData.setText(getString(R.string.no_data));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                hideProgress();
                if (result.getCurrent_list().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    rvData.setAdapter(new ImmunizationAdapter(getActivity(), result.getCurrent_list()));
                } else tvNoData.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure() {

            }
        });
        getImmunization();


//        if (mParam2.equals(Constants.COMMON)) {
//            if (data.getCurrent_list().size() > 0) {
//                tvNoData.setVisibility(View.GONE);
//                rvData.setAdapter(new ImmunizationAdapter(getActivity(), data.getCurrent_list()));
//            } else tvNoData.setVisibility(View.VISIBLE);
//        } else {
//            if (data.getHistory_list().size() > 0) {
//                tvNoData.setVisibility(View.GONE);
//                rvData.setAdapter(new ImmunizationAdapter(getActivity(), data.getHistory_list()));
//            } else tvNoData.setVisibility(View.VISIBLE);
//        }
    }

    @OnClick({R.id.tvBack, R.id.tvNext})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvNext:
                GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                        AddImmunizationFragment.newInstance(Constants.TYPE_PATIENT, ""), R.id.fragment_container);
                break;
        }
    }

    private void getImmunization() {
        showProgress();
        RequestData requestData = new RequestData();
        requestData.type = Constants.EMR;
        requestData.request = RequestName.GET_IMMUNIZATION;
        if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT))
            requestData.parameters.pat_id = loggedInUser.user_id;
        else
            requestData.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(requestData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.common_recycler_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    public void onEvent(CommonEvent ce) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getImmunization();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }


}
