package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;
import webservices.pojos.VitalData;

public class AddBMI extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.pBar)
    ProgressWheel pBar;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public AddBMI() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddVitalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddBMI newInstance(String param1, String param2) {
        AddBMI fragment = new AddBMI();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.done));
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                Appointment appointment = getAppointment();
                appointment.getProfile().height =  Float.parseFloat(VitalData.height) / 100 + "";
                userPrefsManager.saveAppointment(appointment);
                VitalData.height = "0";
                VitalData.weight = "";
                EventBus.getDefault().post(new CommonEvent(CommonEvent.REFRESH));
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_bmi, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @OnClick({R.id.tvNext, R.id.tvBack, R.id.tvWeight, R.id.tvHeight})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvNext:
                if (VitalData.height.equals("0") || VitalData.weight.isEmpty()) {
                    showToast(R.string.fill_all_fields);
                    return;
                }
                RequestData requestData = new RequestData();
                requestData.request = RequestName.INSERT_BMI;
                requestData.type = Constants.EMR;
                requestData.parameters.pat_id = getAppointment().getUser_id();
                requestData.parameters.doc_id = loggedInUser.user_id;
                requestData.parameters.apoint_id = getAppointment().getApoint_id();
                requestData.parameters.bmi = VitalData.bmi;
                requestData.parameters.weight = VitalData.weight;
                requestData.parameters.height = Float.parseFloat(VitalData.height) / 100 + "";
                makeRequest(requestData);
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvWeight:
                getFragmentManager().beginTransaction().add(R.id.fragment_container,
                        CommonMeasurement.newInstance(getString(R.string.weight), "")).
                        addToBackStack(null).commit();
                break;
            case R.id.tvHeight:
                getFragmentManager().beginTransaction().add(R.id.fragment_container,
                        CommonMeasurement.newInstance(getString(R.string.height), "")).
                        addToBackStack(null).commit();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
