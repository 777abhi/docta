package emrfragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.pojos.CommonPojo;


public class PastVital extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.ivMore)
    ImageView ivMore;
    @Bind(R.id.ivMoreTemp)
    ImageView ivMoreTemp;
    @Bind(R.id.ivMorePulse)
    ImageView ivMorePulse;
    @Bind(R.id.ivMoreOsat)
    ImageView ivMoreOsat;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CommonPojo data;


    public PastVital() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PastVital newInstance(String param1) {
        PastVital fragment = new PastVital();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            data = new Gson().fromJson(mParam1, CommonPojo.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_past_vital, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivMore.setTag(Constants.BP);
        ivMoreTemp.setTag(Constants.TEMP);
        ivMorePulse.setTag(Constants.PULSE);
        ivMoreOsat.setTag(Constants.OSAT);

    }
    // TODO: Rename method, update argument and hook method into UI event

    @OnClick({R.id.ivMore, R.id.ivMoreOsat, R.id.ivMoreTemp, R.id.ivMorePulse,
            R.id.rlBpParent, R.id.rlO2SatParent, R.id.rlPulseParent, R.id.rlTempParent})
    public void click(View v) {
        if (data.getHistory_vital().size() < 1) {
            showToast(R.string.no_data);
            return;
        }
        final int containerId;
        if (getAppointment() != null)
            containerId = R.id.fragment_container;
        else containerId = R.id.flAppointmentParent;
        switch (v.getId()) {
            case R.id.rlBpParent:
                GeneralFunctions.addFragmentFromRight(getActivity().getFragmentManager(),
                        VitalDetailFragment.newInstance(mParam1, Constants.BP), containerId);
                break;
            case R.id.rlO2SatParent:
                GeneralFunctions.addFragmentFromRight(getActivity().getFragmentManager(),
                        VitalDetailFragment.newInstance(mParam1, Constants.OSAT), containerId);
                break;
            case R.id.rlPulseParent:
                GeneralFunctions.addFragmentFromRight(getActivity().getFragmentManager(),
                        VitalDetailFragment.newInstance(mParam1, Constants.PULSE), containerId);
                break;
            case R.id.rlTempParent:
                GeneralFunctions.addFragmentFromRight(getActivity().getFragmentManager(),
                        VitalDetailFragment.newInstance(mParam1, Constants.TEMP), containerId);
                break;
            default:
                final int pos = (int) v.getTag();
                String[] items = {"Graph", "Cancel"};
                AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
                build.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //....
                        switch (which) {
                            case 0:
                                GeneralFunctions.addFragmentFromRight(
                                        getActivity().getFragmentManager(),
                                        GraphFragment.newInstance(pos, mParam1), containerId);

                                break;
                        }

                    }
                }).create().show();

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
