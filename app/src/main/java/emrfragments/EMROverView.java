package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import java.util.ArrayList;
import java.util.List;

import adapters.OverViewAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoSympotoms;
import webservices.pojos.Vital;

public class EMROverView extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.elvPatientDetails)
    ExpandableListView elvPatientDetails;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    private List<String> listHeader;
    private List<String> listChild = new ArrayList<>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OverViewAdapter notesExpandableAdapter;

    private View header;

    public EMROverView() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EMRPatientProfile.
     */
    // TODO: Rename and change types and number of parameters
    public static EMROverView newInstance(String param1, String param2) {
        EMROverView fragment = new EMROverView();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_overview, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EventBus.getDefault().register(this);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                final ViewHolder holder = new ViewHolder(header);
                PojoSympotoms pojoSympotoms = new Gson().fromJson(
                        result.getSymptoms(), PojoSympotoms.class);
                List<Common> allergyList = result.getAllergy();
                Vital vital = result.getCurrent_vital();

                holder.tvBmi.setText(vital.getBmi_value() + getString(R.string.bmi_unit));
                holder.tvHeight.setText(vital.getHeight() + getString(R.string.height_unit));
                holder.tvLastVisit.setText(result.getDdate());
                holder.tvWeight.setText(vital.getWeight() + getString(R.string.weight_unit));

                String allergy = "";
                for (int i = 0; i < allergyList.size(); i++) {
                    allergy += allergyList.get(i).getName() + (i == allergyList.size() - 1 ? "" : "\n");
                }
                listChild.add(result.getPrimary_concern());
                ArrayList<String> symptomsList = pojoSympotoms.getAllSymptoms();
                String symptoms = "";
                for (int i = 0; i < symptomsList.size(); i++) {
                    if (symptomsList.size() - 1 == i)
                        symptoms += symptomsList.get(i);
                    else
                        symptoms += symptomsList.get(i) + " \n";
                }
                listChild.add(symptoms);
                listChild.add(allergy);
                listChild.add("");

                //if (notesExpandableAdapter == null) {
                notesExpandableAdapter = new OverViewAdapter(
                        getActivity(), listHeader, listChild, vital);
                elvPatientDetails.setAdapter(notesExpandableAdapter);
//                } else {
//                    notesExpandableAdapter.notifyDataSetChanged();
//                }
            }

            @Override
            public void onFailure() {

            }
        });
        elvPatientDetails.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        header = getActivity().getLayoutInflater().
                inflate(R.layout.overview, null, false);
        elvPatientDetails.addHeaderView(header);

        listHeader = new ArrayList<>();
        listHeader.add(getString(R.string.primary_concern));
        listHeader.add(getString(R.string.current_symptoms));
        listHeader.add(getString(R.string.allergies));
        listHeader.add(getString(R.string.vitals));

        getData();
    }

    private void getData() {

        RequestData apiRequest = new RequestData();
        apiRequest.type = Constants.EMR;
        apiRequest.request = RequestName.GET_OVERVIEW;
        apiRequest.parameters.pat_id = getAppointment().getUser_id();
        apiRequest.parameters.apoint_id = getAppointment().getApoint_id();
        apiRequest.parameters.type = getAppointment().getType();
        makeRequest(apiRequest);
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public void onEvent(CommonEvent commonEvent) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                listChild.clear();
                getData();
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);

    }

    static class ViewHolder {
        @Bind(R.id.tvHeight)
        TextView tvHeight;
        @Bind(R.id.tvWeight)
        TextView tvWeight;
        @Bind(R.id.tvBmi)
        TextView tvBmi;
        @Bind(R.id.tvLastVisit)
        TextView tvLastVisit;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
