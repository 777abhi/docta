package emrfragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.Common;
import webservices.pojos.CommonPojo;

public class PatientSocialFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvBack)
    TextView tvBack;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvTobaccoUse)
    EditText tvTobaccoUse;
    @Bind(R.id.tvAlcoholUse)
    EditText tvAlcoholUse;
    @Bind(R.id.tvDrugUse)
    EditText tvDrugUse;
    @Bind(R.id.tvSexualActivity)
    EditText tvSexualActivity;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.rgTobacco)
    RadioGroup rgTobacco;
    @Bind(R.id.rgAlcoholUse)
    RadioGroup rgAlcoholUse;
    @Bind(R.id.rgDrugUse)
    RadioGroup rgDrugUse;
    @Bind(R.id.rgSexualActivity)
    RadioGroup rgSexualActivity;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public PatientSocialFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PatientSocialFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PatientSocialFragment newInstance(String param1, String param2) {
        PatientSocialFragment fragment = new PatientSocialFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_family_social, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setCompoundDrawables(null, null, null, null);
        tvNext.setText(getString(R.string.edit));
        tvTobaccoUse.setEnabled(false);
        tvAlcoholUse.setEnabled(false);
        tvDrugUse.setEnabled(false);
        tvSexualActivity.setEnabled(false);
        pBaseBar = pBar;
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                if (result.requestName.equals(RequestName.GET_SOCIAL_HOSTORY)) {
                    Common common = result.getCommon();
                    Common data = new Gson().fromJson(common.getContent(), Common.class);
                    tvTobaccoUse.setText(data.getTobaccoUse());
                    tvAlcoholUse.setText(data.getAlcoholUse());
                    tvDrugUse.setText(data.getDrugUse());
                    tvSexualActivity.setText(data.getSexualActivity());

                    if (data.getTobaccoUse().equals(getString(R.string.yes)))
                        rgTobacco.check(R.id.rbYes);
                    else rgTobacco.check(R.id.rbNo);

                    if (data.getAlcoholUse().equals(getString(R.string.yes)))
                        rgAlcoholUse.check(R.id.rbAlcoholYes);
                    else rgAlcoholUse.check(R.id.rbAlcoholNo);

                    if (data.getDrugUse().equals(getString(R.string.yes)))
                        rgDrugUse.check(R.id.rbDrudUseYes);
                    else rgDrugUse.check(R.id.rbDrudUseNo);

                    if (data.getSexualActivity().equals(getString(R.string.active)))
                        rgSexualActivity.check(R.id.rbSexualActivityYes);
                    else rgSexualActivity.check(R.id.rbSexualActivityNo);
                } else {
                    tvNext.setText(getString(R.string.edit));

                    tvTobaccoUse.setVisibility(View.VISIBLE);
                    tvAlcoholUse.setVisibility(View.VISIBLE);
                    tvDrugUse.setVisibility(View.VISIBLE);
                    tvSexualActivity.setVisibility(View.VISIBLE);

                    rgAlcoholUse.setVisibility(View.GONE);
                    rgDrugUse.setVisibility(View.GONE);
                    rgTobacco.setVisibility(View.GONE);
                    rgSexualActivity.setVisibility(View.GONE);
                    getSocialHistory();
                }
            }

            @Override
            public void onFailure() {
                tvTobaccoUse.setText(getString(R.string.edit_here));
                tvAlcoholUse.setText(getString(R.string.edit_here));
                tvDrugUse.setText(getString(R.string.edit_here));
                tvSexualActivity.setText(getString(R.string.edit_here));
            }
        });
        getSocialHistory();

    }

    private void getSocialHistory() {
        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.GET_SOCIAL_HOSTORY;
        apiRequest.type = Constants.EMR;
        apiRequest.parameters.pat_id = getAppointment().getUser_id();
        makeRequest(apiRequest);
    }

    @OnClick({R.id.tvNext, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvNext:
                if (tvNext.getText().equals(getString(R.string.edit))) {
                    tvNext.setText(getString(R.string.done));
                    tvTobaccoUse.setVisibility(View.GONE);
                    tvAlcoholUse.setVisibility(View.GONE);
                    tvDrugUse.setVisibility(View.GONE);
                    tvSexualActivity.setVisibility(View.GONE);

                    rgAlcoholUse.setVisibility(View.VISIBLE);
                    rgDrugUse.setVisibility(View.VISIBLE);
                    rgTobacco.setVisibility(View.VISIBLE);
                    rgSexualActivity.setVisibility(View.VISIBLE);

                } else {
                    Common data = new Common();

                    data.setTobaccoUse(rgTobacco.getCheckedRadioButtonId() == R.id.rbYes ?
                            getString(R.string.yes) : getString(R.string.no));
                    data.setAlcoholUse(rgAlcoholUse.getCheckedRadioButtonId() == R.id.rbAlcoholYes ?
                            getString(R.string.yes) : getString(R.string.no));
                    data.setDrugUse(rgDrugUse.getCheckedRadioButtonId() == R.id.rbDrudUseYes ?
                            getString(R.string.yes) : getString(R.string.no));
                    data.setSexualActivity(rgSexualActivity.getCheckedRadioButtonId() == R.id.rbSexualActivityYes ?
                            getString(R.string.active) : getString(R.string.inactive));

                    hideKeyBoard(tvAlcoholUse);
                    RequestData apiRequest = new RequestData();
                    apiRequest.request = RequestName.INSERT_SOCIAL_HISTORY;
                    apiRequest.type = Constants.EMR;
                    apiRequest.parameters.pat_id = getAppointment().getUser_id();
                    apiRequest.parameters.doc_id = loggedInUser.user_id;
                    apiRequest.parameters.data = new Gson().toJson(data);
                    makeRequest(apiRequest);
                    // call api here
                }
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
