package com.sanguinebits.docta;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import adapters.TabsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import emrfragments.EMRMedicalRecordFragment;
import emrfragments.EMRNotesFragment;
import emrfragments.EMROverView;
import emrfragments.EMRPatientProfile;
import emrfragments.OrderMedication;
import preferences.UserPrefsManager;
import utils.Constants;
import webservices.pojos.Appointment;


/**
 * QuickBlox team
 */
public class EMRActivity extends Activity {
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    @Bind(R.id.fabAppointment)
    FloatingActionButton fabAppointment;
    @Bind(R.id.topParent)
    LinearLayout topParent;
    private Appointment appointment;
    private TabsAdapter tabsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        ButterKnife.bind(this);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        if (savedInstanceState == null) {
            //fabAppointment.setVisibility(View.GONE);
            appointment = new Gson().fromJson(getIntent().
                    getStringExtra("requestData"), Appointment.class);
            if (getIntent().
                    getStringExtra("type").equals(Constants.EMR))
                fabAppointment.setVisibility(View.GONE);
            new UserPrefsManager(this).saveAppointment(appointment);
            setPager();
        }
    }

    private void setPager() {
        int icons[] = {R.drawable.my_account_drawable, R.drawable.notes_drawable,
                R.drawable.medical_record_drawable, R.drawable.overview_drawable, R.drawable.orders_drawable};


        tabsAdapter = new TabsAdapter(getFragmentManager());
        tabsAdapter.addFragment(new EMRPatientProfile());
        tabsAdapter.addFragment(new EMRNotesFragment());
        tabsAdapter.addFragment(new EMRMedicalRecordFragment());
        tabsAdapter.addFragment(new EMROverView());
        tabsAdapter.addFragment(OrderMedication.newInstance(Constants.HIDE, ""));
        viewpager.setAdapter(tabsAdapter);
        tabLayout.setupWithViewPager(viewpager);
        for (int i = 0; i < icons.length; i++) {
            tabLayout.getTabAt(i).setIcon(icons[i]);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @OnClick({R.id.fabAppointment})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.fabAppointment:
//                if (isVideo) {
//                    isVideo = false;
//                    topParent.setVisibility(View.VISIBLE);
//                } else {
//                    isVideo = true;
//                    topParent.setVisibility(View.GONE);
//                }
                finish();
                break;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        try {
            int stackEntryCount = getFragmentManager().getBackStackEntryCount();
            if (stackEntryCount > 0) {
                getFragmentManager().popBackStackImmediate();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {

        }
    }

    protected String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}

