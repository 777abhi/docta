package com.sanguinebits.docta;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import preferences.UserPrefsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Constants;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.CommonPojo;

public class RateActivity extends AppCompatActivity {

    @Bind(R.id.doctorRating)
    RatingBar doctorRating;
    @Bind(R.id.etAdditionalComment)
    EditText etAdditionalComment;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_rate_doctor);
        ButterKnife.bind(this);
    }

    protected void showToast(int messageId) {
        try {
            Toast.makeText(this, getString(messageId), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    protected String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick({R.id.tvSubmit})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvSubmit:

                UserPrefsManager userPrefsManager = new UserPrefsManager(this);
                pBar.setVisibility(View.VISIBLE);
                RequestData requestData = new RequestData();
                requestData.type = Constants.TYPE_PATIENT;
                requestData.request = RequestName.RATE_VIDEO_VISIT;
                requestData.parameters.pat_id = userPrefsManager.getLoggedInUser().user_id;
                requestData.parameters.doc_id = userPrefsManager.getDoctorId();
                requestData.parameters.msg = etAdditionalComment.getText().toString();
                Call<CommonPojo> call = RestClient.get().makeRequest(requestData);
                call.enqueue(new Callback<CommonPojo>() {
                    @Override
                    public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                        try {
                            pBar.setVisibility(View.GONE);
                            if (response.isSuccessful() && response.body() != null &&
                                    response.body().getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                                showToast(R.string.thanks_for_your_rating);
                                finish();
                            } else {
                                showToast(R.string.there_might_be_some_problem);
                                finish();
                            }
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonPojo> call, Throwable t) {
                        pBar.setVisibility(View.GONE);
                        finish();
                    }

                });
                break;
        }
    }
}
