package com.sanguinebits.docta;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSignaling;
import com.quickblox.chat.QBWebRTCSignaling;
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCConfig;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.quickblox.videochat.webrtc.QBSignalingSpec;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionConnectionCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSignalingCallback;
import com.quickblox.videochat.webrtc.exception.QBRTCException;
import com.quickblox.videochat.webrtc.exception.QBRTCSignalException;

import org.webrtc.VideoCapturerAndroid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.TabsPagerAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import doctorfragments.ConsultHistoryFragment;
import doctorfragments.ConversationFragmentCopy;
import doctorfragments.DoctaConsultRequest;
import doctorfragments.DoctaHomeFragment;
import doctorfragments.DoctorAccountFragment;
import doctorfragments.WaitForConsultRequest;
import patientfragments.NotificationFragment;
import preferences.UserPrefsManager;
import utils.Constants;
import utils.DialogUtils;
import utils.GeneralFunctions;
import utils.RingtonePlayer;

public class DoctaHomeActivity extends AppCompatActivity implements
        QBRTCClientSessionCallbacks, QBRTCSessionConnectionCallbacks, QBRTCSignalingCallback {
    //public static final String CONVERSATION_CALL_FRAGMENT = "conversation_call_fragment";
    public static final String START_CONVERSATION_REASON = "start_conversation_reason";
    //    public static final String INCOME_CALL_FRAGMENT = "income_call_fragment";
//    private static final String TAG = CallActivity.class.getSimpleName();
    private static QBChatService chatService;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.fabAppointment)
    FloatingActionButton fabAppointment;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    @Bind(R.id.pBar)
    ProgressWheel progressBar;
    int icons[] = {R.drawable.online_drawable, R.drawable.consult_history, R.drawable.notification_drawable,
            R.drawable.my_account_drawable};
    int iconsDeselected[] = {R.drawable.ic_earnings_off, R.drawable.ic_consult_history_off,
            R.drawable.ic_notifications, R.drawable.ic_my_account};
    private QBRTCSession currentSession;
    private QBRTCClient rtcClient;
    private RingtonePlayer ringtonePlayer;
    private QBRTCSessionUserCallback sessionUserCallback;
    private boolean isDoctaHomeShowing = true;

    private boolean shouldRefresh = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docta_home);
        ButterKnife.bind(this);
        QBSettings.getInstance().fastConfigInit(Constants.APP_ID, Constants.AUTH_KEY, Constants.AUTH_SECRET);
        QBChatService.setDebugEnabled(true);
        fabAppointment.setVisibility(View.GONE);
        TabsPagerAdapter tabsAdapter = new TabsPagerAdapter(getFragmentManager());
        tabsAdapter.addFragment(new DoctaHomeFragment());
        tabsAdapter.addFragment(new ConsultHistoryFragment());
        tabsAdapter.addFragment(new NotificationFragment());
        // tabsAdapter.addFragment(new AboutUsFragment());
        tabsAdapter.addFragment(new DoctorAccountFragment());
        viewpager.setAdapter(tabsAdapter);
        tabLayout.setupWithViewPager(viewpager);
        for (int i = 0; i < icons.length; i++) {
            tabLayout.getTabAt(i).setIcon(icons[i]);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @OnClick(R.id.fabAppointment)
    public void click(View view) {
        switch (view.getId()) {
            case R.id.fabAppointment:
                isDoctaHomeShowing = true;
                tabLayout.setSelectedTabIndicatorColor(Color.TRANSPARENT);
                int pos = tabLayout.getSelectedTabPosition();
                tabLayout.getTabAt(pos).setIcon(iconsDeselected[pos]);
                break;
        }
    }

    public void rejectCurrentSession() {
        if (currentSession != null) {
            currentSession.rejectCall(new HashMap<String, String>());
        }
    }

//    private void stopIncomeCallTimer() {
//        Log.d(TAG, "stopIncomeCallTimer");
//        showIncomingCallWindowTaskHandler.removeCallbacks(showIncomingCallWindowTask);
//    }

    public void releaseCurrentSession() {
        this.currentSession.removeSessionnCallbacksListener(this);
        this.currentSession.removeSignalingCallback(this);
        this.currentSession = null;
    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.showToast(DoctaHomeActivity.this, message);
            }
        });
    }

    private void makeProgressBarInvisible() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    DoctaHomeActivity.this.progressBar.setVisibility(View.GONE);
                } catch (Exception e) {

                }
            }
        });
    }

    private void login() {
        final QBUser user = new UserPrefsManager(this).getLoggedInUser().getQbUser();
        QBAuth.createSession(user.getLogin(), user.getPassword(), new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle bundle) {//8347861
                user.setId(session.getUserId());
                if (!QBChatService.isInitialized()) {
                    QBChatService.init(DoctaHomeActivity.this);
                    chatService = QBChatService.getInstance();
                }
                chatService = QBChatService.getInstance();
                if (chatService.isLoggedIn()) {
                    initQBRTCClient();
                } else {
                    chatService.login(user, new QBEntityCallbackImpl<QBUser>() {
                        @Override
                        public void onSuccess() {
                            initQBRTCClient();
                        }

                        @Override
                        public void onError(List errors) {
                            try {
                                makeProgressBarInvisible();
                                showToast("Error when login " + errors.get(0));
                            } catch (Exception e) {
                                makeProgressBarInvisible();
                            }
                        }
                    });
                }

            }

            @Override
            public void onError(List<String> errors) {
                try {
                    makeProgressBarInvisible();
                    showToast("Error when login " + errors.get(0));
                } catch (Exception e) {

                }

            }
        });
    }

    private void initQBRTCClient() {
        rtcClient = QBRTCClient.getInstance(this);
        // Add signalling manager
        QBChatService.getInstance().getVideoChatWebRTCSignalingManager().
                addSignalingManagerListener(new QBVideoChatSignalingManagerListener() {
                    @Override
                    public void signalingCreated(QBSignaling qbSignaling, boolean createdLocally) {
                        if (!createdLocally) {
                            rtcClient.addSignaling((QBWebRTCSignaling) qbSignaling);
                        }
                    }
                });

        rtcClient.setCameraErrorHendler(new VideoCapturerAndroid.CameraErrorHandler() {
            @Override
            public void onCameraError(final String s) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast(s);
                    }
                });
            }
        });


        // Configure
        //
        QBRTCConfig.setMaxOpponentsCount(6);
        QBRTCConfig.setDisconnectTime(30);
        QBRTCConfig.setAnswerTimeInterval(30l);
        QBRTCConfig.setDebugEnabled(true);


        // Add activity as callback to RTCClient
        rtcClient.addSessionCallbacksListener(this);
        // Start mange QBRTCSessions according to VideoCall parser's callbacks
        rtcClient.prepareToProcessCalls();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    DoctaHomeActivity.this.progressBar.setVisibility(View.GONE);
                } catch (Exception e) {

                }
            }
        });
    }


    public void hangUpCurrentSession() {
        ringtonePlayer.stop();
        if (currentSession != null) {
            currentSession.hangUp(new HashMap<String, String>());
        }
        onBackPressed();
    }

//    private void startIncomeCallTimer(long time) {
//        showIncomingCallWindowTaskHandler.postAtTime(showIncomingCallWindowTask, SystemClock.uptimeMillis() + time);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            chatService.logout();
            chatService.destroy();
            if (currentSession != null)
                onSessionClosed(currentSession);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (shouldRefresh) {
            //progressBar.setVisibility(View.VISIBLE);
            shouldRefresh = false;
            login();
        }
    }


    public void callUser(int qbId) {
        QBUser qbUser = new QBUser();
        qbUser.setId(qbId);
        try {
            addConversationFragmentStartCall(qbUser,
                    QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO);
        } catch (Exception e) {
            showToast("Cant make call now,Please try later");
        }
    }

    @Override
    public void onReceiveNewSession(QBRTCSession qbrtcSession) {
    }

    @Override
    public void onUserNotAnswer(QBRTCSession session, Integer userID) {
        if (!session.equals(currentSession)) {
            return;
        }
        if (sessionUserCallback != null) {
            sessionUserCallback.onUserNotAnswer(session, userID);
        }
        ringtonePlayer.stop();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showToast("User did not pick the call");
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    @Override
    public void onCallRejectByUser(QBRTCSession session, Integer userID, Map<String, String> userInfo) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ringtonePlayer.stop();
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });
        if (sessionUserCallback != null) {
            sessionUserCallback.onCallRejectByUser(session, userID, userInfo);
        }
        showToast("User Busy");
    }

    @Override
    public void onCallAcceptByUser(QBRTCSession session, Integer userId, Map<String, String> map) {
        if (!session.equals(getCurrentSession())) {
            return;
        }
        if (sessionUserCallback != null) {
            sessionUserCallback.onCallAcceptByUser(session, userId, map);
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ringtonePlayer.stop();
            }
        });
    }


    @Override
    public void onReceiveHangUpFromUser(final QBRTCSession session, final Integer userID) {
        //crash here
        if (session.equals(currentSession)) {
            if (sessionUserCallback != null) {
                sessionUserCallback.onReceiveHangUpFromUser(session, userID);
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });
    }

    public void addConversationFragmentStartCall(QBUser opponent, QBRTCTypes.
            QBConferenceType qbConferenceType) throws Exception {
        List<QBUser> opponents = new ArrayList<>();
        opponents.add(opponent);
        List<Integer> idList = new ArrayList<>();
        idList.add(opponent.getId());
        QBRTCSession newSessionWithOpponents = rtcClient.createNewSessionWithOpponents(
                idList, qbConferenceType);
        initCurrentSession(newSessionWithOpponents);
        ConversationFragmentCopy fragment = ConversationFragmentCopy.newInstance(opponents,
                DoctaHomeActivity.StartConversetionReason.OUTCOME_CALL_MADE);
        GeneralFunctions.addFragmentFromRight(getFragmentManager(), fragment, R.id.flRequestFragmentContainer);
        //FragmentExecuotr.addFragment(getFragmentManager(), R.id.flRequestFragmentContainer, fragment, CONVERSATION_CALL_FRAGMENT);
    }

    public void playRingTone() {
        ringtonePlayer = new RingtonePlayer(this, R.raw.beep);
        ringtonePlayer.play(true);
    }

    public void initCurrentSession(QBRTCSession sesion) {
        this.currentSession = sesion;
        this.currentSession.addSessionCallbacksListener(this);
        this.currentSession.addSignalingCallback(this);
    }

    @Override
    public void onBackPressed() {
        try {
            if (ringtonePlayer != null)
                ringtonePlayer.stop();
            int stackEntryCount = getFragmentManager().getBackStackEntryCount();
            WaitForConsultRequest fragment = (WaitForConsultRequest) getFragmentManager().
                    findFragmentByTag(Constants.WAIT_FOR_CONSULT_REQUEST);
            DoctaConsultRequest doctaConsultRequest = (DoctaConsultRequest) getFragmentManager().
                    findFragmentByTag(Constants.TAG_CONSULT_REQUEST);
            if (doctaConsultRequest != null) {
                fragment.onBackPress();
                return;
            }
            if (fragment != null) {
                fragment.onBackPress();
                return;
            }
            if (stackEntryCount > 0) {
                getFragmentManager().popBackStackImmediate();
                if (isDoctaHomeShowing && stackEntryCount == 1) {
                    isDoctaHomeShowing = false;
                    tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.back_text_color));
                    int pos = tabLayout.getSelectedTabPosition();
                    tabLayout.getTabAt(pos).setIcon(icons[pos]);
                }
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {

        }

    }

    @Override
    public void onStartConnectToUser(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onConnectedToUser(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onConnectionClosedForUser(QBRTCSession qbrtcSession, Integer integer) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ringtonePlayer.stop();
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onDisconnectedFromUser(QBRTCSession qbrtcSession, Integer integer) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onDisconnectedTimeoutFromUser(QBRTCSession qbrtcSession, Integer integer) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ringtonePlayer.stop();
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onConnectionFailedWithUser(QBRTCSession qbrtcSession, Integer integer) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ringtonePlayer.stop();
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onError(QBRTCSession qbrtcSession, QBRTCException e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ringtonePlayer.stop();
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onSuccessSendingPacket(QBSignalingSpec.QBSignalCMD qbSignalCMD, Integer integer) {
    }

    @Override
    public void onErrorSendingPacket(QBSignalingSpec.QBSignalCMD qbSignalCMD, Integer integer, QBRTCSignalException e) {

    }


    @Override
    public void onUserNoActions(QBRTCSession qbrtcSession, Integer integer) {
        //startIncomeCallTimer(0);
    }

    @Override
    public void onSessionClosed(QBRTCSession qbrtcSession) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showToast("Hello");
//                releaseCurrentSession();
//                onBackPressed();
//            }
//        });
    }

    public QBRTCSession getCurrentSession() {
        return currentSession;
    }

    @Override
    public void onSessionStartClose(QBRTCSession qbrtcSession) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ringtonePlayer.stop();
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });
    }

    public void addTCClientConnectionCallback(QBRTCSessionConnectionCallbacks clientConnectionCallbacks) {
        if (currentSession != null) {
            currentSession.addSessionCallbacksListener(clientConnectionCallbacks);
        }
    }

    public void removeRTCClientConnectionCallback(QBRTCSessionConnectionCallbacks clientConnectionCallbacks) {
        if (currentSession != null) {
            currentSession.removeSessionnCallbacksListener(clientConnectionCallbacks);
        }
    }

    public void addRTCSessionUserCallback(QBRTCSessionUserCallback sessionUserCallback) {
        this.sessionUserCallback = sessionUserCallback;
    }

    public void addVideoTrackCallbacksListener(QBRTCClientVideoTracksCallbacks videoTracksCallbacks) {
        if (currentSession != null) {
            currentSession.addVideoTrackCallbacksListener(videoTracksCallbacks);
        }
    }

    public void removeRTCSessionUserCallback(QBRTCSessionUserCallback sessionUserCallback) {
        this.sessionUserCallback = null;
    }

    public void removeVideoCallBacks() {
        // write here
        shouldRefresh = true;
        rtcClient.removeSessionsCallbacksListener(this);
    }

    public enum StartConversetionReason {
        INCOME_CALL_FOR_ACCEPTION,
        OUTCOME_CALL_MADE;
    }

    public interface QBRTCSessionUserCallback {
        void onUserNotAnswer(QBRTCSession session, Integer userId);

        void onCallRejectByUser(QBRTCSession session, Integer userId, Map<String, String> userInfo);

        void onCallAcceptByUser(QBRTCSession session, Integer userId, Map<String, String> userInfo);

        void onReceiveHangUpFromUser(QBRTCSession session, Integer userId);
    }
}
