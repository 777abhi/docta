package com.sanguinebits.docta;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;

import io.fabric.sdk.android.Fabric;
import patientfragments.PasscodeFragment;
import patientfragments.SplashFragment;
import preferences.UserPrefsManager;
import utils.Constants;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isNetworkConnected()) {
            Toast.makeText(this, getString(R.string.connect_to_internet), Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        Fabric.with(this, new Crashlytics());
        Fresco.initialize(this);
        setContentView(R.layout.activity_login);
        UserPrefsManager userPrefsManager = new UserPrefsManager(this);
        if (userPrefsManager.getLoggedInUser() != null) {
            if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT))
                getFragmentManager().beginTransaction().add(R.id.flFragmentContainer, new PasscodeFragment()).commit();
                // GeneralFunctions.addFragmentFromRight(getFragmentManager(), new PasscodeFragment(), R.id.flFragmentContainer);
            else {
                startActivity(new Intent(this, DoctaHomeActivity.class));
                finish();
            }
        } else {
            getFragmentManager().beginTransaction().add(R.id.flFragmentContainer,
                    new SplashFragment()).commit();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {
        try {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStackImmediate();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {

        }
    }
}
