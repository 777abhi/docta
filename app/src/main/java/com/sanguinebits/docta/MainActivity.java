package com.sanguinebits.docta;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.quickblox.chat.QBChatService;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBSignalingSpec;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionConnectionCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSignalingCallback;
import com.quickblox.videochat.webrtc.exception.QBRTCException;
import com.quickblox.videochat.webrtc.exception.QBRTCSignalException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import adapters.PatientTabAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import gcm.RegistrationIntentService;
import interfaces.DotInterface;
import patientfragments.AppointmentRequestFragment;
import patientfragments.ConversationFragmentCopy;
import patientfragments.HealthReminders;
import patientfragments.IncomeCallFragmentCopy;
import patientfragments.LoaderFragment;
import patientfragments.MyAccount;
import patientfragments.MyHealthFragment;
import patientfragments.NotificationFragment;
import patientfragments.RateYourCare;
import preferences.UserPrefsManager;
import services.VideoCallService;
import utils.CommonEvent;
import utils.Constants;
import utils.DataHolder;
import utils.GeneralFunctions;
import webservices.pojos.NotificationHandler;

public class MainActivity extends AppCompatActivity implements
        QBRTCClientSessionCallbacks, QBRTCSessionConnectionCallbacks, QBRTCSignalingCallback {
    public static final String START_CONVERSATION_REASON = "start_conversation_reason";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    private boolean isAppointmentFragmentShowing;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String regId = "";
    private int icons[] = {R.drawable.dashboard_drawable, R.drawable.my_health_drawable,
            R.drawable.notification_drawable, R.drawable.my_account_drawable};
    private int iconsDeselected[] = {R.drawable.ic_myhealth, R.drawable.my_health_off,
            R.drawable.ic_notifications, R.drawable.ic_my_account};
    private UserPrefsManager userPrefsManager;
    private QBRTCSessionUserCallback sessionUserCallback;
    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // mServiceBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            VideoCallService.MyBinder myBinder = (VideoCallService.MyBinder) service;
            myBinder.setClentSessionListener(MainActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);
        //startService(new Intent(getBaseContext(), VideoCallService.class));
        ButterKnife.bind(this);
        userPrefsManager = new UserPrefsManager(this);
        if (userPrefsManager.getLoggedInUser() == null) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }
        registerGCM();
        if (getIntent().getExtras() != null) {
            onNewIntent(getIntent());
        }
        Log.v("deviceid", userPrefsManager.getRegId());
        PatientTabAdapter tabsAdapter = new PatientTabAdapter(getFragmentManager());
        tabsAdapter.addFragment(new HealthReminders());
        tabsAdapter.addFragment(new MyHealthFragment());
        tabsAdapter.addFragment(new NotificationFragment());
        //tabsAdapter.addFragment(new AboutUsFragment());
        tabsAdapter.addFragment(new MyAccount());

        viewpager.setAdapter(tabsAdapter);
        tabLayout.setupWithViewPager(viewpager);
        for (int i = 0; i < icons.length; i++) {
            tabLayout.getTabAt(i).setIcon(icons[i]);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (isAppointmentFragmentShowing) {
                    onBackPressed();
                    //isAppointmentFragmentShowing = !isAppointmentFragmentShowing;
                }
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (isAppointmentFragmentShowing) {
                    onBackPressed();
                }
            }
        });
    }

    private void registerGCM() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                regId = userPrefsManager.getRegId();
                if (regId.isEmpty()) {
                    intent = new Intent(MainActivity.this, RegistrationIntentService.class);
                    startService(intent);
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            }
            return false;
        }
        return true;
    }

    public void showAppointment() {
        isAppointmentFragmentShowing = true;
        tabLayout.setSelectedTabIndicatorColor(Color.TRANSPARENT);
        int pos = tabLayout.getSelectedTabPosition();
        tabLayout.getTabAt(pos).setIcon(iconsDeselected[pos]);
        GeneralFunctions.addFragmentFromRight(getFragmentManager()
                , new AppointmentRequestFragment(), R.id.flAppointmentParent);
    }

    @OnClick(R.id.fabAppointment)
    public void click(View view) {
        switch (view.getId()) {
            case R.id.fabAppointment:
//                if (userPrefsManager.showTutorial()) {
//                    userPrefsManager.setShowTutorial(false);
//                    GeneralFunctions.addFragmentFromRight(getFragmentManager(),
//                            new AppointmentTutorialFragment(), R.id.flAppointmentParent);
//                } else {
                showAppointment();
                //  }
                break;
        }
    }

    public void onEvent(CommonEvent commonEvent) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.SET_TAB_INDICATOR:
                isAppointmentFragmentShowing = false;
                tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.back_text_color));
                int pos = tabLayout.getSelectedTabPosition();
                tabLayout.getTabAt(pos).setIcon(icons[pos]);
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        NotificationHandler ce = new Gson().fromJson(
                intent.getStringExtra(Constants.MESSAGE_KEY), NotificationHandler.class);
        if (userPrefsManager == null) userPrefsManager = new UserPrefsManager(this);
        if (ce != null && ce.getMsg().equals(Constants.NOTIFICATION_TYPE_RATE_APP) && !ce.isRead()) {
            ce.setRead(true);
            Bundle b = new Bundle();
            b.putString(Constants.MESSAGE_KEY, new Gson().toJson(ce));
            getIntent().replaceExtras(b);
            setIntent(getIntent());
            GeneralFunctions.addFragmentFromRight(getFragmentManager(),
                    RateYourCare.newInstance(ce.getAppoint_id(), ""), R.id.flAppointmentParent);
        }
        if (intent.hasExtra("video_call")) {
            initCurrentSession(DataHolder.qbrtcSession);
            addIncomeCallFragment(DataHolder.qbrtcSession);
        }
    }

    private void addIncomeCallFragment(QBRTCSession session) {
        if (session != null) {
            Fragment fragment = new IncomeCallFragmentCopy();
            Bundle bundle = new Bundle();
            bundle.putSerializable("sessionDescription", session.getSessionDescription());
            bundle.putIntegerArrayList("opponents", new ArrayList<>(session.getOpponents()));
            bundle.putInt(Constants.CONFERENCE_TYPE, session.getConferenceType().getValue());
            fragment.setArguments(bundle);
            //FragmentExecuotr.addFragment(getFragmentManager(), R.id.fragment_container, fragment, INCOME_CALL_FRAGMENT);
            GeneralFunctions.addFragmentFromRight(getFragmentManager(), fragment, R.id.flAppointmentParent);
        } else {
            Log.d("", "SKIP addIncomeCallFragment method");
        }
    }

    public void initCurrentSession(QBRTCSession sesion) {
        DataHolder.qbrtcSession.addSessionCallbacksListener(MainActivity.this);
        DataHolder.qbrtcSession.addSignalingCallback(MainActivity.this);
    }

    public void rejectCurrentSession() {
        if (DataHolder.qbrtcSession != null) {
            DataHolder.qbrtcSession.rejectCall(new HashMap<String, String>());
        }
    }

    public void removeIncomeCallFragment() {
        onBackPressed();
    }

    public void addConversationFragmentReceiveCall() {

        QBRTCSession session = DataHolder.qbrtcSession;
        if (session != null) {
            Integer myId = QBChatService.getInstance().getUser().getId();
            ArrayList<Integer> opponentsWithoutMe = new ArrayList<>(session.getOpponents());
            opponentsWithoutMe.remove(new Integer(myId));
            opponentsWithoutMe.add(session.getCallerID());
            ArrayList<QBUser> opponents = new ArrayList<>();
            QBUser qbUser = new QBUser();
            qbUser.setId(opponentsWithoutMe.get(0));
            opponents.add(qbUser);
            getFragmentManager().popBackStackImmediate();
            ConversationFragmentCopy fragment = ConversationFragmentCopy.newInstance(opponents,
                    MainActivity.StartConversetionReason.INCOME_CALL_FOR_ACCEPTION);
            GeneralFunctions.addFragmentFromRight(getFragmentManager(), fragment, R.id.flAppointmentParent);
        }
    }

    private Fragment getCurrentFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment currentFragment = getFragmentManager()
                .findFragmentByTag(fragmentTag);
        return currentFragment;
    }

    @Override
    public void onBackPressed() {
        try {
            int stackEntryCount = getFragmentManager().getBackStackEntryCount();
            LoaderFragment fragment = (LoaderFragment) getFragmentManager().
                    findFragmentByTag(Constants.LOADER_FRAGMENT);
            if (fragment != null) {
                fragment.onBackPress();
                return;
            }
            if (stackEntryCount > 0) {
                stopDot();
                getFragmentManager().popBackStackImmediate();
                showDot();
                if (isAppointmentFragmentShowing && stackEntryCount == 1) {
                    isAppointmentFragmentShowing = false;
                    tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.back_text_color));
                    int pos = tabLayout.getSelectedTabPosition();
                    tabLayout.getTabAt(pos).setIcon(icons[pos]);
                }
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {

        }
    }

    private void stopDot() {
        try {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                Fragment topFragment = getCurrentFragment();
                if (topFragment instanceof DotInterface) {
                    ((DotInterface) topFragment).stopDot();
                }
            }
        } catch (Exception e) {
            Log.v("Helo", e + "");
        }
    }

    private void showDot() {
        try {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                Fragment topFragment = getCurrentFragment();
                if (topFragment instanceof DotInterface) {
                    ((DotInterface) topFragment).showDot();
                }
            }
        } catch (Exception e) {
            Log.v("Helo", e + "");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // startService(new Intent(getBaseContext(), VideoCallService.class));
        Intent intent = new Intent(MainActivity.this, VideoCallService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        startService(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    protected void showToast(String message) {
        try {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    @Override
    public void onStartConnectToUser(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onConnectedToUser(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onConnectionClosedForUser(QBRTCSession qbrtcSession, Integer integer) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    removeIncomeCallFragment();
                    getFragmentManager().popBackStackImmediate();
                } catch (Exception e) {

                }
            }
        });

    }

    @Override
    public void onDisconnectedFromUser(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onDisconnectedTimeoutFromUser(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onConnectionFailedWithUser(QBRTCSession qbrtcSession, Integer integer) {

    }

    @Override
    public void onError(QBRTCSession qbrtcSession, QBRTCException e) {
        showToast("Hello onError");
    }

    @Override
    public void onSuccessSendingPacket(QBSignalingSpec.QBSignalCMD qbSignalCMD, Integer integer) {
    }

    public void addRTCSessionUserCallback(QBRTCSessionUserCallback sessionUserCallback) {

        this.sessionUserCallback = sessionUserCallback;
    }

    @Override
    public void onErrorSendingPacket(QBSignalingSpec.QBSignalCMD qbSignalCMD, Integer userId, QBRTCSignalException e) {
        showToast(getString(R.string.dlg_signal_error));
    }

    public void addTCClientConnectionCallback(QBRTCSessionConnectionCallbacks clientConnectionCallbacks) {
        if (DataHolder.qbrtcSession != null) {
            DataHolder.qbrtcSession.addSessionCallbacksListener(clientConnectionCallbacks);
        }
    }

    public void removeRTCClientConnectionCallback(QBRTCSessionConnectionCallbacks clientConnectionCallbacks) {
        if (DataHolder.qbrtcSession != null) {
            DataHolder.qbrtcSession.removeSessionnCallbacksListener(clientConnectionCallbacks);
        }
    }

    public void removeRTCSessionUserCallback(QBRTCSessionUserCallback sessionUserCallback) {
        this.sessionUserCallback = null;
    }

    public void addVideoTrackCallbacksListener(QBRTCClientVideoTracksCallbacks videoTracksCallbacks) {
        if (DataHolder.qbrtcSession != null) {
            DataHolder.qbrtcSession.addVideoTrackCallbacksListener(videoTracksCallbacks);
        }
    }

    public void hangUpCurrentSession() {
        //ringtonePlayer.stop();
        if (DataHolder.qbrtcSession != null) {
            DataHolder.qbrtcSession.hangUp(new HashMap<String, String>());
        }
        onBackPressed();
        startRateActivity();
    }

    @Override
    public void onReceiveNewSession(QBRTCSession qbrtcSession) {

    }

    @Override
    public void onUserNotAnswer(QBRTCSession session, Integer userID) {

        if (sessionUserCallback != null) {
            sessionUserCallback.onUserNotAnswer(session, userID);
        }
        //ringtonePlayer.stop();
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getFragmentManager().popBackStackImmediate();
                }
            });
        } catch (Exception e) {
        }

    }

    @Override
    public void onCallRejectByUser(QBRTCSession session, Integer userID, Map<String, String> userInfo) {
        if (!session.equals(DataHolder.qbrtcSession)) {
            return;
        }
        if (sessionUserCallback != null) {
            sessionUserCallback.onCallRejectByUser(session, userID, userInfo);
        }
        onBackPressed();
    }

    @Override
    public void onCallAcceptByUser(QBRTCSession qbrtcSession, Integer userId, Map<String, String> userInfo) {
        if (sessionUserCallback != null) {
            sessionUserCallback.onCallAcceptByUser(qbrtcSession, userId, userInfo);
        }
    }

    public void startRateActivity() {
        startActivity(new Intent(this, RateActivity.class));
    }

    @Override
    public void onReceiveHangUpFromUser(QBRTCSession qbrtcSession, Integer userID) {
        if (sessionUserCallback != null) {
            sessionUserCallback.onReceiveHangUpFromUser(qbrtcSession, userID);
        }
        onBackPressed();
    }

    @Override
    public void onUserNoActions(QBRTCSession qbrtcSession, Integer integer) {

    }

    public void releaseCurrentSession() {
        DataHolder.qbrtcSession.removeSessionnCallbacksListener(this);
        DataHolder.qbrtcSession.removeSignalingCallback(this);
        DataHolder.qbrtcSession = null;
    }

    @Override
    public void onSessionClosed(QBRTCSession session) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    releaseCurrentSession();
                    onBackPressed();
                }
            });
        } catch (Exception e) {

        }
    }

    @Override
    public void onSessionStartClose(QBRTCSession qbrtcSession) {

    }

    public enum StartConversetionReason {
        INCOME_CALL_FOR_ACCEPTION,
        OUTCOME_CALL_MADE;
    }

    public interface QBRTCSessionUserCallback {
        void onUserNotAnswer(QBRTCSession session, Integer userId);

        void onCallRejectByUser(QBRTCSession session, Integer userId, Map<String, String> userInfo);

        void onCallAcceptByUser(QBRTCSession session, Integer userId, Map<String, String> userInfo);

        void onReceiveHangUpFromUser(QBRTCSession session, Integer userId);
    }
}
