package com.sanguinebits.docta;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBSignaling;
import com.quickblox.chat.QBWebRTCSignaling;
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.users.model.QBUser;
import com.quickblox.videochat.webrtc.QBRTCClient;
import com.quickblox.videochat.webrtc.QBRTCConfig;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.quickblox.videochat.webrtc.QBSignalingSpec;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionConnectionCallbacks;
import com.quickblox.videochat.webrtc.callbacks.QBRTCSignalingCallback;
import com.quickblox.videochat.webrtc.exception.QBRTCException;
import com.quickblox.videochat.webrtc.exception.QBRTCSignalException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.TabsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import doctorfragments.ConversationFragment;
import doctorfragments.DoctaVideoHome;
import doctorfragments.DoctorProfile;
import doctorfragments.IncomeCallFragment;
import emrfragments.EMRMedicalRecordFragment;
import emrfragments.EMRNotesFragment;
import emrfragments.EMROverView;
import emrfragments.EMRPatientProfile;
import emrfragments.OrderMedication;
import preferences.UserPrefsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Constants;
import utils.DataHolder;
import utils.DialogUtils;
import utils.FragmentExecuotr;
import utils.GeneralFunctions;
import utils.RingtonePlayer;
import utils.SettingsUtil;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;


/**
 * QuickBlox team
 */
public class CallActivity extends Activity implements
        QBRTCClientSessionCallbacks, QBRTCSessionConnectionCallbacks, QBRTCSignalingCallback {

    public static final String OPPONENTS_CALL_FRAGMENT = "opponents_call_fragment";
    public static final String INCOME_CALL_FRAGMENT = "income_call_fragment";
    public static final String CONVERSATION_CALL_FRAGMENT = "conversation_call_fragment";
    public static final String SESSION_ID = "sessionID";
    public static final String START_CONVERSATION_REASON = "start_conversation_reason";
    private static final String TAG = CallActivity.class.getSimpleName();
    private static QBChatService chatService;
    public List<QBUser> opponentsList;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    @Bind(R.id.fabAppointment)
    FloatingActionButton fabAppointment;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.topParent)
    LinearLayout topParent;
    private String hangUpReason;
    private QBRTCSession currentSession;
    private Runnable showIncomingCallWindowTask;
    private Handler showIncomingCallWindowTaskHandler;
    private BroadcastReceiver wifiStateReceiver;
    private boolean isInCommingCall;
    private boolean isInFront;
    private QBRTCClient rtcClient;
    private QBRTCSessionUserCallback sessionUserCallback;
    private boolean wifiEnabled = true;
    private SharedPreferences sharedPref;
    private RingtonePlayer ringtonePlayer;
    private QBUser callReceiverUser;
    private boolean typePatient = false;
    private boolean isVideo = false;
    private boolean isCall;
    private Appointment appointment;
    private UserPrefsManager userPrefsManager;

    public static ArrayList<Integer> getOpponentsIds(List<QBUser> opponents) {
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for (QBUser user : opponents) {
            ids.add(user.getId());
        }
        return ids;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        ButterKnife.bind(this);
        pBar.setVisibility(View.VISIBLE);
        userPrefsManager = new UserPrefsManager(this);
        typePatient = getIntent().getStringExtra("type").equals(Constants.TYPE_PATIENT) ? true : false;
        opponentsList = DataHolder.getUsers();
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        QBSettings.getInstance().fastConfigInit(Constants.APP_ID, Constants.AUTH_KEY, Constants.AUTH_SECRET);
        QBChatService.setDebugEnabled(true);
        //createAppSession();
        login();
        initWiFiManagerListener();
        ringtonePlayer = new RingtonePlayer(this, R.raw.beep);

        if (savedInstanceState == null) {
            if (typePatient) {
                callReceiverUser = new QBUser();
                callReceiverUser.setId(Integer.parseInt(userPrefsManager.getAssignedDoctorProfile().qbid));
                callReceiverUser.setCustomData(userPrefsManager.getAssignedDoctorProfile().photo);
                List<QBUser> userList = new ArrayList<>();
                userList.add(callReceiverUser);
                DataHolder.createUsersList(userList);
                FragmentExecuotr.addFragment(getFragmentManager(), R.id.fragment_container,
                        new DoctorProfile(), OPPONENTS_CALL_FRAGMENT);
            } else {
                fabAppointment.setVisibility(View.VISIBLE);
                callReceiverUser = new QBUser();
                appointment = userPrefsManager.getAppointment();
                callReceiverUser.setId(Integer.parseInt(appointment.getProfile().qbid));
                callReceiverUser.setCustomData(appointment.getProfile().photo);
                callReceiverUser.setCustomData(appointment.getProfile().photo);
                List<QBUser> userList = new ArrayList<>();
                userList.add(callReceiverUser);
                DataHolder.createUsersList(userList);
                setPager();
                GeneralFunctions.addFragmentFromRightWithTag(
                        getFragmentManager(), new DoctaVideoHome(), R.id.fragment_video_parent, "video_fragment");
            }

        }
    }


    private void login() {
        final QBUser user = new UserPrefsManager(this).getLoggedInUser().getQbUser();
        QBAuth.createSession(user.getLogin(), user.getPassword(), new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle bundle) {//8347861
                user.setId(session.getUserId());
                if (!QBChatService.isInitialized()) {
                    QBChatService.init(CallActivity.this);
                    chatService = QBChatService.getInstance();
                }
                chatService = QBChatService.getInstance();
                if (chatService.isLoggedIn()) {
                    initQBRTCClient();
                } else {
                    chatService.login(user, new QBEntityCallbackImpl<QBUser>() {
                        @Override
                        public void onSuccess() {
                            initQBRTCClient();
                        }

                        @Override
                        public void onError(List errors) {
                            try {
                                showToast("Error when login " + errors.get(0));
                                pBar.setVisibility(View.GONE);
                            } catch (Exception e) {

                            }
                        }
                    });
                }

            }

            @Override
            public void onError(List<String> errors) {
                try {
                    showToast("Error when login " + errors.get(0));
                    pBar.setVisibility(View.GONE);
                } catch (Exception e) {

                }

            }
        });
    }


    public void makeRequest(final RequestData requestData) {
        Call<CommonPojo> call = RestClient.get().makeRequest(requestData);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {

            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {

            }
        });
    }

    private void setPager() {
        int icons[] = {R.drawable.my_account_drawable, R.drawable.notes_drawable,
                R.drawable.medical_record_drawable, R.drawable.overview_drawable, R.drawable.orders_drawable};


        TabsAdapter tabsAdapter = new TabsAdapter(getFragmentManager());
        tabsAdapter.addFragment(new EMRPatientProfile());
        tabsAdapter.addFragment(new EMRNotesFragment());
        tabsAdapter.addFragment(new EMRMedicalRecordFragment());
        tabsAdapter.addFragment(new EMROverView());
        tabsAdapter.addFragment(OrderMedication.newInstance(Constants.HIDE, ""));

        viewpager.setAdapter(tabsAdapter);
        tabLayout.setupWithViewPager(viewpager);
        for (int i = 0; i < icons.length; i++) {
            tabLayout.getTabAt(i).setIcon(icons[i]);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
                RequestData requestData = new RequestData();
                requestData.parameters.apoint_id = userPrefsManager.getAppointment().getApoint_id();
                requestData.type = Constants.TYPE_PATIENT;
                requestData.request = RequestName.EMR_PROGRESS;
                requestData.parameters.pat_id = userPrefsManager.getAppointment().getUser_id();
                requestData.parameters.type = Constants.VIDEO_VISIT;
                requestData.parameters.ccount = tab.getPosition() + 1;
                requestData.parameters.doc_id = userPrefsManager.getLoggedInUser().user_id;
                makeRequest(requestData);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void createAppSession() {
        QBAuth.createSession(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {
                login();
            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(List<String> list) {
                showToast("Auhentication failure");
                pBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick({R.id.fabAppointment})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.fabAppointment:
                if (isVideo) {
                    isVideo = false;
                    topParent.setVisibility(View.VISIBLE);
                    fabAppointment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_video_switch));
                } else {
                    isVideo = true;
                    topParent.setVisibility(View.GONE);
                    fabAppointment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_emr_switch));
                }
                break;
        }
    }

    private void initQBRTCClient() {
        rtcClient = QBRTCClient.getInstance(this);
        // Add signalling manager
        QBChatService.getInstance().getVideoChatWebRTCSignalingManager().
                addSignalingManagerListener(new QBVideoChatSignalingManagerListener() {
                    @Override
                    public void signalingCreated(QBSignaling qbSignaling, boolean createdLocally) {
                        if (!createdLocally) {
                            rtcClient.addSignaling((QBWebRTCSignaling) qbSignaling);
                        }
                    }
                });

//        rtcClient.setCameraErrorHendler(new VideoCapturerAndroid.CameraErrorHandler() {
//            @Override
//            public void onCameraError(final String s) {
//                CallActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(CallActivity.this, s, Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//        });


        // Configure
        //
        QBRTCConfig.setMaxOpponentsCount(6);
        QBRTCConfig.setDisconnectTime(30);
        QBRTCConfig.setAnswerTimeInterval(30l);
        QBRTCConfig.setDebugEnabled(true);


        // Add activity as callback to RTCClient
        rtcClient.addSessionCallbacksListener(this);
        // Start mange QBRTCSessions according to VideoCall parser's callbacks
        rtcClient.prepareToProcessCalls();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    CallActivity.this.pBar.setVisibility(View.GONE);
                } catch (Exception e) {

                }

            }
        });
    }

    private void initWiFiManagerListener() {
        wifiStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "WIFI was changed");
                processCurrentWifiState(context);
            }
        };
    }

    private void processCurrentWifiState(Context context) {
        WifiManager wifi = (WifiManager) context.getSystemService(WIFI_SERVICE);
        if (wifiEnabled != wifi.isWifiEnabled()) {
            wifiEnabled = wifi.isWifiEnabled();
            showToast("Wifi " + (wifiEnabled ? "enabled" : "disabled"));
        }
    }

    private void disableConversationFragmentButtons() {
        ConversationFragment fragment = (ConversationFragment) getFragmentManager().findFragmentByTag(CONVERSATION_CALL_FRAGMENT);
        if (fragment != null) {
            fragment.actionButtonsEnabled(false);
        }
    }

    private void initIncommingCallTask() {
        showIncomingCallWindowTaskHandler = new Handler(Looper.myLooper());
        showIncomingCallWindowTask = new Runnable() {
            @Override
            public void run() {
                IncomeCallFragment incomeCallFragment = (IncomeCallFragment) getFragmentManager().findFragmentByTag(INCOME_CALL_FRAGMENT);
                if (incomeCallFragment == null) {
                    ConversationFragment conversationFragment = (ConversationFragment) getFragmentManager().findFragmentByTag(CONVERSATION_CALL_FRAGMENT);
                    if (conversationFragment != null) {
                        disableConversationFragmentButtons();
                        ringtonePlayer.stop();
                        hangUpCurrentSession();
                    }
                } else {
                    rejectCurrentSession();
                }
                Toast.makeText(CallActivity.this, "Call was stopped by timer", Toast.LENGTH_LONG).show();
            }
        };
    }

    public void rejectCurrentSession() {
        if (getCurrentSession() != null) {
            getCurrentSession().rejectCall(new HashMap<String, String>());
        }
        if (typePatient)
            startRateActivity();
        else
            visitCompleted();
    }

    public void hangUpCurrentSession() {
        ringtonePlayer.stop();
        if (getCurrentSession() != null) {
            getCurrentSession().hangUp(new HashMap<String, String>());
        }
        if (typePatient)
            startRateActivity();
        else
            visitCompleted();
    }

    private void startIncomeCallTimer(long time) {
        showIncomingCallWindowTaskHandler.postAtTime(showIncomingCallWindowTask, SystemClock.uptimeMillis() + time);
    }

    private void stopIncomeCallTimer() {
        Log.d(TAG, "stopIncomeCallTimer");
        showIncomingCallWindowTaskHandler.removeCallbacks(showIncomingCallWindowTask);
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        registerReceiver(wifiStateReceiver, intentFilter);
    }

    @Override
    protected void onResume() {
        isInFront = true;
        if (currentSession == null) {
            //addOpponentsFragment();
            if (typePatient)
                FragmentExecuotr.addFragment(getFragmentManager(), R.id.fragment_container,
                        new DoctorProfile(), OPPONENTS_CALL_FRAGMENT);
            else
                setPager();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        isInFront = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(wifiStateReceiver);
    }

    public QBRTCSession getCurrentSession() {
        return currentSession;
    }


    public void initCurrentSession(QBRTCSession sesion) {
        this.currentSession = sesion;
        this.currentSession.addSessionCallbacksListener(CallActivity.this);
        this.currentSession.addSignalingCallback(CallActivity.this);
    }

    public void releaseCurrentSession() {
       // this.currentSession.removeSessionnCallbacksListener(CallActivity.this);
        this.currentSession.removeSessionCallbacksListener(CallActivity.this);
        this.currentSession.removeSignalingCallback(CallActivity.this);
        this.currentSession = null;
    }

    // ---------------Chat callback methods implementation  ----------------------//

    @Override
    public void onBackPressed() {
        try {
            if (isVideo) {
                isVideo = false;
                topParent.setVisibility(View.VISIBLE);
                return;
            }
            ConversationFragment fragment = (ConversationFragment) getFragmentManager().
                    findFragmentByTag(CONVERSATION_CALL_FRAGMENT);
            if (fragment != null && isVideo)
                return;
            int stackEntryCount = getFragmentManager().getBackStackEntryCount();
            if (stackEntryCount > 0) {
                getFragmentManager().popBackStackImmediate();
            } else {
                if (!isCall)
                    super.onBackPressed();
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onReceiveNewSession(final QBRTCSession session) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Log.d(TAG, "Session " + session.getSessionID() + " are income");
                //String curSession = (getCurrentSession() == null) ? null : getCurrentSession().getSessionID();

                if (getCurrentSession() == null) {
                    Log.d(TAG, "Start new session");
                    initCurrentSession(session);
                    addIncomeCallFragment(session);

                    isInCommingCall = true;
                    initIncommingCallTask();
                } else {
                    Log.d(TAG, "Stop new session. Device now is busy");
                    session.rejectCall(null);
                }

            }
        });
    }

    @Override
    public void onUserNotAnswer(QBRTCSession session, Integer userID) {
        if (!session.equals(getCurrentSession())) {
            return;
        }
        if (sessionUserCallback != null) {
            sessionUserCallback.onUserNotAnswer(session, userID);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ringtonePlayer.stop();
                if (typePatient)
                    startRateActivity();
                else
                    visitCompleted();
            }
        });
    }

    @Override
    public void onUserNoActions(QBRTCSession qbrtcSession, Integer integer) {
        startIncomeCallTimer(0);
    }

    @Override
    public void onStartConnectToUser(QBRTCSession session, Integer userID) {

    }

    @Override
    public void onCallAcceptByUser(QBRTCSession session, Integer userId, Map<String, String> userInfo) {
        if (!session.equals(getCurrentSession())) {
            return;
        }
        if (sessionUserCallback != null) {
            sessionUserCallback.onCallAcceptByUser(session, userId, userInfo);
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ringtonePlayer.stop();
            }
        });
    }


    @Override
    public void onCallRejectByUser(QBRTCSession session, Integer userID, Map<String, String> userInfo) {
        if (!session.equals(getCurrentSession())) {
            return;
        }
        if (sessionUserCallback != null) {
            sessionUserCallback.onCallRejectByUser(session, userID, userInfo);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ringtonePlayer.stop();
                if (typePatient)
                    startRateActivity();
                else
                    visitCompleted();
            }
        });
    }

    @Override
    public void onConnectionClosedForUser(QBRTCSession session, Integer userID) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Close app after session close of network was disabled
                if (hangUpReason != null && hangUpReason.equals(Constants.WIFI_DISABLED)) {
                    Intent returnIntent = new Intent();
                    setResult(Constants.CALL_ACTIVITY_CLOSE_WIFI_DISABLED, returnIntent);
                    finish();
                }
            }
        });
    }

    @Override
    public void onConnectedToUser(QBRTCSession session, final Integer userID) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isInCommingCall) {
                    stopIncomeCallTimer();
                }

//                startTimer();
                Log.d(TAG, "onConnectedToUser() is started");

            }
        });
    }

    @Override
    public void onDisconnectedTimeoutFromUser(QBRTCSession session, Integer userID) {

    }

    @Override
    public void onConnectionFailedWithUser(QBRTCSession session, Integer userID) {

    }

    @Override
    public void onError(QBRTCSession qbrtcSession, QBRTCException e) {
    }

    @Override
    public void onSessionClosed(final QBRTCSession session) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Log.d(TAG, "Session " + session.getSessionID() + " start stop session");
                String curSession = (getCurrentSession() == null) ? null : getCurrentSession().getSessionID();

                if (session.equals(getCurrentSession())) {

                    Fragment currentFragment = getCurrentFragment();
                    if (isInCommingCall) {
                        stopIncomeCallTimer();
                        if (currentFragment instanceof IncomeCallFragment) {
                            removeIncomeCallFragment();
                        }
                    }
                    releaseCurrentSession();
                    finish();//new addition
                }
            }
        });
    }

    @Override
    public void onSessionStartClose(final QBRTCSession session) {
     //   session.removeSessionnCallbacksListener(CallActivity.this);
        session.removeSessionCallbacksListener(CallActivity.this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ConversationFragment fragment = (ConversationFragment)
                        getFragmentManager().findFragmentByTag(CONVERSATION_CALL_FRAGMENT);
                if (fragment != null && session.equals(getCurrentSession())) {
                    fragment.actionButtonsEnabled(false);
                }
            }
        });
    }

    @Override
    public void onDisconnectedFromUser(QBRTCSession session, Integer userID) {

    }

    private void showToast(final int message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.showToast(CallActivity.this, message);
            }
        });
    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.showToast(CallActivity.this, message);
            }
        });
    }

    private void startRateActivity() {
        Intent intent = new Intent(this, RateActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    protected String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private void visitCompleted() {
        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.request = RequestName.COMPLETE_VISIT;
        requestData.parameters.pat_id = appointment.getUser_id();
        requestData.parameters.apoint_id = appointment.getApoint_id();
        if (pBar != null)
            pBar.setVisibility(View.VISIBLE);
        Call<CommonPojo> call = RestClient.get().makeRequest(requestData);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                if (pBar != null)
                    pBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {
                    CommonPojo data = response.body();
                    if (data.getMsg() != null)
                        showToast(data.getMsg());
                    if (data.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                        try {
                            showToast(R.string.visit_completed);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else try {
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    showToast(R.string.there_might_be_some_problem);
                }
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    if (pBar != null)
                        pBar.setVisibility(View.GONE);
                    finish();
                } catch (Exception e) {

                }
            }

        });
    }
    @Override
    public void onReceiveHangUpFromUser(QBRTCSession qbrtcSession, Integer integer, Map<String, String> map) {
        if (qbrtcSession.equals(getCurrentSession())) {

            if (sessionUserCallback != null) {
                sessionUserCallback.onReceiveHangUpFromUser(qbrtcSession, integer);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (typePatient)
                        startRateActivity();
                    else
                        visitCompleted();
                }
            });
        }

    }
//
//    @Override
//    public void onReceiveHangUpFromUser(final QBRTCSession session, final Integer userID) {
//        if (session.equals(getCurrentSession())) {
//
//            if (sessionUserCallback != null) {
//                sessionUserCallback.onReceiveHangUpFromUser(session, userID);
//            }
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    if (typePatient)
//                        startRateActivity();
//                    else
//                        visitCompleted();
//                }
//            });
//        }
//    }

    private Fragment getCurrentFragment() {
        return getFragmentManager().findFragmentById(R.id.fragment_container);
    }

    public void removeIncomeCallFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(INCOME_CALL_FRAGMENT);

        if (fragment != null) {
            FragmentExecuotr.removeFragment(fragmentManager, fragment);
        }
    }

    private void addIncomeCallFragment(QBRTCSession session) {

        Log.d(TAG, "QBRTCSession in addIncomeCallFragment is " + session);
        if (session != null && isInFront) {
            DoctorProfile doctorProfile = (DoctorProfile) getFragmentManager().findFragmentByTag(OPPONENTS_CALL_FRAGMENT);
            if (doctorProfile != null) doctorProfile.stopAnimation();
            Fragment fragment = new IncomeCallFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("sessionDescription", session.getSessionDescription());
            bundle.putIntegerArrayList("opponents", new ArrayList<>(session.getOpponents()));
            bundle.putInt(Constants.CONFERENCE_TYPE, session.getConferenceType().getValue());
            fragment.setArguments(bundle);
            FragmentExecuotr.addFragment(getFragmentManager(), R.id.fragment_container, fragment, INCOME_CALL_FRAGMENT);
        } else {
            Log.d(TAG, "SKIP addIncomeCallFragment method");
        }
    }

    public void addConversationFragmentStartCall(QBUser opponent,
                                                 QBRTCTypes.QBConferenceType qbConferenceType,
                                                 Map<String, String> userInfo) {
        isCall = true;
        List<QBUser> opponents = new ArrayList<>();
        opponents.add(opponent);
        QBRTCSession newSessionWithOpponents = rtcClient.createNewSessionWithOpponents(
                getOpponentsIds(opponents), qbConferenceType);
        SettingsUtil.setSettingsStrategy(opponents,
                getDefaultSharedPrefs(),
                this);
        Log.d("Crash", "addConversationFragmentStartCall. Set session " + newSessionWithOpponents);
        initCurrentSession(newSessionWithOpponents);
        ConversationFragment fragment = ConversationFragment.newInstance(opponents, opponents.get(0).getFullName(),
                qbConferenceType, userInfo,
                StartConversetionReason.OUTCOME_CALL_MADE, getCurrentSession().getSessionID());
        FragmentExecuotr.addFragment(getFragmentManager(), R.id.fragment_video_parent, fragment, CONVERSATION_CALL_FRAGMENT);
        ringtonePlayer.play(true);
    }

    public void addConversationFragmentReceiveCall() {

        QBRTCSession session = getCurrentSession();
        if (getCurrentSession() != null) {
            Integer myId = QBChatService.getInstance().getUser().getId();
            ArrayList<Integer> opponentsWithoutMe = new ArrayList<>(session.getOpponents());
            opponentsWithoutMe.remove(new Integer(myId));
            opponentsWithoutMe.add(session.getCallerID());

            ArrayList<QBUser> opponents = DataHolder.getUsersByIDs(opponentsWithoutMe.toArray(new Integer[opponentsWithoutMe.size()]));
            SettingsUtil.setSettingsStrategy(opponents, getDefaultSharedPrefs(), this);
            ConversationFragment fragment = ConversationFragment.newInstance(opponents,
                    DataHolder.getUserNameByID(session.getCallerID()),
                    session.getConferenceType(), session.getUserInfo(),
                    StartConversetionReason.INCOME_CALL_FOR_ACCEPTION, getCurrentSession().getSessionID());
            // Start conversation fragment
            FragmentExecuotr.addFragment(getFragmentManager(), R.id.fragment_container, fragment, CONVERSATION_CALL_FRAGMENT);
        }
    }

    public List<QBUser> getOpponentsList() {
        return opponentsList;
    }

    public void setOpponentsList(List<QBUser> qbUsers) {
        this.opponentsList = qbUsers;
    }

    public void addVideoTrackCallbacksListener(QBRTCClientVideoTracksCallbacks videoTracksCallbacks) {
        if (currentSession != null) {
            currentSession.addVideoTrackCallbacksListener(videoTracksCallbacks);
        }
    }

    public void addTCClientConnectionCallback(QBRTCSessionConnectionCallbacks clientConnectionCallbacks) {
        if (currentSession != null) {
            currentSession.addSessionCallbacksListener(clientConnectionCallbacks);
        }
    }

    public void removeRTCClientConnectionCallback(QBRTCSessionConnectionCallbacks clientConnectionCallbacks) {
        if (currentSession != null) {
          //  currentSession.removeSessionnCallbacksListener(clientConnectionCallbacks);
            currentSession.removeSessionCallbacksListener(clientConnectionCallbacks);
        }
    }

    public void addRTCSessionUserCallback(QBRTCSessionUserCallback sessionUserCallback) {
        this.sessionUserCallback = sessionUserCallback;
    }

    public void removeRTCSessionUserCallback(QBRTCSessionUserCallback sessionUserCallback) {
        this.sessionUserCallback = null;
    }

    public SharedPreferences getDefaultSharedPrefs() {
        return sharedPref;
    }

    @Override
    public void onSuccessSendingPacket(QBSignalingSpec.QBSignalCMD qbSignalCMD, Integer integer) {
    }

    @Override
    public void onErrorSendingPacket(QBSignalingSpec.QBSignalCMD qbSignalCMD, Integer userId, QBRTCSignalException e) {
        showToast(R.string.dlg_signal_error);
    }

    @Override
    protected void onDestroy() {
        try {
            chatService.logout();
            chatService.destroy();
            if (currentSession != null)
                onSessionClosed(currentSession);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }


    public enum StartConversetionReason {
        INCOME_CALL_FOR_ACCEPTION,
        OUTCOME_CALL_MADE;
    }

    public interface QBRTCSessionUserCallback {
        void onUserNotAnswer(QBRTCSession session, Integer userId);

        void onCallRejectByUser(QBRTCSession session, Integer userId, Map<String, String> userInfo);

        void onCallAcceptByUser(QBRTCSession session, Integer userId, Map<String, String> userInfo);

        void onReceiveHangUpFromUser(QBRTCSession session, Integer userId);
    }

}

