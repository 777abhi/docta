package utils;

import android.os.Environment;

/**
 * Created by OM on 11/17/2015.
 */
public class Constants {

    // server key - AIzaSyA0nQtgTd5XYiQeCitLcuPTpVhXd28M-0c, sender - 37870097825
    public static final String APP_ID = "33069";
    public static final String AUTH_KEY = "jF8MynUjM4m2aDJ";
    public static final String AUTH_SECRET = "LbhVpMXtR-STR2p";
    public static final String APP_NAME = "Docta";
    public static final String MIME_TYPE_IMAGE = "image/jpg";
    public static final String LOCAL_FILE_PREFIX = "file://";
    public static final String GCM_PROJECT_NO = "685831788973";
    public static final String MALE = "m";
    public static final String FEMALE = "f";
    public static final String MESSAGE_KEY = "msg";
    public static final String EXTRA_MESSAGE_KEY = "msg_display";

    public static final String PUSH_ACTION = "com.codesnickers.docta.gcm";
    public static final String TYPE_DOCTOR = "doctor";
    public static final String TYPE_PATIENT = "patient";
    public static final String USER_TYPE_NURSE = "nurse";

    public static final int GALLERY_REQUEST = 1;
    public static final int CAMERA_REQUEST = 1888;

    public static final String JPEG_FILE_PREFIX = "IMG_";
    public static final String JPEG_FILE_SUFFIX = ".jpg";

    public static final String LOCAL_STORAGE_BASE_PATH_FOR_MEDIA = Environment
            .getExternalStorageDirectory() + "/" + APP_NAME;

    public static final String LOCAL_STORAGE_BASE_PATH_FOR_USER_PHOTOS =
            LOCAL_STORAGE_BASE_PATH_FOR_MEDIA + "/User/Photos/";

    public static final long ROTATION_PERIOD = 1000;
    public static final int PASSWORD_LENGTH = 8;


    public static final int CALL_ACTIVITY_CLOSE = 1000;

    //CALL ACTIVITY CLOSE REASONS
    public static final int CALL_ACTIVITY_CLOSE_WIFI_DISABLED = 1001;
    public static final String WIFI_DISABLED = "wifi_disabled";

    public final static String OPPONENTS = "opponents";
    public static final String CONFERENCE_TYPE = "conference_type";
    public static final long PROGRESS_TIME = 10000;
    public static final String TAG_PRIMARY_HEALTH_CONCERN = "tag_primary_health_concern";
    public static final String WAIT_FOR_CONSULT_REQUEST = "wait_for_consult_request";
    public static final String TAG_CONSULT_REQUEST = "consult_request";
    public static final String COMMON = "common";
    public static final String PAST = "past";

    public static final CharSequence AM = "AM";
    public static final CharSequence PM = "PM";
    public static final int BP = 1;
    public static final int TEMP = 2;
    public static final int PULSE = 3;
    public static final int OSAT = 4;
    public static final int BMI = 5;

    public static final int GREEN = 1;
    public static final int YELLOW = 2;
    public static final int RED = 3;

    public static final String YELLOW_COLOR = "yellow";
    public static final String RED_COLOR = "red";

    public static final String EMR = "emr";
    public static final String COMMA_SEPARATOR = ", ";
    public static final String SCHEDULE = "schedule";
    public static final String DASH = "-";
    public static final String OR = "or";
    public static final String ACTIVATE_DEACTIVATE_MEDICATION = "emr_disablePrescription";
    public static final long REPEAT_TIME = 60000;
    public static final String INCOMPLETE = "0";
    public static final String PATIENT_UPDATE = "patient_update_profile";
    public static final String KEY_TITLE = "title";
    public static final String VIDEO_VISIT = "video_visit";
    public static final float MAX_TEMP = 500;
    public static final float MAX_PULSE = 150;
    public static final float MAX_OSAT = 100;
    public static final float MAX_WEIGHT = 400;
    public static final float MAX_HEIGHT = 300;
    public static final float MIN_TEMP = 0;
    public static final float MIN_PULSE = 0;
    public static final float MIN_OSAT = 0;
    public static final float MIN_WEIGHT = 0;
    public static final float MIN_HEIGHT = 0;
    public static final float MAX_SYS = 200;
    public static final int MIN_SYS = 50;
    public static final int MAX_DIAS = 180;
    public static final int MIN_DIAS = 20;
    public static final String GENERAL = "1";
    public static final String SKIN_CARE = "2";
    public static final String CHILD_CARE = "3";
    public static final String PREGNANCY_NEW_BORN = "4";
    public static final String HIDE = "hide";
    public static final String NOTIFICATION_TYPE_GET_PENDING_VISIT_REQUEST = "1";
    public static final String NOTIFICATION_TYPE_GET_AVAILABLE_DOCTOR = "2";
    public static final String NOTIFICATION_TYPE_RATE_APP = "3";
    public static final String NOTIFICATION_DOCTOR_NURSE_CHECKIN = "4";
    public static final String NOTIFICATION_EMR_PROGRESS = "5";
    public static final String RECEPTIONIST_CHECKIN = "6";
    public static final String NOTIFICATION_PATIENT_CANCEL_APPOINTMENT = "7";
    public static final String CONFIRM_APPOINTMENT_NOTIFICATION = "8";

    public static final String MEDICATION_NOT_TAKEN = "0";
    public static final String MEDICATION_TAKEN = "1";
    public static final String GENERAL_NOTIFICATION = "51";
    public static final int LAB_COMPONENT_GRAPH = 101;
    public static final CharSequence DEFAULT_COUNTRY_CODE = "+233 ";
    public static final int LIMIT = 3;
    public static final String MASTER_KEY = "b5ddc4e8-2df5-47be-8624-e8f2c9d35f59";
    public static final String PUBLIC_KEY = "live_public_70-xBlQfHqqcCqof3S3is6V1H6k";
    public static final String PRIVATE_KEY = "live_private_vrwCMVDCypH1cG4f_LfWC3Iouvc";
    public static final String TOKEN = "566eca329bb587c6c7f3";
    public static final int TYPE_CLINIC_VISIT = 0;
    public static final int TYPE_VIDEO_VISIT = 1;
    public static final String APPOINTMENT_DATA = "appointment_data";

    public static String LOADER_FRAGMENT = "loader_fragment";
}
