package utils;

/**
 * Created by OM on 11/17/2015.
 */
public class WebConstants {
    public static final String ANDROID = "android";
    public static String BASE_URL = "http://ec2-52-29-242-135.eu-central-1.compute.amazonaws.com/test/";
    public static String RETROFIT_SUCCESS = "1";
    public static String USER_TYPE_PATIENT = "patient";
    public static String USER_TYPE_PATIENT_CHILD = "patient_child_user";
    public static String REQUEST_CHILD_UPDATE = "pat_child_update";
    public static String PATIENT_CHILD_USER = "patient_child_user";
}
