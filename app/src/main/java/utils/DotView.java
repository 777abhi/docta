package utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by anirudhnanda on 18/10/2016 AD.
 */

public class DotView extends TextView {
    int status;

    long currentTime = System.currentTimeMillis();
    long timeDelay = 500;
    String str = "...";
    SpannableStringBuilder spannableStringBuilder;// = new SpannableStringBuilder(str + str);

    public DotView(Context context, AttributeSet attrs) {
        super(context, attrs);
//        str = getText().toString() + str;
//        spannableStringBuilder = new SpannableStringBuilder(str);
//        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.BLACK),
//                str.length() - 3, str.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
//        spannableStringBuilder.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
//                str.length() - 3, str.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
//        setText(spannableStringBuilder);
    }

    String a;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//
        a = getText().toString();
        if(!a.contains("...")) {
            a = a + "...";
            spannableStringBuilder = new SpannableStringBuilder(a);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.TRANSPARENT),
                a.length() - 3, a.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableStringBuilder.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                a.length() - 3, a.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }


        if (System.currentTimeMillis() - currentTime < timeDelay) {
            invalidate();
            return;
        } else {
            currentTime = System.currentTimeMillis();
        }
        switch (status) {
            case 0:
                status = 1;
                spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.BLACK),
                        a.length() - 3, a.length() - 2, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                setText(spannableStringBuilder);
                break;
            case 1:
                status = 2;
                spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.BLACK),
                        a.length() - 2, a.length() - 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                setText(spannableStringBuilder);
                break;
            case 2:
                status = 3;
                spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.BLACK),
                        a.length() - 1, a.length() - 0, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                setText(spannableStringBuilder);
                break;
            case 3:
                spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.TRANSPARENT),
                        a.length() - 3, a.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                setText(spannableStringBuilder);
                status = 0;
                break;
        }

        // postInvalidateDelayed(timeDelay);
    }

    public void resume() {
    }


    public void stopHandler() {
    }

    public Runnable run = new Runnable() {
        @Override
        public void run() {
        }
    };
}
