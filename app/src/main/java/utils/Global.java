package utils;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.pnikosis.materialishprogress.ProgressWheel;

import interfaces.LocationGetListener;

/**
 * Created by OM on 11/18/2015.
 */
public class Global extends Application {
    public static double latitude = 0, longitude = 0;
    public static Context context;
    public static MyLocation.LocationResult locationResult;

    public static LocationGetListener locationGetListener;


    public static void getLocation(ProgressWheel progressWheel,
                                   LocationGetListener locationGetListener) {
        if (latitude == 0 && longitude == 0) {
            progressWheel.setVisibility(View.VISIBLE);
            MyLocation myLocation = new MyLocation();
            Global.locationGetListener = locationGetListener;
            myLocation.getLocation(context, locationResult);
        } else locationGetListener.onLocationRetrieved();

    }


    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        context = this;
        locationResult = new MyLocation.LocationResult() {
            @Override
            public void gotLocation(Location location) {
                // Got the location!
                try {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    locationGetListener.onLocationRetrieved();
                } catch (Exception e) {
                    latitude = 30;
                    longitude = 70;
                }

            }
        };
    }

}
