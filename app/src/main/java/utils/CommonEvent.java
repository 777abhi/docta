package utils;

/**
 * Created by OM on 4/15/2016.
 */
public class CommonEvent {
    public static int EVENT_TYPE;

    public static final int FAV_CLINIC_ADDED = 1;
    public static final int FAV_PHARMACY_ADDED = 2;
    public static final int REFRESH = 3;
    public static final int NO_DATA = 4;
    public static final int SET_TAB_INDICATOR = 5;
    public static final int PROFILE_CHANGE = 6;

    public CommonEvent(int eventType) {
        EVENT_TYPE = eventType;
    }

}
