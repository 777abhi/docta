package utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.text.Editable;
import android.text.Layout;
import android.text.Spannable;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import com.sanguinebits.docta.R;


/**
 * Created by OM on 6/7/2016.
 */
public class DotViewBackup extends EditText {
    public int drawableWidth;
    public Handler mHandler = new Handler();
    private int dotColor, dotRadius, dotDistance, blinkSpeed;
    private int track = 2;

    private boolean isHandlerActive = true;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            setVisibility(VISIBLE);
            Editable ett = getText();
            MySpan mySpan = new MySpan(dotRadius, dotDistance, dotColor);
            mySpan.setTrack(track);
            ImageSpan span = new ImageSpan(mySpan, ImageSpan.ALIGN_BOTTOM);
           Log.v("cdhcbdhkcbdhb",getText().toString());
            ett.setSpan(span, getText().toString().length() - 1,
                    getText().toString().length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            if (isHandlerActive)
                mHandler.postDelayed(runnable, blinkSpeed);
        }
    };
    public Runnable run = new Runnable() {
        @Override
        public void run() {
            setSpan();
        }
    };

    public DotViewBackup(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEnabled(false);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.dotanimator,
                0, 0);
        try {
            dotColor = a.getInt(R.styleable.dotanimator_dot_color, Color.DKGRAY);
            dotDistance = a.getDimensionPixelSize(R.styleable.dotanimator_dot_distance, 10);
            dotRadius = a.getDimensionPixelSize(R.styleable.dotanimator_dot_radius, 7);
            blinkSpeed = a.getInteger(R.styleable.dotanimator_blink_speed, 500);
            drawableWidth = 6 * dotRadius + 2 * dotDistance;
        } finally {
            a.recycle();
        }
        //
    }

    public void resume() {
        isHandlerActive = true;
        mHandler.postDelayed(runnable, blinkSpeed);

    }

    
    public void stopHandler() {
        isHandlerActive = false;
    }

    public void setSpan() {
        int lineCount = getLineCount();
        getText().append("a");
        Layout layout = getLayout();
        String text = getText().toString();
        int start = 0;
        int end;
        String str = "";

        for (int i = 0; i < getLineCount(); i++) {
            end = layout.getLineEnd(i);
            str = text.substring(start, end);
            start = end;
        }
        if (lineCount != getLineCount())
            getText().insert(getText().toString().indexOf(str), "\n");
        int textViewWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        int numChars;
        Paint paint = getPaint();
        for (numChars = 1; numChars <= str.length(); ++numChars) {
            float repacementCharacterWidth = paint.measureText("a", 0, 1);
            if (paint.measureText(str, 0, numChars) + drawableWidth + repacementCharacterWidth > textViewWidth) {
                int lastSpaceIndex = text.lastIndexOf(" ");
                if (lastSpaceIndex != -1)
                    getText().insert(lastSpaceIndex, "\n");
                else
                    getText().insert(getText().length() - 1, "\n");
                break;
            }
        }
        mHandler.post(runnable);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        switch (track) {
            case 1:
                track = 2;//
                break;
            case 2:
                track = 3;
                break;
            case 3:
                track = 1;
                break;
        }
    }
}
