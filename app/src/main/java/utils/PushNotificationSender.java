package utils;

import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBNotificationType;

/**
 * Created by tereha on 13.05.16.
 */
public class PushNotificationSender {

    public static void sendPushMessage(Integer recipients, String senderName) {
        String outMessage = String.format("%s is sending you notification", senderName);

        // Send Push: create QuickBlox Push Notification Event
        QBEvent qbEvent = new QBEvent();
        qbEvent.setNotificationType(QBNotificationType.PUSH);
        qbEvent.setEnvironment(QBEnvironment.DEVELOPMENT);
        // Generic push - will be delivered to all platforms (Android, iOS, WP, Blackberry..)
        qbEvent.setMessage(outMessage);

       // StringifyArrayList<Integer> userIds = new StringifyArrayList<>(recipients);

        qbEvent.setUserId(recipients);

        QBPushNotifications.createEvent(qbEvent).performAsync(null);
    }
}
