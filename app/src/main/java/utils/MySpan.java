package utils;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

/**
 * Created by OM on 6/7/2016.
 */
public class MySpan extends Drawable {
    int track = 1;
    private Paint paint;
    private int dotRadius, dotDistance;
    private int y;

    private int dotDiameter;

    public MySpan(int dotRadius, int dotDistance, int dotColor) {
        this.dotDistance = dotDistance;
        this.dotRadius = dotRadius;
        y = -(dotRadius * 2 + 4);
        dotDiameter = 2 * dotRadius;
        paint = new Paint();
        paint.setColor(dotColor);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
    }

    @Override
    public void draw(Canvas canvas) {
        switch (track) {
            case 1:
                canvas.drawCircle(dotDistance + dotRadius, y, dotRadius, paint);
                break;
            case 2:
                canvas.drawCircle(dotDistance + dotRadius, y, dotRadius, paint);
                canvas.drawCircle(3 * dotRadius + 2 * dotDistance, y, dotRadius, paint);
                break;
            case 3:
                canvas.drawCircle(dotDistance + dotRadius, y, dotRadius, paint);
                canvas.drawCircle(dotDiameter + (2 * dotDistance) + dotRadius, y, dotRadius, paint);
                canvas.drawCircle(5 * dotRadius + 3 * dotDistance, y, dotRadius, paint);
                //canvas.drawColor(Color.WHITE);
                break;
        }
        //canvas.drawCircle(10, 10, 10, paint);

    }


    public void setTrack(int track) {
        this.track = track;
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }
}
