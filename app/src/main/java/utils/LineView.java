package utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.sanguinebits.docta.R;


/**
 * Created by OM on 11/17/2015.
 */
public class LineView extends View {

    private boolean isSingleLine;
    private int color1, color2;

    public LineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.myView,
                0, 0);

        try {
            isSingleLine = a.getBoolean(R.styleable.myView_singleLine, true);
            color1 = a.getColor(R.styleable.myView_color1, 0);
            color2 = a.getColor(R.styleable.myView_color2, 0);

        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        paint.setAntiAlias(true);
        paint.setColor(color1);
        if (isSingleLine) {
            canvas.drawLine(0, 0, getWidth(), 0, paint);
        } else {
            canvas.drawLine(0, 0, getWidth()/2, 0, paint);
            paint.setColor(color2);
            canvas.drawLine(getWidth() / 2, 0, getWidth(), 0, paint);
        }

    }
}
