package utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sanguinebits.docta.R;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by OM on 11/17/2015.
 */
public class GeneralFunctions {

    public static void addFragmentFromRight(FragmentManager fragmentManager, Fragment fragment,
                                            int containerId) {
        fragmentManager.beginTransaction().
                setCustomAnimations(R.animator.slide_right_in, 0,
                        0, R.animator.slide_right_out)
                .add(containerId, fragment, fragment.getClass().getName())
                .addToBackStack(fragment.getClass().getName()).commit();
    }

    public static void addFragmentFromRightWithTag(FragmentManager fragmentManager, Fragment fragment,
                                                   int containerId, String tag) {
        fragmentManager.beginTransaction().
                setCustomAnimations(R.animator.slide_right_in, 0,
                        0, R.animator.slide_right_out)
                .add(containerId, fragment, tag)
                .addToBackStack(null).commit();
    }

    public static String getFormatedDate(String date) {
        String pattern = "EEEE MMM d, yyyy";
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return new SimpleDateFormat(pattern).format(df.parse(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void replaceFragFromRight(FragmentManager fragmentManager, Fragment fragment,
                                            int containerId) {
        fragmentManager.beginTransaction().setCustomAnimations(R.animator.slide_right_in, 0, 0,
                R.animator.slide_right_out).replace(containerId, fragment).commit();
    }

    public static void addFragmentFromRightWithBackstack(FragmentManager fragmentManager, Fragment fragment,
                                                         int containerId) {
        fragmentManager.beginTransaction().
                setCustomAnimations(R.animator.slide_right_in, 0,
                        0, R.animator.slide_right_out)
                .replace(containerId, fragment)
                .addToBackStack(null).commit();
    }

    public static void addFragWithOvershoot(FragmentManager fragmentManager, Fragment fragment,
                                            String tag,
                                            int containerId) {
        fragmentManager.beginTransaction().setCustomAnimations(R.animator.overshoot_slide_up, 0, 0,
                R.animator.overshoot_slide_down).add(containerId, fragment, tag).addToBackStack(null)
                .commit();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View focusView = activity.getCurrentFocus();
        if (focusView != null) {
            inputMethodManager.hideSoftInputFromWindow(focusView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static File setUpImageFile(String directory) throws IOException {
        File imageFile = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState())) {
            File storageDir = new File(directory);
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }
            imageFile = File.createTempFile(Constants.JPEG_FILE_PREFIX
                            + System.currentTimeMillis() + "_",
                    Constants.JPEG_FILE_SUFFIX, storageDir);
        }
        return imageFile;
    }
}
