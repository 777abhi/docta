package utils;

/**
 * Created by OM on 11/27/2015.
 */
public class IntentExtras {
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String DOB = "dob";
    public static final String AGE = "age";
    public static final String GENDER = "gender";
    public static final String APPOINTMENT_SERVICE = "gender";


    public static final String DEVICE_ID = "deviceId";
    public static final String IMAGE = "image";
}
