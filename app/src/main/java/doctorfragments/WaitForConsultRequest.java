package doctorfragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.RatingBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.gson.Gson;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBSettings;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.sanguinebits.docta.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import preferences.UserPrefsManager;
import utils.Constants;
import utils.DataHolder;
import utils.GeneralFunctions;
import views.MyArcView;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.NotificationHandler;


public class WaitForConsultRequest extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static QBChatService chatService;
    @Bind(R.id.myArcView)
    MyArcView myArcView;
    @Bind(R.id.doctorRating)
    RatingBar doctorRating;
    QBRTCTypes.QBConferenceType qbConferenceType = null;
    private Handler handler;
    private UserPrefsManager userPrefsManager;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private boolean isOffline = false;

    // TODO: Rename and change types of parameters
    public WaitForConsultRequest() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WaitForConsultRequest.
     */
    // TODO: Rename and change types and number of parameters
    public static WaitForConsultRequest newInstance(String param1, String param2) {
        WaitForConsultRequest fragment = new WaitForConsultRequest();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wait_for_consult_request, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userPrefsManager = new UserPrefsManager(getActivity());

        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.requestName.equals(RequestName.GO_OFFLINE)) {
                    getFragmentManager().beginTransaction().remove(WaitForConsultRequest.this).commit();
                    getFragmentManager().popBackStackImmediate();
                } else if (result.requestName.equals(RequestName.GET_VISIT_REQUEST)) {
                    DataHolder.appointmentId = result.getAppoint_id();
                    userPrefsManager.saveAssignedPatientProfile(result.getUser());
                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(),
                            DoctaConsultRequest.newInstance(""),
                            R.id.flRequestFragmentContainer, Constants.TAG_CONSULT_REQUEST);
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });

        setNotificationListener(new NotificationListener() {
            @Override
            public void onNotification(Intent intent) {
                NotificationHandler ce = new Gson().fromJson(
                        intent.getStringExtra(Constants.MESSAGE_KEY), NotificationHandler.class);
                if (ce.getMsg().equals(Constants.NOTIFICATION_TYPE_GET_PENDING_VISIT_REQUEST))
                    getVistRequest();
            }
        });
        handler = new Handler();
        RotateAnimation anim = new RotateAnimation(0.0f, 360f, Animation.RELATIVE_TO_SELF, .5f,
                Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(Constants.ROTATION_PERIOD);
        myArcView.setAnimation(anim);
        qbConferenceType = QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO;
        QBSettings.getInstance().fastConfigInit(Constants.APP_ID, Constants.AUTH_KEY, Constants.AUTH_SECRET);

        QBChatService.setDebugEnabled(true);
        if (!QBChatService.isInitialized()) {
            QBChatService.init(getActivity());
            chatService = QBChatService.getInstance();
        }
        createAppSession();
        getVistRequest();
    }

    private void showGoOfflineConfirmationDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                .positiveText(R.string.yes)
                .theme(Theme.LIGHT)
                .title(getString(R.string.go_offline_))
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        goOffline();
                    }
                })
                .negativeText(R.string.no)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        dialog.dismiss();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.show();
    }

    public void setRating(String rating) {
        doctorRating.setRating(TextUtils.isEmpty(rating) ? 0 : Float.parseFloat(rating));
    }

    private void goOffline() {
        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.request = RequestName.GO_OFFLINE;
        requestData.parameters.doc_id = loggedInUser.user_id;
        makeRequest(requestData);
    }

    public void onBackPress() {
        showGoOfflineConfirmationDialog();
    }

    private void getVistRequest() {

        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.request = RequestName.GET_VISIT_REQUEST;
        requestData.parameters.doc_id = loggedInUser.user_id;
        makeRequest(requestData);
    }

    private void createAppSession() {
        QBAuth.createSession(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {
                //showToast("session creted");
            }

            @Override
            public void onSuccess() {
                // showToast("session creted success");


            }

            @Override
            public void onError(List<String> list) {
                try {
                    showToast(list.get(0));
                } catch (Exception e) {

                }
            }
        });
    }


    @OnClick({R.id.tvGoOffline})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvGoOffline:
                showGoOfflineConfirmationDialog();
                break;
            default:
                break;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
