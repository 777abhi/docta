package doctorfragments;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import java.util.List;

import adapters.DoctaAppointmentAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import utils.Constants;
import webservices.pojos.Appointment;
import webservices.pojos.CommonPojo;


public class TodaysAppointment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_CAMERA_PERMISSION = 111;
    private final int TODAY_APPOINTMENT = 0;
    private final int UPCOMING_APPOINTMENT = 1;
    private final int PAST_APPOINTMENT = 2;
    @Bind(R.id.rvData)
    RecyclerView rvData;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    Handler handler = new Handler();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private int mParam2;
    private CommonPojo data;
    private DoctaAppointmentAdapter adapter;
    private List<Appointment> appointmentList;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                for (int i = appointmentList.size() - 1; i >= 0; i--) {
                    if (!appointmentList.get(i).getLateTime().isEmpty()) {
                        int lateTime = Integer.parseInt(appointmentList.get(i).getLateTime()) + 1;
//                        if (lateTime > 30)
//                            appointmentList.remove(i);
//                        else
                            appointmentList.get(i).setLateTime(lateTime + "");
                    }
                }
                if (appointmentList.size() == 0) {
                    tvNoData.setVisibility(View.VISIBLE);
                } else tvNoData.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
                handler.postDelayed(runnable, Constants.REPEAT_TIME);
            } catch (Exception e) {

            }
        }
    };

    public TodaysAppointment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TodaysAppointment.
     */
    // TODO: Rename and change types and number of parameters
    public static TodaysAppointment newInstance(String param1, int param2) {
        TodaysAppointment fragment = new TodaysAppointment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = new Gson().fromJson(getArguments().getString(ARG_PARAM1), CommonPojo.class);
            mParam2 = getArguments().getInt(ARG_PARAM2);
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvNoData.setText(getString(R.string.no_appointments));
        switch (mParam2) {
            case TODAY_APPOINTMENT:
                if (data.getToday().size() > 0) {
                    appointmentList = data.getToday();
                    adapter = new DoctaAppointmentAdapter(
                            this, appointmentList, true, TODAY_APPOINTMENT);
                    rvData.setAdapter(adapter);
                    handler.postDelayed(runnable, Constants.REPEAT_TIME);
                } else tvNoData.setVisibility(View.VISIBLE);
                break;
            case UPCOMING_APPOINTMENT:
                if (data.getUpcoming().size() > 0) {
                    rvData.setAdapter(new DoctaAppointmentAdapter(
                            this, data.getUpcoming(), false, UPCOMING_APPOINTMENT));
                } else tvNoData.setVisibility(View.VISIBLE);
                break;
            case PAST_APPOINTMENT:
                if (data.getHistory().size() > 0) {
                    rvData.setAdapter(new DoctaAppointmentAdapter(
                            this, data.getHistory(), false, PAST_APPOINTMENT));
                } else tvNoData.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void requestCameraPermission() {
        boolean permission = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        if (Build.VERSION.SDK_INT >= 23 && !permission) {
            FragmentCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);

        } else {
            adapter.makeCall();
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    adapter.makeCall();
                }
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recycler_only_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
