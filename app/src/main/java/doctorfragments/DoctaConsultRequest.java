package doctorfragments;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.quickblox.chat.QBChatService;
import com.quickblox.videochat.webrtc.QBRTCTypes;
import com.sanguinebits.docta.CallActivity;
import com.sanguinebits.docta.R;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import utils.DataHolder;
import views.CircleProgress;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;


public class DoctaConsultRequest extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ACCEPT = "1";
    private static final String DECLINE = "0";
    private static QBChatService chatService;
    @Bind(R.id.progressBar)
    CircleProgress progressBar;

    // TODO: Rename and change types of parameters
    @Bind(R.id.profilePic)
    SimpleDraweeView profilePic;
    @Bind(R.id.tvTimeStatus)
    TextView tvTimeStatus;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    int progress = 10;//= Constants.PROGRESS_TIME;
    QBRTCTypes.QBConferenceType qbConferenceType = null;
    private Timer timer;
    private MyTimerTask myTimerTask;
    private String STATUS = "";

    public DoctaConsultRequest() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DoctaConsultRequest newInstance(String param1) {
        DoctaConsultRequest fragment = new DoctaConsultRequest();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onBackPress() {
        showDeclineConfirmationDialog();
    }

    private void showDeclineConfirmationDialog() {
//        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
//                .positiveText(R.string.yes)
//                .theme(Theme.LIGHT)
//                .title(getString(R.string.go_offline_))
//                .cancelable(false)
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        // TODO
//                        declineRequest();
//                    }
//                })
//                .negativeText(R.string.no)
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        // TODO
//                        dialog.dismiss();
//                    }
//                });
//        MaterialDialog dialog = builder.build();
//        dialog.show();
        declineRequest();
    }

    private void declineRequest() {
        timer.cancel();
        progressBar.cancelAnimation();
        RequestData requestData = new RequestData();
        requestData.request = RequestName.ACCEPT_REQUEST;
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.parameters.doc_id = loggedInUser.user_id;
        requestData.parameters.apoint_id = DataHolder.appointmentId;
        requestData.parameters.clinic_id = loggedInUser.clinic_id;
        requestData.parameters.pat_id = userPrefsManager.getAssignedPatientProfile().user_id;
        requestData.parameters.status = DECLINE;
        STATUS = DECLINE;
        makeRequest(requestData);
    }

    @OnClick({R.id.tvAccept, R.id.tvDecline})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvDecline:
                declineRequest();
                break;
            case R.id.tvAccept:
                timer.cancel();
                progressBar.cancelAnimation();
                RequestData requestData = new RequestData();
                requestData.request = RequestName.ACCEPT_REQUEST;
                requestData.type = Constants.TYPE_DOCTOR;
                requestData.parameters.doc_id = loggedInUser.user_id;
                requestData.parameters.apoint_id = DataHolder.appointmentId;
                requestData.parameters.clinic_id = loggedInUser.clinic_id;
                requestData.parameters.ttime = getTime();
                requestData.parameters.pat_id = userPrefsManager.getAssignedPatientProfile().user_id;
                requestData.parameters.status = ACCEPT;
                STATUS = ACCEPT;
                makeRequest(requestData);
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_docta_consult_request, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.requestName.equals(RequestName.ACCEPT_REQUEST)) {
                    if (STATUS.equals(DECLINE)) {
                        getFragmentManager().popBackStackImmediate();
                    } else {
                        userPrefsManager.saveAppointment(result.getAppointment());
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        Intent intent = new Intent(getActivity(), CallActivity.class);
                        intent.putExtra("type", Constants.TYPE_DOCTOR);
                        startActivityForResult(intent, Constants.CALL_ACTIVITY_CLOSE);
                    }
                }
            }

            @Override
            public void onFailure() throws Exception {
                getActivity().onBackPressed();
            }
        });
        profilePic.setImageURI(Uri.parse(loggedInUser.photo));
        timer = new Timer();
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 0, 1000);
        progressBar.setProgressWithAnimation(Constants.PROGRESS_TIME, 100);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    tvTimeStatus.setText((progress--) + " " + getString(R.string.seconds_left));
                    if (progress == 0)
                        declineRequest();

                }
            });
        }

    }
}
