package doctorfragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import utils.DataHolder;
import utils.DotView;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.User;


public class DoctorProfile extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvDictorReviewingChart)
    DotView tvDictorReviewingChart;
    //    @Bind(R.id.progressBar)
//    RoundCornerProgressBar progressBar;
    @Bind(R.id.profilePic)
    SimpleDraweeView profilePic;
    @Bind(R.id.tvEmail)
    TextView tvEmail;
    @Bind(R.id.tvPhone)
    TextView tvPhone;
    @Bind(R.id.tvCancel)
    TextView tvCancel;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tvName)
    TextView tvName;
    @Bind(R.id.tvMedicalSchool)
    TextView tvMedicalSchool;
    @Bind(R.id.tvSpeciality)
    TextView tvSpeciality;
    @Bind(R.id.tvYearOfPractice)
    TextView tvYearOfPractice;
    @Bind(R.id.tvAReaOfInterest)
    TextView tvAReaOfInterest;
    @Bind(R.id.tvHobby)
    TextView tvHobby;

    private User doctorProfile;
    // TODO: Rename and change types of parameters
    private String mParam1;

    private int total = 4;

    public DoctorProfile() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DoctorProfile newInstance(String param1) {
        DoctorProfile fragment = new DoctorProfile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNext.setVisibility(View.GONE);
//        setNotificationListener(new NotificationListener() {
//            @Override
//            public void onNotification(Intent intent) {
//                NotificationHandler ce = new Gson().fromJson(
//                        intent.getStringExtra(Constants.MESSAGE_KEY), NotificationHandler.class);
//                if (ce.getMsg().equals(Constants.NOTIFICATION_EMR_PROGRESS)) {
//                    float progress = Float.parseFloat(ce.getMsg_display()) / total;// ;
//                    progressBar.setProgress(progress);
//                }
//
//            }
//        });
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                DataHolder.appointmentId = "";
                getActivity().onBackPressed();
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
        if (mParam1 != null) {
            doctorProfile = new Gson().fromJson(mParam1, User.class);
        } else
            doctorProfile = userPrefsManager.getAssignedDoctorProfile();
        if (mParam1 != null) {
            //progressBar.setVisibility(View.INVISIBLE);
            tvDictorReviewingChart.setVisibility(View.INVISIBLE);
            doctorProfile = new Gson().fromJson(mParam1, User.class);
            tvCancel.setVisibility(View.GONE);
            frTopBar.setVisibility(View.VISIBLE);
        } else {
            frTopBar.setVisibility(View.GONE);
        }
        tvName.setText(doctorProfile.getFullName());
        profilePic.setImageURI(Uri.parse(doctorProfile.photo));
        tvEmail.setText("Email\n" + doctorProfile.email);
        tvPhone.setText("Phone\n" + doctorProfile.phone);
        tvMedicalSchool.setText("Medical School\n" + doctorProfile.medical_school);
        tvAReaOfInterest.setText("Area Of Interest\n" + doctorProfile.aoi);
        tvHobby.setText("Hobby\n" + doctorProfile.hobby);
        tvYearOfPractice.setText("Year Of Practice\n" + doctorProfile.yop);
        tvSpeciality.setText("Speciality\n" + doctorProfile.speciality);

        tvDictorReviewingChart.setText(getString(R.string.standby_while) + " " +
                doctorProfile.getFullName() + " " + getString(R.string.reviewing_cart));
        tvDictorReviewingChart.post(tvDictorReviewingChart.run);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onDestroy() {
        // tvDictorReviewingChart.stopHandler();
        super.onDestroy();
    }

    @OnClick({R.id.tvCancel, R.id.tvBack})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvCancel:
                showCancelConfirmationDialog();
                break;
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            default:
                break;
        }
    }

    private void showCancelConfirmationDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                .content(R.string.cancel_visit)
                .positiveText(R.string.yes)
                .theme(Theme.LIGHT)
                .title(getString(R.string.do_you_want_to_cancel_visit))
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        if (TextUtils.isEmpty(DataHolder.appointmentId)) {
                            getActivity().onBackPressed();
                        } else {
                            cancelVisit();
                        }
                    }
                })
                .negativeText(R.string.no)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        // TODO
                        dialog.dismiss();
                    }
                });
        MaterialDialog dialog = builder.build();
        dialog.show();
    }

    public void stopAnimation() {
        if (tvDictorReviewingChart != null) tvDictorReviewingChart.stopHandler();
    }

    private void cancelVisit() {

        RequestData apiRequest = new RequestData();
        apiRequest.request = RequestName.CANCEl_VISIT;
        apiRequest.type = Constants.TYPE_PATIENT;
        apiRequest.parameters.pat_id = loggedInUser.user_id;
        apiRequest.parameters.apoint_id = DataHolder.appointmentId;
        makeRequest(apiRequest);
    }


    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        tvDictorReviewingChart.stopHandler();
        ButterKnife.unbind(this);
    }
}
