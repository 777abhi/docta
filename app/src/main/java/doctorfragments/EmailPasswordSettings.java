package doctorfragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import utils.WebConstants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;

public class EmailPasswordSettings extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.tvEmail)
    TextView tvEmail;
    @Bind(R.id.etCurrentPassword)
    EditText etCurrentPassword;
    @Bind(R.id.etNewPassword)
    EditText etNewPassword;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public EmailPasswordSettings() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmailPasswordSettings.
     */
    // TODO: Rename and change types and number of parameters
    public static EmailPasswordSettings newInstance(String param1, String param2) {
        EmailPasswordSettings fragment = new EmailPasswordSettings();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_email_password_settings, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @OnClick({R.id.tvBack, R.id.tvSaveChanges})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvSaveChanges:
                //showProgress();
                if (TextUtils.isEmpty(etCurrentPassword.getText()) || TextUtils.isEmpty(etNewPassword.getText())) {
                    showToast(R.string.please_fill_all_fields);
                    return;
                } else if (getMd5(etNewPassword.getText().toString()).equals(loggedInUser.password)) {
                    showToast(R.string.you_can_not_use_old_password);
                    return;
                }
                if (getMd5(etCurrentPassword.getText().toString()).equals(loggedInUser.password)) {
                    RequestData requestData = new RequestData();
                    if (userPrefsManager.getUserType().equals(Constants.TYPE_PATIENT)) {
                        requestData.type = Constants.TYPE_PATIENT;
                        requestData.request = RequestName.PATIENT_UPDATE_PASSWORD;
                        requestData.parameters.pat_id = loggedInUser.user_id;
                    }
                    else {
                        requestData.type = Constants.TYPE_DOCTOR;
                        requestData.request = RequestName.DOCTOR_UPDATE_PASSWORD;
                        requestData.parameters.doc_id = loggedInUser.user_id;
                    }
                    requestData.parameters.new_password = etNewPassword.getText().toString();
                    requestData.parameters.old_password = loggedInUser.password;
                    makeRequest(requestData);
                 //   hideProgress();
//                    loggedInUser.password = getMd5(etNewPassword.getText().toString());
//                    userPrefsManager.saveLoginUser(loggedInUser);

                } else showToast(R.string.wrong_password);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pBaseBar = pBar;
        tvNext.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.email_n_password));
        tvEmail.setText(loggedInUser.email);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                if (result.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                    loggedInUser.password = getMd5(etNewPassword.getText().toString());
                    userPrefsManager.saveLoginUser(loggedInUser);
                    getFragmentManager().beginTransaction().remove(EmailPasswordSettings.this).commit();
                }
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
