package doctorfragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;

import adapters.TabsAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import patientfragments.BaseFragment;
import utils.CommonEvent;
import utils.Constants;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.NotificationHandler;


public class ConsultHistoryFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.frTopBar)
    FrameLayout frTopBar;
    @Bind(R.id.tabLayout)
    TabLayout tabLayout;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvOrder)
    TextView tvOrder;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ConsultHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConsultHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConsultHistoryFragment newInstance(String param1, String param2) {
        ConsultHistoryFragment fragment = new ConsultHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        tvOrder.setVisibility(View.VISIBLE);
        tvOrder.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        tvOrder.setTextColor(getColor(R.color.black_color));
        tvOrder.setText(getString(R.string.consult_history));
        final String titles[] = {getString(R.string.today), getString(R.string.upcoming),
                getString(R.string.history)};
        frTopBar.setVisibility(View.GONE);
        setNotificationListener(new NotificationListener() {
            @Override
            public void onNotification(Intent intent) {
                NotificationHandler ce = new Gson().fromJson(
                        intent.getStringExtra(Constants.MESSAGE_KEY), NotificationHandler.class);
                if (ce.getMsg().equals(Constants.NOTIFICATION_DOCTOR_NURSE_CHECKIN) ||
                        ce.getMsg().equals(Constants.RECEPTIONIST_CHECKIN) ||
                        ce.getMsg().equals(Constants.CONFIRM_APPOINTMENT_NOTIFICATION) ||
                        ce.getMsg().equals(Constants.NOTIFICATION_PATIENT_CANCEL_APPOINTMENT))
                    getHistory();
            }
        });
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                TabsAdapter tabsAdapter = new TabsAdapter(getChildFragmentManager());
                String resultJson = new Gson().toJson(result);
                tabsAdapter.addFragment(TodaysAppointment.newInstance(resultJson, 0),
                        titles[0]);
                tabsAdapter.addFragment(TodaysAppointment.newInstance(resultJson, 1),
                        titles[1]);
                tabsAdapter.addFragment(TodaysAppointment.newInstance(resultJson, 2),
                        titles[2]);
                pager.setAdapter(tabsAdapter);
                tabLayout.setupWithViewPager(pager);
                TabLayout.Tab tab = tabLayout.getTabAt(2);
                int incompleteNotes = Integer.parseInt(result.getIncomplete_notes());
                View customView = LayoutInflater.from(getActivity()).inflate(R.layout.notification_view, null);
                if (incompleteNotes == 0) {
                    customView.findViewById(R.id.tvCount).setVisibility(View.GONE);
                } else
                    ((TextView) customView.findViewById(R.id.tvCount)).setText(incompleteNotes + "");
                tab.setCustomView(customView);
            }

            @Override
            public void onFailure() {

            }
        });
        getHistory();
    }

    private void getHistory() {
        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.request = RequestName.DOC_HISTORY;
        requestData.parameters.doc_id = loggedInUser.user_id;
        requestData.parameters.clinic_id = loggedInUser.clinic_id;
        requestData.parameters.seconds = getTime().toLowerCase();
        makeRequest(requestData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.commonpager, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;

        return view;
    }
    public void onEvent(CommonEvent commonEvent) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                getHistory();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);

    }
}
