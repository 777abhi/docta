package doctorfragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.facebook.drawee.view.SimpleDraweeView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.DoctaHomeActivity;
import com.sanguinebits.docta.R;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import patientfragments.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.CommonEvent;
import utils.Constants;
import utils.GeneralFunctions;
import utils.GetSampledImage;
import utils.WebConstants;
import views.RoundCornerProgressBar;
import webservices.RequestData;
import webservices.RequestName;
import webservices.RestClient;
import webservices.pojos.CommonPojo;


public class DoctaHomeFragment extends BaseFragment implements GetSampledImage.SampledImageAsyncResp {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_WRITE_STORAGE = 112;
    private static final int REQUEST_CAMERA_PERMISSION = 111;
    @Bind(R.id.profilePic)
    SimpleDraweeView profilePic;
    @Bind(R.id.tvDoctorName)
    TextView tvDoctorName;
    @Bind(R.id.doctorRating)
    RatingBar doctorRating;
    @Bind(R.id.tvDoctorDesignation)
    TextView tvDoctorSpeciality;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.progressBar)
    RoundCornerProgressBar progressBar;
    @Bind(R.id.tvCompleteProfile)
    TextView tvCompleteProfile;
    private ProgressDialog mProgressDialog;
    private String picturePath;
    private File imageFile;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public DoctaHomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DoctaHomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoctaHomeFragment newInstance(String param1, String param2) {
        DoctaHomeFragment fragment = new DoctaHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_docta_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        pBaseBar = pBar;
        setProfileCompleteParam();
        if (loggedInUser != null) {
            tvDoctorName.setText(loggedInUser.getFullName());
            tvDoctorSpeciality.setText(loggedInUser.speciality);
            profilePic.setImageURI(Uri.parse(loggedInUser.photo));
        }
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) {
                if (result.requestName.equals(RequestName.GET_DOC_RATING)) {
                    doctorRating.setRating(result.getRating());
                } else {
                    ((DoctaHomeActivity) getActivity()).removeVideoCallBacks();
                    GeneralFunctions.addFragmentFromRightWithTag(getFragmentManager(),
                            WaitForConsultRequest.newInstance("", ""),
                            R.id.flRequestFragmentContainer, Constants.WAIT_FOR_CONSULT_REQUEST);
                }
            }

            @Override
            public void onFailure() {

            }
        });

        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.request = RequestName.GET_DOC_RATING;
        requestData.parameters.doc_id = loggedInUser.user_id;
        makeRequest(requestData);
    }

    @OnClick({R.id.tvGoOnline, R.id.profilePic})
    public void click(View view) {
        boolean permission;
        switch (view.getId()) {
            case R.id.tvGoOnline:
                permission = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                if (Build.VERSION.SDK_INT >= 23 && !permission) {
                    FragmentCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION);

                } else {
                    goOnline();
                }
                break;
            case R.id.profilePic:
                if (imageFile != null && imageFile.exists()) {
                    imageFile.delete();
                }
                permission = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                if (Build.VERSION.SDK_INT >= 23 && !permission) {
                    FragmentCompat.requestPermissions(DoctaHomeFragment.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);

                } else {
                    showPicOptionDialog();
                }
                break;
        }

    }

    private void goOnline() {
        RequestData requestData = new RequestData();
        requestData.type = Constants.TYPE_DOCTOR;
        requestData.request = RequestName.GO_ONLINE;
        requestData.parameters.doc_id = loggedInUser.user_id;
        makeRequest(requestData);
    }

    private void showPicOptionDialog() {
        new MaterialDialog.Builder(getActivity())
                .theme(Theme.LIGHT)
                .items(R.array.media_options)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                Intent i = new Intent(
                                        Intent.ACTION_PICK,
                                        MediaStore.Images.Media
                                                .EXTERNAL_CONTENT_URI);
                                startActivityForResult(i,
                                        Constants.GALLERY_REQUEST);
                                break;
                            case 1:
                                Intent takePictureIntent = new Intent(
                                        MediaStore.ACTION_IMAGE_CAPTURE);
                                File f = null;
                                try {
                                    f = GeneralFunctions.setUpImageFile(Constants
                                            .LOCAL_STORAGE_BASE_PATH_FOR_USER_PHOTOS);
                                    picturePath = f.getAbsolutePath();
                                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                            Uri.fromFile(f));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    f = null;
                                    picturePath = null;
                                }
                                startActivityForResult(takePictureIntent,
                                        Constants.CAMERA_REQUEST);
                                break;
                            case 2:
                                dialog.dismiss();
                                break;
                            default:
                                break;
                        }
                    }
                })
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    // Log.i(TAG, "Permission has been denied by user");

                } else {

                    //Log.i(TAG, "Permission has been granted by user");
                    showPicOptionDialog();
                }
                return;
            }
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    // Log.i(TAG, "Permission has been denied by user");

                } else {

                    //Log.i(TAG, "Permission has been granted by user");
                    goOnline();
                }
                break;
        }
    }

    public void setProfileCompleteParam() {
        int totalProfileParam = 10;
        float count = 0;
        if (TextUtils.isEmpty(loggedInUser.fname))
            count++;
        if (TextUtils.isEmpty(loggedInUser.lname))
            count++;
        if (TextUtils.isEmpty(loggedInUser.phone))
            count++;
        if (TextUtils.isEmpty(loggedInUser.dob))
            count++;
        if (TextUtils.isEmpty(loggedInUser.medical_school))
            count++;
        if (TextUtils.isEmpty(loggedInUser.speciality))
            count++;
        if (TextUtils.isEmpty(loggedInUser.yop))
            count++;
        if (TextUtils.isEmpty(loggedInUser.aoi))
            count++;
        if (TextUtils.isEmpty(loggedInUser.hobby))
            count++;
        if (TextUtils.isEmpty(loggedInUser.photo))
            count++;
        if (count == 0) {
            progressBar.setVisibility(View.GONE);
            tvCompleteProfile.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            tvCompleteProfile.setVisibility(View.VISIBLE);
            float progress = (totalProfileParam - count) / totalProfileParam;// ;
            progressBar.setProgress(progress);
            tvCompleteProfile.setText(getString(R.string.complete_your_profile) +
                    (int) ((totalProfileParam - count) * 10) + "%");
        }
    }

    public void onEvent(CommonEvent commonEvent) {
        switch (CommonEvent.EVENT_TYPE) {
            case CommonEvent.REFRESH:
                setProfileCompleteParam();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                boolean isGalleryImage = false;
                if (requestCode == Constants.GALLERY_REQUEST) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    isGalleryImage = true;
                }
                showProgressDialog();
                new GetSampledImage(this).execute(picturePath,
                        Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_PHOTOS,
                        String.valueOf(isGalleryImage),
                        String.valueOf((int) getResources().getDimension(R.dimen.h_256)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
        }
        mProgressDialog.setMessage(getString(R.string.processing_image));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void onSampledImageAsyncPostExecute(File file) {
        imageFile = file;
        if (imageFile != null) {
            profilePic.setImageURI(Uri.parse(Constants.LOCAL_FILE_PREFIX +
                    imageFile));
            updateImage();
        }
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private void updateImage() {
        showProgress();
        RequestBody requestBody = RequestBody.create(
                MediaType.parse(Constants.MIME_TYPE_IMAGE), new File(imageFile.getAbsolutePath()));
        RequestBody typeBody = RequestBody.create(MediaType.parse("text/plain"), Constants.TYPE_DOCTOR);
        Call<CommonPojo> call = RestClient.get().updateImage(requestBody, typeBody,
                RequestName.PROFILE_PIC_UPDATE, loggedInUser.user_id);
        call.enqueue(new Callback<CommonPojo>() {
            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    CommonPojo pojo = response.body();
                    showToast(pojo.getMsg());
                    if (pojo.getStatus().equals(WebConstants.RETROFIT_SUCCESS)) {
                        loggedInUser.photo = response.body().getImage();
                        userPrefsManager.saveLoginUser(loggedInUser);
                    }
                } else {
                    showToast(R.string.there_might_be_some_problem);
                }
            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    hideProgress();
                    handleException(t);
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
