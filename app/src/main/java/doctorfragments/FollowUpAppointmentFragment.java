package doctorfragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapters.TimeSlotAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import utils.DataHolder;
import utils.Global;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;
import webservices.pojos.PojoAppointmentRequest;


public class FollowUpAppointmentFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener,
        DatePickerDialog.OnDateChangedListener {//implements DotInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.rvData)
    RecyclerView rvDoctors;
    @Bind(R.id.pBar)
    ProgressWheel pBar;
    @Bind(R.id.tvNext)
    TextView tvNext;
    @Bind(R.id.ivPrevMonth)
    ImageView ivPrevMonth;
    @Bind(R.id.tvSelectedMonthName)
    TextView tvSelectedMonthName;
    @Bind(R.id.ivNextMonth)
    ImageView ivNextMonth;
    @Bind(R.id.tvNoData)
    TextView tvNoData;
    private TimeSlotAdapter adapter;
    private int limit = 0;
    private Calendar today;
    private Calendar now;
    private DatePickerDialog dpd;
    private int color;


    // TODO: Rename and change types of parameters
    private PojoAppointmentRequest pojoAppointmentRequest;

    private List<String> timeSlotList = new ArrayList<>();

    public FollowUpAppointmentFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FollowUpAppointmentFragment newInstance(String pojoAppointmentRequest, String availableTime) {
        FollowUpAppointmentFragment fragment = new FollowUpAppointmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, pojoAppointmentRequest);
        args.putString(ARG_PARAM2, availableTime);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_follow_up_appointment, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
//        now = Calendar.getInstance();
//        now = Calendar.getInstance();
//        now.set(Calendar.HOUR_OF_DAY, 0);
//        now.set(Calendar.MINUTE, 0);
//        now.set(Calendar.SECOND, 0);
//        now.set(Calendar.MILLISECOND, 0);
        now = DataHolder.selectedDate;// = now;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        if (today.getTime().compareTo(DataHolder.selectedDate.getTime()) == 0) {
            ivPrevMonth.setVisibility(View.INVISIBLE);
        }
        tvNext.setVisibility(View.GONE);
        if (getArguments() != null) {
            pojoAppointmentRequest = new Gson().fromJson(getArguments().
                    getString(ARG_PARAM1), PojoAppointmentRequest.class);
        }
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                hideProgress();
                if (result.getFollowUpAppointmentTimeSlot().size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    if (limit == 0) {
                        timeSlotList.clear();
                        timeSlotList.addAll(result.getFollowUpAppointmentTimeSlot());
                        rvDoctors.setLayoutManager(new LinearLayoutManager(getActivity()));
                        adapter = new TimeSlotAdapter(getActivity(),
                                timeSlotList, pojoAppointmentRequest);
                        rvDoctors.setAdapter(adapter);
                    } else {
                        timeSlotList.addAll(result.getFollowUpAppointmentTimeSlot());
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    tvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure() throws Exception {
                hideProgress();
                tvNoData.setVisibility(View.VISIBLE);
            }
        });
        tvSelectedMonthName.setText(monthDateFormat.format(DataHolder.selectedDate.getTime()));
        getTimeSlots();
    }

    @OnClick({R.id.tvBack, R.id.ivChooseDate, R.id.ivPrevMonth, R.id.ivNextMonth})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvBack:
                getActivity().onBackPressed();
                break;
            case R.id.ivChooseDate:
                if (now.get(Calendar.DAY_OF_WEEK) ==
                        Calendar.SUNDAY) {
                    now.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH) + 1);
                } else if (now.get(Calendar.DAY_OF_WEEK) ==
                        Calendar.SATURDAY) {
                    now.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH) + 2);
                }
                dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setMinDate(now);
                dpd.registerOnDateChangedListener(this);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.ivPrevMonth:
                DataHolder.selectedDate.set(Calendar.DAY_OF_MONTH,
                        DataHolder.selectedDate.get(Calendar.DAY_OF_MONTH) - 1);
                tvSelectedMonthName.setText(monthDateFormat.format(DataHolder.selectedDate.getTime()));
                if (today.getTime().compareTo(DataHolder.selectedDate.getTime()) == 0) {
                    ivPrevMonth.setVisibility(View.INVISIBLE);
                }
                getTimeSlots();
                break;
            case R.id.ivNextMonth:
                ivPrevMonth.setVisibility(View.VISIBLE);
                DataHolder.selectedDate.set(Calendar.DAY_OF_MONTH,
                        DataHolder.selectedDate.get(Calendar.DAY_OF_MONTH) + 1);
                tvSelectedMonthName.setText(monthDateFormat.format(DataHolder.selectedDate.getTime()));
                getTimeSlots();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void getTimeSlots() {
        showProgress();
        RequestData requestData = new RequestData();
        requestData.request = RequestName.GET_TIME_SLOTS_FOR_FOLLOW_APPOONTMENTS;
        requestData.type = Constants.TYPE_PATIENT;
        requestData.parameters.doc_id = pojoAppointmentRequest.getFollowUpDoctor().user_id;
        requestData.parameters.service_id = pojoAppointmentRequest.getServiceId();
        requestData.parameters.visit_type = Constants.TYPE_CLINIC_VISIT;
        requestData.parameters.latitude = Global.latitude;
        requestData.parameters.longitude = Global.longitude;
        requestData.parameters.ttime = getTime();
        requestData.parameters.date = dateFormat.format(DataHolder.selectedDate.getTime());
        makeRequest(requestData);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        now.set(Calendar.YEAR, year);
        now.set(Calendar.MONTH, monthOfYear);
        now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        tvSelectedMonthName.setText(monthDateFormat.format(DataHolder.selectedDate.getTime()));
        getTimeSlots();
    }

    @Override
    public void onDateChanged() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, dpd.getSelectedDay().getDay());
        calendar.set(Calendar.MONTH, dpd.getSelectedDay().getMonth());
        calendar.set(Calendar.YEAR, dpd.getSelectedDay().getYear());
        if (color == 0)
            color = ((Button) dpd.getView().findViewById(R.id.ok)).getCurrentTextColor();
        if (calendar.get(Calendar.DAY_OF_WEEK) ==
                Calendar.SUNDAY || calendar.get(Calendar.DAY_OF_WEEK) ==
                Calendar.SATURDAY) {
            ((Button) dpd.getView().findViewById(R.id.ok)).setTextColor(Color.LTGRAY);
            dpd.getView().findViewById(R.id.ok).setEnabled(false);
        } else {
            ((Button) dpd.getView().findViewById(R.id.ok)).setTextColor(color);
            dpd.getView().findViewById(R.id.ok).setEnabled(true);
        }
    }

}
