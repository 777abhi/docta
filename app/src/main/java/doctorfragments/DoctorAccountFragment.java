package doctorfragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sanguinebits.docta.LoginActivity;
import com.sanguinebits.docta.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patientfragments.BaseFragment;
import utils.Constants;
import utils.GeneralFunctions;
import webservices.RequestData;
import webservices.RequestName;
import webservices.pojos.CommonPojo;

public class DoctorAccountFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tvMyAccount)
    TextView tvMyAccount;
    @Bind(R.id.pBar)
    ProgressWheel pBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DoctorAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DoctorAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoctorAccountFragment newInstance(String param1, String param2) {
        DoctorAccountFragment fragment = new DoctorAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setServiceCallbackListener(new ServiceCallback() {
            @Override
            public void onResult(CommonPojo result) throws Exception {
                userPrefsManager.clearUserPrefs();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().overridePendingTransition(0, 0);
                getActivity().finish();
            }

            @Override
            public void onFailure() throws Exception {

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_account, container, false);
        ButterKnife.bind(this, view);
        pBaseBar = pBar;
        return view;
    }

    @OnClick({R.id.tvPrivacyPolicy, R.id.tvPersonalSettings,
            R.id.tvAboutUs, R.id.tvFeedback, R.id.tvSignOut})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.tvPersonalSettings:
                GeneralFunctions.addFragmentFromRight(getFragmentManager()
                        , new PersonalSettingsOptions(), R.id.flRequestFragmentContainer);
                break;
            case R.id.tvPrivacyPolicy:
                break;
            case R.id.tvAboutUs:
                break;
            case R.id.tvFeedback:
                String[] TO = {"info@doctagroup.com"};
                String[] CC = {""};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Type your feedback here..");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    showToast("There is no email client installed.");
                }
                break;
            case R.id.tvSignOut:
                MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                        .content(getString(R.string.are_you_sure_you_want_to_logout))
                        .title(getString(R.string.logout))
                        .theme(Theme.LIGHT)
                        .positiveText(R.string.yes)
                        .negativeText(R.string.no)
                        .autoDismiss(true)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // TODO
                                RequestData requestData = new RequestData();
                                requestData.request = RequestName.DOCTOR_LOGOUT;
                                requestData.type = Constants.TYPE_DOCTOR;
                                requestData.parameters.doc_id = loggedInUser.user_id;
                                makeRequest(requestData);
                            }

                        }).onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // TODO

                            }

                        });
                MaterialDialog dialog = builder.build();
                dialog.show();
                break;
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
