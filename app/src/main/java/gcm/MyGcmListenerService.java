/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gcm;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import preferences.UserPrefsManager;
import services.VideoCallService;

public class MyGcmListenerService extends GcmListenerService {
 //   String message = data.getString(GcmConsts.EXTRA_GCM_MESSAGE);
//        Log.e(TAG, "From: " + from);
//       Log.e(TAG, "Message: " + message);}
//       // QBUser user=(QBUser) userPrefsManager.getPatientProfile();

    //    startLoginService(user);}
    //QBUser qbUser = sharedPrefsHelper.getQbUser();
//    private void startLoginService(QBUser qbUser){
    //      VideoCallService.start(this, qbUser);
    //  }
//
//        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
//        if (sharedPrefsHelper.hasQbUser()) {
//            Log.d(TAG, "App have logined user");
//            QBUser qbUser = sharedPrefsHelper.getQbUser();
//            startLoginService(qbUser);
//
    private static final String TAG = "MyGcmListenerService";
    @Override
    public void onMessageReceived(String from, Bundle data) {
      //  String message = data.getString(GcmConsts.EXTRA_GCM_MESSAGE);
        Log.v(TAG, "From: " + from);
     //  Log.v(TAG, "Message: " + message);
String userId= UserPrefsManager.PREFS_PATIENT_PROFILE;
  //      UserPrefsManager sharedPrefsHelper = new UserPrefsManager(userId);
        if (userId!= null) {
            Log.d(TAG, "App have logined user");
          //  QBUser qbUser = sharedPrefsHelper.getQbUser();
            startLoginService();
        }
    }

    private void startLoginService(){
        VideoCallService.start(this);
    }


    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
//    @Override
//    public void onMessageReceived(String from, Bundle data) {
//        String fullMessage = data.getString("message");
//        String extraMessage = "", message = "", title = "";
//        try {
//            JSONObject obj = new JSONObject(fullMessage);
//            message = obj.getString(Constants.MESSAGE_KEY);
//            extraMessage = obj.getString(Constants.EXTRA_MESSAGE_KEY);
//            title = obj.getString(Constants.KEY_TITLE);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        Log.d(TAG, "From: " + from);
//        Log.d(TAG, "Message: " + fullMessage);
//
//        // [START_EXCLUDE]
//        /**
//         * Production applications would usually process the message here.
//         * Eg: - Syncing with server.
//         *     - Store message in local database.
//         *     - Update UI.
//         */
//
//        /**
//         * In some cases it may be useful to show a notification indicating to the user
//         * that a message was received.
//         */
//        if (message.equals(Constants.NOTIFICATION_TYPE_RATE_APP) ||
//                message.equals(Constants.CONFIRM_APPOINTMENT_NOTIFICATION) ||
//                message.equals(Constants.GENERAL_NOTIFICATION) ||
//                message.equals(Constants.NOTIFICATION_PATIENT_CANCEL_APPOINTMENT)) {
//            sendNotification(fullMessage, extraMessage, title);
//            if (message.equals(Constants.NOTIFICATION_PATIENT_CANCEL_APPOINTMENT) ||
//                    message.equals(Constants.CONFIRM_APPOINTMENT_NOTIFICATION)) {
//                //sendNotification(fullMessage, extraMessage, title);
//                Intent i = new Intent();
//                i.setAction(Constants.PUSH_ACTION);
//                i.putExtra(Constants.MESSAGE_KEY, fullMessage);
//                sendBroadcast(i);
//            }
//        } else if (message.equals(Constants.NOTIFICATION_DOCTOR_NURSE_CHECKIN) ||
//                message.equals(Constants.RECEPTIONIST_CHECKIN)) {
//            sendNotification(fullMessage, extraMessage, title);
//            Intent i = new Intent();
//            i.setAction(Constants.PUSH_ACTION);
//            i.putExtra(Constants.MESSAGE_KEY, fullMessage);
//            sendBroadcast(i);
//        } else {
//            Intent i = new Intent();
//            i.setAction(Constants.PUSH_ACTION);
//            i.putExtra(Constants.MESSAGE_KEY, fullMessage);
//            sendBroadcast(i);
//        }
//        // sendNotification(message, fullMessage);
//        // [END_EXCLUDE]
//    }
//    // [END receive_message]
//
//    /**
//     * Create and show a simple notification containing the received GCM message.
//     *
//     * @param message GCM message received.
//     */
//    private void sendNotification(String message, String data, String title) {
//        Intent intent;
//        if (new UserPrefsManager(this).getUserType().equals(Constants.TYPE_PATIENT))
//            intent = new Intent(this, MainActivity.class);
//        else intent = new Intent(this, DoctaHomeActivity.class);
//
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        intent.setAction(Constants.PUSH_ACTION);
//        intent.putExtra(Constants.MESSAGE_KEY, message);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis() /* Request code */, intent,
//                PendingIntent.FLAG_UPDATE_CURRENT);
//
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.ic_app_icon)
//                .setContentTitle(title)
//                .setContentText(data)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
//
//    }
}
