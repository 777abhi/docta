// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TimeSlotAdapter$ViewHolder$$ViewBinder<T extends adapters.TimeSlotAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755659, "field 'tvTimeSlot'");
    target.tvTimeSlot = finder.castView(view, 2131755659, "field 'tvTimeSlot'");
  }

  @Override public void unbind(T target) {
    target.tvTimeSlot = null;
  }
}
