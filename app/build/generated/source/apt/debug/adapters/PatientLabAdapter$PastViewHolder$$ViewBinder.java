// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientLabAdapter$PastViewHolder$$ViewBinder<T extends adapters.PatientLabAdapter.PastViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755447, "field 'ivMore'");
    target.ivMore = finder.castView(view, 2131755447, "field 'ivMore'");
    view = finder.findRequiredView(source, 2131755344, "field 'tvName'");
    target.tvName = finder.castView(view, 2131755344, "field 'tvName'");
  }

  @Override public void unbind(T target) {
    target.ivMore = null;
    target.tvName = null;
  }
}
