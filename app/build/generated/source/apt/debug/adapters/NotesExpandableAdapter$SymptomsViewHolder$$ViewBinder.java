// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NotesExpandableAdapter$SymptomsViewHolder$$ViewBinder<T extends adapters.NotesExpandableAdapter.SymptomsViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755596, "field 'tvSymptoms'");
    target.tvSymptoms = finder.castView(view, 2131755596, "field 'tvSymptoms'");
    view = finder.findRequiredView(source, 2131755597, "field 'tvAdd'");
    target.tvAdd = finder.castView(view, 2131755597, "field 'tvAdd'");
  }

  @Override public void unbind(T target) {
    target.tvSymptoms = null;
    target.tvAdd = null;
  }
}
