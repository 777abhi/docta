// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NotesAdapter$DataViewHolder$$ViewBinder<T extends adapters.NotesAdapter.DataViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755224, "field 'tvDoctorName'");
    target.tvDoctorName = finder.castView(view, 2131755224, "field 'tvDoctorName'");
    view = finder.findRequiredView(source, 2131755348, "field 'tvSpeciality'");
    target.tvSpeciality = finder.castView(view, 2131755348, "field 'tvSpeciality'");
    view = finder.findRequiredView(source, 2131755625, "field 'cbNoteCompleted'");
    target.cbNoteCompleted = finder.castView(view, 2131755625, "field 'cbNoteCompleted'");
    view = finder.findRequiredView(source, 2131755241, "field 'tvDate'");
    target.tvDate = finder.castView(view, 2131755241, "field 'tvDate'");
    view = finder.findRequiredView(source, 2131755240, "field 'tvTime'");
    target.tvTime = finder.castView(view, 2131755240, "field 'tvTime'");
  }

  @Override public void unbind(T target) {
    target.tvDoctorName = null;
    target.tvSpeciality = null;
    target.cbNoteCompleted = null;
    target.tvDate = null;
    target.tvTime = null;
  }
}
