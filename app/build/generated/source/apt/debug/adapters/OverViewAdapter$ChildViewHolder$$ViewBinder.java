// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class OverViewAdapter$ChildViewHolder$$ViewBinder<T extends adapters.OverViewAdapter.ChildViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755590, "field 'etChild'");
    target.etChild = finder.castView(view, 2131755590, "field 'etChild'");
  }

  @Override public void unbind(T target) {
    target.etChild = null;
  }
}
