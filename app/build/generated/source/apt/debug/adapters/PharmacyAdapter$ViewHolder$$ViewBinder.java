// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PharmacyAdapter$ViewHolder$$ViewBinder<T extends adapters.PharmacyAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755297, "field 'tvClinicName'");
    target.tvClinicName = finder.castView(view, 2131755297, "field 'tvClinicName'");
    view = finder.findRequiredView(source, 2131755641, "field 'ivFav'");
    target.ivFav = finder.castView(view, 2131755641, "field 'ivFav'");
  }

  @Override public void unbind(T target) {
    target.tvClinicName = null;
    target.ivFav = null;
  }
}
