// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientLabTestAdapter$PastViewHolder$$ViewBinder<T extends adapters.PatientLabTestAdapter.PastViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755642, "field 'tvMedicationame'");
    target.tvMedicationame = finder.castView(view, 2131755642, "field 'tvMedicationame'");
    view = finder.findRequiredView(source, 2131755447, "field 'ivMore'");
    target.ivMore = finder.castView(view, 2131755447, "field 'ivMore'");
    view = finder.findRequiredView(source, 2131755650, "field 'ivIndicator'");
    target.ivIndicator = finder.castView(view, 2131755650, "field 'ivIndicator'");
  }

  @Override public void unbind(T target) {
    target.tvMedicationame = null;
    target.ivMore = null;
    target.ivIndicator = null;
  }
}
