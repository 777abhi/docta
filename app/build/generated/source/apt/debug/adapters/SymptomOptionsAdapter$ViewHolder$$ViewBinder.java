// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SymptomOptionsAdapter$ViewHolder$$ViewBinder<T extends adapters.SymptomOptionsAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755643, "field 'tvOption'");
    target.tvOption = finder.castView(view, 2131755643, "field 'tvOption'");
  }

  @Override public void unbind(T target) {
    target.tvOption = null;
  }
}
