// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientLabTestAdapter$ViewHolder$$ViewBinder<T extends adapters.PatientLabTestAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755642, "field 'tvMedicationame'");
    target.tvMedicationame = finder.castView(view, 2131755642, "field 'tvMedicationame'");
    view = finder.findRequiredView(source, 2131755624, "field 'tvOrderBy'");
    target.tvOrderBy = finder.castView(view, 2131755624, "field 'tvOrderBy'");
    view = finder.findRequiredView(source, 2131755652, "field 'chk'");
    target.chk = finder.castView(view, 2131755652, "field 'chk'");
  }

  @Override public void unbind(T target) {
    target.tvMedicationame = null;
    target.tvOrderBy = null;
    target.chk = null;
  }
}
