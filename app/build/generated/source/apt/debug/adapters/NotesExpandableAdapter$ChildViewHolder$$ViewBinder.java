// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NotesExpandableAdapter$ChildViewHolder$$ViewBinder<T extends adapters.NotesExpandableAdapter.ChildViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755590, "field 'tvChild'");
    target.tvChild = finder.castView(view, 2131755590, "field 'tvChild'");
  }

  @Override public void unbind(T target) {
    target.tvChild = null;
  }
}
