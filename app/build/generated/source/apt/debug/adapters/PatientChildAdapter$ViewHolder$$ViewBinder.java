// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientChildAdapter$ViewHolder$$ViewBinder<T extends adapters.PatientChildAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755228, "field 'profilePic'");
    target.profilePic = finder.castView(view, 2131755228, "field 'profilePic'");
    view = finder.findRequiredView(source, 2131755344, "field 'tvName'");
    target.tvName = finder.castView(view, 2131755344, "field 'tvName'");
  }

  @Override public void unbind(T target) {
    target.profilePic = null;
    target.tvName = null;
  }
}
