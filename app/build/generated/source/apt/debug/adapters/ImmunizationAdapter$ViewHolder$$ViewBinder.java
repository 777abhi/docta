// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ImmunizationAdapter$ViewHolder$$ViewBinder<T extends adapters.ImmunizationAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755344, "field 'tvName'");
    target.tvName = finder.castView(view, 2131755344, "field 'tvName'");
    view = finder.findRequiredView(source, 2131755222, "field 'tvLastDone'");
    target.tvLastDone = finder.castView(view, 2131755222, "field 'tvLastDone'");
    view = finder.findRequiredView(source, 2131755223, "field 'tvNextDue'");
    target.tvNextDue = finder.castView(view, 2131755223, "field 'tvNextDue'");
  }

  @Override public void unbind(T target) {
    target.tvName = null;
    target.tvLastDone = null;
    target.tvNextDue = null;
  }
}
