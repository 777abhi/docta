// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NotesExpandableAdapter$ViewHolder$$ViewBinder<T extends adapters.NotesExpandableAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755591, "field 'tvDiagnosis'");
    target.tvDiagnosis = finder.castView(view, 2131755591, "field 'tvDiagnosis'");
    view = finder.findRequiredView(source, 2131755592, "field 'tvDiagnosisName'");
    target.tvDiagnosisName = finder.castView(view, 2131755592, "field 'tvDiagnosisName'");
    view = finder.findRequiredView(source, 2131755593, "field 'etComment'");
    target.etComment = finder.castView(view, 2131755593, "field 'etComment'");
  }

  @Override public void unbind(T target) {
    target.tvDiagnosis = null;
    target.tvDiagnosisName = null;
    target.etComment = null;
  }
}
