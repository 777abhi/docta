// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class VisitAdapter$ViewHolder$$ViewBinder<T extends adapters.VisitAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755622, "field 'tvAppointmentDate'");
    target.tvAppointmentDate = finder.castView(view, 2131755622, "field 'tvAppointmentDate'");
    view = finder.findRequiredView(source, 2131755623, "field 'tvAppointmentName'");
    target.tvAppointmentName = finder.castView(view, 2131755623, "field 'tvAppointmentName'");
  }

  @Override public void unbind(T target) {
    target.tvAppointmentDate = null;
    target.tvAppointmentName = null;
  }
}
