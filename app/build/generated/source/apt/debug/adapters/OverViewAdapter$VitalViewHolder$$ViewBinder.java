// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class OverViewAdapter$VitalViewHolder$$ViewBinder<T extends adapters.OverViewAdapter.VitalViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755321, "field 'bpValue'");
    target.bpValue = finder.castView(view, 2131755321, "field 'bpValue'");
    view = finder.findRequiredView(source, 2131755655, "field 'bpParent'");
    target.bpParent = finder.castView(view, 2131755655, "field 'bpParent'");
    view = finder.findRequiredView(source, 2131755322, "field 'tempValue'");
    target.tempValue = finder.castView(view, 2131755322, "field 'tempValue'");
    view = finder.findRequiredView(source, 2131755656, "field 'tempParent'");
    target.tempParent = finder.castView(view, 2131755656, "field 'tempParent'");
    view = finder.findRequiredView(source, 2131755323, "field 'pulseValue'");
    target.pulseValue = finder.castView(view, 2131755323, "field 'pulseValue'");
    view = finder.findRequiredView(source, 2131755657, "field 'pulseParent'");
    target.pulseParent = finder.castView(view, 2131755657, "field 'pulseParent'");
    view = finder.findRequiredView(source, 2131755324, "field 'osatValue'");
    target.osatValue = finder.castView(view, 2131755324, "field 'osatValue'");
    view = finder.findRequiredView(source, 2131755658, "field 'o2satParent'");
    target.o2satParent = finder.castView(view, 2131755658, "field 'o2satParent'");
    view = finder.findRequiredView(source, 2131755320, "field 'vitalParent'");
    target.vitalParent = finder.castView(view, 2131755320, "field 'vitalParent'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
  }

  @Override public void unbind(T target) {
    target.bpValue = null;
    target.bpParent = null;
    target.tempValue = null;
    target.tempParent = null;
    target.pulseValue = null;
    target.pulseParent = null;
    target.osatValue = null;
    target.o2satParent = null;
    target.vitalParent = null;
    target.tvNoData = null;
  }
}
