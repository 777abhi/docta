// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class VaccineTrackAdapter$ViewHolder$$ViewBinder<T extends adapters.VaccineTrackAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755153, "field 'date'");
    target.date = finder.castView(view, 2131755153, "field 'date'");
  }

  @Override public void unbind(T target) {
    target.date = null;
  }
}
