// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NotesExpandableAdapter$HeaderViewHolder$$ViewBinder<T extends adapters.NotesExpandableAdapter.HeaderViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755594, "field 'tvGroupName'");
    target.tvGroupName = finder.castView(view, 2131755594, "field 'tvGroupName'");
    view = finder.findRequiredView(source, 2131755595, "field 'cbCheck'");
    target.cbCheck = finder.castView(view, 2131755595, "field 'cbCheck'");
  }

  @Override public void unbind(T target) {
    target.tvGroupName = null;
    target.cbCheck = null;
  }
}
