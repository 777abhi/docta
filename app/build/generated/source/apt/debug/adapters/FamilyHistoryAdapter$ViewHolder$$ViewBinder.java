// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class FamilyHistoryAdapter$ViewHolder$$ViewBinder<T extends adapters.FamilyHistoryAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755638, "field 'tvMedicalProblem'");
    target.tvMedicalProblem = finder.castView(view, 2131755638, "field 'tvMedicalProblem'");
    view = finder.findRequiredView(source, 2131755639, "field 'tvRelation'");
    target.tvRelation = finder.castView(view, 2131755639, "field 'tvRelation'");
    view = finder.findRequiredView(source, 2131755640, "field 'tvAge'");
    target.tvAge = finder.castView(view, 2131755640, "field 'tvAge'");
    view = finder.findRequiredView(source, 2131755630, "field 'tvStatus'");
    target.tvStatus = finder.castView(view, 2131755630, "field 'tvStatus'");
  }

  @Override public void unbind(T target) {
    target.tvMedicalProblem = null;
    target.tvRelation = null;
    target.tvAge = null;
    target.tvStatus = null;
  }
}
