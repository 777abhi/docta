// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NotificationAdapter$ViewHolder$$ViewBinder<T extends adapters.NotificationAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755344, "field 'tvName'");
    target.tvName = finder.castView(view, 2131755344, "field 'tvName'");
    view = finder.findRequiredView(source, 2131755621, "field 'ivDelete'");
    target.ivDelete = finder.castView(view, 2131755621, "field 'ivDelete'");
  }

  @Override public void unbind(T target) {
    target.tvName = null;
    target.ivDelete = null;
  }
}
