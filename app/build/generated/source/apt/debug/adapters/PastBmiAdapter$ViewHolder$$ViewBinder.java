// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PastBmiAdapter$ViewHolder$$ViewBinder<T extends adapters.PastBmiAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755241, "field 'tvDate'");
    target.tvDate = finder.castView(view, 2131755241, "field 'tvDate'");
    view = finder.findRequiredView(source, 2131755624, "field 'tvOrderBy'");
    target.tvOrderBy = finder.castView(view, 2131755624, "field 'tvOrderBy'");
    view = finder.findRequiredView(source, 2131755271, "field 'tvValue'");
    target.tvValue = finder.castView(view, 2131755271, "field 'tvValue'");
    view = finder.findRequiredView(source, 2131755195, "field 'tvWeight'");
    target.tvWeight = finder.castView(view, 2131755195, "field 'tvWeight'");
  }

  @Override public void unbind(T target) {
    target.tvDate = null;
    target.tvOrderBy = null;
    target.tvValue = null;
    target.tvWeight = null;
  }
}
