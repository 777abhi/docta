// Generated code from Butter Knife. Do not modify!
package adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class VaccineListAdapter$ViewHolder$$ViewBinder<T extends adapters.VaccineListAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755642, "field 'tvMedicationame'");
    target.tvMedicationame = finder.castView(view, 2131755642, "field 'tvMedicationame'");
  }

  @Override public void unbind(T target) {
    target.tvMedicationame = null;
  }
}
