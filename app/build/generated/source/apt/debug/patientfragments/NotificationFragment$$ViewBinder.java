// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NotificationFragment$$ViewBinder<T extends patientfragments.NotificationFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755441, "field 'rvNotifications'");
    target.rvNotifications = finder.castView(view, 2131755441, "field 'rvNotifications'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
    view = finder.findRequiredView(source, 2131755440, "field 'tvClearAll' and method 'click'");
    target.tvClearAll = finder.castView(view, 2131755440, "field 'tvClearAll'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
  }

  @Override public void unbind(T target) {
    target.rvNotifications = null;
    target.tvNoData = null;
    target.tvClearAll = null;
    target.pBar = null;
  }
}
