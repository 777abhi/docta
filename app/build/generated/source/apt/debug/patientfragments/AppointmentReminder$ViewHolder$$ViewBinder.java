// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AppointmentReminder$ViewHolder$$ViewBinder<T extends patientfragments.AppointmentReminder.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755240, "field 'tvTime'");
    target.tvTime = finder.castView(view, 2131755240, "field 'tvTime'");
    view = finder.findRequiredView(source, 2131755653, "field 'tvClinic'");
    target.tvClinic = finder.castView(view, 2131755653, "field 'tvClinic'");
    view = finder.findRequiredView(source, 2131755244, "field 'tvDoctor'");
    target.tvDoctor = finder.castView(view, 2131755244, "field 'tvDoctor'");
    view = finder.findRequiredView(source, 2131755245, "field 'tvVisitType'");
    target.tvVisitType = finder.castView(view, 2131755245, "field 'tvVisitType'");
  }

  @Override public void unbind(T target) {
    target.tvTime = null;
    target.tvClinic = null;
    target.tvDoctor = null;
    target.tvVisitType = null;
  }
}
