// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class VaccineReminder$$ViewBinder<T extends patientfragments.VaccineReminder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755246, "field 'ivCalander' and method 'click'");
    target.ivCalander = finder.castView(view, 2131755246, "field 'ivCalander'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755144, "field 'linearParent'");
    target.linearParent = finder.castView(view, 2131755144, "field 'linearParent'");
    view = finder.findRequiredView(source, 2131755240, "field 'tvTime'");
    target.tvTime = finder.castView(view, 2131755240, "field 'tvTime'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
  }

  @Override public void unbind(T target) {
    target.ivCalander = null;
    target.linearParent = null;
    target.tvTime = null;
    target.tvNoData = null;
  }
}
