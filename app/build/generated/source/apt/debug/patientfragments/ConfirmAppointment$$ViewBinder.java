// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ConfirmAppointment$$ViewBinder<T extends patientfragments.ConfirmAppointment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755241, "field 'tvDate'");
    target.tvDate = finder.castView(view, 2131755241, "field 'tvDate'");
    view = finder.findRequiredView(source, 2131755295, "field 'tvDay'");
    target.tvDay = finder.castView(view, 2131755295, "field 'tvDay'");
    view = finder.findRequiredView(source, 2131755294, "field 'tvMonth'");
    target.tvMonth = finder.castView(view, 2131755294, "field 'tvMonth'");
    view = finder.findRequiredView(source, 2131755240, "field 'tvTime'");
    target.tvTime = finder.castView(view, 2131755240, "field 'tvTime'");
    view = finder.findRequiredView(source, 2131755299, "field 'tvCost'");
    target.tvCost = finder.castView(view, 2131755299, "field 'tvCost'");
    view = finder.findRequiredView(source, 2131755296, "field 'tvAppointmentFor'");
    target.tvAppointmentFor = finder.castView(view, 2131755296, "field 'tvAppointmentFor'");
    view = finder.findRequiredView(source, 2131755297, "field 'tvClinicName'");
    target.tvClinicName = finder.castView(view, 2131755297, "field 'tvClinicName'");
    view = finder.findRequiredView(source, 2131755298, "field 'tvProviderName'");
    target.tvProviderName = finder.castView(view, 2131755298, "field 'tvProviderName'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755293, "field 'tvConfirmationText'");
    target.tvConfirmationText = finder.castView(view, 2131755293, "field 'tvConfirmationText'");
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755300, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvDate = null;
    target.tvDay = null;
    target.tvMonth = null;
    target.tvTime = null;
    target.tvCost = null;
    target.tvAppointmentFor = null;
    target.tvClinicName = null;
    target.tvProviderName = null;
    target.pBar = null;
    target.tvConfirmationText = null;
    target.tvTitle = null;
    target.tvNext = null;
  }
}
