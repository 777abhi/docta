// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class VaccineReminder$ViewHolder$$ViewBinder<T extends patientfragments.VaccineReminder.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755643, "field 'tvVaccineName'");
    target.tvVaccineName = finder.castView(view, 2131755643, "field 'tvVaccineName'");
  }

  @Override public void unbind(T target) {
    target.tvVaccineName = null;
  }
}
