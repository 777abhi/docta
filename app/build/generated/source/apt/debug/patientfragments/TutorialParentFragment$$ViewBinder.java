// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TutorialParentFragment$$ViewBinder<T extends patientfragments.TutorialParentFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755537, "field 'tutorialPager'");
    target.tutorialPager = finder.castView(view, 2131755537, "field 'tutorialPager'");
    view = finder.findRequiredView(source, 2131755258, "field 'iv1'");
    target.iv1 = finder.castView(view, 2131755258, "field 'iv1'");
    view = finder.findRequiredView(source, 2131755259, "field 'iv2'");
    target.iv2 = finder.castView(view, 2131755259, "field 'iv2'");
    view = finder.findRequiredView(source, 2131755260, "field 'iv3'");
    target.iv3 = finder.castView(view, 2131755260, "field 'iv3'");
    view = finder.findRequiredView(source, 2131755263, "field 'iv4'");
    target.iv4 = finder.castView(view, 2131755263, "field 'iv4'");
    view = finder.findRequiredView(source, 2131755264, "field 'iv5'");
    target.iv5 = finder.castView(view, 2131755264, "field 'iv5'");
  }

  @Override public void unbind(T target) {
    target.tutorialPager = null;
    target.iv1 = null;
    target.iv2 = null;
    target.iv3 = null;
    target.iv4 = null;
    target.iv5 = null;
  }
}
