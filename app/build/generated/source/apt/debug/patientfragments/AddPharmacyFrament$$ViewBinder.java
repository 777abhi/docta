// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AddPharmacyFrament$$ViewBinder<T extends patientfragments.AddPharmacyFrament> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755203, "field 'etName'");
    target.etName = finder.castView(view, 2131755203, "field 'etName'");
    view = finder.findRequiredView(source, 2131755204, "field 'rvData'");
    target.rvData = finder.castView(view, 2131755204, "field 'rvData'");
    view = finder.findRequiredView(source, 2131755198, "field 'rgSelection'");
    target.rgSelection = finder.castView(view, 2131755198, "field 'rgSelection'");
    view = finder.findRequiredView(source, 2131755202, "field 'listParent'");
    target.listParent = finder.castView(view, 2131755202, "field 'listParent'");
    view = finder.findRequiredView(source, 2131755201, "field 'mapParent'");
    target.mapParent = finder.castView(view, 2131755201, "field 'mapParent'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvTitle = null;
    target.tvNext = null;
    target.pBar = null;
    target.etName = null;
    target.rvData = null;
    target.rgSelection = null;
    target.listParent = null;
    target.mapParent = null;
    target.tvNoData = null;
  }
}
