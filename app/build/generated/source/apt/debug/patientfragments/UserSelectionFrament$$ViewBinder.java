// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UserSelectionFrament$$ViewBinder<T extends patientfragments.UserSelectionFrament> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755538, "field 'tvPatient' and method 'click'");
    target.tvPatient = finder.castView(view, 2131755538, "field 'tvPatient'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755244, "field 'tvDoctor' and method 'click'");
    target.tvDoctor = finder.castView(view, 2131755244, "field 'tvDoctor'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvPatient = null;
    target.tvDoctor = null;
  }
}
