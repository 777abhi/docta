// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TestDetailFragment$$ViewBinder<T extends patientfragments.TestDetailFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext' and method 'click'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755533, "field 'tvResultValue'");
    target.tvResultValue = finder.castView(view, 2131755533, "field 'tvResultValue'");
    view = finder.findRequiredView(source, 2131755239, "field 'tvPatientName'");
    target.tvPatientName = finder.castView(view, 2131755239, "field 'tvPatientName'");
    view = finder.findRequiredView(source, 2131755532, "field 'tvDateTime'");
    target.tvDateTime = finder.castView(view, 2131755532, "field 'tvDateTime'");
    view = finder.findRequiredView(source, 2131755534, "field 'testProgress'");
    target.testProgress = finder.castView(view, 2131755534, "field 'testProgress'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvTitle = null;
    target.tvNext = null;
    target.tvResultValue = null;
    target.tvPatientName = null;
    target.tvDateTime = null;
    target.testProgress = null;
  }
}
