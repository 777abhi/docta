// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LoginFragment$$ViewBinder<T extends patientfragments.LoginFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755230, "field 'etEmail'");
    target.etEmail = finder.castView(view, 2131755230, "field 'etEmail'");
    view = finder.findRequiredView(source, 2131755406, "field 'etPassword'");
    target.etPassword = finder.castView(view, 2131755406, "field 'etPassword'");
    view = finder.findRequiredView(source, 2131755408, "field 'tvSignUp' and method 'click'");
    target.tvSignUp = finder.castView(view, 2131755408, "field 'tvSignUp'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755407, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755409, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.etEmail = null;
    target.etPassword = null;
    target.tvSignUp = null;
    target.pBar = null;
  }
}
