// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HealthReminders$$ViewBinder<T extends patientfragments.HealthReminders> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755147, "field 'tabLayout'");
    target.tabLayout = finder.castView(view, 2131755147, "field 'tabLayout'");
    view = finder.findRequiredView(source, 2131755166, "field 'pager'");
    target.pager = finder.castView(view, 2131755166, "field 'pager'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755164, "field 'tvOrder'");
    target.tvOrder = finder.castView(view, 2131755164, "field 'tvOrder'");
  }

  @Override public void unbind(T target) {
    target.frTopBar = null;
    target.tabLayout = null;
    target.pager = null;
    target.pBar = null;
    target.tvOrder = null;
  }
}
