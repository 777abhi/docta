// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AddUser$$ViewBinder<T extends patientfragments.AddUser> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755228, "field 'profilePic' and method 'click'");
    target.profilePic = finder.castView(view, 2131755228, "field 'profilePic'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755231, "field 'etFirstName'");
    target.etFirstName = finder.castView(view, 2131755231, "field 'etFirstName'");
    view = finder.findRequiredView(source, 2131755232, "field 'etLastName'");
    target.etLastName = finder.castView(view, 2131755232, "field 'etLastName'");
    view = finder.findRequiredView(source, 2131755233, "field 'etPhoneNumber'");
    target.etPhoneNumber = finder.castView(view, 2131755233, "field 'etPhoneNumber'");
    view = finder.findRequiredView(source, 2131755234, "field 'tvDob' and method 'click'");
    target.tvDob = finder.castView(view, 2131755234, "field 'tvDob'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755217, "field 'etAge'");
    target.etAge = finder.castView(view, 2131755217, "field 'etAge'");
    view = finder.findRequiredView(source, 2131755236, "field 'rgGender'");
    target.rgGender = finder.castView(view, 2131755236, "field 'rgGender'");
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext' and method 'click'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755230, "field 'etEmail'");
    target.etEmail = finder.castView(view, 2131755230, "field 'etEmail'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.profilePic = null;
    target.etFirstName = null;
    target.etLastName = null;
    target.etPhoneNumber = null;
    target.tvDob = null;
    target.etAge = null;
    target.rgGender = null;
    target.tvTitle = null;
    target.tvNext = null;
    target.pBar = null;
    target.etEmail = null;
  }
}
