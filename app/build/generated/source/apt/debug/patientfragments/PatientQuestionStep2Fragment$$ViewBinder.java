// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientQuestionStep2Fragment$$ViewBinder<T extends patientfragments.PatientQuestionStep2Fragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755464, "field 'rvAvailableTimes'");
    target.rvAvailableTimes = finder.castView(view, 2131755464, "field 'rvAvailableTimes'");
    view = finder.findRequiredView(source, 2131755463, "field 'dotView'");
    target.dotView = finder.castView(view, 2131755463, "field 'dotView'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755518, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.rvAvailableTimes = null;
    target.dotView = null;
  }
}
