// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AppointmentTutorialFragment$$ViewBinder<T extends patientfragments.AppointmentTutorialFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755256, "field 'tvTut1'");
    target.tvTut1 = finder.castView(view, 2131755256, "field 'tvTut1'");
    view = finder.findRequiredView(source, 2131755261, "field 'tvTut2'");
    target.tvTut2 = finder.castView(view, 2131755261, "field 'tvTut2'");
    view = finder.findRequiredView(source, 2131755266, "field 'tvTut3'");
    target.tvTut3 = finder.castView(view, 2131755266, "field 'tvTut3'");
    view = finder.findRequiredView(source, 2131755514, "field 'tvBack' and method 'click'");
    target.tvBack = finder.castView(view, 2131755514, "field 'tvBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755258, "field 'iv1'");
    target.iv1 = finder.castView(view, 2131755258, "field 'iv1'");
    view = finder.findRequiredView(source, 2131755259, "field 'iv2'");
    target.iv2 = finder.castView(view, 2131755259, "field 'iv2'");
    view = finder.findRequiredView(source, 2131755260, "field 'iv3'");
    target.iv3 = finder.castView(view, 2131755260, "field 'iv3'");
    view = finder.findRequiredView(source, 2131755263, "field 'iv4'");
    target.iv4 = finder.castView(view, 2131755263, "field 'iv4'");
    view = finder.findRequiredView(source, 2131755264, "field 'iv5'");
    target.iv5 = finder.castView(view, 2131755264, "field 'iv5'");
    view = finder.findRequiredView(source, 2131755265, "field 'iv6'");
    target.iv6 = finder.castView(view, 2131755265, "field 'iv6'");
    view = finder.findRequiredView(source, 2131755257, "field 'animParent1'");
    target.animParent1 = finder.castView(view, 2131755257, "field 'animParent1'");
    view = finder.findRequiredView(source, 2131755262, "field 'animParent2'");
    target.animParent2 = finder.castView(view, 2131755262, "field 'animParent2'");
  }

  @Override public void unbind(T target) {
    target.tvTut1 = null;
    target.tvTut2 = null;
    target.tvTut3 = null;
    target.tvBack = null;
    target.tvTitle = null;
    target.tvNext = null;
    target.iv1 = null;
    target.iv2 = null;
    target.iv3 = null;
    target.iv4 = null;
    target.iv5 = null;
    target.iv6 = null;
    target.animParent1 = null;
    target.animParent2 = null;
  }
}
