// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UpcomingAppointmentFragment$$ViewBinder<T extends patientfragments.UpcomingAppointmentFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755163, "field 'rvAppointmentList'");
    target.rvAppointmentList = finder.castView(view, 2131755163, "field 'rvAppointmentList'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
  }

  @Override public void unbind(T target) {
    target.rvAppointmentList = null;
    target.tvNoData = null;
  }
}
