// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MedicationDetails$$ViewBinder<T extends patientfragments.MedicationDetails> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755419, "field 'tvMedicationName'");
    target.tvMedicationName = finder.castView(view, 2131755419, "field 'tvMedicationName'");
    view = finder.findRequiredView(source, 2131755421, "field 'tvQuantity'");
    target.tvQuantity = finder.castView(view, 2131755421, "field 'tvQuantity'");
    view = finder.findRequiredView(source, 2131755420, "field 'tvDose'");
    target.tvDose = finder.castView(view, 2131755420, "field 'tvDose'");
    view = finder.findRequiredView(source, 2131755422, "field 'tvFrequency'");
    target.tvFrequency = finder.castView(view, 2131755422, "field 'tvFrequency'");
    view = finder.findRequiredView(source, 2131755423, "field 'tvStart'");
    target.tvStart = finder.castView(view, 2131755423, "field 'tvStart'");
    view = finder.findRequiredView(source, 2131755424, "field 'tvEnd'");
    target.tvEnd = finder.castView(view, 2131755424, "field 'tvEnd'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvTitle = null;
    target.tvNext = null;
    target.tvMedicationName = null;
    target.tvQuantity = null;
    target.tvDose = null;
    target.tvFrequency = null;
    target.tvStart = null;
    target.tvEnd = null;
  }
}
