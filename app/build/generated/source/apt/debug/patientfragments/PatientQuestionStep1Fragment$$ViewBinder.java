// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientQuestionStep1Fragment$$ViewBinder<T extends patientfragments.PatientQuestionStep1Fragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755462, "field 'etSympotomDesc'");
    target.etSympotomDesc = finder.castView(view, 2131755462, "field 'etSympotomDesc'");
    view = finder.findRequiredView(source, 2131755159, "field 'tvHeading'");
    target.tvHeading = finder.castView(view, 2131755159, "field 'tvHeading'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext' and method 'click'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.etSympotomDesc = null;
    target.tvHeading = null;
    target.tvNext = null;
  }
}
