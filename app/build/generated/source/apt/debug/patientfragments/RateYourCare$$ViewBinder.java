// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RateYourCare$$ViewBinder<T extends patientfragments.RateYourCare> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755514, "field 'tvBack' and method 'click'");
    target.tvBack = finder.castView(view, 2131755514, "field 'tvBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755496, "field 'receptionistRating'");
    target.receptionistRating = finder.castView(view, 2131755496, "field 'receptionistRating'");
    view = finder.findRequiredView(source, 2131755497, "field 'nurseRating'");
    target.nurseRating = finder.castView(view, 2131755497, "field 'nurseRating'");
    view = finder.findRequiredView(source, 2131755336, "field 'doctorRating'");
    target.doctorRating = finder.castView(view, 2131755336, "field 'doctorRating'");
    view = finder.findRequiredView(source, 2131755499, "field 'overallRating'");
    target.overallRating = finder.castView(view, 2131755499, "field 'overallRating'");
    view = finder.findRequiredView(source, 2131755495, "field 'etAdditionalComment'");
    target.etAdditionalComment = finder.castView(view, 2131755495, "field 'etAdditionalComment'");
    view = finder.findRequiredView(source, 2131755458, "field 'tvSubmit' and method 'click'");
    target.tvSubmit = finder.castView(view, 2131755458, "field 'tvSubmit'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755498, "field 'tvOverallRateText'");
    target.tvOverallRateText = finder.castView(view, 2131755498, "field 'tvOverallRateText'");
  }

  @Override public void unbind(T target) {
    target.tvBack = null;
    target.tvTitle = null;
    target.tvNext = null;
    target.frTopBar = null;
    target.receptionistRating = null;
    target.nurseRating = null;
    target.doctorRating = null;
    target.overallRating = null;
    target.etAdditionalComment = null;
    target.tvSubmit = null;
    target.pBar = null;
    target.tvOverallRateText = null;
  }
}
