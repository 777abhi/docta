// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MedicationReminder$ViewHolder$$ViewBinder<T extends patientfragments.MedicationReminder.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755642, "field 'tvMedicationame'");
    target.tvMedicationame = finder.castView(view, 2131755642, "field 'tvMedicationame'");
    view = finder.findRequiredView(source, 2131755421, "field 'tvQuantity'");
    target.tvQuantity = finder.castView(view, 2131755421, "field 'tvQuantity'");
    view = finder.findRequiredView(source, 2131755654, "field 'cbTakeMedication'");
    target.cbTakeMedication = finder.castView(view, 2131755654, "field 'cbTakeMedication'");
  }

  @Override public void unbind(T target) {
    target.tvMedicationame = null;
    target.tvQuantity = null;
    target.cbTakeMedication = null;
  }
}
