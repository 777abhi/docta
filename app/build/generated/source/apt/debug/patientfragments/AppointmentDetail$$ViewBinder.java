// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AppointmentDetail$$ViewBinder<T extends patientfragments.AppointmentDetail> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755239, "field 'tvPatientName'");
    target.tvPatientName = finder.castView(view, 2131755239, "field 'tvPatientName'");
    view = finder.findRequiredView(source, 2131755241, "field 'tvDate'");
    target.tvDate = finder.castView(view, 2131755241, "field 'tvDate'");
    view = finder.findRequiredView(source, 2131755242, "field 'tvReason'");
    target.tvReason = finder.castView(view, 2131755242, "field 'tvReason'");
    view = finder.findRequiredView(source, 2131755243, "field 'tvProvider'");
    target.tvProvider = finder.castView(view, 2131755243, "field 'tvProvider'");
    view = finder.findRequiredView(source, 2131755244, "field 'tvDoctor'");
    target.tvDoctor = finder.castView(view, 2131755244, "field 'tvDoctor'");
    view = finder.findRequiredView(source, 2131755245, "field 'tvVisitType'");
    target.tvVisitType = finder.castView(view, 2131755245, "field 'tvVisitType'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext' and method 'click'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755240, "field 'tvTime'");
    target.tvTime = finder.castView(view, 2131755240, "field 'tvTime'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvPatientName = null;
    target.tvDate = null;
    target.tvReason = null;
    target.tvProvider = null;
    target.tvDoctor = null;
    target.tvVisitType = null;
    target.pBar = null;
    target.tvTitle = null;
    target.tvNext = null;
    target.tvTime = null;
  }
}
