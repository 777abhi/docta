// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MedicationReminder$$ViewBinder<T extends patientfragments.MedicationReminder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755240, "field 'tvTime'");
    target.tvTime = finder.castView(view, 2131755240, "field 'tvTime'");
    view = finder.findRequiredView(source, 2131755428, "field 'tvTapToTake'");
    target.tvTapToTake = finder.castView(view, 2131755428, "field 'tvTapToTake'");
    view = finder.findRequiredView(source, 2131755429, "field 'tvTargetTime1'");
    target.tvTargetTime1 = finder.castView(view, 2131755429, "field 'tvTargetTime1'");
    view = finder.findRequiredView(source, 2131755430, "field 'tvTargetTime2'");
    target.tvTargetTime2 = finder.castView(view, 2131755430, "field 'tvTargetTime2'");
    view = finder.findRequiredView(source, 2131755144, "field 'linearParent'");
    target.linearParent = finder.castView(view, 2131755144, "field 'linearParent'");
    view = finder.findRequiredView(source, 2131755427, "field 'concentricView'");
    target.concentricView = finder.castView(view, 2131755427, "field 'concentricView'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
    view = finder.findRequiredView(source, 2131755246, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvTime = null;
    target.tvTapToTake = null;
    target.tvTargetTime1 = null;
    target.tvTargetTime2 = null;
    target.linearParent = null;
    target.concentricView = null;
    target.pBar = null;
    target.tvNoData = null;
  }
}
