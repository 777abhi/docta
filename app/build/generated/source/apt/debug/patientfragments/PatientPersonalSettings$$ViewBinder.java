// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientPersonalSettings$$ViewBinder<T extends patientfragments.PatientPersonalSettings> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755231, "field 'etFirstName'");
    target.etFirstName = finder.castView(view, 2131755231, "field 'etFirstName'");
    view = finder.findRequiredView(source, 2131755232, "field 'etLastName'");
    target.etLastName = finder.castView(view, 2131755232, "field 'etLastName'");
    view = finder.findRequiredView(source, 2131755234, "field 'tvDob' and method 'click'");
    target.tvDob = finder.castView(view, 2131755234, "field 'tvDob'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755237, "field 'rbMale'");
    target.rbMale = finder.castView(view, 2131755237, "field 'rbMale'");
    view = finder.findRequiredView(source, 2131755238, "field 'rbFemale'");
    target.rbFemale = finder.castView(view, 2131755238, "field 'rbFemale'");
    view = finder.findRequiredView(source, 2131755236, "field 'rgGender'");
    target.rgGender = finder.castView(view, 2131755236, "field 'rgGender'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755514, "field 'tvBack' and method 'click'");
    target.tvBack = finder.castView(view, 2131755514, "field 'tvBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755457, "field 'etPhone'");
    target.etPhone = finder.castView(view, 2131755457, "field 'etPhone'");
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755458, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.etFirstName = null;
    target.etLastName = null;
    target.tvDob = null;
    target.rbMale = null;
    target.rbFemale = null;
    target.rgGender = null;
    target.pBar = null;
    target.tvBack = null;
    target.etPhone = null;
    target.tvTitle = null;
    target.tvNext = null;
  }
}
