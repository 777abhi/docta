// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ClosestClinic$$ViewBinder<T extends patientfragments.ClosestClinic> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755292, "field 'gridParent'");
    target.gridParent = finder.castView(view, 2131755292, "field 'gridParent'");
    view = finder.findRequiredView(source, 2131755291, "field 'tvIFound'");
    target.tvIFound = finder.castView(view, 2131755291, "field 'tvIFound'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755518, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.gridParent = null;
    target.tvIFound = null;
    target.tvNoData = null;
    target.pBar = null;
  }
}
