// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TutorialFragment$$ViewBinder<T extends patientfragments.TutorialFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755311, "field 'ivImg'");
    target.ivImg = finder.castView(view, 2131755311, "field 'ivImg'");
    view = finder.findRequiredView(source, 2131755536, "field 'tvTutorialText'");
    target.tvTutorialText = finder.castView(view, 2131755536, "field 'tvTutorialText'");
    view = finder.findRequiredView(source, 2131755535, "field 'tvTutorialHeading'");
    target.tvTutorialHeading = finder.castView(view, 2131755535, "field 'tvTutorialHeading'");
  }

  @Override public void unbind(T target) {
    target.ivImg = null;
    target.tvTutorialText = null;
    target.tvTutorialHeading = null;
  }
}
