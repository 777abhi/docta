// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientSelectionFragment$$ViewBinder<T extends patientfragments.PatientSelectionFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755467, "field 'patientList'");
    target.patientList = finder.castView(view, 2131755467, "field 'patientList'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext' and method 'click'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755468, "field 'tvNoOtherUser'");
    target.tvNoOtherUser = finder.castView(view, 2131755468, "field 'tvNoOtherUser'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.patientList = null;
    target.pBar = null;
    target.tvNext = null;
    target.frTopBar = null;
    target.tvNoOtherUser = null;
  }
}
