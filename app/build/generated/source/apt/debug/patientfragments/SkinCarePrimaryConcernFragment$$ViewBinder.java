// Generated code from Butter Knife. Do not modify!
package patientfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SkinCarePrimaryConcernFragment$$ViewBinder<T extends patientfragments.SkinCarePrimaryConcernFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755159, "field 'tvHeading'");
    target.tvHeading = finder.castView(view, 2131755159, "field 'tvHeading'");
    view = finder.findRequiredView(source, 2131755402, "field 'rgParent'");
    target.rgParent = finder.castView(view, 2131755402, "field 'rgParent'");
    view = finder.findRequiredView(source, 2131755616, "field 'etPrimaryHealthConcern'");
    target.etPrimaryHealthConcern = finder.castView(view, 2131755616, "field 'etPrimaryHealthConcern'");
    view = finder.findRequiredView(source, 2131755615, "field 'rbOther'");
    target.rbOther = finder.castView(view, 2131755615, "field 'rbOther'");
    view = finder.findRequiredView(source, 2131755465, "field 'otherParent'");
    target.otherParent = finder.castView(view, 2131755465, "field 'otherParent'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755518, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvTitle = null;
    target.tvHeading = null;
    target.rgParent = null;
    target.etPrimaryHealthConcern = null;
    target.rbOther = null;
    target.otherParent = null;
  }
}
