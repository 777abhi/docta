// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ChooseSymptoms$$ViewBinder<T extends emrfragments.ChooseSymptoms> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext' and method 'click'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755273, "field 'gridGeneralSymptomms'");
    target.gridGeneralSymptomms = finder.castView(view, 2131755273, "field 'gridGeneralSymptomms'");
    view = finder.findRequiredView(source, 2131755275, "field 'gridHeadNeck'");
    target.gridHeadNeck = finder.castView(view, 2131755275, "field 'gridHeadNeck'");
    view = finder.findRequiredView(source, 2131755276, "field 'gridChest'");
    target.gridChest = finder.castView(view, 2131755276, "field 'gridChest'");
    view = finder.findRequiredView(source, 2131755277, "field 'gridGastrointestinal'");
    target.gridGastrointestinal = finder.castView(view, 2131755277, "field 'gridGastrointestinal'");
    view = finder.findRequiredView(source, 2131755278, "field 'gridUrinary'");
    target.gridUrinary = finder.castView(view, 2131755278, "field 'gridUrinary'");
    view = finder.findRequiredView(source, 2131755279, "field 'gridMusculoskeletal'");
    target.gridMusculoskeletal = finder.castView(view, 2131755279, "field 'gridMusculoskeletal'");
    view = finder.findRequiredView(source, 2131755280, "field 'gridSkin'");
    target.gridSkin = finder.castView(view, 2131755280, "field 'gridSkin'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755281, "field 'etOtherSymptom'");
    target.etOtherSymptom = finder.castView(view, 2131755281, "field 'etOtherSymptom'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvTitle = null;
    target.tvNext = null;
    target.gridGeneralSymptomms = null;
    target.gridHeadNeck = null;
    target.gridChest = null;
    target.gridGastrointestinal = null;
    target.gridUrinary = null;
    target.gridMusculoskeletal = null;
    target.gridSkin = null;
    target.pBar = null;
    target.etOtherSymptom = null;
  }
}
