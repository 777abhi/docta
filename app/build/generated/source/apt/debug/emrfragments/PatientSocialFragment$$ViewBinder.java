// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PatientSocialFragment$$ViewBinder<T extends emrfragments.PatientSocialFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755514, "field 'tvBack' and method 'click'");
    target.tvBack = finder.castView(view, 2131755514, "field 'tvBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext' and method 'click'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755371, "field 'tvTobaccoUse'");
    target.tvTobaccoUse = finder.castView(view, 2131755371, "field 'tvTobaccoUse'");
    view = finder.findRequiredView(source, 2131755373, "field 'tvAlcoholUse'");
    target.tvAlcoholUse = finder.castView(view, 2131755373, "field 'tvAlcoholUse'");
    view = finder.findRequiredView(source, 2131755377, "field 'tvDrugUse'");
    target.tvDrugUse = finder.castView(view, 2131755377, "field 'tvDrugUse'");
    view = finder.findRequiredView(source, 2131755381, "field 'tvSexualActivity'");
    target.tvSexualActivity = finder.castView(view, 2131755381, "field 'tvSexualActivity'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755372, "field 'rgTobacco'");
    target.rgTobacco = finder.castView(view, 2131755372, "field 'rgTobacco'");
    view = finder.findRequiredView(source, 2131755374, "field 'rgAlcoholUse'");
    target.rgAlcoholUse = finder.castView(view, 2131755374, "field 'rgAlcoholUse'");
    view = finder.findRequiredView(source, 2131755378, "field 'rgDrugUse'");
    target.rgDrugUse = finder.castView(view, 2131755378, "field 'rgDrugUse'");
    view = finder.findRequiredView(source, 2131755382, "field 'rgSexualActivity'");
    target.rgSexualActivity = finder.castView(view, 2131755382, "field 'rgSexualActivity'");
  }

  @Override public void unbind(T target) {
    target.tvBack = null;
    target.tvTitle = null;
    target.tvNext = null;
    target.frTopBar = null;
    target.tvTobaccoUse = null;
    target.tvAlcoholUse = null;
    target.tvDrugUse = null;
    target.tvSexualActivity = null;
    target.pBar = null;
    target.rgTobacco = null;
    target.rgAlcoholUse = null;
    target.rgDrugUse = null;
    target.rgSexualActivity = null;
  }
}
