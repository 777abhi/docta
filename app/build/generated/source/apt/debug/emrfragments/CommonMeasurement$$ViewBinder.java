// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CommonMeasurement$$ViewBinder<T extends emrfragments.CommonMeasurement> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755271, "field 'tvValue'");
    target.tvValue = finder.castView(view, 2131755271, "field 'tvValue'");
    view = finder.findRequiredView(source, 2131755272, "field 'tvUnit'");
    target.tvUnit = finder.castView(view, 2131755272, "field 'tvUnit'");
    view = finder.findRequiredView(source, 2131755268, "field 'circularSeekBar'");
    target.circularSeekBar = finder.castView(view, 2131755268, "field 'circularSeekBar'");
    view = finder.findRequiredView(source, 2131755662, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755661, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvTitle = null;
    target.frTopBar = null;
    target.tvValue = null;
    target.tvUnit = null;
    target.circularSeekBar = null;
  }
}
