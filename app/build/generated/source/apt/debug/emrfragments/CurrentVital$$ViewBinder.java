// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CurrentVital$$ViewBinder<T extends emrfragments.CurrentVital> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755321, "field 'bpValue'");
    target.bpValue = finder.castView(view, 2131755321, "field 'bpValue'");
    view = finder.findRequiredView(source, 2131755322, "field 'tempValue'");
    target.tempValue = finder.castView(view, 2131755322, "field 'tempValue'");
    view = finder.findRequiredView(source, 2131755323, "field 'pulseValue'");
    target.pulseValue = finder.castView(view, 2131755323, "field 'pulseValue'");
    view = finder.findRequiredView(source, 2131755324, "field 'osatValue'");
    target.osatValue = finder.castView(view, 2131755324, "field 'osatValue'");
    view = finder.findRequiredView(source, 2131755241, "field 'tvDate'");
    target.tvDate = finder.castView(view, 2131755241, "field 'tvDate'");
    view = finder.findRequiredView(source, 2131755319, "field 'tvCompletedBy'");
    target.tvCompletedBy = finder.castView(view, 2131755319, "field 'tvCompletedBy'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
    view = finder.findRequiredView(source, 2131755320, "field 'vitalParent'");
    target.vitalParent = finder.castView(view, 2131755320, "field 'vitalParent'");
  }

  @Override public void unbind(T target) {
    target.bpValue = null;
    target.tempValue = null;
    target.pulseValue = null;
    target.osatValue = null;
    target.tvDate = null;
    target.tvCompletedBy = null;
    target.tvNoData = null;
    target.vitalParent = null;
  }
}
