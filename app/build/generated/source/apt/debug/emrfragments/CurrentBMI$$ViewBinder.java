// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CurrentBMI$$ViewBinder<T extends emrfragments.CurrentBMI> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755241, "field 'tvDate'");
    target.tvDate = finder.castView(view, 2131755241, "field 'tvDate'");
    view = finder.findRequiredView(source, 2131755319, "field 'tvCompletedBy'");
    target.tvCompletedBy = finder.castView(view, 2131755319, "field 'tvCompletedBy'");
    view = finder.findRequiredView(source, 2131755316, "field 'weightValue'");
    target.weightValue = finder.castView(view, 2131755316, "field 'weightValue'");
    view = finder.findRequiredView(source, 2131755317, "field 'heightValue'");
    target.heightValue = finder.castView(view, 2131755317, "field 'heightValue'");
    view = finder.findRequiredView(source, 2131755318, "field 'bmiValue'");
    target.bmiValue = finder.castView(view, 2131755318, "field 'bmiValue'");
    view = finder.findRequiredView(source, 2131755195, "field 'tvWeight'");
    target.tvWeight = finder.castView(view, 2131755195, "field 'tvWeight'");
    view = finder.findRequiredView(source, 2131755194, "field 'tvHeight'");
    target.tvHeight = finder.castView(view, 2131755194, "field 'tvHeight'");
    view = finder.findRequiredView(source, 2131755315, "field 'bmiParent'");
    target.bmiParent = finder.castView(view, 2131755315, "field 'bmiParent'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
  }

  @Override public void unbind(T target) {
    target.tvDate = null;
    target.tvCompletedBy = null;
    target.weightValue = null;
    target.heightValue = null;
    target.bmiValue = null;
    target.tvWeight = null;
    target.tvHeight = null;
    target.bmiParent = null;
    target.tvNoData = null;
  }
}
