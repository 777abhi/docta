// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class BloodPressureFragment$$ViewBinder<T extends emrfragments.BloodPressureFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755661, "field 'ivClose' and method 'click'");
    target.ivClose = finder.castView(view, 2131755661, "field 'ivClose'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755662, "field 'ivCheck' and method 'click'");
    target.ivCheck = finder.castView(view, 2131755662, "field 'ivCheck'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755269, "field 'rbSys'");
    target.rbSys = finder.castView(view, 2131755269, "field 'rbSys'");
    view = finder.findRequiredView(source, 2131755270, "field 'rbDias'");
    target.rbDias = finder.castView(view, 2131755270, "field 'rbDias'");
    view = finder.findRequiredView(source, 2131755198, "field 'rgSelection'");
    target.rgSelection = finder.castView(view, 2131755198, "field 'rgSelection'");
    view = finder.findRequiredView(source, 2131755271, "field 'tvValue'");
    target.tvValue = finder.castView(view, 2131755271, "field 'tvValue'");
    view = finder.findRequiredView(source, 2131755272, "field 'tvUnit'");
    target.tvUnit = finder.castView(view, 2131755272, "field 'tvUnit'");
    view = finder.findRequiredView(source, 2131755268, "field 'circularSeekBar'");
    target.circularSeekBar = finder.castView(view, 2131755268, "field 'circularSeekBar'");
  }

  @Override public void unbind(T target) {
    target.tvTitle = null;
    target.ivClose = null;
    target.ivCheck = null;
    target.frTopBar = null;
    target.rbSys = null;
    target.rbDias = null;
    target.rgSelection = null;
    target.tvValue = null;
    target.tvUnit = null;
    target.circularSeekBar = null;
  }
}
