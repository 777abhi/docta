// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NoteDetailFragment$$ViewBinder<T extends emrfragments.NoteDetailFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755514, "field 'tvBack' and method 'click'");
    target.tvBack = finder.castView(view, 2131755514, "field 'tvBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755224, "field 'tvDoctorName'");
    target.tvDoctorName = finder.castView(view, 2131755224, "field 'tvDoctorName'");
    view = finder.findRequiredView(source, 2131755225, "field 'tvDoctorDesignationAndClinic'");
    target.tvDoctorDesignationAndClinic = finder.castView(view, 2131755225, "field 'tvDoctorDesignationAndClinic'");
    view = finder.findRequiredView(source, 2131755226, "field 'elvNotesOptions'");
    target.elvNotesOptions = finder.castView(view, 2131755226, "field 'elvNotesOptions'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755193, "field 'tvSave' and method 'click'");
    target.tvSave = finder.castView(view, 2131755193, "field 'tvSave'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755197, "field 'tvDone' and method 'click'");
    target.tvDone = finder.castView(view, 2131755197, "field 'tvDone'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvBack = null;
    target.tvTitle = null;
    target.tvNext = null;
    target.frTopBar = null;
    target.tvDoctorName = null;
    target.tvDoctorDesignationAndClinic = null;
    target.elvNotesOptions = null;
    target.pBar = null;
    target.tvSave = null;
    target.tvDone = null;
  }
}
