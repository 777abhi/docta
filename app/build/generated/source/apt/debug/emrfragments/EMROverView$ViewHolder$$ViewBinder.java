// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class EMROverView$ViewHolder$$ViewBinder<T extends emrfragments.EMROverView.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755194, "field 'tvHeight'");
    target.tvHeight = finder.castView(view, 2131755194, "field 'tvHeight'");
    view = finder.findRequiredView(source, 2131755195, "field 'tvWeight'");
    target.tvWeight = finder.castView(view, 2131755195, "field 'tvWeight'");
    view = finder.findRequiredView(source, 2131755196, "field 'tvBmi'");
    target.tvBmi = finder.castView(view, 2131755196, "field 'tvBmi'");
    view = finder.findRequiredView(source, 2131755613, "field 'tvLastVisit'");
    target.tvLastVisit = finder.castView(view, 2131755613, "field 'tvLastVisit'");
  }

  @Override public void unbind(T target) {
    target.tvHeight = null;
    target.tvWeight = null;
    target.tvBmi = null;
    target.tvLastVisit = null;
  }
}
