// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CurrentImmunization$$ViewBinder<T extends emrfragments.CurrentImmunization> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755163, "field 'rvData'");
    target.rvData = finder.castView(view, 2131755163, "field 'rvData'");
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
  }

  @Override public void unbind(T target) {
    target.rvData = null;
    target.tvNoData = null;
    target.pBar = null;
  }
}
