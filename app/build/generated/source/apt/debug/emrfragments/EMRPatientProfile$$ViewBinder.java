// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class EMRPatientProfile$$ViewBinder<T extends emrfragments.EMRPatientProfile> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755228, "field 'profilePic'");
    target.profilePic = finder.castView(view, 2131755228, "field 'profilePic'");
    view = finder.findRequiredView(source, 2131755239, "field 'tvPatientName'");
    target.tvPatientName = finder.castView(view, 2131755239, "field 'tvPatientName'");
    view = finder.findRequiredView(source, 2131755369, "field 'tvMedicalId'");
    target.tvMedicalId = finder.castView(view, 2131755369, "field 'tvMedicalId'");
    view = finder.findRequiredView(source, 2131755235, "field 'tvSex'");
    target.tvSex = finder.castView(view, 2131755235, "field 'tvSex'");
    view = finder.findRequiredView(source, 2131755475, "field 'etHeight'");
    target.etHeight = finder.castView(view, 2131755475, "field 'etHeight'");
    view = finder.findRequiredView(source, 2131755472, "field 'etLanguage'");
    target.etLanguage = finder.castView(view, 2131755472, "field 'etLanguage'");
    view = finder.findRequiredView(source, 2131755479, "field 'etOccupation'");
    target.etOccupation = finder.castView(view, 2131755479, "field 'etOccupation'");
    view = finder.findRequiredView(source, 2131755482, "field 'etMaritalStatus'");
    target.etMaritalStatus = finder.castView(view, 2131755482, "field 'etMaritalStatus'");
    view = finder.findRequiredView(source, 2131755485, "field 'etEthnicGroup'");
    target.etEthnicGroup = finder.castView(view, 2131755485, "field 'etEthnicGroup'");
    view = finder.findRequiredView(source, 2131755488, "field 'etAddress'");
    target.etAddress = finder.castView(view, 2131755488, "field 'etAddress'");
    view = finder.findRequiredView(source, 2131755477, "field 'tvBirthday'");
    target.tvBirthday = finder.castView(view, 2131755477, "field 'tvBirthday'");
    view = finder.findRequiredView(source, 2131755346, "field 'tvPhone'");
    target.tvPhone = finder.castView(view, 2131755346, "field 'tvPhone'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755284, "field 'llRootClinic'");
    target.llRootClinic = finder.castView(view, 2131755284, "field 'llRootClinic'");
    view = finder.findRequiredView(source, 2131755470, "field 'llRootPersonal'");
    target.llRootPersonal = finder.castView(view, 2131755470, "field 'llRootPersonal'");
    view = finder.findRequiredView(source, 2131755147, "field 'tabLayout'");
    target.tabLayout = finder.castView(view, 2131755147, "field 'tabLayout'");
    view = finder.findRequiredView(source, 2131755224, "field 'tvDoctorName'");
    target.tvDoctorName = finder.castView(view, 2131755224, "field 'tvDoctorName'");
    view = finder.findRequiredView(source, 2131755287, "field 'tvPrimaryClinic'");
    target.tvPrimaryClinic = finder.castView(view, 2131755287, "field 'tvPrimaryClinic'");
    view = finder.findRequiredView(source, 2131755289, "field 'tvPrimaryPharmacy'");
    target.tvPrimaryPharmacy = finder.castView(view, 2131755289, "field 'tvPrimaryPharmacy'");
    view = finder.findRequiredView(source, 2131755476, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755489, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755486, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755483, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755480, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755288, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755286, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755290, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755285, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755473, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.profilePic = null;
    target.tvPatientName = null;
    target.tvMedicalId = null;
    target.tvSex = null;
    target.etHeight = null;
    target.etLanguage = null;
    target.etOccupation = null;
    target.etMaritalStatus = null;
    target.etEthnicGroup = null;
    target.etAddress = null;
    target.tvBirthday = null;
    target.tvPhone = null;
    target.pBar = null;
    target.llRootClinic = null;
    target.llRootPersonal = null;
    target.tabLayout = null;
    target.tvDoctorName = null;
    target.tvPrimaryClinic = null;
    target.tvPrimaryPharmacy = null;
  }
}
