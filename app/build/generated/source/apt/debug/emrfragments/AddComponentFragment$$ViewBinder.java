// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AddComponentFragment$$ViewBinder<T extends emrfragments.AddComponentFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755514, "field 'tvBack' and method 'click'");
    target.tvBack = finder.castView(view, 2131755514, "field 'tvBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755205, "field 'etComponentName'");
    target.etComponentName = finder.castView(view, 2131755205, "field 'etComponentName'");
    view = finder.findRequiredView(source, 2131755206, "field 'rbResultType'");
    target.rbResultType = finder.castView(view, 2131755206, "field 'rbResultType'");
    view = finder.findRequiredView(source, 2131755209, "field 'etUnits'");
    target.etUnits = finder.castView(view, 2131755209, "field 'etUnits'");
    view = finder.findRequiredView(source, 2131755213, "field 'etAbNormalStart'");
    target.etAbNormalStart = finder.castView(view, 2131755213, "field 'etAbNormalStart'");
    view = finder.findRequiredView(source, 2131755214, "field 'etAbNormalEnd'");
    target.etAbNormalEnd = finder.castView(view, 2131755214, "field 'etAbNormalEnd'");
    view = finder.findRequiredView(source, 2131755211, "field 'etNormalStart'");
    target.etNormalStart = finder.castView(view, 2131755211, "field 'etNormalStart'");
    view = finder.findRequiredView(source, 2131755212, "field 'etNormalEnd'");
    target.etNormalEnd = finder.castView(view, 2131755212, "field 'etNormalEnd'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755210, "field 'rangeParent'");
    target.rangeParent = finder.castView(view, 2131755210, "field 'rangeParent'");
    view = finder.findRequiredView(source, 2131755158, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755193, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvBack = null;
    target.tvTitle = null;
    target.tvNext = null;
    target.frTopBar = null;
    target.etComponentName = null;
    target.rbResultType = null;
    target.etUnits = null;
    target.etAbNormalStart = null;
    target.etAbNormalEnd = null;
    target.etNormalStart = null;
    target.etNormalEnd = null;
    target.pBar = null;
    target.rangeParent = null;
  }
}
