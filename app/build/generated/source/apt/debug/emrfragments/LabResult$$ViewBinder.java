// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LabResult$$ViewBinder<T extends emrfragments.LabResult> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755203, "field 'etName' and method 'click'");
    target.etName = finder.castView(view, 2131755203, "field 'etName'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755401, "field 'etResult'");
    target.etResult = finder.castView(view, 2131755401, "field 'etResult'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755272, "field 'tvUnit'");
    target.tvUnit = finder.castView(view, 2131755272, "field 'tvUnit'");
    view = finder.findRequiredView(source, 2131755400, "field 'resultParent'");
    target.resultParent = finder.castView(view, 2131755400, "field 'resultParent'");
    view = finder.findRequiredView(source, 2131755403, "field 'rbPositive'");
    target.rbPositive = finder.castView(view, 2131755403, "field 'rbPositive'");
    view = finder.findRequiredView(source, 2131755404, "field 'rbNegative'");
    target.rbNegative = finder.castView(view, 2131755404, "field 'rbNegative'");
    view = finder.findRequiredView(source, 2131755402, "field 'rgParent'");
    target.rgParent = finder.castView(view, 2131755402, "field 'rgParent'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755193, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755158, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvTitle = null;
    target.tvNext = null;
    target.etName = null;
    target.etResult = null;
    target.pBar = null;
    target.tvUnit = null;
    target.resultParent = null;
    target.rbPositive = null;
    target.rbNegative = null;
    target.rgParent = null;
  }
}
