// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class BarGraphFragment$$ViewBinder<T extends emrfragments.BarGraphFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755267, "field 'chart'");
    target.chart = finder.castView(view, 2131755267, "field 'chart'");
  }

  @Override public void unbind(T target) {
    target.chart = null;
  }
}
