// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class EMROverView$$ViewBinder<T extends emrfragments.EMROverView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755444, "field 'elvPatientDetails'");
    target.elvPatientDetails = finder.castView(view, 2131755444, "field 'elvPatientDetails'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
  }

  @Override public void unbind(T target) {
    target.elvPatientDetails = null;
    target.pBar = null;
  }
}
