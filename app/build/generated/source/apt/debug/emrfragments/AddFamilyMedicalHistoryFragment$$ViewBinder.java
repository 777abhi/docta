// Generated code from Butter Knife. Do not modify!
package emrfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AddFamilyMedicalHistoryFragment$$ViewBinder<T extends emrfragments.AddFamilyMedicalHistoryFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755514, "field 'tvBack' and method 'click'");
    target.tvBack = finder.castView(view, 2131755514, "field 'tvBack'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext' and method 'click'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755215, "field 'etMidicalProblem'");
    target.etMidicalProblem = finder.castView(view, 2131755215, "field 'etMidicalProblem'");
    view = finder.findRequiredView(source, 2131755216, "field 'erRelation'");
    target.erRelation = finder.castView(view, 2131755216, "field 'erRelation'");
    view = finder.findRequiredView(source, 2131755217, "field 'etAge'");
    target.etAge = finder.castView(view, 2131755217, "field 'etAge'");
    view = finder.findRequiredView(source, 2131755203, "field 'etName'");
    target.etName = finder.castView(view, 2131755203, "field 'etName'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755198, "field 'rgSelection'");
    target.rgSelection = finder.castView(view, 2131755198, "field 'rgSelection'");
  }

  @Override public void unbind(T target) {
    target.tvBack = null;
    target.tvTitle = null;
    target.tvNext = null;
    target.frTopBar = null;
    target.etMidicalProblem = null;
    target.erRelation = null;
    target.etAge = null;
    target.etName = null;
    target.pBar = null;
    target.rgSelection = null;
  }
}
