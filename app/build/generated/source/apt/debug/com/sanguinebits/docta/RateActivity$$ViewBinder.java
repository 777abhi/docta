// Generated code from Butter Knife. Do not modify!
package com.sanguinebits.docta;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RateActivity$$ViewBinder<T extends com.sanguinebits.docta.RateActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755336, "field 'doctorRating'");
    target.doctorRating = finder.castView(view, 2131755336, "field 'doctorRating'");
    view = finder.findRequiredView(source, 2131755495, "field 'etAdditionalComment'");
    target.etAdditionalComment = finder.castView(view, 2131755495, "field 'etAdditionalComment'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755458, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.doctorRating = null;
    target.etAdditionalComment = null;
    target.pBar = null;
  }
}
