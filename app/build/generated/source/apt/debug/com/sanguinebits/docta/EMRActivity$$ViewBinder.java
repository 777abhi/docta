// Generated code from Butter Knife. Do not modify!
package com.sanguinebits.docta;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class EMRActivity$$ViewBinder<T extends com.sanguinebits.docta.EMRActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755145, "field 'viewpager'");
    target.viewpager = finder.castView(view, 2131755145, "field 'viewpager'");
    view = finder.findRequiredView(source, 2131755147, "field 'tabLayout'");
    target.tabLayout = finder.castView(view, 2131755147, "field 'tabLayout'");
    view = finder.findRequiredView(source, 2131755148, "field 'fabAppointment' and method 'click'");
    target.fabAppointment = finder.castView(view, 2131755148, "field 'fabAppointment'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755143, "field 'topParent'");
    target.topParent = finder.castView(view, 2131755143, "field 'topParent'");
  }

  @Override public void unbind(T target) {
    target.viewpager = null;
    target.tabLayout = null;
    target.fabAppointment = null;
    target.topParent = null;
  }
}
