// Generated code from Butter Knife. Do not modify!
package doctorfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DoctaHomeFragment$$ViewBinder<T extends doctorfragments.DoctaHomeFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755228, "field 'profilePic' and method 'click'");
    target.profilePic = finder.castView(view, 2131755228, "field 'profilePic'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755224, "field 'tvDoctorName'");
    target.tvDoctorName = finder.castView(view, 2131755224, "field 'tvDoctorName'");
    view = finder.findRequiredView(source, 2131755336, "field 'doctorRating'");
    target.doctorRating = finder.castView(view, 2131755336, "field 'doctorRating'");
    view = finder.findRequiredView(source, 2131755334, "field 'tvDoctorSpeciality'");
    target.tvDoctorSpeciality = finder.castView(view, 2131755334, "field 'tvDoctorSpeciality'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755329, "field 'progressBar'");
    target.progressBar = finder.castView(view, 2131755329, "field 'progressBar'");
    view = finder.findRequiredView(source, 2131755333, "field 'tvCompleteProfile'");
    target.tvCompleteProfile = finder.castView(view, 2131755333, "field 'tvCompleteProfile'");
    view = finder.findRequiredView(source, 2131755335, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.profilePic = null;
    target.tvDoctorName = null;
    target.doctorRating = null;
    target.tvDoctorSpeciality = null;
    target.pBar = null;
    target.progressBar = null;
    target.tvCompleteProfile = null;
  }
}
