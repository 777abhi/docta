// Generated code from Butter Knife. Do not modify!
package doctorfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DoctorProfile$$ViewBinder<T extends doctorfragments.DoctorProfile> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755343, "field 'tvDictorReviewingChart'");
    target.tvDictorReviewingChart = finder.castView(view, 2131755343, "field 'tvDictorReviewingChart'");
    view = finder.findRequiredView(source, 2131755228, "field 'profilePic'");
    target.profilePic = finder.castView(view, 2131755228, "field 'profilePic'");
    view = finder.findRequiredView(source, 2131755345, "field 'tvEmail'");
    target.tvEmail = finder.castView(view, 2131755345, "field 'tvEmail'");
    view = finder.findRequiredView(source, 2131755346, "field 'tvPhone'");
    target.tvPhone = finder.castView(view, 2131755346, "field 'tvPhone'");
    view = finder.findRequiredView(source, 2131755158, "field 'tvCancel' and method 'click'");
    target.tvCancel = finder.castView(view, 2131755158, "field 'tvCancel'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755229, "field 'frTopBar'");
    target.frTopBar = finder.castView(view, 2131755229, "field 'frTopBar'");
    view = finder.findRequiredView(source, 2131755344, "field 'tvName'");
    target.tvName = finder.castView(view, 2131755344, "field 'tvName'");
    view = finder.findRequiredView(source, 2131755347, "field 'tvMedicalSchool'");
    target.tvMedicalSchool = finder.castView(view, 2131755347, "field 'tvMedicalSchool'");
    view = finder.findRequiredView(source, 2131755348, "field 'tvSpeciality'");
    target.tvSpeciality = finder.castView(view, 2131755348, "field 'tvSpeciality'");
    view = finder.findRequiredView(source, 2131755349, "field 'tvYearOfPractice'");
    target.tvYearOfPractice = finder.castView(view, 2131755349, "field 'tvYearOfPractice'");
    view = finder.findRequiredView(source, 2131755350, "field 'tvAReaOfInterest'");
    target.tvAReaOfInterest = finder.castView(view, 2131755350, "field 'tvAReaOfInterest'");
    view = finder.findRequiredView(source, 2131755351, "field 'tvHobby'");
    target.tvHobby = finder.castView(view, 2131755351, "field 'tvHobby'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvDictorReviewingChart = null;
    target.profilePic = null;
    target.tvEmail = null;
    target.tvPhone = null;
    target.tvCancel = null;
    target.tvNext = null;
    target.frTopBar = null;
    target.tvName = null;
    target.tvMedicalSchool = null;
    target.tvSpeciality = null;
    target.tvYearOfPractice = null;
    target.tvAReaOfInterest = null;
    target.tvHobby = null;
  }
}
