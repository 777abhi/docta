// Generated code from Butter Knife. Do not modify!
package doctorfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class FollowUpAppointmentFragment$$ViewBinder<T extends doctorfragments.FollowUpAppointmentFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755163, "field 'rvDoctors'");
    target.rvDoctors = finder.castView(view, 2131755163, "field 'rvDoctors'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755251, "field 'ivPrevMonth' and method 'click'");
    target.ivPrevMonth = finder.castView(view, 2131755251, "field 'ivPrevMonth'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755252, "field 'tvSelectedMonthName'");
    target.tvSelectedMonthName = finder.castView(view, 2131755252, "field 'tvSelectedMonthName'");
    view = finder.findRequiredView(source, 2131755253, "field 'ivNextMonth' and method 'click'");
    target.ivNextMonth = finder.castView(view, 2131755253, "field 'ivNextMonth'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755167, "field 'tvNoData'");
    target.tvNoData = finder.castView(view, 2131755167, "field 'tvNoData'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755388, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.rvDoctors = null;
    target.pBar = null;
    target.tvNext = null;
    target.ivPrevMonth = null;
    target.tvSelectedMonthName = null;
    target.ivNextMonth = null;
    target.tvNoData = null;
  }
}
