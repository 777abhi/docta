// Generated code from Butter Knife. Do not modify!
package doctorfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class WaitForConsultRequest$$ViewBinder<T extends doctorfragments.WaitForConsultRequest> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755405, "field 'myArcView'");
    target.myArcView = finder.castView(view, 2131755405, "field 'myArcView'");
    view = finder.findRequiredView(source, 2131755336, "field 'doctorRating'");
    target.doctorRating = finder.castView(view, 2131755336, "field 'doctorRating'");
    view = finder.findRequiredView(source, 2131755543, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.myArcView = null;
    target.doctorRating = null;
  }
}
