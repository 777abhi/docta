// Generated code from Butter Knife. Do not modify!
package doctorfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DoctaConsultRequest$$ViewBinder<T extends doctorfragments.DoctaConsultRequest> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755329, "field 'progressBar'");
    target.progressBar = finder.castView(view, 2131755329, "field 'progressBar'");
    view = finder.findRequiredView(source, 2131755228, "field 'profilePic'");
    target.profilePic = finder.castView(view, 2131755228, "field 'profilePic'");
    view = finder.findRequiredView(source, 2131755327, "field 'tvTimeStatus'");
    target.tvTimeStatus = finder.castView(view, 2131755327, "field 'tvTimeStatus'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755330, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755331, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.progressBar = null;
    target.profilePic = null;
    target.tvTimeStatus = null;
    target.pBar = null;
  }
}
