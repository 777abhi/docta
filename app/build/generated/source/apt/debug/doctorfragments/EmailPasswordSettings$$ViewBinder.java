// Generated code from Butter Knife. Do not modify!
package doctorfragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class EmailPasswordSettings$$ViewBinder<T extends doctorfragments.EmailPasswordSettings> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755518, "field 'tvNext'");
    target.tvNext = finder.castView(view, 2131755518, "field 'tvNext'");
    view = finder.findRequiredView(source, 2131755660, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131755660, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131755345, "field 'tvEmail'");
    target.tvEmail = finder.castView(view, 2131755345, "field 'tvEmail'");
    view = finder.findRequiredView(source, 2131755354, "field 'etCurrentPassword'");
    target.etCurrentPassword = finder.castView(view, 2131755354, "field 'etCurrentPassword'");
    view = finder.findRequiredView(source, 2131755355, "field 'etNewPassword'");
    target.etNewPassword = finder.castView(view, 2131755355, "field 'etNewPassword'");
    view = finder.findRequiredView(source, 2131755168, "field 'pBar'");
    target.pBar = finder.castView(view, 2131755168, "field 'pBar'");
    view = finder.findRequiredView(source, 2131755514, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
    view = finder.findRequiredView(source, 2131755356, "method 'click'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.click(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.tvNext = null;
    target.tvTitle = null;
    target.tvEmail = null;
    target.etCurrentPassword = null;
    target.etNewPassword = null;
    target.pBar = null;
  }
}
